﻿using Microsoft.Xrm.Sdk;
using System;
using System.Threading.Tasks;

namespace BLL.Extensions
{
    public static class IOrganizationServiceExtensions
    {
        public static Task<Guid> CreateAsync(this IOrganizationService orgService, Entity entity)
        {
            return Task.Run(() =>
            {
                return orgService.Create(entity);
            });
        }
    }
}