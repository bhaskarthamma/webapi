﻿using System.Threading.Tasks;
using BLL.Services.Abstract;
using BLL.Services.Interfaces;
using DataUploader;
using DataUploader.Helper;
using DataUploaderApp.Helper;
using Newtonsoft.Json;
using Serilog;
using Microsoft.Xrm.Sdk;

namespace BLL.Services.Concrete
{
    public class DataUploaderService : BaseService, IDataUploaderService
    {
        public DataUploaderService(string conn) : base(conn)
        {
        }


        public DataUploaderService(IOrganizationService service) : base(service)
        {
        }

        public DataUploaderService() : base(SingletonBaseService.GetInstance()._orgService)
        {
        }


        //Parser
        public Task RunDataUploaderParserAsync(DataUploaderSettings thisSettings)
        {
            return Task.Run(() => RunDataUploaderParser(thisSettings));
        }

        public void RunDataUploaderParser(DataUploaderSettings thisSettings)
        {
            DataUploaderApp.DataUploaderApp.StartParserApp(thisSettings, _orgService);
        }

        public Task RunDataUploaderParserAsync(AppArg jsonInput)
        {
            var request = JsonConvert.SerializeObject(jsonInput);

            Log.Debug($" DU request= {request}");

            return Task.Run(() => RunDataUploaderParser(jsonInput));
        }

        public void RunDataUploaderParser(AppArg jsonInput)
        {
            DataUploaderApp.DataUploaderApp.StartParserApp(jsonInput, _orgService);
        }

        //Creator
        public void RunDataUploaderCreator(DataUploaderSettings thisSettings)
        {
            DataUploaderApp.DataUploaderApp.StartCreatorApp(thisSettings, _orgService);
        }

        public void RunDataUploaderCreator(AppArg jsonInput)
        {
            DataUploaderApp.DataUploaderApp.StartCreatorApp(jsonInput, _orgService);
        }

        public Task RunDataUploaderCreatorAsync(DataUploaderSettings thisSettings)
        {
            return Task.Run(() => RunDataUploaderCreator(thisSettings));
        }

        public Task RunDataUploaderCreatorAsync(AppArg jsonInput)
        {
            return Task.Run(() => RunDataUploaderCreator(jsonInput));
        }
    }
}