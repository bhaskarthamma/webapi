﻿using System;
using System.Threading.Tasks;
using BLL.Services.Abstract;
using BLL.Services.Interfaces;
using EspCalculation.App;
using EspCalculation.Options;
using Microsoft.Xrm.Sdk;

namespace BLL.Services.Concrete
{
    public class EspService : BaseService, IEspService
    {
        public EspService(string conn) : base(conn)
        {
        }

        public EspService(IOrganizationService service) : base(service)
        {
        }

        public EspService() : base(SingletonBaseService.GetInstance()._orgService)
        {
        }

        public Task CalculateMeasureAsync(CalculationOptions options)
        {
            var app = new AppCalculateMeasure(_orgService, options);
            return app.RunAsync();
        }

        public Task GroupCalculateMeasuresAsync(CalculationOptions options)
        {
            if (options == null)
                throw new ArgumentNullException("GroupCalculateOptions");

            var app = new AppGroupCalculateMeasure(_orgService, options, OnCalculationEnd);
            return app.RunAsync();
        }

        public Task SyncMetadata(CalculationOptions options)
        {
            if (options == null)
                throw new ArgumentNullException("SyncMetadataOptions");

            var app = new AppSyncMetadata(_orgService, options, OnSyncMetadataEnd);
            return app.RunAsync();
        }

        public Task GetLatestMtData(CalculationOptions options)
        {
            if (options == null)
                throw new ArgumentNullException("GetLatestMtDataOptions");

            var app = new AppGetLatestMtData(_orgService, options, OnSyncMetadataEnd);
            return app.RunAsync();
        }
    }
}