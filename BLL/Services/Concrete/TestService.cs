﻿using BLL.Extensions;
using BLL.Services.Abstract;
using BLL.Services.Interfaces;
using Microsoft.Xrm.Sdk;
using Serilog;
using SocketClients;
using System;
using System.Threading.Tasks;

namespace BLL.Services.Concrete
{
    public class TestService : BaseService, ITestService
    {
        public TestService(string connection) : base(connection)
        {

        }

        public async Task CreateRecords()
        {
            for (var i = 0; i < 5; i++)
            {
                var entity = new Entity("new_testentity");
                entity["new_name"] = "delete";

                StaticSocketClient.Send(new Guid(), "start entity creation");

                try
                {
                    Guid createdEntityId = await _orgService.CreateAsync(entity);
                    StaticSocketClient.Send(new Guid(), createdEntityId.ToString());
                }
                catch (Exception ex)
                {
                    StaticSocketClient.Send(new Guid(), "entity does not created");
                    throw;
                }
            }
        }

        public string GetAppDomain()
        {
            Log.Information("Returned domain");
            return AppDomain.CurrentDomain.BaseDirectory;
        }
        public string GetAppDomain(int i)
        {   
            if(i> 1)
            {
                Log.Error("Error inside Service");
                throw new Exception("failed to return data");
            }

            Log.Information("Returned domain");
            return AppDomain.CurrentDomain.BaseDirectory;
        }

        public async Task TestSocket()
        {
            await StaticSocketClient.SendAsync(new Guid(), "hello");
        }
    }
}