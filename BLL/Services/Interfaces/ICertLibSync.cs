﻿using System.Threading.Tasks;
using CertLibSync.Models.Options;

namespace BLL.Services.Interfaces
{
    public interface ICertLibSync
    {
        Task LoadDlcMetadata(CertLibOptions options);
        Task LoadDlcProducts(CertLibOptions options);
        Task LoadEsMetadata(CertLibOptions options);
        Task LoadEsProducts(CertLibOptions options);
    }
}
