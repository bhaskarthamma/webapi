﻿using DmnEngineApp.Dto;
using System.Threading.Tasks;

namespace BLL.Services.Interfaces
{
    public interface IDmnService
    {
        string LaunchDmn(InputArgsDto dmnInputArgs);
        Task RunDmnAsync(InputArgsDto dmnInputArgs);
    }
}