﻿using System;
using System.Threading.Tasks;

namespace BLL.Services.Interfaces
{
    public interface IEToolsService : IDisposable
    {
        Task<byte[]> GetFileAsync(string fileName);
        byte[] GetFile(string fileName);

        Task SetFileAsync(Guid eToolsId);
        void SetFile(Guid eToolsId);
    }
}
