﻿using System.Threading.Tasks;
using EspCalculation.Options;

namespace BLL.Services.Interfaces
{
    public interface IEspService
    {
        Task CalculateMeasureAsync(CalculationOptions options);
        Task GroupCalculateMeasuresAsync(CalculationOptions options);
        Task SyncMetadata(CalculationOptions options);
        Task GetLatestMtData(CalculationOptions options);
    }
}
