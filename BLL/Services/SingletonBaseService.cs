﻿
using Microsoft.Xrm.Sdk;
using System;
using System.Configuration;
using Serilog;
using System.Threading.Tasks;
using System.Net;
using Microsoft.Xrm.Tooling.Connector;

namespace BLL.Services
{
    class SingletonBaseService
    {
        private static  SingletonBaseService baseService = new SingletonBaseService("CRMConnectionString");
        private static  Guid _guid = Guid.NewGuid();
        private static int _countCall = 0;

        public IOrganizationService _orgService { get; private set; }
        public string _connectionStringName { get; private set; }
        public string _connectionString { get; private set; }
       

        private SingletonBaseService(string connectionStringName)
        {
            //  var crmConnection = new CrmConnection(connectionStringName);
            //  _orgService = new OrganizationService(crmConnection);

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            // Get the CRM connection string and connect to the CRM Organization
            var crmConnectionString = ConfigurationManager.ConnectionStrings[connectionStringName].ConnectionString;
            var client = new CrmServiceClient(crmConnectionString);
            var orgServiceProxy = client.OrganizationServiceProxy;

            _orgService = client.OrganizationServiceProxy;
           _connectionStringName = connectionStringName;
            _connectionString = ConfigurationManager.ConnectionStrings[_connectionStringName].ToString();           
        }

        public static SingletonBaseService GetInstance()
        {
            if (baseService == null)
            {
                baseService = new SingletonBaseService("CRMConnectionString");
                Guid _guid = Guid.NewGuid();
            }
            //Task.Run(() => Log.Debug("SingletonBaseService"));
            //Task.Run(() => Log.Debug("GUID=" + _guid));
            //Task.Run(() => Log.Debug("Count Call=" + _countCall++));
            //Log.Debug("SingletonBaseService");
            //Log.Debug("GUID="+ _guid);
            //Log.Debug("Count Call=" + _countCall++);            
            return baseService;
        }
        //call this method when your calculation finished
        public void OnCalculationEnd()
        { }


    }
}
