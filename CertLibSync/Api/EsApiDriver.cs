﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using CertLibSync.App;
using CertLibSync.Models;
using CertLibSync.Models.DLC;
using CertLibSync.Models.Es;
using CoreUtils.Utils;
using Microsoft.Xrm.Sdk;
using Newtonsoft.Json.Linq;

namespace CertLibSync.Api
{
    public class EsApiDriver : IApiDriver
    {
        #region internal fields
        private string _sqlLimit = "?$limit=50000&$offset=0&$$exclude_system_fields=false";
        private readonly string _sql = "";

        public IApiConfig Config { get; set; }
        // public string MainURL = "http://data.energystar.gov/";
        string CategoriesPart = "api/views/";
        string ResourcesPart = "resource/";
       // private readonly string[] _columns = new string[] { "ddsm_esurl", "ddsm_eslogin", "ddsm_espass", "ddsm_estoken", "ddsm_esonlyofficialcategories" };
        #endregion

        public string GetCategorieURL(string metadataId)
        {
            return metadataId + ".json" + _sqlLimit + _sql;
        }
        public EsApiDriver(IOrganizationService service)
        {
            Config = SettingsProvider.GetConfig<EsConfig>(service);
        }

        public string GetResourcesUrl(string catId)
        {
            return Config.Url + ResourcesPart + catId + ".json" + _sqlLimit + _sql;
        }

        public string GetMetadataUrlPart(string catId)
        {
            return Config.Url + CategoriesPart + catId; //+ MetadataPart;
        }

        public string GetCategories()
        {
            return Config.Url + CategoriesPart; //+ MetadataPart;
        }

        //public void GetConfig()
        //{
        //    var defConfName = "Admin Data";
        //    try
        //    {
        //        var expr = new QueryExpression
        //        {
        //            EntityName = "ddsm_admindata",
        //            ColumnSet = new ColumnSet(_columns),
        //            Criteria = new FilterExpression
        //            {
        //                FilterOperator = LogicalOperator.And,
        //                Conditions = { new ConditionExpression("ddsm_name", ConditionOperator.Equal, defConfName) }
        //            }
        //        };
        //        var adminData = _service.RetrieveMultiple(expr);
        //        if (adminData != null && adminData.Entities?.Count >= 1)
        //        {
        //            Config = new EsConfig(adminData.Entities[0]);
        //        }
        //        else
        //        {
        //            throw new Exception("Can't get Energy Star configuration. Check Admin data record.");
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        // return null;
        //        throw new Exception("error in getting Config: " + ex.Message + ex.Data);
        //    }
        //}

        public Dictionary<ICategory, IView> GetMetadata()
        {
            var metadata = new Dictionary<ICategory, IView>();

            var categories = new List<EsCategory>();

            var config = Config as EsConfig;

            if (config == null)
            {
                return new Dictionary<ICategory, IView>();
            }

            using (var client = new HttpClient())
            {
                //specify to use TLS 1.2 as default connection
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
                client.BaseAddress = new Uri(Config.Url);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("X-App-Token", config.Token);

                HttpResponseMessage response = client.GetAsync(GetCategories()).Result;
                if (response.IsSuccessStatusCode)
                {
                    var categoriesTask = response.Content.ReadAsAsync<List<EsCategory>>();
                    categories = categoriesTask.Result;

                    categories = categories.Where(x => x.Category == EsCategoryActive.ActiveSpecifications).ToList();
                    if (config.OnlyOfficialCategories)
                    {
                        categories = categories.Where(x => x.Provenance.Equals(EsCategoryProvenance.Official)).ToList();
                    }
                    foreach (var cat in categories)
                    {
                        metadata.Add(cat, GetCategoryMetadata(cat, client));
                    }
                }
            }
            return metadata;
        }

        View GetCategoryMetadata(EsCategory category, HttpClient client)
        {
            var response = client.GetAsync(GetMetadataUrlPart(category.Id)).Result;
            if (response.IsSuccessStatusCode)
            {
                var dataTask = response.Content.ReadAsAsync<View>();
                return dataTask.Result;
            }
            else
            {
                return null;
            }
        }

        public Dictionary<Tab, List<JObject>> GetProducts(List<Tab> selectedCategories)
        {
            Dictionary<Tab, List<JObject>> result = new Dictionary<Tab, List<JObject>>();

            // JArray arrCertAllData = new JArray();

            var config = Config as EsConfig;

            if (config == null)
            {
                return new Dictionary<Tab, List<JObject>>();
            }

            using (var client = new HttpClient())
            {
                //specify to use TLS 1.2 as default connection
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
                // client.BaseAddress = new Uri(config.Url);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("X-App-Token", config.Token);

                foreach (var categorie in selectedCategories)
                {
                    var metadataId = categorie.MetadataId;
                    var url = GetResourcesUrl(metadataId);
                    List<JObject> categoryList = new List<JObject>();

                    //var response = client.DownloadString(url);
                    HttpResponseMessage response = client.GetAsync(url).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var arrCertData = response.Content.ReadAsAsync<JArray>();
                        foreach (var jObj in arrCertData.Result.Children<JObject>())
                        {
                            jObj.Add("type_product", metadataId);
                            categoryList.Add((JObject)jObj);
                            //jObj.Add("certified", "Energy Star");
                            //jObj.Add("is_delete", "false");
                            //jObj.Add("status", "");
                            //jObj.Add("certlibsyncid", mainEntityRef.Id.ToString());
                        }
                        //arrCertAllData.Merge(arrCertData.Result);
                        result.Add(categorie,categoryList);
                    }
                    else
                    {
                        throw new Exception(response.StatusCode.ToString());
                    }
                }
            }
            return result;
        }

        //Dictionary<ICategory, IView> IApiDriver.GetMetadata()
        //{
        //    throw new NotImplementedException();
        //}

        //Dictionary<Tab, List<JObject>> IApiDriver.GetProducts(List<Tab> selectedCategories)
        //{
        //    throw new NotImplementedException();
        //}
    }
}
