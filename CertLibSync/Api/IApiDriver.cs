﻿using System.Collections.Generic;
using CertLibSync.Models.DLC;

namespace CertLibSync.Utils
{

    interface IGetMetadata<T>
    {
        List<T> GetMetadata<T>();
    }
}