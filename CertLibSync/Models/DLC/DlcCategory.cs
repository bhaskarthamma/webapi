﻿using CertLibSync.App;

namespace CertLibSync.Models.DLC
{
    public class DlcCategory: ICategory
    {
        public string categoryDescription { get; set; }
        public string restrictAccessFlag { get; set; }
        public string cmsCategoryID { get; set; }
        public string allowProductAssignmentFlag { get; set; }
        public string categoryIDPath { get; set; }
        public string primaryUseFlag { get; set; }
        public string categoryID { get; set; }
        public string generalApplicationFlag { get; set; }
        public string categoryName { get; set; }
        public string urlTitle { get; set; }
    }
}
