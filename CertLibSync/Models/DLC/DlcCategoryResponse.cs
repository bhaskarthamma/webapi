﻿using System.Collections.Generic;

namespace CertLibSync.Models.DLC
{
    public class DlcCategoryResponse: DlcResponse
    {
        public List<DlcCategory> pageRecords { get; set; }
    }
}
