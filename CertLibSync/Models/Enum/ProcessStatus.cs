﻿namespace CertLibSync.Models.Enum
{
    public enum ProcessStatus
    {
        ToBeReviewed = 962080000, Disabled = 962080001, Active = 962080002
    }
}
