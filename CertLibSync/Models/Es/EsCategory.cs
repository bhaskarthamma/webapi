﻿using CertLibSync.App;

namespace CertLibSync.Models
{
    public class EsCategory : ICategory
    {
        private string namePart = "ENERGY STAR Certified ";
        private string namePart2 = "ENERGY STAR ";

        public string Id { get; set; }
        private string _name;
        public string Name
        {
            get { return _name; }
            set
            {
                _name = value.Contains(namePart) ? value.Replace(namePart, "") : value;
                _name = _name.Contains(namePart2) ? _name.Replace(namePart2, "") : _name;
            }
        }
        public string Description { get; set; }
        public string Category { get; set; }
        public string Provenance { get; set; }
    }
}
