﻿namespace CertLibSync.Models.Es
{
    public class EsField
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string DataTypeName { get; set; }
        public string FieldName { get; set; }
        public string RenderTypeName { get; set; }
        // public string Description { get; set; }
    }
}