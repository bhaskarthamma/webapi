﻿using System;

namespace CertLibSync.Models.Es
{
    public class Tab
    {
        public string MetadataId { get; set; }
        public string Label { get; set; }
        public Guid LogicalName { get; set; }
    }
}