﻿using System.Collections.Generic;
using CertLibSync.App;

namespace CertLibSync.Models.Es
{
    public class View : EsCategory, IView
    {
        public List<EsField> Columns { get; set; }
    }
}