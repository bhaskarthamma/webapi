namespace CertLibSync.Models
{
    public class MappingAttribute
    {
        public string AttrLogicalName { get; set; }
        public string AttrType { get; set; }
    }
}