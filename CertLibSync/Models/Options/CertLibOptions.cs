﻿using System;
using Microsoft.Xrm.Sdk;

namespace CertLibSync.Models.Options
{
    public class CertLibOptions
    {
        public Guid UserId { get; set; }
        public EntityReference Target { get; set; }
    }
}
