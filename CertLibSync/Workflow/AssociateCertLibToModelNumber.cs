﻿using System;
using System.Activities;
using System.Collections.Generic;
using System.Linq;
using CertLibSync.Models;
using CertLibSync.Models.Es;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Workflow;
using Newtonsoft.Json;

namespace CertLibSync
{
    public class AssociateCertLibToModelNumber : CodeActivity
    {
        [RequiredArgument]
        [Input("Mapping")]
        [ArgumentEntity("ddsm_mapping")]
        [ReferenceTarget("ddsm_mapping")]
        public InArgument<EntityReference> MappingRef { get; set; }

        [RequiredArgument]
        [Input("CertificationId")]
        public InArgument<string> CertificationId { get; set; }

        [RequiredArgument]
        [Input("Create Model Numbers")]
        public InArgument<bool> CreateModelNumbers { get; set; }

        protected override void Execute(CodeActivityContext executionContext)
        {
            var tracer = executionContext.GetExtension<ITracingService>();
            var context = executionContext.GetExtension<IWorkflowContext>();
            var serviceFactory = executionContext.GetExtension<IOrganizationServiceFactory>();
            var service = serviceFactory.CreateOrganizationService(context.UserId);
            //var helper = new EnergyStarHelper(service);

            var certificationId = "";
            try
            {
                certificationId = CertificationId.Get(executionContext);
                var createModelNumbers = CreateModelNumbers.Get(executionContext);
                var target = Helper.GetTargetData(executionContext);
                var mappingRef = MappingRef.Get(executionContext);
                List<Tab2> tabs;
                var mappingData = GetMappingData(mappingRef, service, out tabs);
                var modelNumber = FillEntity(mappingData, tabs, target, service, tracer);

                EntityReference modelNumberRef = null;
                if (createModelNumbers)
                {
                    var respounse = (UpsertResponse)service.Execute(new UpsertRequest() { Target = new Entity("ddsm_modelnumber", "ddsm_certificationid", certificationId) { Attributes = modelNumber.Attributes } });
                    modelNumberRef = respounse.Target;
                    tracer.Trace("Update Model Number record. createModelNumbers =true");
                }
                else
                {
                    tracer.Trace("Trying get Model Number record. createModelNumbers =false");
                    var query = new QueryExpression
                    {
                        EntityName = "ddsm_modelnumber",
                        Criteria = new FilterExpression(LogicalOperator.And)
                        {
                            Conditions = { new ConditionExpression("ddsm_modelname", ConditionOperator.Equal, certificationId) }
                        }
                    };
                    var existingModelNumber = service.RetrieveMultiple(query);
                    modelNumberRef = existingModelNumber.Entities.Count > 0
                        ? existingModelNumber.Entities[0].ToEntityReference()
                        : null;
                }
                if (modelNumberRef != null)
                {
                    // Creating EntityReferenceCollection for the Contact
                    var relatedEntities = new EntityReferenceCollection { target };
                    // Add the related entity contact
                    //relatedEntities.Add(contact);
                    // Add the Account Contact relationship schema name
                    var relationship = new Relationship("ddsm_ddsm_modelnumber_ddsm_certlib");
                    // Associate the contact record to Account
                    service.Associate(modelNumberRef.LogicalName, modelNumberRef.Id, relationship, relatedEntities);
                }
                else
                {
                    tracer.Trace("CertLib record wasn't associated with model Number. Model number can't found and parameter Create Model Numbers is false");
                }
            }
            catch (Exception e)
            {
                var msg = e.Message + " Certification Id: " + certificationId;
                tracer.Trace(msg);
                throw new Exception(msg);
            }
        }
        // GetMappingData(EntityReference mappingRef, IOrganizationService service
        private Entity FillEntity(Dictionary<string, List<EntityMappingField>> mappingDict, List<Tab2> entities, EntityReference targetRef, IOrganizationService service, ITracingService tracer)
        {
            var mapping = mappingDict[entities[0].LogicalName];
            var columns = mapping.Where(x => !string.IsNullOrEmpty(x.TargetFieldLogicalName)).Select(y => y.SourceFieldLogicalName.ToLower()).ToArray();

            var result = new Entity();

            var certLibData = service.Retrieve(targetRef.LogicalName, targetRef.Id, new ColumnSet(columns));

            foreach (var attr in certLibData.Attributes)
            {
                var mappingData = mapping.FirstOrDefault(x => !string.IsNullOrEmpty(x.TargetFieldLogicalName) && x.SourceFieldLogicalName.ToLower().Equals(attr.Key.ToLower()));

                if (mappingData?.TargetFieldLogicalName.ToLower() != "ddsm_certificationid" && !string.IsNullOrEmpty(mappingData?.TargetFieldType))
                {
                    tracer.Trace("Field: " + mappingData.TargetFieldLogicalName.ToLower() + " | Value: " + attr.Value);
                    if (mappingData.TargetFieldType.Equals("String"))
                    {
                        result[mappingData.TargetFieldLogicalName.ToLower()] = Convert.ToString(attr.Value);
                    }
                    if (mappingData.TargetFieldType.Equals("Decimal"))
                    {
                        result[mappingData.TargetFieldLogicalName.ToLower()] = Decimal.Parse(Convert.ToString(attr.Value));
                    }
                    if (mappingData.TargetFieldType.Equals("Integer"))
                    {
                        result[mappingData.TargetFieldLogicalName.ToLower()] = Int32.Parse(Convert.ToString(attr.Value));
                    }
                    if (mappingData.TargetFieldType.Equals("DateTime"))
                    {
                        result[mappingData.TargetFieldLogicalName.ToLower()] = DateTime.Parse(Convert.ToString(attr.Value));
                    }
                }
            }
            return result;
        }

        private Dictionary<string, List<EntityMappingField>> GetMappingData(EntityReference mappingRef, IOrganizationService service, out List<Tab2> tabs)
        {
            tabs = new List<Tab2>();
            var mappingRecord = service.Retrieve(mappingRef.LogicalName, mappingRef.Id,
                new ColumnSet("ddsm_jsondata", "ddsm_entities"));

            var result = new Dictionary<string, List<EntityMappingField>>();
            if (mappingRecord != null && mappingRecord.Attributes.ContainsKey("ddsm_jsondata"))
            {
                var json = (string)mappingRecord["ddsm_jsondata"];
                result = JsonConvert.DeserializeObject<Dictionary<string, List<EntityMappingField>>>(json);
            }
            if (mappingRecord != null && mappingRecord.Attributes.ContainsKey("ddsm_entities"))
            {
                var json = (string)mappingRecord["ddsm_entities"];
                tabs = JsonConvert.DeserializeObject<List<Tab2>>(json);
            }
            return result;
        }
    }
}
