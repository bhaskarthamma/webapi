﻿using System;
using System.Activities;
using System.Collections.Generic;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Workflow;


    public class CreateCertLibSyncRecord : CodeActivity
    {
        [Output("Complete")]
        public OutArgument<bool> Complete { get; set; }

        [Output("Result")]
        public OutArgument<string> Result { get; set; }
        protected override void Execute(CodeActivityContext executionContext)
        {
            //var common = new Common(context);
            var tracer = executionContext.GetExtension<ITracingService>();
            var context = executionContext.GetExtension<IWorkflowContext>();
            var serviceFactory = executionContext.GetExtension<IOrganizationServiceFactory>();
            var service = serviceFactory.CreateOrganizationService(context.UserId);
            Entity currentConfig = GetCurrentConfig(service);
            Result.Set(executionContext, CreateNewRecord(service, currentConfig).ToString());
            Complete.Set(executionContext, true);
        }

    //TODO: Fix impl of init from ddsm_certlibsyncconf record using request and system mapping
    private Guid CreateNewRecord(IOrganizationService service, Entity config)
        {
            var mainEntName = "ddsm_certlibsync";
            var currentDate = DateTime.UtcNow;
            var record = new Entity(mainEntName)
            {
                Attributes =
                {
                    new KeyValuePair<string, object>("ddsm_certlibsyncconfid", config.ToEntityReference()),
                    new KeyValuePair<string, object>("ddsm_name", "SyncOn_" + currentDate.ToString()),
                    new KeyValuePair<string, object>("ddsm_createmodelnumbers",config.GetAttributeValue<bool>("ddsm_createmodelnumbers") ),
                    new KeyValuePair<string, object>("ddsm_keep_unchecked_categories",config.GetAttributeValue<bool>("ddsm_keep_unchecked_categories") ),
                }
            };
            return service.Create(record);
        }

        private Entity GetCurrentConfig(IOrganizationService service)
        {
            Entity result = new Entity();
            var query = new QueryExpression
            {
                EntityName = "ddsm_certlibsyncconf",
                ColumnSet = new ColumnSet("ddsm_createmodelnumbers", "ddsm_keep_unchecked_categories"),
                Criteria = new FilterExpression(LogicalOperator.And)
                {
                    Conditions =
                    {
                        new ConditionExpression("statuscode",ConditionOperator.Equal,1) //get active record
                    }
                }
            };

            var records = service.RetrieveMultiple(query);
            if (records.Entities.Count > 0)
            {
                result = records.Entities[0];
            }
            return result;
        }
    }

