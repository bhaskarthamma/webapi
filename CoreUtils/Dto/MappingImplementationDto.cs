﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xrm.Sdk;

namespace CoreUtils.Dto
{
    public class MappingImplementationDto
    {
        public enum RecordCreator
        {
            UnassignedValue = -2147483648,
            Front = 962080000,
            DataUploader = 962080001,
            Portal = 962080002,
            PrepTool = 962080003
        }

        public IOrganizationService OrgService { get; set; }
        public RecordCreator CreatorRecordType { get; set; }
        public Guid CurrentUserId;
    }
}
