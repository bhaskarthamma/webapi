﻿using System;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System.Text.RegularExpressions;

namespace CoreUtils.Service.Implementation
{
    public class AutoNumberingService
    {
        private readonly IOrganizationService _orgService;
        private readonly string _logicalName;
        private readonly Int32 _reservedNumber;
        private readonly Guid _autoNumberingId;
        private readonly string _prefix;
        private readonly Int32 _increment;
        private readonly Int32 _length;
        private Decimal _counter;

        public AutoNumberingService(IOrganizationService orgService, string logicalName, Int32 reservedNumber = 1, string parentEntityLookupName = "")
        {
            _orgService = orgService;
            _reservedNumber = (reservedNumber > 0)? reservedNumber: 1;
            _logicalName = logicalName;
            if (string.IsNullOrEmpty(logicalName))
            {
                _prefix = string.Empty;
                _increment = 0;
                _length = 0;
                _counter = 0;
                _autoNumberingId = Guid.Empty;
            }
            else
            {
                //Getting of autonumber entity record
                QueryExpression getAutonumberEntities = new QueryExpression("ddsm_autonumbering");
                getAutonumberEntities.ColumnSet = new ColumnSet("ddsm_autonumberingid", "ddsm_prefix", "ddsm_counter", "ddsm_length", "ddsm_increment");
                getAutonumberEntities.Criteria.AddCondition("ddsm_name", ConditionOperator.Equal, _logicalName);
                if (!string.IsNullOrEmpty(parentEntityLookupName))
                    getAutonumberEntities.Criteria.AddCondition("ddsm_lookupforanalyze", ConditionOperator.Equal, parentEntityLookupName);
                var autonumberArray = _orgService.RetrieveMultiple(getAutonumberEntities).Entities;
                if (autonumberArray.Count > 0)
                {
                    _autoNumberingId = (autonumberArray[0].Attributes.ContainsKey("ddsm_autonumberingid"))
                        ? autonumberArray[0].GetAttributeValue<Guid>("ddsm_autonumberingid")
                        : Guid.Empty;
                    _prefix = (autonumberArray[0].Attributes.ContainsKey("ddsm_prefix"))
                        ? autonumberArray[0].GetAttributeValue<string>("ddsm_prefix")
                        : "";
                    _increment = (autonumberArray[0].Attributes.ContainsKey("ddsm_increment"))
                        ? autonumberArray[0].GetAttributeValue<Int32>("ddsm_increment")
                        : 1;
                    _length = (autonumberArray[0].Attributes.ContainsKey("ddsm_length"))
                        ? autonumberArray[0].GetAttributeValue<Int32>("ddsm_length")
                        : 7;
                    _counter = (autonumberArray[0].Attributes.ContainsKey("ddsm_counter"))
                        ? autonumberArray[0].GetAttributeValue<Decimal>("ddsm_counter")
                        : 0;
                    if (_reservedNumber != 1)
                    {
                        Entity autonumbering = new Entity("ddsm_autonumbering", _autoNumberingId);
                        autonumbering["ddsm_counter"] = _counter + _increment * _reservedNumber;
                        _orgService.Update(autonumbering);
                    }
                }
                else
                {
                    _prefix = string.Empty;
                    _increment = 0;
                    _length = 0;
                    _counter = 0;
                    _autoNumberingId = Guid.Empty;
                }
            }
        }

        public string GetNumber()
        {
            string number = string.Empty;
            if (_autoNumberingId == Guid.Empty)
                return number;
            if (_reservedNumber == 1)
            {
                //Getting of autonumber entity record
                QueryExpression getAutonumberEntities = new QueryExpression("ddsm_autonumbering");
                getAutonumberEntities.ColumnSet = new ColumnSet("ddsm_counter");
                getAutonumberEntities.Criteria.AddCondition("ddsm_autonumberingid", ConditionOperator.Equal, _autoNumberingId);
                var autonumberArray = _orgService.RetrieveMultiple(getAutonumberEntities).Entities;
                if (autonumberArray.Count > 0)
                {
                    _counter = (autonumberArray[0].Attributes.ContainsKey("ddsm_counter"))
                        ? autonumberArray[0].GetAttributeValue<Decimal>("ddsm_counter")
                        : 0;
                    Entity autonumbering = new Entity("ddsm_autonumbering", _autoNumberingId);
                    autonumbering["ddsm_counter"] = _counter + _increment;
                    _orgService.Update(autonumbering);
                }
                else
                    return number;
            }

            var roundedCounter = decimal.Round(_counter).ToString();
            if (roundedCounter.Length < _length)
                roundedCounter = roundedCounter.PadLeft(_length, '0');
            number = _prefix + roundedCounter;
            number = Regex.Replace(number, "[^a-zA-Z0-9]", "");
            _counter = _counter + _increment;
            return number;
        }
    }
}
