﻿using System;

namespace CoreUtils.Service.Implementation.DataBase
{
    internal class BatchRequestService
    {
        private readonly int _defaultMaxBatchTimeExecution;
        private readonly int _defaultMinBatchTimeExecution;
        private readonly int _deltaTimeMax;
        private readonly int _deltaTimeMin;
        private readonly int _entitiesIntoStepDecrement;

        private readonly int _entitiesIntoStepIncrement;

        public BatchRequestService()
        {
            DefaultEntitiesIntoStep = GetDefaultEntitiesIntoStep();
            _defaultMaxBatchTimeExecution = GetDefaultMaxBatchTimeExecution();
            _defaultMinBatchTimeExecution = GetDefaultMinBatchTimeExecution();
            _entitiesIntoStepIncrement = GetEntitiesIntoStepIncrement();
            _entitiesIntoStepDecrement = GetEntitiesIntoStepDecrement();
            _deltaTimeMin = GetDeltaTimeMin();
            _deltaTimeMax = GetDeltaTimeMax();
        }

        public int DefaultEntitiesIntoStep { get; }

        private int PreviousBatchExecutionTime { get; set; }


        private int GetDefaultMaxBatchTimeExecution()
        {
            return 10;
        }

        private int GetDefaultMinBatchTimeExecution()
        {
            return 4;
        }

        private int GetDefaultEntitiesIntoStep()
        {
            return 5;
        }

        private int GetEntitiesIntoStepIncrement()
        {
            return 1;
        }

        private int GetEntitiesIntoStepDecrement()
        {
            return 5;
        }

        private int GetDeltaTimeMin()
        {
            return 1;
        }

        private int GetDeltaTimeMax()
        {
            return 3;
        }

        public int GetDynamicallyOffset(TimeSpan currentExecutionTime, int currentOffset)
        {
            int offset;
            if (currentExecutionTime.Seconds > _defaultMaxBatchTimeExecution)
                offset = currentOffset - _entitiesIntoStepDecrement;
            else if (currentExecutionTime.Seconds < _defaultMinBatchTimeExecution)
                offset = currentOffset + _entitiesIntoStepIncrement;
            else
                offset = GetStepIncrement(currentExecutionTime, currentOffset);

            return offset <= 0 ? 1 : offset;
        }

        private int GetStepIncrement(TimeSpan currentExecutionTime, int currentIncrement)
        {
            var offset = currentIncrement;
            if (PreviousBatchExecutionTime != 0)
            {
                var currentDeltaTime = PreviousBatchExecutionTime - currentExecutionTime.Seconds;
                if (currentDeltaTime > _deltaTimeMax)
                    offset--;
                else if (currentDeltaTime > _deltaTimeMin && currentDeltaTime < _deltaTimeMax)
                    offset++;
                else
                    offset = currentIncrement;
            }
            PreviousBatchExecutionTime = currentExecutionTime.Seconds;
            return offset;
        }
    }
}