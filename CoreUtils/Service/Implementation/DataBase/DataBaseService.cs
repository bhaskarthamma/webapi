﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using CoreUtils.Service.Interfaces.DataBase;
using DmnEngineApp.Service;
using Microsoft.Xrm.Sdk;
using SocketClients.Dto;

namespace CoreUtils.Service.Implementation.DataBase
{
    /// <summary>
    ///     Class implemented IDataBaseService and provides CRUD operation into data base
    /// </summary>
    public class DataBaseService : AbstractDataBaseService
    {
        private readonly IDataBaseTaskManagerCreate _dataBaseTaskManagerCreate;
        private readonly IDataBaseTaskManagerUpdate _dataBaseTaskManagerUpdate;

        //Provides programmatic access to the metadata and data for an organization.
        private readonly IOrganizationService _organizationService;

        public DataBaseService(IOrganizationService organizationService)
        {
            _organizationService = organizationService;
            _dataBaseTaskManagerCreate = new DataBaseTaskManagerCreate(this, _organizationService);
            _dataBaseTaskManagerUpdate = new DataBaseTaskManagerUpdate(this);
            WsMessageService = new WSMessageService(new WsCreateMsgDto());
        }

        /// <summary>
        ///     Update each object from the set of entitycollection
        /// </summary>
        /// <param name="entities"></param>
        public override void Update(List<Entity> entities)
        {
            _dataBaseTaskManagerUpdate.Update(entities);
        }

        /// <summary>
        ///     Update entity
        /// </summary>
        /// <param name="entity"></param>
        public override void Update(Entity entity)
        {
            _organizationService.Update(entity);
        }

        /// <summary>
        ///     Create entity
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>Object Id</returns>
        public override Guid Create(Entity entity)
        {
            return _organizationService.Create(entity);
        }

        /// <summary>
        ///     Create each object from the set of entitycollection
        /// </summary>
        /// <param name="entities"></param>
        /// <param name="totalEntitiesCount"></param>
        /// <returns></returns>
        public override ConcurrentDictionary<int, Guid> Create(List<Entity> entities, int totalEntitiesCount)
        {
            return _dataBaseTaskManagerCreate.Create(entities, totalEntitiesCount);
        }
    }
}