﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using Microsoft.Crm.Sdk.Messages;
using System.Collections.Concurrent;

namespace CoreUtils.Service.Implementation
{
    public static class TransactionCurrency
    {
        private static ConcurrentDictionary<string, EntityReference> CurrencyCache = new ConcurrentDictionary<string, EntityReference>();
        private static ConcurrentDictionary<string, bool> EntityIsCurrencyCache = new ConcurrentDictionary<string, bool>();

        public static EntityReference GetCurrency(IOrganizationService orgService, Guid UserGuid = new Guid())
        {
            var currency = new EntityReference();
            //Get User Currency
            if(UserGuid == Guid.Empty)
                UserGuid = ((WhoAmIResponse)orgService.Execute(new WhoAmIRequest())).UserId;
            if (CurrencyCache.ContainsKey(UserGuid.ToString()))
                return CurrencyCache[UserGuid.ToString()];

            var query = new QueryExpression("usersettings");
            query.ColumnSet = new ColumnSet("transactioncurrencyid");
            query.Criteria.AddCondition("systemuserid", ConditionOperator.Equal, UserGuid);

            Entity userSettings = orgService.RetrieveMultiple(query).Entities.FirstOrDefault();
            if (userSettings.Attributes.ContainsKey("transactioncurrencyid"))
                currency = userSettings.GetAttributeValue<EntityReference>("transactioncurrencyid");

            //Get Org default currency
            if (currency.Id == Guid.Empty)
                currency = GetCurrencyOrg(orgService);

            CurrencyCache.AddOrUpdate(UserGuid.ToString(), currency, (oldkey, oldvalue) => currency);

            return currency;
        }

        public static EntityReference GetCurrency(IOrganizationService orgService, Guid UserGuid, string logicalName)
        {
            if (string.IsNullOrEmpty(logicalName))
                return new EntityReference();

            var isField = EntityIsCurrency(orgService, logicalName);

            if (isField)
                return GetCurrency(orgService, UserGuid);

            return new EntityReference();
        }

        public static bool EntityIsCurrency(IOrganizationService orgService, string logicalName)
        {
            if (string.IsNullOrEmpty(logicalName))
                return false;

            if (EntityIsCurrencyCache.ContainsKey(logicalName))
                return EntityIsCurrencyCache[logicalName];

            RetrieveEntityRequest request = new RetrieveEntityRequest
            {
                EntityFilters = EntityFilters.Attributes,
                LogicalName = logicalName
            };
            RetrieveEntityResponse response = (RetrieveEntityResponse)orgService.Execute(request);
            var isField = response.EntityMetadata.Attributes.FirstOrDefault(element => element.LogicalName == "transactioncurrencyid") != null;
            EntityIsCurrencyCache.AddOrUpdate(logicalName, isField, (oldkey, oldvalue) => isField);

            return isField;
        }

        private static EntityReference GetCurrencyOrg(IOrganizationService orgService)
        {
            var orgId = ((WhoAmIResponse)orgService.Execute(new WhoAmIRequest())).OrganizationId;
            var currency = new EntityReference();
            var query = new QueryExpression("organization");
            query.ColumnSet = new ColumnSet("basecurrencyid");
            query.Criteria.AddCondition("organizationid", ConditionOperator.Equal, orgId);
            var orgInfo = orgService.RetrieveMultiple(query).Entities.FirstOrDefault();
            if (orgInfo.Attributes.ContainsKey("basecurrencyid"))
                currency = orgInfo.GetAttributeValue<EntityReference>("basecurrencyid");
            return currency;
        }
    }
}
