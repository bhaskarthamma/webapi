﻿using System.Collections.Generic;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;

namespace CoreUtils.Service.Interfaces.DataBase
{
    internal interface IDataBaseRequestManager
    {
        ExecuteMultipleRequest UpdateRequest(List<Entity> entities);
        ExecuteMultipleRequest CreateRequest(List<Entity> entities);
    }
}