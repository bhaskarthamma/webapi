﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using DmnEngineApp.Service;
using Microsoft.Xrm.Sdk;

namespace CoreUtils.Service.Interfaces.DataBase
{
    public interface IDataBaseService
    {
        WSMessageService WsMessageService { get; set; }

        /// <summary>
        ///     Update each object from the set of entitycollection
        /// </summary>
        /// <param name="entities"></param>
        void UpdateEntities(List<Entity> entities);


        /// <summary>
        ///     Create each object from the set of entitycollection
        /// </summary>
        /// <param name="entities"></param>
        /// <param name="totalEntitiesCount"></param>
        /// <returns>List Ids of created objects  </returns>
        ConcurrentDictionary<int, Guid> CreateEntities(List<Entity> entities, int totalEntitiesCount = 0);


        /// <summary>
        ///     Update the entity
        /// </summary>
        /// <param name="entity"></param>
        void UpdateEntity(Entity entity);


        /// <summary>
        ///     Create entity
        /// </summary>
        /// <returns>Object Id</returns>
        Guid CreateEntity(Entity entity);
    }
}