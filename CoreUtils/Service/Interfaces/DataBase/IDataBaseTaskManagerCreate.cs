﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using Microsoft.Xrm.Sdk;

namespace CoreUtils.Service.Interfaces.DataBase
{
    internal interface IDataBaseTaskManagerCreate
    {
        ConcurrentDictionary<int, Guid> Create(List<Entity> outputEntities, int totalEntitiesCount);
    }
}