﻿using CoreUtils.Wrap;
using Microsoft.Xrm.Sdk.Messages;

namespace CoreUtils.Utils
{
    public static class Extentions
    {
        public static void ResultToLog(this ExecuteMultipleResponse responseWithResults)
        {
            if (responseWithResults.IsFaulted)
            {
                foreach (var resp in responseWithResults.Responses)
                {
                    if (resp.Fault != null)
                    {
                        Log.Debug("in ExecuteMultipleResponse: " + resp.Fault.Message);
                    }
                }
            }
        }
    }
}
