﻿namespace CrmEmailSender.Enums
{
    public enum EmailStatus
    {
        //Open
        Draft = 1,
        Failed = 8,

        //Completed
        Completed = 2,
        Sent = 3,
        Received = 4,
        PendingSend = 6,
        Sending = 7, 

        //Canceled
        Canceled = 5
    }
}
