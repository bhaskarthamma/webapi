﻿using System;
using System.Collections.Generic;
using CrmEmailSender.Extensions;
using System.Configuration;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Tooling.Connector;

namespace CrmEmailSender
{
    class Program
    {
        static void Main(string[] args)
        {
            string connectionStringName = "CRMConnectionString";

            try
            {
                using (CrmServiceClient orgService = new CrmServiceClient(ConfigurationManager.ConnectionStrings[connectionStringName].ToString()))
                {
                    IEnumerable<Guid> draftedEmailsIdList = orgService.GetDraftedEmailsIdList();
                    var result = orgService.SendDraftedEmails(draftedEmailsIdList);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            Environment.Exit(0);
        }
    }
}
