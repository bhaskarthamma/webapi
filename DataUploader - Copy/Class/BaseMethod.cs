﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using DataUploader.Helper;
using static DataUploader.Helper.Errors;
using static DataUploader.Helper.Enums;
using DataUploader.Extentions;
using JsonConvert = CoreUtils.Wrap.JsonConvert;
using EntityJson = CoreUtils.DataUploader.Dto.EntityJson;
using AttrJson = CoreUtils.DataUploader.Dto.AttrJson;
using MappingImplementation.Service.Interfaces;
using CoreUtils.Service.Implementation;

namespace DataUploader.Creator
{
    public abstract class BaseMethod : IDisposable
    {
        protected PaginationRetriveMultiple EntityCollectionPagination;
        protected StatisticRecords StatisticRecords;
        protected ConcurrentDictionary<string, StatisticRecords> StatisticsRecords = new ConcurrentDictionary<string, StatisticRecords>();
        protected ConcurrentDictionary<string, EntityJson> ConfigObject;
        protected ConcurrentDictionary<string, CreatedEntity> SkippedEntities = new ConcurrentDictionary<string, CreatedEntity>();
        protected ConcurrentDictionary<string, ConcurrentDictionary<string, CreatedEntity>> RecordEntities;
        protected DataUploaderSettings UploaderSettings;
        protected IMappingImplementationService Implementation;
        protected MappingImplementation.Dto.ImplementationLookupFieldsDto ImplementationLookupFields;
        protected string SheetName = string.Empty;
        protected bool IsCurrencyField = false;
        protected bool IsCreatorField = false;
        protected bool IsPrimarykeyField = false;
        protected bool IsAutoNumberingField = false;
        protected string PrimaryAttributeName = string.Empty;
        protected LogService LogService;
        protected CacheService CacheService;
        protected AutoNumberingService AutoNumbering;
        protected AutoNumberingService AutoNumberingChild;
        protected GenerationNameService GenerationName;
        protected GenerationNameService GenerationNameChild;
        private bool _disposed = false;

        //Generation Entity Object
        public Entity GetFieldValueEntity(IOrganizationService orgService, String tableName, Entity dr, Guid recordUploaded, Dictionary<string, AttrJson> configEntityAttr, string entityUniqueId)
        {
            Entity entityObj = new Entity();

            try
            {
                foreach (var attr in configEntityAttr)
                {
                    if (dr.Attributes.ContainsKey("ddsm_" + (attr.Key).ToLower()))
                    {
                        if ((attr.Value.ColumnName.ToLower()).IndexOf("guid") != -1) { }
                        else if ((attr.Value.ColumnName.ToLower()).IndexOf("milestone") != -1) { }
                        else
                        {
                            if (!string.IsNullOrEmpty(Convert.ToString(dr["ddsm_" + (attr.Key).ToLower()])))
                            {
                                if (string.IsNullOrEmpty(attr.Value.AttrType)) { continue; }
                                if (attr.Value.AttrType.ToLower() != "lookup")
                                {
                                    dynamic tmpEntityObj = GetAttributeValue(orgService, ConfigObject[tableName].Name.ToLower(), attr.Value.AttrLogicalName.ToLower(), attr.Value.AttrType, dr["ddsm_" + (attr.Key).ToLower()], string.Empty, string.Empty);
                                    if (tmpEntityObj != null)
                                        entityObj[attr.Value.AttrLogicalName.ToLower()] = tmpEntityObj;
                                    else
                                        LogService.AddMessage(SheetName + ": " + " Excel Unique GUID: '" + entityUniqueId + "'; " + "Excel column: '" + attr.Key + "' / Header: '" + attr.Value.ColumnName + "' / Value: '" + dr["ddsm_" + (attr.Key).ToLower()] + "' - " + GetTextError(380016), MessageType.ERROR, LogType.LogDataIssues, false);
                                }
                                else
                                {
                                    dynamic tmpEntityObj = GetAttributeValue(orgService, ConfigObject[tableName].Name.ToLower(), attr.Value.AttrLogicalName.ToLower(), attr.Value.AttrType, dr["ddsm_" + (attr.Key).ToLower()], attr.Value.AttrTargetEntity.ToLower(), string.Empty);
                                    if (tmpEntityObj != null)
                                        entityObj[attr.Value.AttrLogicalName.ToLower()] = tmpEntityObj;
                                    else
                                        LogService.AddMessage(SheetName + ": " + " Excel Unique GUID: '" + entityUniqueId + "'; " + "Excel column: '" + attr.Key + "' / Header: '" + attr.Value.ColumnName + "' / Value: '" + dr["ddsm_" + (attr.Key).ToLower()] + "' / lookupEntity: '" + attr.Value.AttrTargetEntity + "' - " + GetTextError(380015), MessageType.ERROR, LogType.LogDataIssues, false);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                entityObj = new Entity();
                return entityObj;
            }
            return entityObj;
        }

        public ConcurrentDictionary<string, EntityCollection> GetIndicatorValueEntity(IOrganizationService orgService, String tableName, Entity dr, Guid recordUploaded, Dictionary<string, AttrJson> configEntityAttr, string entityUniqueId)
        {
            ConcurrentDictionary<string, EntityCollection> indicatorsValue = new ConcurrentDictionary<string, EntityCollection>();
            foreach (var attr in configEntityAttr)
            {
                if (!string.IsNullOrEmpty(attr.Value.IRelationName))
                {
                    var iRelationName = (attr.Value.IRelationName).Split(':')[0].Trim();
                    indicatorsValue.AddOrUpdate(iRelationName, new EntityCollection(), (oldkey, oldvalue) => new EntityCollection());
                }
            }
            try
            {
                foreach (var attr in configEntityAttr)
                {
                    if (dr.Attributes.ContainsKey("ddsm_" + (attr.Key).ToLower()))
                    {
                        if ((attr.Value.ColumnName.ToLower()).IndexOf("guid") != -1) { }
                        else if ((attr.Value.ColumnName.ToLower()).IndexOf("milestone") != -1) { }
                        else
                        {
                            if (!string.IsNullOrEmpty(Convert.ToString(dr["ddsm_" + (attr.Key).ToLower()])))
                            {
                                if (string.IsNullOrEmpty(attr.Value.AttrType)) { continue; }
                                if (string.IsNullOrEmpty(attr.Value.IEntity)) { continue; }
                                Entity entityObj = new Entity(attr.Value.IEntity);
                                if (attr.Value.AttrType.ToLower() != "lookup")
                                {
                                    dynamic tmpEntityObj = GetAttributeValue(orgService, attr.Value.IEntity.ToLower(), attr.Value.IFieldLogicalName.ToLower(), attr.Value.AttrType, dr["ddsm_" + (attr.Key).ToLower()], attr.Value.IFieldTargetEntity.ToLower(), attr.Value.IndicatorId);
                                    if (tmpEntityObj != null)
                                    {
                                        var iLookupLName = (attr.Value.IRelationName).Split(':')[1];
                                        if (!string.IsNullOrEmpty(iLookupLName))
                                            entityObj[iLookupLName] = new EntityReference("ddsm_indicator", new Guid(attr.Value.IndicatorId));
                                        entityObj[attr.Value.IFieldLogicalName.ToLower()] = tmpEntityObj;
                                        entityObj[(attr.Value.IRelationName).Split(':')[2]] = attr.Value.IName.Trim();
                                    }
                                    else
                                        LogService.AddMessage(SheetName + ": " + " Excel Unique GUID: '" + entityObj + "'; " + "Excel column: '" + attr.Key + "' / Header: '" + attr.Value.ColumnName + "' / Value: '" + dr["ddsm_" + (attr.Key).ToLower()] + "' - " + GetTextError(380016), MessageType.ERROR, LogType.LogDataIssues, false);
                                }
                                else
                                {
                                    dynamic tmpEntityObj = GetAttributeValue(orgService, attr.Value.IEntity.ToLower(), attr.Value.IFieldLogicalName.ToLower(), attr.Value.AttrType, dr["ddsm_" + (attr.Key).ToLower()], attr.Value.IFieldTargetEntity.ToLower(), attr.Value.IndicatorId);
                                    if (tmpEntityObj != null)
                                    {
                                        var iLookupLName = (attr.Value.IRelationName).Split(':')[1];
                                        if (!string.IsNullOrEmpty(iLookupLName))
                                            entityObj[iLookupLName] = new EntityReference("ddsm_indicator", new Guid(attr.Value.IndicatorId));
                                        entityObj[attr.Value.IFieldLogicalName.ToLower()] = tmpEntityObj;
                                        entityObj[(attr.Value.IRelationName).Split(':')[2]] = attr.Value.IName.Trim();
                                    }
                                    else
                                        LogService.AddMessage(SheetName + ": " + " Excel Unique GUID: '" + entityObj + "'; " + "Excel column: '" + attr.Key + "' / Header: '" + attr.Value.ColumnName + "' / Value: '" + dr["ddsm_" + (attr.Key).ToLower()] + "' / lookupEntity: '" + attr.Value.IFieldTargetEntity + "' - " + GetTextError(380015), MessageType.ERROR, LogType.LogDataIssues, false);
                                }
                                if (entityObj.Attributes.Count > 0)
                                {
                                    if ((attr.Value.IRelationName).Split(':')[3].ToLower() == "true" && UploaderSettings.CurrencyGuid != Guid.Empty)
                                        entityObj["transactioncurrencyid"] = new EntityReference("transactioncurrency", UploaderSettings.CurrencyGuid);
                                    if ((attr.Value.IRelationName).Split(':')[4].ToLower() == "true")
                                        entityObj["ddsm_creatorrecordtype"] = new OptionSetValue(962080001);
                                    var iRelationName = (attr.Value.IRelationName).Split(':')[0];
                                    var eCollection = indicatorsValue[iRelationName];
                                    eCollection.Entities.Add(entityObj);
                                    eCollection.EntityName = entityObj.LogicalName;
                                    indicatorsValue.AddOrUpdate(iRelationName, eCollection, (oldkey, oldvalue) => eCollection);

                                }

                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                indicatorsValue = new ConcurrentDictionary<string, EntityCollection>();
                return indicatorsValue;
            }
            return indicatorsValue;

        }

        //Create value attribute entity
        private dynamic GetAttributeValue(IOrganizationService orgService, string entityName, string attrName, string attrType, object attrValue, string lookupEntity, string indicatorId)
        {
            try
            {
                string cacheStr = string.Empty;
                //Get cache value
                switch (attrType) {
                    case "Picklist":
                    case "Lookup":
                    case "Boolean":
                        if (!string.IsNullOrEmpty(indicatorId) && !string.IsNullOrEmpty(lookupEntity) && attrType == "Picklist")
                            cacheStr = entityName + attrName + attrType + attrValue.ToString() + lookupEntity + indicatorId;
                        else 
                            cacheStr = entityName + attrName + attrType + attrValue.ToString() + lookupEntity;
                        if (CacheService.ContainsKey(cacheStr))
                            return CacheService.Get(cacheStr);
                        break;
                }
                //Get attribute by type and value
                switch (attrType)
                {
                    case "String":
                        return Convert.ToString(attrValue);
                    case "Memo":
                        return Convert.ToString(attrValue);
                    case "Picklist":
                        if (!string.IsNullOrEmpty(indicatorId) && !string.IsNullOrEmpty(lookupEntity))
                        {
                            QueryExpression lookupQuery = new QueryExpression { EntityName = lookupEntity, ColumnSet = new ColumnSet(lookupEntity + "id") };
                            lookupQuery.Criteria.AddCondition("ddsm_name", ConditionOperator.Equal, attrValue.ToString());
                            lookupQuery.Criteria.AddCondition("ddsm_indicator", ConditionOperator.Equal, new Guid(indicatorId));
                            EntityCollection lookupRetrieve = orgService.RetrieveMultiple(lookupQuery);
                            if (lookupRetrieve != null && lookupRetrieve.Entities.Count == 1)
                            {
                                CacheService.AddOrUpdate(cacheStr, new EntityReference(lookupEntity, lookupRetrieve.Entities[0].GetAttributeValue<Guid>(lookupEntity + "id")));
                                return new EntityReference(lookupEntity, lookupRetrieve.Entities[0].GetAttributeValue<Guid>(lookupEntity + "id"));
                            } else
                            {
                                CacheService.AddOrUpdate(cacheStr, null);
                            }
                        }
                        else {
                            RetrieveAttributeRequest retrieveAttributeRequest = new RetrieveAttributeRequest
                            {
                                EntityLogicalName = entityName,
                                LogicalName = attrName,
                                RetrieveAsIfPublished = true
                            };
                            RetrieveAttributeResponse retrieveAttributeResponse = (RetrieveAttributeResponse)orgService.Execute(retrieveAttributeRequest);
                            Microsoft.Xrm.Sdk.Metadata.PicklistAttributeMetadata retrievedPicklistAttributeMetadata = (Microsoft.Xrm.Sdk.Metadata.PicklistAttributeMetadata)
                            retrieveAttributeResponse.AttributeMetadata;
                            OptionMetadata[] optionList = retrievedPicklistAttributeMetadata.OptionSet.Options.ToArray();
                            int selectedOptionValue = 0;
                            foreach (OptionMetadata oMd in optionList)
                            {
                                if (DataUploader.IsNumeric(attrValue.ToString()))
                                {
                                    if (oMd.Value.Value == Convert.ToInt32(attrValue.ToString()))
                                    {
                                        selectedOptionValue = oMd.Value.Value;
                                        break;
                                    }
                                }
                                else
                                {
                                    if (oMd.Label.LocalizedLabels[0].Label.ToString().ToLower() == attrValue.ToString().ToLower())
                                    {
                                        selectedOptionValue = oMd.Value.Value;
                                        break;
                                    }
                                }
                            }
                            if (selectedOptionValue != 0)
                            {
                                //add value to cache
                                if (!string.IsNullOrEmpty(cacheStr))
                                    CacheService.AddOrUpdate(cacheStr, new OptionSetValue(selectedOptionValue));
                                return new OptionSetValue(selectedOptionValue);
                            }
                            else
                            {
                                //defalue value optionset
                                //add value to cache
                                /*
                                if(!string.IsNullOrEmpty(_cacheValKey))
                                    CollectionAttrValue.AddOrUpdate(_cacheValKey, new OptionSetValue(retrievedPicklistAttributeMetadata.DefaultFormValue.Value), (oldkey, oldvalue) => new OptionSetValue(retrievedPicklistAttributeMetadata.DefaultFormValue.Value));
                                return new OptionSetValue(retrievedPicklistAttributeMetadata.DefaultFormValue.Value);
                                */
                            }
                            if (!string.IsNullOrEmpty(cacheStr))
                                CacheService.AddOrUpdate(cacheStr, null);
                        }
                        return null;

                    case "Lookup":
                        if (string.IsNullOrEmpty(attrValue.ToString())) 
                            return null;

                        if (lookupEntity != string.Empty)
                        {
                            //Get Lookup Over Record UniqueGUID (UniqueGUID Excel)
                            var overUniqueId = GetLookupOverUniqueGuid(orgService, attrValue.ToString(), lookupEntity);
                            if (overUniqueId != null) {
                                //add value to cache
                                if (!string.IsNullOrEmpty(cacheStr))
                                    CacheService.AddOrUpdate(cacheStr, overUniqueId);
                                return overUniqueId;
                            }

                            string lokupNameField = string.Empty;
                            string lokupIdField = string.Empty;

                            if (lookupEntity.IndexOf("ddsm_") != -1) {
                                if (lookupEntity == "ddsm_projecttask")
                                {
                                    lokupNameField = "subject";
                                    lokupIdField = lookupEntity + "activityid";
                                }
                                else {
                                    lokupNameField = "ddsm_name";
                                    lokupIdField = lookupEntity + "id";
                                }
                            }
                            else if (lookupEntity == "systemuser" || lookupEntity == "contact" || lookupEntity == "lead")
                            {
                                lokupNameField = "fullname";
                                lokupIdField = lookupEntity + "id";
                            }
                            else if (lookupEntity == "task" || lookupEntity == "appoinment")
                            {
                                lokupNameField = "subject";
                                lokupIdField = "activityid";
                            }
                            else if (lookupEntity == "annotation")
                            {
                                lokupNameField = "subject";
                                lokupIdField = lookupEntity + "id";
                            }
                            else if (lookupEntity == "incident")
                            {
                                lokupNameField = "title";
                                lokupIdField = lookupEntity + "id";
                            }
                            else
                            {
                                lokupNameField = "name";
                                lokupIdField = lookupEntity + "id";
                            }
                            try
                            {
                                //Get Lookup Over Record Guid
                                Guid isGuid = Guid.Parse(attrValue.ToString());
                                var overRecordGuid = GetLookupOverRecordGuidOrName(orgService, lokupNameField, lokupIdField, attrValue.ToString(), lookupEntity);
                                if (overRecordGuid != null)
                                {
                                    //add value to cache
                                    if (!string.IsNullOrEmpty(cacheStr))
                                        CacheService.AddOrUpdate(cacheStr, overRecordGuid);
                                    return overRecordGuid;
                                }
                            }
                            catch (ArgumentNullException){}
                            catch (FormatException){}

                            //Get Lookup Over Record Name
                            var overRecordName = GetLookupOverRecordGuidOrName(orgService, lokupNameField, lokupIdField, attrValue.ToString(), lookupEntity, false);
                            if (overRecordName != null)
                            {
                                //add value to cache
                                if (!string.IsNullOrEmpty(cacheStr))
                                    CacheService.AddOrUpdate(cacheStr, overRecordName);
                                return overRecordName;
                            }
                            //add value to cache
                            if (!string.IsNullOrEmpty(cacheStr))
                                CacheService.AddOrUpdate(cacheStr, null);
                            return null;
                        }
                        else
                        {
                            //add value to cache
                            if (!string.IsNullOrEmpty(cacheStr))
                                CacheService.AddOrUpdate(cacheStr, null);
                            return null;
                        }
                    case "Decimal":
                        if (DataUploader.IsNumeric(attrValue))
                            return Convert.ToDecimal(attrValue);
                        else
                            return null;
                    case "Double":
                        if (DataUploader.IsNumeric(attrValue))
                            return Convert.ToDouble(attrValue);
                        else
                            return null;
                    case "Money":
                        if (DataUploader.IsNumeric(attrValue))
                            return new Money(Convert.ToDecimal(attrValue));
                        else
                            return null;
                    case "Boolean":
                        if (attrValue.ToString().ToLower() == "true") return true;
                        if (attrValue.ToString().ToLower() == "false") return false;
                        RetrieveAttributeRequest retrieveAttributeBoolRequest = new RetrieveAttributeRequest
                        {
                            EntityLogicalName = entityName,
                            LogicalName = attrName,
                            RetrieveAsIfPublished = true
                        };
                        RetrieveAttributeResponse retrieveAttributeBoolResponse = (RetrieveAttributeResponse)orgService.Execute(retrieveAttributeBoolRequest);
                        BooleanAttributeMetadata retrievedBooleanAttributeMetadata = (BooleanAttributeMetadata)retrieveAttributeBoolResponse.AttributeMetadata;
                        if (retrievedBooleanAttributeMetadata.OptionSet.FalseOption.Label.LocalizedLabels[0].Label.ToString().ToLower() == attrValue.ToString().ToLower())
                        {
                            //add value to cache
                            if (!string.IsNullOrEmpty(cacheStr))
                                CacheService.AddOrUpdate(cacheStr, false);
                            return false;
                        }
                        else if (retrievedBooleanAttributeMetadata.OptionSet.TrueOption.Label.LocalizedLabels[0].Label.ToString().ToLower() == attrValue.ToString().ToLower())
                        {
                            //add value to cache
                            if (!string.IsNullOrEmpty(cacheStr))
                                CacheService.AddOrUpdate(cacheStr, true);
                            return true;
                        }

                        //add value to cache
                        if (!string.IsNullOrEmpty(cacheStr))
                            CacheService.AddOrUpdate(cacheStr, retrievedBooleanAttributeMetadata.DefaultValue.Value);
                        return retrievedBooleanAttributeMetadata.DefaultValue.Value;
                    case "DateTime":
                        return DataUploader.ConverAttributToDateTimeUtc(attrValue, UploaderSettings.TimeZoneInfo);
                    case "Integer":
                        if (DataUploader.IsNumeric(attrValue))
                            return Convert.ToInt32(attrValue);
                        else
                            return null;
                    case "BigInt":
                        if (DataUploader.IsNumeric(attrValue))
                            return Convert.ToInt64(attrValue);
                        else
                            return null;
                }
                return null;
            }
            catch (Exception e)
            {
                LogService.AddMessage("Error > " + e.Message, MessageType.FATAL);
                return null;
            }
        }

        private EntityReference GetLookupOverUniqueGuid(IOrganizationService orgService, string attrValue, string lookupEntity) {
            try
            {
                if (attrValue.ToLower().IndexOf("guid_") != 0)
                    return null;
                ConcurrentDictionary<string, CreatedEntity> currentEntities = RecordEntities.FirstOrDefault(x => x.Key == lookupEntity).Value;
                if (currentEntities != null && !currentEntities.ContainsKey(attrValue))
                    GetDictionaryObjectbyGuid(orgService, attrValue, lookupEntity, currentEntities);
                if (currentEntities != null && currentEntities.ContainsKey(attrValue))
                {
                    Guid entityGuid = currentEntities.FirstOrDefault(x => x.Key == attrValue).Value.EntityGuid;
                    if (entityGuid != null && entityGuid != Guid.Empty)
                        return new EntityReference(lookupEntity, entityGuid);
                }
            }
            catch (Exception e) { }
            return null;
        }

        private EntityReference GetLookupOverRecordGuidOrName(IOrganizationService orgService, string lokupNameField, string lokupIdField, string attrValue, string lookupEntity, bool isGuid = true)
        {
            try
            {
                QueryExpression lookupQuery;
                EntityCollection lookupRetrieve;
                if (!isGuid)
                {
                    lookupQuery = new QueryExpression { EntityName = lookupEntity, ColumnSet = new ColumnSet(lokupNameField, lokupIdField) };
                    lookupQuery.Criteria.AddCondition(lokupNameField, ConditionOperator.Equal, attrValue);
                    lookupRetrieve = orgService.RetrieveMultiple(lookupQuery);
                    if (lookupRetrieve != null && lookupRetrieve.Entities.Count == 1)
                        return new EntityReference(lookupEntity, new Guid(lookupRetrieve.Entities[0].Attributes[lokupIdField].ToString()));
                }
                else {
                    lookupQuery = new QueryExpression { EntityName = lookupEntity, ColumnSet = new ColumnSet(lokupNameField, lokupIdField) };
                    lookupQuery.Criteria.AddCondition(lokupIdField, ConditionOperator.Equal, new Guid(attrValue));
                    lookupRetrieve = orgService.RetrieveMultiple(lookupQuery);
                    if (lookupRetrieve != null && lookupRetrieve.Entities.Count == 1)
                        return new EntityReference(lookupEntity, new Guid(lookupRetrieve.Entities[0].Attributes[lokupIdField].ToString()));
                }

            }
            catch (Exception e) { }
            return null;
        }

        private void GetDictionaryObjectbyGuid(IOrganizationService orgService, string uniqueGuid, string entityName, ConcurrentDictionary<string, CreatedEntity> dictionaryEntity)
        {
            if (string.IsNullOrEmpty(entityName)) return;
            if (string.IsNullOrEmpty(uniqueGuid)) return;

            var keyEntity = ConfigObject.FirstOrDefault(x => x.Value.Name.ToLower() == entityName).Key;
            if (string.IsNullOrEmpty(keyEntity)) return;
            string uniqueIdColumnKey = ConfigObject[keyEntity].Attributes.FirstOrDefault(x => x.Value.ColumnName.ToLower() == (keyEntity + " GUID").ToLower()).Key;
            if (string.IsNullOrEmpty(uniqueIdColumnKey)) return;
            string jsonObject = string.Empty;
            if (!dictionaryEntity.ContainsKey(uniqueGuid))
            {
                if (keyEntity != null)
                {
                    QueryExpression query = new QueryExpression();
                    query.EntityName = "ddsm_exceldata";
                    query.ColumnSet = new ColumnSet("ddsm_entityobjectdictionary");
                    query.Criteria = new FilterExpression(LogicalOperator.And);
                    query.Criteria.AddCondition(new ConditionExpression("subject", ConditionOperator.Equal, keyEntity));
                    query.Criteria.AddCondition(new ConditionExpression("ddsm_" + uniqueIdColumnKey.ToLower(), ConditionOperator.Equal, uniqueGuid));
                    query.Criteria.AddCondition(new ConditionExpression("ddsm_processed", ConditionOperator.Equal, true));
                    query.Criteria.AddCondition(new ConditionExpression("statecode", ConditionOperator.Equal, 1));
                    query.Criteria.AddCondition(new ConditionExpression("statuscode", ConditionOperator.Equal, 2));
                    query.Criteria.AddCondition(new ConditionExpression("ddsm_datauploader", ConditionOperator.Equal, UploaderSettings.TargetEntity.Id));
                    EntityCollection queryRetrieve = orgService.RetrieveMultiple(query);
                    if (queryRetrieve != null && queryRetrieve.Entities.Count == 1)
                    {
                        if (queryRetrieve[0].Attributes.ContainsKey("ddsm_entityobjectdictionary"))
                            jsonObject = (string)queryRetrieve[0].GetAttributeValue<string>("ddsm_entityobjectdictionary");
                    }
                    if (string.IsNullOrEmpty(jsonObject)) return;
                    var dictionaryObject = JsonConvert.DeserializeObject<ConcurrentDictionary<string, CreatedEntity>>(jsonObject);
                    if (dictionaryObject != null)
                        dictionaryEntity.AddOrUpdate(uniqueGuid, dictionaryObject[uniqueGuid], (oldkey, oldvalue) => dictionaryObject[uniqueGuid]);
                }
            }

        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    if (SkippedEntities != null)
                        SkippedEntities?.Clear();
                    if (StatisticsRecords != null)
                        StatisticsRecords?.Clear();
                    if (EntityCollectionPagination != null)
                        EntityCollectionPagination = null;
                    if (StatisticRecords != null)
                        StatisticRecords = null;
                    if (ConfigObject != null)
                        ConfigObject?.Clear();
                    CacheService.Dispose();
                    LogService.Dispose();
                }
                _disposed = true;
            }
        }

    }
}
