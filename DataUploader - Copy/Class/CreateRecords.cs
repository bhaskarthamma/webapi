﻿using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using static DataUploader.Helper.Enums;
using static DataUploader.Helper.Errors;
using DataUploader.Helper;
using DataUploader.Extentions;

using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Crm.Sdk.Messages;
using JsonConvert = CoreUtils.Wrap.JsonConvert;
using EntityJson = CoreUtils.DataUploader.Dto.EntityJson;
using MappingImplementation.Service.Implementation;
using Microsoft.Xrm.Sdk.Metadata.Query;
using CoreUtils.Dto;
using CoreUtils.Service.Implementation;
using Microsoft.Xrm.Sdk.Metadata;

namespace DataUploader.Creator
{
    public class CreateRecords : BaseMethod
    {

        private ConcurrentDictionary<string, CreatedEntity> _requestEntities;

        private CreateUpdate _createUpdate;
        private GetData _getData;
        private int _totalRecordCount;
        private readonly int _limitItemColletcion;

        public CreateRecords(ConcurrentDictionary<string, ConcurrentDictionary<string, CreatedEntity>> recordEntities, DataUploaderSettings uploaderSettings) {
            RecordEntities = recordEntities;
            UploaderSettings = uploaderSettings;
            Implementation = null;
            ImplementationLookupFields = null;
            AutoNumbering = null;
            AutoNumberingChild = null;
            GenerationName = null;
            GenerationNameChild = null;
            LogService = new LogService(UploaderSettings);
            ConfigObject = JsonConvert.DeserializeObject<ConcurrentDictionary<string, EntityJson>>(UploaderSettings.JsonObjects);
            IsCurrencyField = false;
            IsCreatorField = false;
            IsPrimarykeyField = false;
            IsAutoNumberingField = false;
            SheetName = string.Empty;
            PrimaryAttributeName = string.Empty;
            CacheService = new CacheService();
            _limitItemColletcion = UploaderSettings.GlobalRequestsCount;

            _totalRecordCount = 0;
        }

        //Create New
        public void CreateRecord(IOrganizationService orgService, string logicalName)
        {
            SheetName = ConfigObject.FirstOrDefault(x => x.Value.Name.ToLower() == logicalName).Key;
            if (string.IsNullOrEmpty(SheetName))
            {
                return;
            }
            if (UploaderSettings.MappingImpl.ContainsKey(logicalName) && !string.IsNullOrEmpty(UploaderSettings.MappingImpl[logicalName])) {
                try
                {
                    Guid isGuid = Guid.Parse(UploaderSettings.MappingImpl[logicalName]);
                    var mappingImplementationDto = new MappingImplementationDto()
                    {
                        OrgService = orgService,
                        CreatorRecordType = MappingImplementationDto.RecordCreator.DataUploader,
                        CurrentUserId = UploaderSettings.UserGuid
                    };
                    Implementation = new MappingImplementationService(mappingImplementationDto);
                    ImplementationLookupFields = Implementation.GetImplementationLookup(new Entity(logicalName), new Guid(UploaderSettings.MappingImpl[logicalName]));

                }
                catch (ArgumentNullException) { }
                catch (FormatException) { }
            }

            string uniqueIdColumnKey = ConfigObject[SheetName].Attributes.FirstOrDefault(x => x.Value.ColumnName.ToLower() == (SheetName + " GUID").ToLower()).Key;
            if (string.IsNullOrEmpty(uniqueIdColumnKey))
            {

                LogService.AddMessage("Column 'Sheet Name + GUID' Not found in sheet " + SheetName, MessageType.ERROR, LogType.Log, false);
                LogService.SaveLog(orgService);
                return;
            }

            _getData = new GetData(orgService);
            _createUpdate = new CreateUpdate(orgService, SheetName, logicalName, UploaderSettings.CallExecuteMultiple);

            //check for a field 'transactioncurrencyid' in the entity
            IsCurrencyField = DataUploader.DoesFieldExist(orgService, logicalName, "transactioncurrencyid");
            //check for a field 'ddsm_creatorrecordtype' in the entity
            IsCreatorField = DataUploader.DoesFieldExist(orgService, logicalName, "ddsm_creatorrecordtype");
            //check for a field 'ddsm_autonumbering' in the entity
            IsAutoNumberingField = DataUploader.DoesFieldExist(orgService, logicalName, "ddsm_autonumbering");
            //check for a field 'ddsm_primarykey' in the entity
            IsPrimarykeyField = DataUploader.DoesFieldExist(orgService, logicalName, "ddsm_primarykey");
            //Get entity primary attribute logical name
            PrimaryAttributeName = GetPrimaryAttributeName(orgService, logicalName);

            _totalRecordCount = GetCountAllRecords(orgService, UploaderSettings.TargetEntity.Id, logicalName);

            if (IsAutoNumberingField)
            {
                if (logicalName != "ddsm_financial")
                {
                    AutoNumbering = new AutoNumberingService(orgService, logicalName, _totalRecordCount);
                }
                else
                {
                    string parentProjectColumn = ConfigObject[SheetName].Attributes.FirstOrDefault(x => x.Value.AttrLogicalName.ToLower() == "ddsm_parentproject").Key;
                    string parentProjectGroupColumn = ConfigObject[SheetName].Attributes.FirstOrDefault(x => x.Value.AttrLogicalName.ToLower() == "ddsm_projectgroup").Key;
                    if (!string.IsNullOrEmpty(parentProjectColumn))
                    {
                        AutoNumbering = new AutoNumberingService(orgService, "ddsm_financial", _totalRecordCount, "ddsm_parentproject");
                    }
                    else if (!string.IsNullOrEmpty(parentProjectGroupColumn))
                    {
                        AutoNumbering = new AutoNumberingService(orgService, "ddsm_financial", _totalRecordCount, "ddsm_projectgroup");
                    }
                    else
                    {
                        AutoNumbering = new AutoNumberingService(orgService, logicalName, _totalRecordCount);
                    }
                }

                //hardcoding
                if (logicalName == "ddsm_project")
                {
                    AutoNumberingChild = new AutoNumberingService(orgService, "ddsm_financial", 1, "ddsm_parentproject");
                    GenerationNameChild = new GenerationNameService(orgService, "ddsm_financial", "ddsm_name");
                }
                if (logicalName == "ddsm_projectgroup")
                {
                    AutoNumberingChild = new AutoNumberingService(orgService, "ddsm_financial", 1, "ddsm_projectgroup");
                    GenerationNameChild = new GenerationNameService(orgService, "ddsm_financial", "ddsm_name");
                }
            }

            GenerationName = new GenerationNameService(orgService, logicalName, PrimaryAttributeName);

            LogService.AddMessage(GetTextError(380030) + SheetName + GetTextError(380031));

            StatisticRecords = new StatisticRecords();
            StatisticsRecords.AddOrUpdate("stat", StatisticRecords, (oldkey, oldvalue) => StatisticRecords);

            EntityCollectionPagination = new PaginationRetriveMultiple();
            DataUploader.SetUploadStatus(orgService, (int)UploadStatus.RecordsProcessing, UploaderSettings.TargetEntity.Id);
            do
            {
                EntityCollectionPagination = _getData.GetDataByEntityName(logicalName, ConfigObject, UploaderSettings.TargetEntity.Id, false, LogService, EntityCollectionPagination.PageNumber, EntityCollectionPagination.PagingCookie, EntityCollectionPagination.MoreRecords, UploaderSettings.GlobalPageRecordsCount);
                if (EntityCollectionPagination != null && EntityCollectionPagination.RetrieveCollection.Entities.Count > 0)
                {
                    StatisticsRecords["stat"].All += EntityCollectionPagination.RetrieveCollection.Entities.Count;
                    CreateRecordSet(orgService, logicalName, EntityCollectionPagination.RetrieveCollection, StatisticsRecords["stat"]);
                }
            }
            while (EntityCollectionPagination != null && EntityCollectionPagination.MoreRecords);
            if (StatisticsRecords["stat"].All > 0)
            {
                LogService.AddMessage(SheetName + ": " + GetTextError(380019) + StatisticsRecords["stat"].All + "; " + GetTextError(380020) + StatisticsRecords["stat"].Created + "; " + GetTextError(380021) + StatisticsRecords["stat"].Updated + "; " + GetTextError(380022) + StatisticsRecords["stat"].DoNothing + ";", MessageType.SUCCESS, LogType.Log, false);
                LogService.SaveLog(orgService);
            }
        }

        private void CreateRecordSet(IOrganizationService orgService, string logicalName, EntityCollection table, StatisticRecords statisticRecords)
        {
            ConcurrentDictionary<string, CreatedEntity> currentEntities = RecordEntities.FirstOrDefault(x => x.Key == logicalName).Value;
            string entityUniqueId = string.Empty;

            Int64 itemsRequestCount = 0, requestSets = 1;

            int entityDataRow = 0;
            string uniqueIdColumnKey = ConfigObject[SheetName].Attributes.FirstOrDefault(x => x.Value.ColumnName.ToLower() == (SheetName + " GUID").ToLower()).Key;

            if (string.IsNullOrEmpty(uniqueIdColumnKey))
            {
                return;
            }

            ExecuteMultipleRequest emRequest = new ExecuteMultipleRequest
            {
                Requests = new OrganizationRequestCollection(),
                Settings = new ExecuteMultipleSettings
                {
                    ContinueOnError = true,
                    ReturnResponses = true
                }
            };

            List<string> setUniqueId = new List<string>();
            foreach (Entity dr in table.Entities)
            {

                entityDataRow++;
                if (entityDataRow >= 1)
                {
                    try
                    {
                        entityUniqueId = string.Empty;

                        entityUniqueId = dr["ddsm_" + uniqueIdColumnKey.ToLower()].ToString();
                        if (string.IsNullOrEmpty(entityUniqueId)) continue;

                        //Verify duplicate record GUID
                        if (currentEntities.ContainsKey(entityUniqueId))
                        {
                            LogService.AddMessage(SheetName + ": " + " Excel Unique GUID: " + entityUniqueId + "; Not unique in sheet " + SheetName + " list", MessageType.ERROR, LogType.Log, false);
                            SetStateResponse setStateResponse = (SetStateResponse)orgService.Execute(new SetStateRequest
                            {
                                EntityMoniker = new EntityReference("ddsm_exceldata", (Guid)dr["activityid"]),
                                State = new OptionSetValue(2),
                                Status = new OptionSetValue(3)
                            });
                            continue;
                        }

                        //TypeField == "Field"
                        var configEntity = ConfigObject[SheetName].Attributes;
                        var configEntityField = configEntity.Where(x => x.Value.TypeField.ToLower() == "field").ToDictionary(x => x.Key, x => x.Value);
                        Entity entiyFieldData = GetFieldValueEntity(orgService, SheetName, dr, UploaderSettings.TargetEntity.Id, configEntityField, entityUniqueId);

                        /*
                                                if (entiyFieldData.Attributes.Count == 0)
                                                {
                                                    LogService.AddMessage(SheetName + ": " + SheetName + " Excel Unique GUID: " + EntityUniqueID + "; Incoming data was not converted", MessageType.ERROR, LogType.LogDataIssues, false);
                                                    Entity updateSourceRecord = new Entity("ddsm_exceldata", (Guid)dr["activityid"]);
                                                    updateSourceRecord["ddsm_processed"] = true;
                                                    updateSourceRecord["statecode"] = new OptionSetValue(2);
                                                    updateSourceRecord["statuscode"] = new OptionSetValue(3);
                                                    orgService.Update(updateSourceRecord);
                                                    continue;
                                                };
                        */

                        //TypeField == "Indicator"
                        configEntityField = configEntity.Where(x => x.Value.TypeField.ToLower() == "indicator").ToDictionary(x => x.Key, x => x.Value);
                        ConcurrentDictionary<string, EntityCollection> entityIndicatorData = GetIndicatorValueEntity(orgService, SheetName, dr, UploaderSettings.TargetEntity.Id, configEntityField, entityUniqueId);


                        //Gen AutoNumber for record
                        if (AutoNumbering != null && !entiyFieldData.Attributes.ContainsKey("ddsm_autonumbering"))
                        {
                            var autoNumber = AutoNumbering.GetNumber();
                            if (!string.IsNullOrEmpty(autoNumber))
                            {
                                entiyFieldData["ddsm_autonumbering"] = autoNumber;
                            }
                        }


                        //Get Implementation entity object
                        Entity newEntiy = new Entity(logicalName);
                        Entity cacheEntiy = new Entity(logicalName);
                        if (Implementation != null &&  ImplementationLookupFields != null)
                        {
                            string cacheStr = logicalName;
                            for (var i = 0; i < ImplementationLookupFields.ImplementFields.Count; i++)
                            {
                                cacheStr = cacheStr + ImplementationLookupFields.ImplementFields[i].LookupLogicalName;
                                if (entiyFieldData.Attributes.ContainsKey(ImplementationLookupFields.ImplementFields[i].LookupLogicalName))
                                {
                                    cacheEntiy[ImplementationLookupFields.ImplementFields[i].LookupLogicalName] = entiyFieldData[ImplementationLookupFields.ImplementFields[i].LookupLogicalName];
                                    cacheStr = cacheStr + entiyFieldData.GetAttributeValue<EntityReference>(ImplementationLookupFields.ImplementFields[i].LookupLogicalName).Id;
                                }
                            }
                            if (cacheEntiy.Attributes.Count > 0)
                            {
                                if (CacheService.ContainsKey(cacheStr))
                                {
                                    cacheEntiy = CacheService.Get(cacheStr, true);
                                    if (cacheEntiy.Attributes.Count > 0)
                                        newEntiy.Attributes.AddRange(cacheEntiy.Attributes);
                                    if (cacheEntiy.RelatedEntities.Count > 0)
                                        newEntiy.RelatedEntities.AddRange(cacheEntiy.RelatedEntities);
                                }
                                else {
                                    try {
                                        cacheEntiy = Implementation.ExecuteMapping(cacheEntiy, new Guid(UploaderSettings.MappingImpl[logicalName]))[0];
                                        if (cacheEntiy.Attributes.Count > 0)
                                            newEntiy.Attributes.AddRange(cacheEntiy.Attributes);
                                        if (cacheEntiy.RelatedEntities.Count > 0)
                                            newEntiy.RelatedEntities.AddRange(cacheEntiy.RelatedEntities);
                                        CacheService.AddOrUpdate(cacheStr, cacheEntiy, true);
                                    }
                                    catch (Exception e) {
                                        LogService.AddMessage("Error > " + SheetName + ": " + " Excel Unique GUID: " + entityUniqueId + "; Entity LogicalName: " + logicalName + " ImpMapping Id: " + UploaderSettings.MappingImpl[logicalName] + "; Message: " + e.Message, MessageType.FATAL);
                                    }
                                }
                            }
                        }

                        //Add/Update values of Indicators with values from an Excel file
                        foreach (KeyValuePair<String, EntityCollection> iDictionary in entityIndicatorData)
                        {
                            if (iDictionary.Value.Entities.Count > 0)
                            {
                                Relationship iRelationship = new Relationship(iDictionary.Key);
                                if (!newEntiy.RelatedEntities.ContainsKey(iRelationship))
                                    newEntiy.RelatedEntities.Add(iRelationship, iDictionary.Value);
                                else
                                {
                                    var primaryEntityAttributeName = "";
                                    if (iDictionary.Value.Entities[0].Attributes.ContainsKey("ddsm_name"))
                                        primaryEntityAttributeName = "ddsm_name";
                                    else if (iDictionary.Value.Entities[0].Attributes.ContainsKey("subject"))
                                        primaryEntityAttributeName = "subject";
                                    if (!string.IsNullOrEmpty(primaryEntityAttributeName))
                                    {
                                        for (var j = 0; j < iDictionary.Value.Entities.Count; j++)
                                        {
                                            bool isTrue = false;
                                            for (var i = 0; i < newEntiy.RelatedEntities[iRelationship].Entities.Count; i++)
                                            {
                                                if (newEntiy.RelatedEntities[iRelationship].Entities[i].Attributes.ContainsKey(primaryEntityAttributeName))
                                                {
                                                    string name1 = newEntiy.RelatedEntities[iRelationship].Entities[i].GetAttributeValue<string>(primaryEntityAttributeName).ToLower().Trim();
                                                    string name2 = iDictionary.Value.Entities[j].GetAttributeValue<string>(primaryEntityAttributeName).ToLower().Trim();
                                                    if (string.Equals(name1, name2))
                                                    {
                                                        foreach (KeyValuePair<String, Object> attr in iDictionary.Value.Entities[j].Attributes)
                                                        {
                                                            newEntiy.RelatedEntities[iRelationship].Entities[i][attr.Key] = iDictionary.Value.Entities[j].Attributes[attr.Key];
                                                        }
                                                        isTrue = true;
                                                        break;
                                                    }
                                                }
                                            }
                                            if (!isTrue)
                                                newEntiy.RelatedEntities[iRelationship].Entities.Add(iDictionary.Value.Entities[j]);
                                        }
                                    }
                                }
                            }
                        }

                        //Gen Name record
                        if (!string.IsNullOrEmpty(PrimaryAttributeName) && !entiyFieldData.Attributes.ContainsKey(PrimaryAttributeName))
                        {
                            var newName = GenerationName.GetName(entiyFieldData, newEntiy);
                            if (!string.IsNullOrEmpty(newName))
                            {
                                entiyFieldData[PrimaryAttributeName] = newName;
                            }
                        }

                        //Add values of the excel row
                        foreach (KeyValuePair<String, Object> attr in entiyFieldData.Attributes)
                        {
                            newEntiy[attr.Key] = entiyFieldData.Attributes[attr.Key];
                        }

                        //Currency
                        if (IsCurrencyField && !newEntiy.Attributes.ContainsKey("transactioncurrencyid") && UploaderSettings.CurrencyGuid != Guid.Empty)
                            newEntiy["transactioncurrencyid"] = new EntityReference("transactioncurrency", UploaderSettings.CurrencyGuid);

                        //Creator Record Type
                        if (IsCreatorField)
                            newEntiy["ddsm_creatorrecordtype"] = new OptionSetValue(962080001);


                        //Gen AutoNumbering and Name for financial child record (project & project group)
                        var fnlcRsName = string.Empty;
                        fnlcRsName = (logicalName == "ddsm_project") ? "ddsm_project_ddsm_financial_ParentProject" : string.Empty;
                        fnlcRsName = (logicalName == "ddsm_projectgroup") ? "ddsm_ddsm_projectgroup_ddsm_financial" : string.Empty;

                        if (!string.IsNullOrEmpty(fnlcRsName))
                        {
                            Relationship fnlcRs = new Relationship(fnlcRsName);
                            if (newEntiy.RelatedEntities.ContainsKey(fnlcRs))
                            {
                                foreach (var enFnlc in newEntiy.RelatedEntities[fnlcRs].Entities)
                                {
                                    //Gen AutoNumber for financial
                                    if (AutoNumberingChild != null && !enFnlc.Attributes.ContainsKey("ddsm_autonumbering"))
                                    {
                                        var autoNumber = AutoNumberingChild.GetNumber();
                                        if (!string.IsNullOrEmpty(autoNumber))
                                        {
                                            enFnlc["ddsm_autonumbering"] = autoNumber;
                                        }
                                        //Gen Name for financial
                                        var newName = GenerationNameChild.GetName(enFnlc, enFnlc);
                                        if (!string.IsNullOrEmpty(newName))
                                        {
                                            enFnlc["ddsm_name"] = newName;
                                        }

                                    }

                                }
                            }
                        }

                        CreatedEntity recEntity = new CreatedEntity();
                        CreateRequest createRequest = new CreateRequest();
                        createRequest.Target = newEntiy;
                        emRequest.Requests.Add(createRequest);

                        setUniqueId.Add(entityUniqueId);
                        itemsRequestCount++;

                        recEntity.EntityGuid = new Guid();
                        recEntity.SourceId = (Guid)dr["activityid"];
                        recEntity.Status = false;
                        recEntity.StatusDb = false;

                        currentEntities.AddOrUpdate(entityUniqueId, recEntity, (oldkey, oldvalue) => recEntity);

                    }
                    catch (Exception e)
                    {
                        itemsRequestCount++;
                        LogService.AddMessage("Error > " + SheetName + ": " + " Excel Unique GUID: " + entityUniqueId + "; Message: " + e.Message, MessageType.FATAL);
                    }
                }

                //Create Multiple
                if (itemsRequestCount == (requestSets * _limitItemColletcion))
                {
                    if (emRequest.Requests.Count > 0)
                    {
                        _requestEntities = new ConcurrentDictionary<string, CreatedEntity>();
                        _createUpdate.CreateUpdateEntityRecords(emRequest, setUniqueId, currentEntities, _requestEntities, statisticRecords, LogService, _totalRecordCount);
                        //Update Source records
                        if (_requestEntities.Count > 0)
                            _createUpdate.UpdateSourceRecords(_requestEntities, LogService);
                    }
                    //Update Source records (Skipped)
                    if (SkippedEntities.Count > 0)
                    {
                        _createUpdate.UpdateSourceRecords(SkippedEntities, LogService);
                        SkippedEntities = new ConcurrentDictionary<string, CreatedEntity>();
                    }

                    requestSets++;
                    setUniqueId = new List<string>();
                    emRequest = new ExecuteMultipleRequest
                    {
                        Requests = new OrganizationRequestCollection(),
                        Settings = new ExecuteMultipleSettings
                        {
                            ContinueOnError = true,
                            ReturnResponses = true
                        }
                    };

                }

            }
            if (setUniqueId.Count > 0 && emRequest.Requests.Count > 0)
            {
                _requestEntities = new ConcurrentDictionary<string, CreatedEntity>();
                _createUpdate.CreateUpdateEntityRecords(emRequest, setUniqueId, currentEntities, _requestEntities, statisticRecords, LogService, _totalRecordCount);
                //Update Source records
                if (_requestEntities.Count > 0)
                    _createUpdate.UpdateSourceRecords(_requestEntities, LogService);
            }
            //Update Source records (Skipped)
            if (SkippedEntities.Count > 0)
            {
                _createUpdate.UpdateSourceRecords(SkippedEntities, LogService);
                SkippedEntities = new ConcurrentDictionary<string, CreatedEntity>();
            }
        }

        private static string GetPrimaryAttributeName(IOrganizationService orgService, string entityLogicalName)
        {
            string fieldName = string.Empty;
            MetadataFilterExpression eFilter = new MetadataFilterExpression(LogicalOperator.And);
            eFilter.Conditions.Add(new MetadataConditionExpression("LogicalName", MetadataConditionOperator.Equals, entityLogicalName));
            MetadataPropertiesExpression eProperties = new MetadataPropertiesExpression() { AllProperties = false };
            eProperties.PropertyNames.Add("PrimaryNameAttribute");
            EntityQueryExpression eQuery = new EntityQueryExpression()
            {
                Criteria = eFilter,
                Properties = eProperties
            };
            RetrieveMetadataChangesRequest retriveMetadata = new RetrieveMetadataChangesRequest() { Query = eQuery };
            var resp = (RetrieveMetadataChangesResponse)orgService.Execute(retriveMetadata);
            if (resp != null && resp.EntityMetadata != null && resp.EntityMetadata.Count == 1)
                fieldName = resp.EntityMetadata[0].PrimaryNameAttribute;
            return fieldName;
        }

        private static OneToManyRelationshipMetadata GetRelationship(IOrganizationService orgService, string targetEntity, string referencedEntity)
        {
            try
            {
                var retrieveBankAccountEntityRequest = new RetrieveEntityRequest
                {
                    EntityFilters = EntityFilters.Relationships,
                    LogicalName = targetEntity
                };
                var retrieveBankEntityResponse = (RetrieveEntityResponse)orgService.Execute(retrieveBankAccountEntityRequest);
                var manyToOneRelationships = retrieveBankEntityResponse.EntityMetadata.ManyToOneRelationships;
                var rs = manyToOneRelationships.Where(r => r.ReferencedEntity == referencedEntity).ToList();
                return rs.Count == 0 ? null : rs[0];
            }
            catch (Exception e)
            {
                return null;
            }
        }

        private int GetCountAllRecords(IOrganizationService orgService, Guid id, string logicalName)
        {
            int count = 0;
            try
            {
                string fetch = @"
                <fetch  distinct='false' mapping='logical' aggregate='true'>
                <entity name = 'ddsm_exceldata'>
                    <attribute name = 'activityid' alias = 'records' aggregate = 'count' />
                    <filter type = 'and'>
                        <condition attribute = 'ddsm_datauploader' operator= 'eq' value = '" + id.ToString() + @"' />
                        <condition attribute = 'ddsm_logicalname' operator= 'eq' value = '" + logicalName + @"' />
                    </filter >
               </entity >
               </fetch >";

                EntityCollection result = orgService.RetrieveMultiple(new FetchExpression(fetch));
                count = (Int32)((AliasedValue)result.Entities[0]["records"]).Value;
            }
            catch (Exception e)
            { }

            return count;
        }

    }
}
