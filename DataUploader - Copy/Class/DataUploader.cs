﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;

using System;
using System.Collections.Generic;
using Microsoft.Xrm.Sdk.Query;
using System.Globalization;
using System.Linq;

namespace DataUploader
{

    public class ParsedRecords
    {
        public Int64 RequestCount { get; set; } = 0;
        public Int64 ResponceCount { get; set; } = 0;
        public Int64 ErrorCount { get; set; } = 0;
        public Int64 ExcludedDataCount { get; set; } = 0;
    }

    public class DataUploader
    {
        public static string ParseAttributToDateTime(Object inputDate)
        {
            try
            {
                if (inputDate.GetType().Name == "DateTime")
                {
                    return (Convert.ToDateTime(inputDate)).ToShortDateString();
                }
                else if (inputDate.GetType().Name == "Double")
                {
                    return (DateTime.FromOADate(Convert.ToDouble(inputDate))).ToShortDateString();
                }
                else if (inputDate.GetType().Name == "String")
                {
                    if (inputDate.ToString() == "")
                    {
                        return null;
                    }
                    else if (inputDate.ToString() == "today")
                    {
                        return (DateTime.Now).ToShortDateString();
                    }
                    else
                    {
                        return (Convert.ToDateTime(inputDate.ToString())).ToShortDateString();
                    }
                }
                else
                {
                    return null;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        //Conver to UTC Date Time from Exel Cell
        public static dynamic ConverAttributToDateTimeUtc(Object xlsxDate)
        {
            try
            {
                if (xlsxDate.GetType().Name == "DateTime")
                {
                    return xlsxDate;
                }
                else if (xlsxDate.GetType().Name == "Double")
                {
                    return DateTime.FromOADate(Convert.ToDouble(xlsxDate)).ToUniversalTime();
                }
                else if (xlsxDate.GetType().Name == "String")
                {
                    if (xlsxDate.ToString() == "")
                    {
                        return null;
                    }
                    else if (xlsxDate.ToString() == "today")
                    {
                        return DateTime.UtcNow;
                    }
                    else {
                        return Convert.ToDateTime(xlsxDate.ToString()).ToUniversalTime();
                    }
                }
                else {
                    return null;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public static dynamic ConverAttributToDateTimeUtc(Object inputDate, TimeZoneInfo timeZoneInfo)
        {
            try
            {
                if (inputDate.GetType().Name == "DateTime")
                {
                    var convertDate = Convert.ToDateTime(inputDate);
                    return TimeZoneInfo.ConvertTimeToUtc(DateTime.Parse(convertDate.ToShortDateString()), timeZoneInfo);
                }
                else if (inputDate.GetType().Name == "Double")
                {
                    var convertDate = DateTime.FromOADate(Convert.ToDouble(inputDate));
                    return TimeZoneInfo.ConvertTimeToUtc(DateTime.Parse(convertDate.ToShortDateString()), timeZoneInfo);
                }
                else if (inputDate.GetType().Name == "String")
                {
                    if (inputDate.ToString() == "")
                    {
                        return null;
                    }
                    else if (inputDate.ToString() == "today")
                    {
                        var convertDate = DateTime.Now;
                        return TimeZoneInfo.ConvertTimeToUtc(DateTime.Parse(convertDate.ToShortDateString()), timeZoneInfo);
                    }
                    else
                    {
                        var convertDate = Convert.ToDateTime(inputDate.ToString());
                        return TimeZoneInfo.ConvertTimeToUtc(DateTime.Parse(convertDate.ToShortDateString()), timeZoneInfo);
                    }
                }
                else
                {
                    return null;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        //Is Numeric Format
        public static Boolean IsNumeric(Object attrValue)
        {
            try
            {
                if (attrValue == null || attrValue is DateTime)
                    throw new Exception("Value '" + attrValue.ToString() + "' is not a number");

                if (attrValue is Int16 || attrValue is Int32 || attrValue is Int64 || attrValue is Decimal || attrValue is Single || attrValue is Double || attrValue is Boolean)
                    return true;
                double number;
                if (Double.TryParse(Convert.ToString(attrValue, CultureInfo.InvariantCulture), System.Globalization.NumberStyles.Any, NumberFormatInfo.InvariantInfo, out number))
                    return true;
                else
                    throw new Exception("Value '" + attrValue.ToString() + "' is not a number");
            }
            catch (Exception e)
            {
                return false;
            }
        }

        //Save Log
        public static void SaveLog(IOrganizationService orgService, Guid recordUploaded, List<string> logger, string fieldName, bool isAddLog = true)
        {
            var tmpStr = string.Empty;
            if (isAddLog)
            {
                QueryExpression recordUploadedQuery = new QueryExpression { EntityName = "ddsm_datauploader", ColumnSet = new ColumnSet(fieldName) };
                recordUploadedQuery.Criteria.AddCondition("ddsm_datauploaderid", ConditionOperator.Equal, recordUploaded);
                EntityCollection recordUploadedRetrieve = orgService.RetrieveMultiple(recordUploadedQuery);
                if (recordUploadedRetrieve[0].Attributes.ContainsKey(fieldName))
                    tmpStr = recordUploadedRetrieve[0].GetAttributeValue<string>(fieldName);
            }

            Entity dp = new Entity("ddsm_datauploader", recordUploaded);
            if (tmpStr != string.Empty)
                tmpStr = tmpStr + "\n" + string.Join("\n", logger);
            else
                tmpStr = string.Join("\n", logger);

            tmpStr = tmpStr.Substring((tmpStr.Length > 1000000) ? (tmpStr.Length - 1000000) : 0, (tmpStr.Length > 1000000) ? 1000000 : tmpStr.Length);
            dp[fieldName] = tmpStr;
            orgService.Update(dp);
        }

        public static void SetUploadStatus(IOrganizationService orgService, int status, Guid recordId)
        {
            Entity setStatus = new Entity("ddsm_datauploader", recordId);
            setStatus.Attributes.Add("ddsm_status", new OptionSetValue(status));
            orgService.Update(setStatus);
        }

        public static OptionMetadata[] GetUploadStatus(IOrganizationService orgService)
        {
            RetrieveAttributeRequest retrieveAttributeRequest = new RetrieveAttributeRequest
            {
                EntityLogicalName = "ddsm_datauploader",
                LogicalName = "ddsm_status",
                RetrieveAsIfPublished = true
            };
            RetrieveAttributeResponse retrieveAttributeResponse = (RetrieveAttributeResponse)orgService.Execute(retrieveAttributeRequest);
            PicklistAttributeMetadata retrievedPicklistAttributeMetadata = (PicklistAttributeMetadata)retrieveAttributeResponse.AttributeMetadata;
            return retrievedPicklistAttributeMetadata.OptionSet.Options.ToArray();
        }

        public static string GetTextUploadStatus(int optionValue, OptionMetadata[] uploadStatus)
        {
            string selectedOptionText = string.Empty;
            foreach (OptionMetadata oMd in uploadStatus)
            {
                if (optionValue == oMd.Value.Value)
                {
                    selectedOptionText = oMd.Label.LocalizedLabels[0].Label.ToString();
                    break;
                }
            }
            return selectedOptionText;
        }


        public static bool DoesFieldExist(IOrganizationService orgService, String entityName, String fieldName)
        {
            RetrieveEntityRequest request = new RetrieveEntityRequest
            {
                EntityFilters = EntityFilters.Attributes,
                LogicalName = entityName
            };
            RetrieveEntityResponse response
              = (RetrieveEntityResponse)orgService.Execute(request);
            return response.EntityMetadata.Attributes.FirstOrDefault(element
              => element.LogicalName == fieldName) != null;
        }

        private static Guid GetSystemUserId(string name, IOrganizationService orgService)
        {
            QueryByAttribute queryUsers = new QueryByAttribute
            {
                EntityName = "systemuser",
                ColumnSet = new ColumnSet("systemuserid")
            };

            queryUsers.AddAttributeValue("fullname", name);
            EntityCollection retrievedUsers = orgService.RetrieveMultiple(queryUsers);
            Guid systemUserId = ((Entity)retrievedUsers.Entities[0]).Id;

            return systemUserId;
        }

        public static IOrganizationService GetSystemOrgService(IOrganizationServiceFactory serviceFactory, IOrganizationService orgService)
        {
            IOrganizationService systemOrgService = serviceFactory.CreateOrganizationService(GetSystemUserId("SYSTEM", orgService));
            return systemOrgService;
            //return SystemService ?? (SystemService = serviceFactory.CreateOrganizationService(GetSystemUserId("SYSTEM", orgService)));
        }

    }
}
