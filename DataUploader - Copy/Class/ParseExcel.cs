﻿using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using DocumentFormat.OpenXml.Spreadsheet;
using Excel;
using DocumentFormat.OpenXml.Packaging;
using static DataUploader.Helper.Errors;
using DataUploader.Helper;
using JsonConvert = CoreUtils.Wrap.JsonConvert;
using EntityJson = CoreUtils.DataUploader.Dto.EntityJson;
using DataUploader.Extentions;

namespace DataUploader.Parser
{
    public class ParseExcel: IDisposable
    {
        private readonly IOrganizationService _orgService;
        private readonly DataUploaderSettings _thisSettings;
        private readonly MemoryStream _msFile;
        private readonly ConcurrentDictionary<string, EntityJson> _configObject;
        private Int64 _amountParsingErrors;
        private readonly LogService _logService;
        private List<Task> _tasks;

        private bool _disposed = false;

        public ParseExcel(IOrganizationService orgService, MemoryStream msFile, DataUploaderSettings thisSettings)
        {
            _orgService = orgService;
            _msFile = msFile;
            _thisSettings = thisSettings;
            _configObject = JsonConvert.DeserializeObject<ConcurrentDictionary<string, EntityJson>>(_thisSettings.JsonObjects);
            _amountParsingErrors = 0;
            _logService = new LogService(_thisSettings);
        }

        public bool ParseXls()
        {
            var listOutputReadAsDt = new List<OutputReadAsDt>();
            IExcelDataReader excelReader2007 = ExcelReaderFactory.CreateBinaryReader(_msFile);
            //excelReader2007.IsFirstRowAsColumnNames = true;
            var resultNotCleared = excelReader2007.AsDataSet();
            excelReader2007.Close();
            _logService.AddMessage(GetTextError(380008));
            _tasks = new List<Task>();
            foreach (DataTable dt in resultNotCleared.Tables)
            {
                var filteredValues = dt.AsEnumerable().Where(row => row.Field<dynamic>("A") != null && string.Compare((Convert.ToString(row.Field<dynamic>("A"))).Trim(), string.Empty) != 0).ToArray();
                var copyDt = filteredValues.CopyToDataTable();
                copyDt.TableName = dt.TableName;

                if (copyDt.Rows.Count == 0) continue;

                if (_configObject[copyDt.TableName] != null && !string.IsNullOrEmpty(_configObject[copyDt.TableName].Name))
                {
                    _logService.AddMessage(GetTextError(380004) + copyDt.TableName);

                    var readAsDataTable = new ReadAsDataTable(_orgService, _thisSettings, _configObject, copyDt, copyDt.TableName);
                    var bufTask = Task.Factory.StartNew(() => {
                        var outputReadAsDt = readAsDataTable.Xls();
                        _amountParsingErrors += outputReadAsDt.TableParsingErrors;
                        listOutputReadAsDt.Add(outputReadAsDt);
                    });
                    _tasks.Add(bufTask);

                }

            }
            Task.WaitAll(_tasks.ToArray(), -1);
            for (var i = 0; i < listOutputReadAsDt.Count; i++)
            {
                listOutputReadAsDt[i].LogService.SaveLog(_orgService);
                listOutputReadAsDt[i].LogService.Dispose();
            }
            _logService.AddMessage(GetTextError(380009));
            _logService.SaveLog(_orgService);
            //Clear resultNotCleared (excel) DataSet
            resultNotCleared.Tables.Clear();
            resultNotCleared.Dispose();
            if (_amountParsingErrors == 0)
                return false;
            else
                return true;
        }

        public bool ParseXlsx()
        {
            var listOutputReadAsDt = new List<OutputReadAsDt>();
            using (var excelDoc = SpreadsheetDocument.Open(_msFile, true))
            {
                WorkbookPart workbookPart = excelDoc.WorkbookPart;
                IEnumerable<Sheet> sheets = excelDoc.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>();
                var sheetCount = 0;
                try
                {
                    sheetCount = sheets.Count();
                }
                catch
                {
                    sheetCount = 0;
                }
                if (sheetCount == 0)
                {
                    return false;
                }
                _logService.AddMessage(GetTextError(380008));
                foreach (EntityJson sheetJson in _configObject.Values)
                {
                    if (!string.IsNullOrEmpty(sheetJson.Name))
                    {

                    }

                    var sheetName = _configObject.FirstOrDefault(x => x.Value.Name.ToLower() == sheetJson.Name.ToLower()).Key;
                    var sheet = sheets.Where(s => s.Name == sheetName).FirstOrDefault();
                        WorksheetPart worksheetPart = (WorksheetPart)excelDoc.WorkbookPart.GetPartById(sheet.Id.Value);

                    _logService.AddMessage(GetTextError(380004) + sheetName);

                    var readAsDataTable = new ReadAsDataTable(_orgService, _thisSettings, _configObject, excelDoc, worksheetPart, sheetName);
                    var bufTask = Task.Factory.StartNew(() => {
                        var outputReadAsDt = readAsDataTable.Xlsx();
                        _amountParsingErrors += outputReadAsDt.TableParsingErrors;
                        listOutputReadAsDt.Add(outputReadAsDt);
                    });
                    _tasks.Add(bufTask);
                }
            }
            Task.WaitAll(_tasks.ToArray(), -1);
            for (var i = 0; i < listOutputReadAsDt.Count; i++)
            {
                listOutputReadAsDt[i].LogService.SaveLog(_orgService);
                listOutputReadAsDt[i].LogService.Dispose();
            }
            _logService.AddMessage(GetTextError(380009));
            _logService.SaveLog(_orgService);
            if (_amountParsingErrors == 0)
                return false;
            else
                return true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    if (_configObject != null)
                    {
                        _configObject?.Clear();
                    }

                    _logService.Dispose();
                }
                _disposed = true;
            }
        }

    }
}
