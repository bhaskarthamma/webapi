﻿using System;
using System.Collections.Generic;
using static DataUploader.Helper.Enums;
using DataUploader.Helper;
using SocketClients;
using JsonConvert = CoreUtils.Wrap.JsonConvert;

namespace DataUploader.Service
{
    public class WsMessageUploaderService
    {

        private readonly WsMessageDu _wsMessageUploader;

        public WsMessageUploaderService(DataUploaderSettings thisSettings)
        {
            var wsMessageUploader = new WsMessageDu();
            wsMessageUploader.PrimaryEntityId = thisSettings.TargetEntity.Id;
            wsMessageUploader.MessageId = Guid.NewGuid();
            wsMessageUploader.PrimaryEntityName = "ddsm_datauploader";
            wsMessageUploader.UserId = thisSettings.UserGuid;
            wsMessageUploader.FileName = thisSettings.FileName;
            _wsMessageUploader = wsMessageUploader;
        }

        /// <summary>
        /// </summary>
        /// <param name="messageType"></param>
        /// <param name="message"></param>
        /// <param name="status"></param>
        public void SendMessage(MessageType messageType, string message, MessageStatus status)
        {
            _wsMessageUploader.MessageType = messageType.ToString();
            _wsMessageUploader.Message = _wsMessageUploader.FileName + "<br/>" + message;
            _wsMessageUploader.Status = status.ToString();
            try
            {
                StaticSocketClient.Send(_wsMessageUploader.UserId, JsonConvert.SerializeObject(_wsMessageUploader));
            }
            catch (Exception e)
            {
                //
            }
        }
    }

    public class WsMessageDu
    {
        public WsMessageDu()
        {
            RelatedEnityName = new List<string>();
            RelatedEntity = new List<RelatedEntityData>();
        }

        public string Message { get; set; }
        public Guid MessageId { get; set; }
        public string MessageType { get; set; }
        public string PrimaryEntityName { get; set; }
        public Guid PrimaryEntityId { get; set; }
        public string FileName { get; set; }

        public List<string> RelatedEnityName { get; set; }
        public List<RelatedEntityData> RelatedEntity { get; set; }
        public string Status { get; set; }
        public Guid UserId { get; set; }

        public class RelatedEntityData
        {
            public RelatedEntityData(Guid recordId, string entityLogicalName)
            {
                EntityLogicalName = entityLogicalName;
                RecordId = recordId;
            }

            public string EntityLogicalName { get; set; }
            public Guid RecordId { get; set; }
        }
    }

}
