﻿namespace DataUploader.Helper
{
    public class Enums
    {
        public enum MessageStatus
        {
            START,
            FINISH
        }

        public enum MessageType
        {
            SUCCESS,
            INFORMATION,
            ERROR,
            FATAL,
            DEBUG
        }
        public enum LogType
        {
            Log = 0,
            LogDataIssues = 1
        }

        public enum MilestoneStatus
        {
            NotStarted = 962080000,
            Active = 962080001,
            Completed = 962080002,
            Skipped = 962080003
        }

        public enum StatusXLSRecord
        {
            NotProcessed = 0,
            Processed = 1
        }

        public enum UploadStatus
        {
            FileUploadStarted = 962080000,
            FileUploadCompleted = 962080001,
            FileUploadFailed = 962080002,
            ParsingFile = 962080003,
            ParsingFileCompleted = 962080004,
            ParsingFileFailed = 962080005,
            RecordsProcessing = 962080006,
            RecordsUploadCompleted = 962080007,
            RecordsUploadFailed = 962080008,
        }


        public enum CreatorRecordType
        {
            UnassignedValue = -2147483648,
            Front = 962080000,
            DataUploader = 962080001,
            Portal = 962080002,
            PrepTool = 962080003
        }


    }
}
