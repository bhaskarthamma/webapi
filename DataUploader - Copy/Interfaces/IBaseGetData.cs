﻿using DataUploader.Extentions;
using DataUploader.Helper;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections.Concurrent;
using EntityJson = CoreUtils.DataUploader.Dto.EntityJson;

namespace DataUploader.Interfaces
{
    public interface IBaseGetData
    {
        Entity GetDataRelationMapping(string targetEntity, string sourceEntity, Guid sourceGuid);

        Entity GetEntityAttrsValue(Guid recordGuid, string entityName, ColumnSet columns);

        EntityCollection GetDataByEntityName(string entityName, ConcurrentDictionary<string, EntityJson> objects, Guid recordUploaded, bool statusRecord, LogService logService);

        PaginationRetriveMultiple GetDataByEntityName(string entityName, ConcurrentDictionary<string, EntityJson> objects, Guid recordUploaded, bool statusRecord, LogService logService, int pageNumber, dynamic pagingCookie, bool moreRecords, Int32 recordCount);

        string VerifyDuplicateRecord(Entity entityObj, string entityName, string sheetName, string uniqueId, LogService logService);

        bool ComparingAttributeValues(object oAttribute, object oAttribute2);

    }
}
