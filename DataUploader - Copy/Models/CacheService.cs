﻿using DataUploader.Helper;
using DataUploader.Interfaces;
using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Concurrent;
using System.Security.Cryptography;
using System.Text;

namespace DataUploader.Extentions
{
    public class CacheService : IBaseCacheService, IDisposable
    {
        //Cashe
        private readonly ConcurrentDictionary<string, object> _casheRelationDataAttributes = new ConcurrentDictionary<string, object>();
        private readonly ConcurrentDictionary<string, object> _collectionAttrValue = new ConcurrentDictionary<string, object>();
        private readonly ConcurrentDictionary<string, Entity> _collectionEntityValue = new ConcurrentDictionary<string, Entity>();
        private readonly ConcurrentDictionary<string, object> _collectionNullValue = new ConcurrentDictionary<string, object>();

        private bool _disposed = false;

        public void AddOrUpdate(string keyStrCache, object oAttribute)
        {
            if (!string.IsNullOrEmpty(keyStrCache))
            {
                MD5 md5 = new MD5CryptoServiceProvider();
                byte[] checkSum1 = md5.ComputeHash(Encoding.UTF8.GetBytes(keyStrCache));
                string keyCache = BitConverter.ToString(checkSum1).Replace("-", String.Empty);

                if (oAttribute != null)
                {
                    _collectionAttrValue.AddOrUpdate(keyCache, oAttribute, (oldkey, oldvalue) => oAttribute);
                }
                else
                {
                    _collectionNullValue.AddOrUpdate(keyCache, new IsNull(), (oldkey, oldvalue) => new IsNull());
                }
            }
        }

        public void AddOrUpdate(string keyStrCache, RelationDataAttributes oRelationData)
        {
            if (!string.IsNullOrEmpty(keyStrCache))
            {
                MD5 md5 = new MD5CryptoServiceProvider();
                byte[] checkSum1 = md5.ComputeHash(Encoding.UTF8.GetBytes(keyStrCache));
                string keyCache = BitConverter.ToString(checkSum1).Replace("-", String.Empty);

                if (oRelationData != null)
                {
                    _casheRelationDataAttributes.AddOrUpdate(keyCache, oRelationData, (oldkey, oldvalue) => oRelationData);
                }
                else
                {
                    _collectionNullValue.AddOrUpdate(keyCache, new IsNull(), (oldkey, oldvalue) => new IsNull());
                }
            }
        }

        public void AddOrUpdate(string keyStrCache, Entity oEntity, bool isEnity)
        {
            if (!string.IsNullOrEmpty(keyStrCache))
            {
                MD5 md5 = new MD5CryptoServiceProvider();
                byte[] checkSum1 = md5.ComputeHash(Encoding.UTF8.GetBytes(keyStrCache));
                string keyCache = BitConverter.ToString(checkSum1).Replace("-", String.Empty);

                if (oEntity != null)
                {
                    _collectionEntityValue.AddOrUpdate(keyCache, oEntity, (oldkey, oldvalue) => oEntity);
                }
                else
                {
                    _collectionNullValue.AddOrUpdate(keyCache, new IsNull(), (oldkey, oldvalue) => new IsNull());
                }
            }
        }


        public dynamic Get(string keyStrCache)
        {
            if (!string.IsNullOrEmpty(keyStrCache))
            {
                MD5 md5 = new MD5CryptoServiceProvider();
                byte[] checkSum1 = md5.ComputeHash(Encoding.UTF8.GetBytes(keyStrCache));
                string keyCache = BitConverter.ToString(checkSum1).Replace("-", String.Empty);

                if (_collectionNullValue.ContainsKey(keyCache))
                {
                    if (_collectionNullValue[keyCache].GetType().FullName == "DataUploader.Helper.IsNull")
                    {
                        return null;
                    }
                }
                else if (_collectionAttrValue.ContainsKey(keyCache))
                {
                        return _collectionAttrValue[keyCache];
                }
                else if (_casheRelationDataAttributes.ContainsKey(keyCache))
                {
                    return (RelationDataAttributes)_casheRelationDataAttributes[keyCache];
                }
                
            }
            return null;
        }

        public dynamic Get(string keyStrCache, bool isEnity)
        {
            if (!string.IsNullOrEmpty(keyStrCache))
            {
                MD5 md5 = new MD5CryptoServiceProvider();
                byte[] checkSum1 = md5.ComputeHash(Encoding.UTF8.GetBytes(keyStrCache));
                string keyCache = BitConverter.ToString(checkSum1).Replace("-", String.Empty);

                if (_collectionNullValue.ContainsKey(keyCache))
                {
                    if (_collectionNullValue[keyCache].GetType().FullName == "DataUploader.Helper.IsNull")
                    {
                        return null;
                    }
                }
                else if (_collectionEntityValue.ContainsKey(keyCache))
                {
                    return (Entity)_collectionEntityValue[keyCache];
                }
            }
            return null;
        }

        public bool ContainsKey(string keyStrCache)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] checkSum1 = md5.ComputeHash(Encoding.UTF8.GetBytes(keyStrCache));
            string keyCache = BitConverter.ToString(checkSum1).Replace("-", String.Empty);

            if (_collectionNullValue.ContainsKey(keyCache))
            {
                return true;
            }
            else if (_collectionAttrValue.ContainsKey(keyCache))
            {
                return true;
            }
            else if (_casheRelationDataAttributes.ContainsKey(keyCache))
            {
                return true;
            }
            else if (_collectionEntityValue.ContainsKey(keyCache))
            {
                return true;
            }
            return false;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    if (_collectionNullValue != null)
                    {
                        _collectionNullValue?.Clear();
                    }
                    if (_collectionAttrValue != null)
                    {
                        _collectionAttrValue?.Clear();
                    }
                    if (_casheRelationDataAttributes != null)
                    {
                        _casheRelationDataAttributes?.Clear();
                    }
                    if (_collectionEntityValue != null)
                    {
                        _collectionEntityValue?.Clear();
                    }
                }
                _disposed = true;
            }
        }

    }
}
