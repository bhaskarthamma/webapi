﻿using DataUploader.Helper;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading.Tasks;
using static DataUploader.Helper.Enums;

namespace DataUploader.Extentions
{

    public class TaskExecuteMultiple
    {
        private readonly IOrganizationService _orgService;
        private readonly ExecuteMultipleRequest _emRequest;

        public TaskExecuteMultiple(IOrganizationService orgService, ExecuteMultipleRequest emRequest)
        {
            _orgService = orgService;
            _emRequest = emRequest;
        }

        public void Run()
        {
            ExecuteMultipleResponse emResponse = (ExecuteMultipleResponse)_orgService.Execute(_emRequest);
            /*
            foreach (ExecuteMultipleResponseItem responseItem in _emResponse.Responses)
            {
                if (responseItem.Response != null)
                {
                    if (((OrganizationResponse)responseItem.Response).Results.ContainsKey("id"))
                    {
                    }
                }

                else if (responseItem.Fault != null)
                {
                    _logService.AddMessage("*** Error: Create Milestones: Request: " + emRequest.Requests[responseItem.RequestIndex].RequestName + "; Index: " + (responseItem.RequestIndex + 1) + "; Message: " + responseItem.Fault.Message + "; TraceText: " + responseItem.Fault.TraceText, MessageType.FATAL);
                }
            }
            */
        }
    }


    public class ChildrenRelationships
    {
        private List<Task> _tasks;
        private readonly IOrganizationService _orgService;
        private readonly ConcurrentDictionary<string, DataCollection<Entity>> _msEntities;
        private readonly ConcurrentDictionary<string, CreatedEntity> _requestEntities;
        private readonly LogService _logService;
        private readonly string _lookupName;
        private readonly string _parentEntityName;

        public ChildrenRelationships(IOrganizationService orgService, ConcurrentDictionary<string, CreatedEntity> requestEntities, ConcurrentDictionary<string, DataCollection<Entity>> msEntities, string lookupName, string parentEntityName, LogService logService)
        {
            _orgService = orgService;
            _msEntities = msEntities;
            _requestEntities = requestEntities;
            _logService = logService;
            _lookupName = lookupName;
            _parentEntityName = parentEntityName;
        }

        public void Create()
        {
            ExecuteMultipleRequest emRequest = new ExecuteMultipleRequest
            {
                Requests = new OrganizationRequestCollection(),
                Settings = new ExecuteMultipleSettings
                {
                    ContinueOnError = true,
                    ReturnResponses = false
                }
            };

            _tasks = new List<Task>();

            try
            {
                foreach (var cEn in _requestEntities)
                {
                    if (_msEntities.ContainsKey(cEn.Key))
                    {
                        foreach (var en in _msEntities[cEn.Key])
                        {
                            var ms = en;
                            ms[_lookupName] = new EntityReference(_parentEntityName, cEn.Value.EntityGuid);

                            CreateRequest createRequest = new CreateRequest();
                            createRequest.Target = ms;
                            emRequest.Requests.Add(createRequest);
                        }

                        if (emRequest.Requests.Count > 0)
                        {
                            var taskEMultiple = new TaskExecuteMultiple(_orgService, emRequest);
                            var bufTask = Task.Factory.StartNew(() => taskEMultiple.Run());
                            _tasks.Add(bufTask);

                            emRequest = new ExecuteMultipleRequest
                            {
                                Requests = new OrganizationRequestCollection(),
                                Settings = new ExecuteMultipleSettings
                                {
                                    ContinueOnError = true,
                                    ReturnResponses = false
                                }
                            };
                        }
                    }
                }

            }
            catch (Exception e)
            {
                _logService.AddMessage("Error > Create Milestones: " + e.Message, MessageType.FATAL, LogType.Log, false);
            }

            Task.WaitAll(_tasks.ToArray(), -1);

        }

    }
}
