﻿using DataUploader.Interfaces;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using static DataUploader.Helper.Enums;
using DataUploader.Helper;
using Log = CoreUtils.Wrap.Log;
using DataUploader.Service;


namespace DataUploader.Extentions
{
    public class LogService : IBaseLogService, IDisposable
    {
        private readonly ConcurrentDictionary<string, List<string>> _logList;
        private readonly List<string> _fieldName = new List<string>(new string[] { "ddsm_log", "ddsm_logdataissues" });
        private readonly Guid _uploaderId;
        private readonly string _separator = "--------------------------------------------------";
        private bool _disposed = false;

#if RELESEPLUGIN
#else
        private WsMessageUploaderService WsMessageService { get; }
#endif

        public LogService(DataUploaderSettings thisSettings)
        {
            _uploaderId = thisSettings.TargetEntity.Id;
            _logList = new ConcurrentDictionary<string, List<string>>();
            for (var i = 0; i < _fieldName.Count; i++)
            {
                _logList.AddOrUpdate(_fieldName[i], new List<string>(), (oldkey, oldvalue) => new List<string>());
            }

#if RELESEPLUGIN
#else
            WsMessageService = new WsMessageUploaderService(thisSettings);
#endif

        }

        public void AddMessage(string message = "", MessageType messageType = MessageType.INFORMATION, LogType logType = LogType.Log, bool onlySocket = true)
        {
            MessageStatus messageStatus = MessageStatus.START;

            if (!string.IsNullOrEmpty(message))
            {

                if (!onlySocket)
                {
                    switch (messageType)
                    {
                        case MessageType.ERROR:
                        case MessageType.FATAL:
                            _logList[_fieldName[(int)logType]].Add("ERROR: " + message);
                            Log.Error("DU >>> " + message);
                            break;
                        case MessageType.INFORMATION:
                        case MessageType.SUCCESS:
                            _logList[_fieldName[(int)logType]].Add(message);
                            Log.Debug("DU >>> " + message);
                            break;
                        case MessageType.DEBUG:
                            _logList[_fieldName[(int)logType]].Add("DEBUG: " + message);
                            Log.Debug("DU >>> " + message);
                            break;
                    }
                }
                else {

                    switch (messageType)
                    {
                        case MessageType.INFORMATION:
                        case MessageType.SUCCESS:
                            WsMessageService.SendMessage(messageType, message, messageStatus);
                            break;
                        case MessageType.ERROR:
                        case MessageType.FATAL:
                            WsMessageService.SendMessage(MessageType.ERROR, message, messageStatus);
                            Log.Error("DU >>> " + message);
                            break;
                        case MessageType.DEBUG:
                            Log.Debug("DU >>> " + message);
                            break;
                    }
                }
            }
        }

        public void AddSeparator(string separator = "", LogType logType = LogType.Log)
        {
            if (string.IsNullOrEmpty(separator))
            {
                separator = _separator;
            }
            _logList[_fieldName[(int)logType]].Add(separator);

        }
        public void SaveLog(IOrganizationService orgService, bool isAddLog = true)
        {
            try
            {
                bool logIsEmpty = true;
                for (var i = 0; i < _fieldName.Count; i++)
                {
                    if (_logList[_fieldName[i]].Count > 0)
                    {
                        logIsEmpty = false;
                        break;
                    }
                }

                if (logIsEmpty) {
                    return;
                }

                EntityCollection recordUploadedRetrieve = null;
                string[] strLog = new string[_fieldName.Count];

                if (isAddLog)
                {
                    ColumnSet columns = new ColumnSet();
                    for (var i = 0; i < _fieldName.Count; i++)
                    {
                        columns.AddColumn(_fieldName[i]);
                    }
                    QueryExpression recordUploadedQuery = new QueryExpression { EntityName = "ddsm_datauploader", ColumnSet = columns };
                    recordUploadedQuery.Criteria.AddCondition("ddsm_datauploaderid", ConditionOperator.Equal, _uploaderId);
                    recordUploadedRetrieve = orgService.RetrieveMultiple(recordUploadedQuery);
                }

                Entity dp = new Entity("ddsm_datauploader", _uploaderId);

                for (var i = 0; i < _fieldName.Count; i++)
                {

                    strLog[i] = string.Empty;

                    if (isAddLog)
                    {
                        if (recordUploadedRetrieve != null && recordUploadedRetrieve[0].Attributes.ContainsKey(_fieldName[i]))
                        {
                            strLog[i] = recordUploadedRetrieve[0].GetAttributeValue<string>(_fieldName[i]);
                        }
                    }

                    if (_logList[_fieldName[i]].Count > 0)
                    {
                        if (!string.IsNullOrEmpty(strLog[i]))
                        {
                            strLog[i] = strLog[i] + "\n" + string.Join("\n", _logList[_fieldName[i]]);
                        }
                        else
                        {
                            strLog[i] = string.Join("\n", _logList[_fieldName[i]]);
                        }

                        strLog[i] = strLog[i].Substring((strLog[i].Length > 1000000) ? (strLog[i].Length - 1000000) : 0, (strLog[i].Length > 1000000) ? 1000000 : strLog[i].Length);

                        dp[_fieldName[i]] = strLog[i];
                    }

                }

                if (dp.Attributes.Count > 0)
                {
                    orgService.Update(dp);
                }

                for (var i = 0; i < _fieldName.Count; i++)
                {
                    _logList.AddOrUpdate(_fieldName[i], new List<string>(), (oldkey, oldvalue) => new List<string>());
                }

            }
            catch (Exception e)
            {
                Log.Error("DU >>> Error > " + e.Message);
                for (var i = 0; i < _fieldName.Count; i++)
                {
                    _logList.AddOrUpdate(_fieldName[i], new List<string>(), (oldkey, oldvalue) => new List<string>());
                }

            }

        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {

                    for (var i = 0; i < _fieldName.Count; i++)
                    {
                        if (_logList[_fieldName[i]] != null) {
                            _logList[_fieldName[i]]?.Clear();
                        }
                    }

                    if (_logList != null)
                    {
                        _logList?.Clear();
                    }
                }
                _disposed = true;
            }
        }

    }
}
