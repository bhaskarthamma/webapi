﻿using CoreUtils.DataUploader.Dto;
using CoreUtils.Service.Implementation;
using CoreUtils.Wrap;
using DataUploader;
using DataUploader.Extentions;
using DataUploader.Helper;
using MappingImplementation.Dto;
using MappingImplementation.Service.Interfaces;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;


namespace DataUploader.Creator
{
    public abstract class BaseMethod : IDisposable
    {
        protected PaginationRetriveMultiple EntityCollectionPagination;

        protected StatisticRecords StatisticRecords;

        protected ConcurrentDictionary<string, StatisticRecords> StatisticsRecords = new ConcurrentDictionary<string, StatisticRecords>();

        protected ConcurrentDictionary<string, EntityJson> ConfigObject;

        protected ConcurrentDictionary<string, CreatedEntity> SkippedEntities = new ConcurrentDictionary<string, CreatedEntity>();

        protected ConcurrentDictionary<string, ConcurrentDictionary<string, CreatedEntity>> RecordEntities;

        protected DataUploaderSettings UploaderSettings;

        protected IMappingImplementationService Implementation;

        protected ImplementationLookupFieldsDto ImplementationLookupFields;

        protected string SheetName = string.Empty;

        protected bool IsCurrencyField = false;

        protected bool IsCreatorField = false;

        protected bool IsPrimarykeyField = false;

        protected bool IsAutoNumberingField = false;

        protected string PrimaryAttributeName = string.Empty;

        protected LogService LogService;

        protected CacheService CacheService;

        protected AutoNumberingService AutoNumbering;

        protected AutoNumberingService AutoNumberingChild;

        protected GenerationNameService GenerationName;

        protected GenerationNameService GenerationNameChild;

        private bool _disposed = false;

        public Entity GetFieldValueEntity(IOrganizationService orgService, string tableName, Entity dr, Guid recordUploaded, Dictionary<string, AttrJson> configEntityAttr, string entityUniqueId)
        {
            Entity entity = new Entity();
            try
            {
                foreach (KeyValuePair<string, AttrJson> item in configEntityAttr)
                {
                    if (dr.Attributes.ContainsKey("ddsm_" + item.Key.ToLower()) && item.Value.ColumnName.ToLower().IndexOf("guid") == -1 && item.Value.ColumnName.ToLower().IndexOf("milestone") == -1 && !string.IsNullOrEmpty(Convert.ToString(dr["ddsm_" + item.Key.ToLower()])) && !string.IsNullOrEmpty(item.Value.AttrType))
                    {
                        if (item.Value.AttrType.ToLower() != "lookup")
                        {
                            dynamic attributeValue = GetAttributeValue(orgService, ConfigObject[tableName].Name.ToLower(), item.Value.AttrLogicalName.ToLower(), item.Value.AttrType, dr["ddsm_" + item.Key.ToLower()], string.Empty, string.Empty);
                            if (attributeValue != null)
                            {
                                entity[item.Value.AttrLogicalName.ToLower()] = (object)attributeValue;
                            }
                            else
                            {
                                LogService.AddMessage(SheetName + ":  Excel Unique GUID: '" + entityUniqueId + "'; Excel column: '" + item.Key + "' / Header: '" + item.Value.ColumnName + "' / Value: '" + dr["ddsm_" + item.Key.ToLower()] + "' - " + Errors.GetTextError(380016), Enums.MessageType.ERROR, Enums.LogType.LogDataIssues, onlySocket: false);
                            }
                        }
                        else
                        {
                            dynamic attributeValue2 = GetAttributeValue(orgService, ConfigObject[tableName].Name.ToLower(), item.Value.AttrLogicalName.ToLower(), item.Value.AttrType, dr["ddsm_" + item.Key.ToLower()], item.Value.AttrTargetEntity.ToLower(), string.Empty);
                            if (attributeValue2 != null)
                            {
                                entity[item.Value.AttrLogicalName.ToLower()] = (object)attributeValue2;
                            }
                            else
                            {
                                LogService.AddMessage(SheetName + ":  Excel Unique GUID: '" + entityUniqueId + "'; Excel column: '" + item.Key + "' / Header: '" + item.Value.ColumnName + "' / Value: '" + dr["ddsm_" + item.Key.ToLower()] + "' / lookupEntity: '" + item.Value.AttrTargetEntity + "' - " + Errors.GetTextError(380015), Enums.MessageType.ERROR, Enums.LogType.LogDataIssues, onlySocket: false);
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
                return new Entity();
            }
            return entity;
        }

        public ConcurrentDictionary<string, EntityCollection> GetIndicatorValueEntity(IOrganizationService orgService, string tableName, Entity dr, Guid recordUploaded, Dictionary<string, AttrJson> configEntityAttr, string entityUniqueId)
        {
            ConcurrentDictionary<string, EntityCollection> concurrentDictionary = new ConcurrentDictionary<string, EntityCollection>();
            foreach (KeyValuePair<string, AttrJson> item in configEntityAttr)
            {
                if (!string.IsNullOrEmpty(item.Value.IRelationName))
                {
                    string key = item.Value.IRelationName.Split(':')[0].Trim();
                    concurrentDictionary.AddOrUpdate(key, new EntityCollection(), (string oldkey, EntityCollection oldvalue) => new EntityCollection());
                }
            }
            try
            {
                foreach (KeyValuePair<string, AttrJson> item2 in configEntityAttr)
                {
                    if (dr.Attributes.ContainsKey("ddsm_" + item2.Key.ToLower()) && item2.Value.ColumnName.ToLower().IndexOf("guid") == -1 && item2.Value.ColumnName.ToLower().IndexOf("milestone") == -1 && !string.IsNullOrEmpty(Convert.ToString(dr["ddsm_" + item2.Key.ToLower()])) && !string.IsNullOrEmpty(item2.Value.AttrType) && !string.IsNullOrEmpty(item2.Value.IEntity))
                    {
                        Entity entity = new Entity(item2.Value.IEntity);
                        if (item2.Value.AttrType.ToLower() != "lookup")
                        {
                            dynamic attributeValue = GetAttributeValue(orgService, item2.Value.IEntity.ToLower(), item2.Value.IFieldLogicalName.ToLower(), item2.Value.AttrType, dr["ddsm_" + item2.Key.ToLower()], item2.Value.IFieldTargetEntity.ToLower(), item2.Value.IndicatorId);
                            if (attributeValue != null)
                            {
                                string text = item2.Value.IRelationName.Split(':')[1];
                                if (!string.IsNullOrEmpty(text))
                                {
                                    entity[text] = new EntityReference("ddsm_indicator", new Guid(item2.Value.IndicatorId));
                                }
                                entity[item2.Value.IFieldLogicalName.ToLower()] = (object)attributeValue;
                                entity[item2.Value.IRelationName.Split(':')[2]] = item2.Value.IName.Trim();
                            }
                            else
                            {
                                LogService.AddMessage(SheetName + ":  Excel Unique GUID: '" + entity + "'; Excel column: '" + item2.Key + "' / Header: '" + item2.Value.ColumnName + "' / Value: '" + dr["ddsm_" + item2.Key.ToLower()] + "' - " + Errors.GetTextError(380016), Enums.MessageType.ERROR, Enums.LogType.LogDataIssues, onlySocket: false);
                            }
                        }
                        else
                        {
                            dynamic attributeValue2 = GetAttributeValue(orgService, item2.Value.IEntity.ToLower(), item2.Value.IFieldLogicalName.ToLower(), item2.Value.AttrType, dr["ddsm_" + item2.Key.ToLower()], item2.Value.IFieldTargetEntity.ToLower(), item2.Value.IndicatorId);
                            if (attributeValue2 != null)
                            {
                                string text2 = item2.Value.IRelationName.Split(':')[1];
                                if (!string.IsNullOrEmpty(text2))
                                {
                                    entity[text2] = new EntityReference("ddsm_indicator", new Guid(item2.Value.IndicatorId));
                                }
                                entity[item2.Value.IFieldLogicalName.ToLower()] = (object)attributeValue2;
                                entity[item2.Value.IRelationName.Split(':')[2]] = item2.Value.IName.Trim();
                            }
                            else
                            {
                                LogService.AddMessage(SheetName + ":  Excel Unique GUID: '" + entity + "'; Excel column: '" + item2.Key + "' / Header: '" + item2.Value.ColumnName + "' / Value: '" + dr["ddsm_" + item2.Key.ToLower()] + "' / lookupEntity: '" + item2.Value.IFieldTargetEntity + "' - " + Errors.GetTextError(380015), Enums.MessageType.ERROR, Enums.LogType.LogDataIssues, onlySocket: false);
                            }
                        }
                        if (entity.Attributes.Count > 0)
                        {
                            if (item2.Value.IRelationName.Split(':')[3].ToLower() == "true")
                            {
                                entity["transactioncurrencyid"] = UploaderSettings.CurrencyGuid;
                            }
                            if (item2.Value.IRelationName.Split(':')[4].ToLower() == "true")
                            {
                                entity["ddsm_creatorrecordtype"] = new OptionSetValue(962080001);
                            }
                            string key2 = item2.Value.IRelationName.Split(':')[0];
                            EntityCollection eCollection = concurrentDictionary[key2];
                            eCollection.Entities.Add(entity);
                            eCollection.EntityName = entity.LogicalName;
                            concurrentDictionary.AddOrUpdate(key2, eCollection, (string oldkey, EntityCollection oldvalue) => eCollection);
                        }
                    }
                }
            }
            catch (Exception)
            {
                return new ConcurrentDictionary<string, EntityCollection>();
            }
            return concurrentDictionary;
        }

        private dynamic GetAttributeValue(IOrganizationService orgService, string entityName, string attrName, string attrType, object attrValue, string lookupEntity, string indicatorId)
        {
            try
            {
                string text = string.Empty;
                switch (attrType)
                {
                    case "Picklist":
                    case "Lookup":
                    case "Boolean":
                        text = ((string.IsNullOrEmpty(indicatorId) || string.IsNullOrEmpty(lookupEntity) || !(attrType == "Picklist")) ? (entityName + attrName + attrType + attrValue.ToString() + lookupEntity) : (entityName + attrName + attrType + attrValue.ToString() + lookupEntity + indicatorId));
                        if (CacheService.ContainsKey(text))
                        {
                            return CacheService.Get(text);
                        }
                        break;
                }
                switch (attrType)
                {
                    case "String":
                        return Convert.ToString(attrValue);
                    case "Memo":
                        return Convert.ToString(attrValue);
                    case "Picklist":
                        if (!string.IsNullOrEmpty(indicatorId) && !string.IsNullOrEmpty(lookupEntity))
                        {
                            QueryExpression queryExpression = new QueryExpression();
                            queryExpression.EntityName = lookupEntity;
                            queryExpression.ColumnSet = new ColumnSet(lookupEntity + "id");
                            QueryExpression queryExpression2 = queryExpression;
                            queryExpression2.Criteria.AddCondition("ddsm_name", ConditionOperator.Equal, attrValue.ToString());
                            queryExpression2.Criteria.AddCondition("ddsm_indicator", ConditionOperator.Equal, new Guid(indicatorId));
                            EntityCollection entityCollection = orgService.RetrieveMultiple(queryExpression2);
                            if (entityCollection != null && entityCollection.Entities.Count == 1)
                            {
                                CacheService.AddOrUpdate(text, new EntityReference(lookupEntity, entityCollection.Entities[0].GetAttributeValue<Guid>(lookupEntity + "id")));
                                return new EntityReference(lookupEntity, entityCollection.Entities[0].GetAttributeValue<Guid>(lookupEntity + "id"));
                            }
                            CacheService.AddOrUpdate(text, null);
                        }
                        else
                        {
                            RetrieveAttributeRequest request = new RetrieveAttributeRequest
                            {
                                EntityLogicalName = entityName,
                                LogicalName = attrName,
                                RetrieveAsIfPublished = true
                            };
                            RetrieveAttributeResponse retrieveAttributeResponse = (RetrieveAttributeResponse)orgService.Execute(request);
                            PicklistAttributeMetadata picklistAttributeMetadata = (PicklistAttributeMetadata)retrieveAttributeResponse.AttributeMetadata;
                            OptionMetadata[] array = picklistAttributeMetadata.OptionSet.Options.ToArray();
                            int num = 0;
                            OptionMetadata[] array2 = array;
                            foreach (OptionMetadata optionMetadata in array2)
                            {
                                if (DataUploader.IsNumeric(attrValue.ToString()))
                                {
                                    if (optionMetadata.Value.Value == Convert.ToInt32(attrValue.ToString()))
                                    {
                                        num = optionMetadata.Value.Value;
                                        break;
                                    }
                                }
                                else if (optionMetadata.Label.LocalizedLabels[0].Label.ToString().ToLower() == attrValue.ToString().ToLower())
                                {
                                    num = optionMetadata.Value.Value;
                                    break;
                                }
                            }
                            if (num != 0)
                            {
                                if (!string.IsNullOrEmpty(text))
                                {
                                    CacheService.AddOrUpdate(text, new OptionSetValue(num));
                                }
                                return new OptionSetValue(num);
                            }
                            if (!string.IsNullOrEmpty(text))
                            {
                                CacheService.AddOrUpdate(text, null);
                            }
                        }
                        return null;
                    case "Lookup":
                        if (string.IsNullOrEmpty(attrValue.ToString()))
                        {
                            return null;
                        }
                        if (lookupEntity != string.Empty)
                        {
                            EntityReference lookupOverUniqueGuid = GetLookupOverUniqueGuid(orgService, attrValue.ToString(), lookupEntity);
                            if (lookupOverUniqueGuid != null)
                            {
                                if (!string.IsNullOrEmpty(text))
                                {
                                    CacheService.AddOrUpdate(text, lookupOverUniqueGuid);
                                }
                                return lookupOverUniqueGuid;
                            }
                            string empty = string.Empty;
                            string empty2 = string.Empty;
                            if (lookupEntity.IndexOf("ddsm_") != -1)
                            {
                                empty = "ddsm_name";
                                empty2 = lookupEntity + "id";
                            }
                            else if (lookupEntity == "systemuser" || lookupEntity == "contact" || lookupEntity == "lead")
                            {
                                empty = "fullname";
                                empty2 = lookupEntity + "id";
                            }
                            else if (lookupEntity == "task" || lookupEntity == "appoinment")
                            {
                                empty = "subject";
                                empty2 = "activityid";
                            }
                            else if (lookupEntity == "annotation")
                            {
                                empty = "subject";
                                empty2 = lookupEntity + "id";
                            }
                            else if (lookupEntity == "incident")
                            {
                                empty = "title";
                                empty2 = lookupEntity + "id";
                            }
                            else
                            {
                                empty = "name";
                                empty2 = lookupEntity + "id";
                            }
                            try
                            {
                                Guid guid = Guid.Parse(attrValue.ToString());
                                EntityReference lookupOverRecordGuidOrName = GetLookupOverRecordGuidOrName(orgService, empty, empty2, attrValue.ToString(), lookupEntity);
                                if (lookupOverRecordGuidOrName != null)
                                {
                                    if (!string.IsNullOrEmpty(text))
                                    {
                                        CacheService.AddOrUpdate(text, lookupOverRecordGuidOrName);
                                    }
                                    return lookupOverRecordGuidOrName;
                                }
                            }
                            catch (ArgumentNullException)
                            {
                            }
                            catch (FormatException)
                            {
                            }
                            EntityReference lookupOverRecordGuidOrName2 = GetLookupOverRecordGuidOrName(orgService, "ddsm_primerykey", empty2, attrValue.ToString(), lookupEntity, isGuid: false);
                            if (lookupOverRecordGuidOrName2 != null)
                            {
                                if (!string.IsNullOrEmpty(text))
                                {
                                    CacheService.AddOrUpdate(text, lookupOverRecordGuidOrName2);
                                }
                                return lookupOverRecordGuidOrName2;
                            }
                            EntityReference lookupOverRecordGuidOrName3 = GetLookupOverRecordGuidOrName(orgService, empty, empty2, attrValue.ToString(), lookupEntity, isGuid: false);
                            if (lookupOverRecordGuidOrName3 != null)
                            {
                                if (!string.IsNullOrEmpty(text))
                                {
                                    CacheService.AddOrUpdate(text, lookupOverRecordGuidOrName3);
                                }
                                return lookupOverRecordGuidOrName3;
                            }
                            if (!string.IsNullOrEmpty(text))
                            {
                                CacheService.AddOrUpdate(text, null);
                            }
                            return null;
                        }
                        if (!string.IsNullOrEmpty(text))
                        {
                            CacheService.AddOrUpdate(text, null);
                        }
                        return null;
                    case "Decimal":
                        if (DataUploader.IsNumeric(attrValue))
                        {
                            return Convert.ToDecimal(attrValue);
                        }
                        return null;
                    case "Double":
                        if (DataUploader.IsNumeric(attrValue))
                        {
                            return Convert.ToDouble(attrValue);
                        }
                        return null;
                    case "Money":
                        if (DataUploader.IsNumeric(attrValue))
                        {
                            return new Money(Convert.ToDecimal(attrValue));
                        }
                        return null;
                    case "Boolean":
                        {
                            if (attrValue.ToString().ToLower() == "true")
                            {
                                return true;
                            }
                            if (attrValue.ToString().ToLower() == "false")
                            {
                                return false;
                            }
                            RetrieveAttributeRequest request2 = new RetrieveAttributeRequest
                            {
                                EntityLogicalName = entityName,
                                LogicalName = attrName,
                                RetrieveAsIfPublished = true
                            };
                            RetrieveAttributeResponse retrieveAttributeResponse2 = (RetrieveAttributeResponse)orgService.Execute(request2);
                            BooleanAttributeMetadata booleanAttributeMetadata = (BooleanAttributeMetadata)retrieveAttributeResponse2.AttributeMetadata;
                            if (booleanAttributeMetadata.OptionSet.FalseOption.Label.LocalizedLabels[0].Label.ToString().ToLower() == attrValue.ToString().ToLower())
                            {
                                if (!string.IsNullOrEmpty(text))
                                {
                                    CacheService.AddOrUpdate(text, false);
                                }
                                return false;
                            }
                            if (booleanAttributeMetadata.OptionSet.TrueOption.Label.LocalizedLabels[0].Label.ToString().ToLower() == attrValue.ToString().ToLower())
                            {
                                if (!string.IsNullOrEmpty(text))
                                {
                                    CacheService.AddOrUpdate(text, true);
                                }
                                return true;
                            }
                            if (!string.IsNullOrEmpty(text))
                            {
                                CacheService.AddOrUpdate(text, booleanAttributeMetadata.DefaultValue.Value);
                            }
                            return booleanAttributeMetadata.DefaultValue.Value;
                        }
                    case "DateTime":
                        return DataUploader.ConverAttributToDateTimeUtc(attrValue, UploaderSettings.TimeZoneInfo);
                    case "Integer":
                        if (DataUploader.IsNumeric(attrValue))
                        {
                            return Convert.ToInt32(attrValue);
                        }
                        return null;
                    case "BigInt":
                        if (DataUploader.IsNumeric(attrValue))
                        {
                            return Convert.ToInt64(attrValue);
                        }
                        return null;
                    default:
                        return null;
                }
            }
            catch (Exception ex3)
            {
                LogService.AddMessage("Error > " + ex3.Message, Enums.MessageType.FATAL);
                return null;
            }
        }

        private EntityReference GetLookupOverUniqueGuid(IOrganizationService orgService, string attrValue, string lookupEntity)
        {
            try
            {
                if (attrValue.ToLower().IndexOf("guid_") != 0)
                {
                    return null;
                }
                ConcurrentDictionary<string, CreatedEntity> value = RecordEntities.FirstOrDefault((KeyValuePair<string, ConcurrentDictionary<string, CreatedEntity>> x) => x.Key == lookupEntity).Value;
                if (value != null && !value.ContainsKey(attrValue))
                {
                    GetDictionaryObjectbyGuid(orgService, attrValue, lookupEntity, value);
                }
                if (value?.ContainsKey(attrValue) ?? false)
                {
                    Guid entityGuid = value.FirstOrDefault((KeyValuePair<string, CreatedEntity> x) => x.Key == attrValue).Value.EntityGuid;
                    if (entityGuid != Guid.Empty)
                    {
                        return new EntityReference(lookupEntity, entityGuid);
                    }
                }
            }
            catch (Exception)
            {
            }
            return null;
        }

        private EntityReference GetLookupOverRecordGuidOrName(IOrganizationService orgService, string lokupNameField, string lokupIdField, string attrValue, string lookupEntity, bool isGuid = true)
        {
            try
            {
                if (!isGuid)
                {
                    QueryExpression queryExpression = new QueryExpression();
                    queryExpression.EntityName = lookupEntity;
                    queryExpression.ColumnSet = new ColumnSet(lokupNameField, lokupIdField);
                    QueryExpression queryExpression2 = queryExpression;
                    queryExpression2.Criteria.AddCondition(lokupNameField, ConditionOperator.Equal, attrValue);
                    queryExpression2.AddOrder("createdon", OrderType.Ascending);
                    EntityCollection entityCollection = orgService.RetrieveMultiple(queryExpression2);
                    if (entityCollection != null && entityCollection.Entities.Count > 0)
                    {
                        return new EntityReference(lookupEntity, new Guid(entityCollection.Entities[0].Attributes[lokupIdField].ToString()));
                    }
                }
                else
                {
                    QueryExpression queryExpression = new QueryExpression();
                    queryExpression.EntityName = lookupEntity;
                    queryExpression.ColumnSet = new ColumnSet(lokupNameField, lokupIdField);
                    QueryExpression queryExpression2 = queryExpression;
                    queryExpression2.Criteria.AddCondition(lokupIdField, ConditionOperator.Equal, new Guid(attrValue));
                    EntityCollection entityCollection = orgService.RetrieveMultiple(queryExpression2);
                    if (entityCollection != null && entityCollection.Entities.Count == 1)
                    {
                        return new EntityReference(lookupEntity, new Guid(entityCollection.Entities[0].Attributes[lokupIdField].ToString()));
                    }
                }
            }
            catch (Exception)
            {
            }
            return null;
        }

        private void GetDictionaryObjectbyGuid(IOrganizationService orgService, string uniqueGuid, string entityName, ConcurrentDictionary<string, CreatedEntity> dictionaryEntity)
        {
            if (!string.IsNullOrEmpty(entityName) && !string.IsNullOrEmpty(uniqueGuid))
            {
                string keyEntity = ConfigObject.FirstOrDefault((KeyValuePair<string, EntityJson> x) => x.Value.Name.ToLower() == entityName).Key;
                if (!string.IsNullOrEmpty(keyEntity))
                {
                    string key = ConfigObject[keyEntity].Attributes.FirstOrDefault((KeyValuePair<string, AttrJson> x) => x.Value.ColumnName.ToLower() == (keyEntity + " GUID").ToLower()).Key;
                    if (!string.IsNullOrEmpty(key))
                    {
                        string text = string.Empty;
                        if (!dictionaryEntity.ContainsKey(uniqueGuid) && keyEntity != null)
                        {
                            QueryExpression queryExpression = new QueryExpression();
                            queryExpression.EntityName = "ddsm_exceldata";
                            queryExpression.ColumnSet = new ColumnSet("ddsm_entityobjectdictionary");
                            queryExpression.Criteria = new FilterExpression(LogicalOperator.And);
                            queryExpression.Criteria.AddCondition(new ConditionExpression("subject", ConditionOperator.Equal, keyEntity));
                            queryExpression.Criteria.AddCondition(new ConditionExpression("ddsm_" + key.ToLower(), ConditionOperator.Equal, uniqueGuid));
                            queryExpression.Criteria.AddCondition(new ConditionExpression("ddsm_processed", ConditionOperator.Equal, true));
                            queryExpression.Criteria.AddCondition(new ConditionExpression("statecode", ConditionOperator.Equal, 1));
                            queryExpression.Criteria.AddCondition(new ConditionExpression("statuscode", ConditionOperator.Equal, 2));
                            queryExpression.Criteria.AddCondition(new ConditionExpression("ddsm_datauploader", ConditionOperator.Equal, UploaderSettings.TargetEntity.Id));
                            EntityCollection entityCollection = orgService.RetrieveMultiple(queryExpression);
                            if (entityCollection != null && entityCollection.Entities.Count == 1 && entityCollection[0].Attributes.ContainsKey("ddsm_entityobjectdictionary"))
                            {
                                text = entityCollection[0].GetAttributeValue<string>("ddsm_entityobjectdictionary");
                            }
                            if (!string.IsNullOrEmpty(text))
                            {
                                ConcurrentDictionary<string, CreatedEntity> dictionaryObject = JsonConvert.DeserializeObject<ConcurrentDictionary<string, CreatedEntity>>(text);
                                if (dictionaryObject != null)
                                {
                                    dictionaryEntity.AddOrUpdate(uniqueGuid, dictionaryObject[uniqueGuid], (string oldkey, CreatedEntity oldvalue) => dictionaryObject[uniqueGuid]);
                                }
                            }
                        }
                    }
                }
            }
        }

        public void Dispose()
        {
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    if (SkippedEntities != null)
                    {
                        SkippedEntities?.Clear();
                    }
                    if (StatisticsRecords != null)
                    {
                        StatisticsRecords?.Clear();
                    }
                    if (EntityCollectionPagination != null)
                    {
                        EntityCollectionPagination = null;
                    }
                    if (StatisticRecords != null)
                    {
                        StatisticRecords = null;
                    }
                    if (ConfigObject != null)
                    {
                        ConfigObject?.Clear();
                    }
                    CacheService.Dispose();
                    LogService.Dispose();
                }
                _disposed = true;
            }
        }

    }
}
