﻿using CoreUtils.DataUploader.Dto;
using CoreUtils.Dto;
using CoreUtils.Service.Implementation;
using CoreUtils.Wrap;
using DataUploader;
using DataUploader.Creator;
using DataUploader.Extentions;
using DataUploader.Helper;
using MappingImplementation.Service.Implementation;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using Microsoft.Xrm.Sdk.Metadata.Query;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataUploader.Creator
{
    public class CreateRecords : BaseMethod
    {
        private ConcurrentDictionary<string, CreatedEntity> _requestEntities;

        private CreateUpdate _createUpdate;

        private GetData _getData;

        private int _totalRecordCount;

        private readonly int _limitItemColletcion;

        private string _msRsName;

        private static Dictionary<string, int> _msColumnList;

        private string _fnlcRsName;

        public CreateRecords(ConcurrentDictionary<string, ConcurrentDictionary<string, CreatedEntity>> recordEntities, DataUploaderSettings uploaderSettings)
        {
            RecordEntities = recordEntities;
            UploaderSettings = uploaderSettings;
            Implementation = null;
            ImplementationLookupFields = null;
            AutoNumbering = null;
            AutoNumberingChild = null;
            GenerationName = null;
            GenerationNameChild = null;
            LogService = new LogService(UploaderSettings);
            ConfigObject = JsonConvert.DeserializeObject<ConcurrentDictionary<string, EntityJson>>(UploaderSettings.JsonObjects);
            IsCurrencyField = false;
            IsCreatorField = false;
            IsPrimarykeyField = false;
            IsAutoNumberingField = false;
            SheetName = string.Empty;
            PrimaryAttributeName = string.Empty;
            CacheService = new CacheService();
            _limitItemColletcion = UploaderSettings.GlobalRequestsCount;
            _totalRecordCount = 0;
            _msColumnList = new Dictionary<string, int>();
        }

        public bool CreateRecord(IOrganizationService orgService, string logicalName)
        {
            SheetName = ConfigObject.FirstOrDefault((KeyValuePair<string, EntityJson> x) => x.Value.Name.ToLower() == logicalName).Key;
            if (string.IsNullOrEmpty(SheetName))
            {
                return true;
            }
            try
            {
                if (UploaderSettings.MappingImpl.ContainsKey(logicalName) && !string.IsNullOrEmpty(UploaderSettings.MappingImpl[logicalName]))
                {
                    try
                    {
                        Guid guid = Guid.Parse(UploaderSettings.MappingImpl[logicalName]);
                        MappingImplementationDto mappingImplementationDto = new MappingImplementationDto
                        {
                            OrgService = orgService,
                            CreatorRecordType = MappingImplementationDto.RecordCreator.DataUploader,
                            CurrentUserId = UploaderSettings.UserGuid
                        };
                        Implementation = new MappingImplementationService(mappingImplementationDto);
                        ImplementationLookupFields = Implementation.GetImplementationLookup(new Entity(logicalName), new Guid(UploaderSettings.MappingImpl[logicalName]));
                    }
                    catch (ArgumentNullException x)
                    {
                        Console.WriteLine("Message: " + x.Message);
                        LogService.AddMessage("Message: " + x.Message + " - " + x.InnerException, Enums.MessageType.FATAL);
                    }
                    catch (FormatException y)
                    {
                        Console.WriteLine("Message: " + y.Message);
                        LogService.AddMessage("Message: " + y.Message + " - " + y.InnerException, Enums.MessageType.FATAL);
                    }
                    catch (Exception y)
                    {
                        Console.WriteLine("Message: " + y.Message);
                        LogService.AddMessage("Message: " + y.Message + " - " + y.InnerException, Enums.MessageType.FATAL);
                    }
                }
                string key = ConfigObject[SheetName].Attributes.FirstOrDefault((KeyValuePair<string, AttrJson> x) => x.Value.ColumnName.ToLower() == (SheetName + " GUID").ToLower()).Key;
                if (string.IsNullOrEmpty(key))
                {
                    LogService.AddMessage("Column 'Sheet Name + GUID' Not found in sheet " + SheetName, Enums.MessageType.ERROR, Enums.LogType.Log, onlySocket: false);
                    LogService.SaveLog(orgService);
                    return false;
                }
                _getData = new GetData(orgService, LogService);
                List<string> dmnRules = UploaderSettings.DmnRules.ContainsKey(logicalName) ? UploaderSettings.DmnRules[logicalName] : new List<string>();
                _createUpdate = new CreateUpdate(orgService, UploaderSettings.UserGuid, SheetName, logicalName, dmnRules, UploaderSettings.CallExecuteMultiple);
                IsCurrencyField = DataUploader.DoesFieldExist(orgService, logicalName, "transactioncurrencyid");
                IsCreatorField = DataUploader.DoesFieldExist(orgService, logicalName, "ddsm_creatorrecordtype");
                IsAutoNumberingField = DataUploader.DoesFieldExist(orgService, logicalName, "ddsm_autonumbering");
                IsPrimarykeyField = DataUploader.DoesFieldExist(orgService, logicalName, "ddsm_primarykey");
                PrimaryAttributeName = GetPrimaryAttributeName(orgService, logicalName);
                _totalRecordCount = GetCountAllRecords(orgService, UploaderSettings.TargetEntity.Id, logicalName);
                GenerationName = new GenerationNameService(orgService, logicalName, PrimaryAttributeName);
                _msRsName = GetRelationship(orgService, "ddsm_milestone", logicalName)?.SchemaName;
                _msColumnList = (from x in ConfigObject[SheetName].Attributes
                                 where x.Value.ColumnName.ToLower().Contains("milestone")
                                 select x).ToDictionary((KeyValuePair<string, AttrJson> x) => x.Key, (KeyValuePair<string, AttrJson> x) => ConvertColumnToMsIndex(x.Value.ColumnName));
                _fnlcRsName = GetRelationship(orgService, "ddsm_financial", logicalName)?.SchemaName;
                LogService.AddMessage(Errors.GetTextError(380030) + SheetName + Errors.GetTextError(380031));
                StatisticRecords = new StatisticRecords();
                StatisticsRecords.AddOrUpdate("stat", StatisticRecords, (string oldkey, StatisticRecords oldvalue) => StatisticRecords);
                EntityCollectionPagination = new PaginationRetriveMultiple();
                do
                {
                    EntityCollectionPagination = (PaginationRetriveMultiple)_getData.GetDataByEntityName(logicalName, ConfigObject, UploaderSettings.TargetEntity.Id, false, EntityCollectionPagination.PageNumber, EntityCollectionPagination.PagingCookie, EntityCollectionPagination.MoreRecords, UploaderSettings.GlobalPageRecordsCount);
                    if (EntityCollectionPagination != null && EntityCollectionPagination.RetrieveCollection.Entities.Count > 0)
                    {
                        if (IsAutoNumberingField)
                        {
                            if (logicalName != "ddsm_financial")
                            {
                                AutoNumbering = new AutoNumberingService(orgService, logicalName, EntityCollectionPagination.RetrieveCollection.Entities.Count);
                            }
                            else
                            {
                                string key2 = ConfigObject[SheetName].Attributes.FirstOrDefault((KeyValuePair<string, AttrJson> x) => x.Value.AttrLogicalName.ToLower() == "ddsm_parentproject").Key;
                                string key3 = ConfigObject[SheetName].Attributes.FirstOrDefault((KeyValuePair<string, AttrJson> x) => x.Value.AttrLogicalName.ToLower() == "ddsm_projectgroup").Key;
                                if (!string.IsNullOrEmpty(key2))
                                {
                                    AutoNumbering = new AutoNumberingService(orgService, "ddsm_financial", EntityCollectionPagination.RetrieveCollection.Entities.Count, "ddsm_parentproject");
                                }
                                else if (!string.IsNullOrEmpty(key3))
                                {
                                    AutoNumbering = new AutoNumberingService(orgService, "ddsm_financial", EntityCollectionPagination.RetrieveCollection.Entities.Count, "ddsm_projectgroup");
                                }
                                else
                                {
                                    AutoNumbering = new AutoNumberingService(orgService, logicalName, EntityCollectionPagination.RetrieveCollection.Entities.Count);
                                }
                            }
                            if (logicalName == "ddsm_project")
                            {
                                AutoNumberingChild = new AutoNumberingService(orgService, "ddsm_financial", 1, "ddsm_parentproject");
                                GenerationNameChild = new GenerationNameService(orgService, "ddsm_financial", "ddsm_name");
                            }
                            if (logicalName == "ddsm_projectgroup")
                            {
                                AutoNumberingChild = new AutoNumberingService(orgService, "ddsm_financial", 1, "ddsm_projectgroup");
                                GenerationNameChild = new GenerationNameService(orgService, "ddsm_financial", "ddsm_name");
                            }
                        }
                        StatisticsRecords["stat"].All += EntityCollectionPagination.RetrieveCollection.Entities.Count;
                        try
                        {

                            CreateRecordSet(orgService, logicalName, EntityCollectionPagination.RetrieveCollection, StatisticsRecords["stat"]);
                        }

                        catch (Exception y)
                        {
                            Console.WriteLine("Message: " + y.Message);
                            LogService.AddMessage("Message: " + y.Message + " - " + y.InnerException, Enums.MessageType.FATAL);
                        }
                    
                    }
                }
                while (EntityCollectionPagination != null && EntityCollectionPagination.MoreRecords);
                Task.WaitAll(_createUpdate.dmnTasks.ToArray(), -1);
                if (StatisticsRecords["stat"].All > 0)
                {
                    LogService.AddMessage(SheetName + ": " + Errors.GetTextError(380019) + StatisticsRecords["stat"].All + "; " + Errors.GetTextError(380020) + StatisticsRecords["stat"].Created + "; " + Errors.GetTextError(380021) + StatisticsRecords["stat"].Updated + "; " + Errors.GetTextError(380022) + StatisticsRecords["stat"].DoNothing + ";", Enums.MessageType.SUCCESS, Enums.LogType.Log, onlySocket: false);
                    LogService.SaveLog(orgService);
                }
                if (StatisticsRecords["stat"].All != StatisticsRecords["stat"].Created + StatisticsRecords["stat"].Updated + StatisticsRecords["stat"].DoNothing)
                {
                    return false;
                }
            }
            catch(Exception ex2)
            {
                LogService.AddMessage("Message: " + ex2.Message +" - " + ex2.InnerException, Enums.MessageType.FATAL);
            }

            return true;
        }

        private void CreateRecordSet(IOrganizationService orgService, string logicalName, EntityCollection table, StatisticRecords statisticRecords)
        {
            ConcurrentDictionary<string, CreatedEntity> currentEntities = RecordEntities.FirstOrDefault((KeyValuePair<string, ConcurrentDictionary<string, CreatedEntity>> x) => x.Key == logicalName).Value;
            string entityUniqueId = string.Empty;
            long num = 0L;
            long num2 = 1L;
            int num3 = 0;
            string key = ConfigObject[SheetName].Attributes.FirstOrDefault((KeyValuePair<string, AttrJson> x) => x.Value.ColumnName.ToLower() == (SheetName + " GUID").ToLower()).Key;
            if (!string.IsNullOrEmpty(key))
            {
                ExecuteMultipleRequest executeMultipleRequest = new ExecuteMultipleRequest
                {
                    Requests = new OrganizationRequestCollection(),
                    Settings = new ExecuteMultipleSettings
                    {
                        ContinueOnError = true,
                        ReturnResponses = true
                    }
                };
                List<string> list = new List<string>();
                foreach (Entity entity4 in table.Entities)
                {
                    num3++;
                    if (num3 >= 1)
                    {
                        try
                        {
                            entityUniqueId = string.Empty;
                            entityUniqueId = entity4["ddsm_" + key.ToLower()].ToString();
                            if (!string.IsNullOrEmpty(entityUniqueId))
                            {
                                if (!currentEntities.ContainsKey(entityUniqueId))
                                {
                                    CreatedEntity recEntity = new CreatedEntity();
                                    ConcurrentDictionary<string, AttrJson> attributes = ConfigObject[SheetName].Attributes;
                                    Dictionary<string, AttrJson> configEntityAttr = (from x in attributes
                                                                                     where x.Value.TypeField.ToLower() == "field"
                                                                                     select x).ToDictionary((KeyValuePair<string, AttrJson> x) => x.Key, (KeyValuePair<string, AttrJson> x) => x.Value);
                                    Entity fieldValueEntity = GetFieldValueEntity(orgService, SheetName, entity4, UploaderSettings.TargetEntity.Id, configEntityAttr, entityUniqueId);
                                    fieldValueEntity.LogicalName = logicalName;
                                    configEntityAttr = (from x in attributes
                                                        where x.Value.TypeField.ToLower() == "indicator"
                                                        select x).ToDictionary((KeyValuePair<string, AttrJson> x) => x.Key, (KeyValuePair<string, AttrJson> x) => x.Value);
                                    ConcurrentDictionary<string, EntityCollection> indicatorValueEntity = GetIndicatorValueEntity(orgService, SheetName, entity4, UploaderSettings.TargetEntity.Id, configEntityAttr, entityUniqueId);
                                    Guid guid = Guid.Empty;
                                    if (UploaderSettings.DeduplicationEntitiesList.Contains(logicalName) && UploaderSettings.DuplicateDetect)
                                    {
                                        guid = _getData.VerifyDuplicateRecord(fieldValueEntity, logicalName, SheetName, entityUniqueId);
                                    }
                                    if (guid != Guid.Empty)
                                    {
                                        recEntity.EntityGuid = guid;
                                        recEntity.SourceId = (Guid)entity4["activityid"];
                                        recEntity.Status = false;
                                        recEntity.StatusDb = false;
                                        currentEntities.AddOrUpdate(entityUniqueId, recEntity, (string oldkey, CreatedEntity oldvalue) => recEntity);
                                        switch (UploaderSettings.DedupRules)
                                        {
                                            case 962080001:
                                                currentEntities[entityUniqueId].Status = true;
                                                SkippedEntities.AddOrUpdate(entityUniqueId, currentEntities[entityUniqueId], (string oldkey, CreatedEntity oldvalue) => currentEntities[entityUniqueId]);
                                                statisticRecords.DoNothing++;
                                                break;
                                            case 962080000:
                                                {
                                                    Entity entity = new Entity(logicalName, guid);
                                                    foreach (KeyValuePair<string, object> attribute in fieldValueEntity.Attributes)
                                                    {
                                                        entity[attribute.Key] = fieldValueEntity.Attributes[attribute.Key];
                                                    }
                                                    UpdateRequest updateRequest = new UpdateRequest();
                                                    updateRequest.Target = entity;
                                                    executeMultipleRequest.Requests.Add(updateRequest);
                                                    list.Add(entityUniqueId);
                                                    if (indicatorValueEntity.Count <= 0)
                                                    {
                                                    }
                                                    break;
                                                }
                                        }
                                        num++;
                                    }
                                    else
                                    {
                                        if (AutoNumbering != null && !fieldValueEntity.Attributes.ContainsKey("ddsm_autonumbering"))
                                        {
                                            string number = AutoNumbering.GetNumber();
                                            if (!string.IsNullOrEmpty(number))
                                            {
                                                fieldValueEntity["ddsm_autonumbering"] = number;
                                            }
                                        }
                                        Entity entity2 = new Entity(logicalName);
                                        Entity entity3 = new Entity(logicalName);
                                        if (Implementation != null && ImplementationLookupFields != null)
                                        {
                                            string text = logicalName;
                                            for (int i = 0; i < ImplementationLookupFields.ImplementFields.Count; i++)
                                            {
                                                text += ImplementationLookupFields.ImplementFields[i].LookupLogicalName;
                                                if (fieldValueEntity.Attributes.ContainsKey(ImplementationLookupFields.ImplementFields[i].LookupLogicalName))
                                                {
                                                    entity3[ImplementationLookupFields.ImplementFields[i].LookupLogicalName] = fieldValueEntity[ImplementationLookupFields.ImplementFields[i].LookupLogicalName];
                                                    text += fieldValueEntity.GetAttributeValue<EntityReference>(ImplementationLookupFields.ImplementFields[i].LookupLogicalName).Id;
                                                }
                                            }
                                            if (entity3.Attributes.Count > 0)
                                            {
                                                try
                                                {
                                                    List<Entity> list2 = Implementation.ExecuteMapping(entity3, new Guid(UploaderSettings.MappingImpl[logicalName]));
                                                    if (list2[0].Attributes.Count > 0)
                                                    {
                                                        entity2.Attributes.AddRange(list2[0].Attributes);
                                                    }
                                                    if (list2[0].RelatedEntities.Count > 0)
                                                    {
                                                        entity2.RelatedEntities.AddRange(list2[0].RelatedEntities);
                                                    }
                                                }
                                                catch (Exception ex)
                                                {
                                                    LogService.AddMessage("Error > " + SheetName + ":  Excel Unique GUID: " + entityUniqueId + "; Entity LogicalName: " + logicalName + " ImpMapping Id: " + UploaderSettings.MappingImpl[logicalName] + "; Message: " + ex.Message, Enums.MessageType.FATAL);
                                                }
                                            }
                                        }
                                        foreach (KeyValuePair<string, EntityCollection> item in indicatorValueEntity)
                                        {
                                            if (item.Value.Entities.Count > 0)
                                            {
                                                Relationship key2 = new Relationship(item.Key);
                                                if (!entity2.RelatedEntities.ContainsKey(key2))
                                                {
                                                    entity2.RelatedEntities.Add(key2, item.Value);
                                                }
                                                else
                                                {
                                                    string text2 = "";
                                                    if (item.Value.Entities[0].Attributes.ContainsKey("ddsm_name"))
                                                    {
                                                        text2 = "ddsm_name";
                                                    }
                                                    else if (item.Value.Entities[0].Attributes.ContainsKey("subject"))
                                                    {
                                                        text2 = "subject";
                                                    }
                                                    if (!string.IsNullOrEmpty(text2))
                                                    {
                                                        for (int j = 0; j < item.Value.Entities.Count; j++)
                                                        {
                                                            bool flag = false;
                                                            for (int k = 0; k < entity2.RelatedEntities[key2].Entities.Count; k++)
                                                            {
                                                                if (entity2.RelatedEntities[key2].Entities[k].Attributes.ContainsKey(text2))
                                                                {
                                                                    string a = entity2.RelatedEntities[key2].Entities[k].GetAttributeValue<string>(text2).ToLower().Trim();
                                                                    string b = item.Value.Entities[j].GetAttributeValue<string>(text2).ToLower().Trim();
                                                                    if (string.Equals(a, b))
                                                                    {
                                                                        foreach (KeyValuePair<string, object> attribute2 in item.Value.Entities[j].Attributes)
                                                                        {
                                                                            entity2.RelatedEntities[key2].Entities[k][attribute2.Key] = item.Value.Entities[j].Attributes[attribute2.Key];
                                                                        }
                                                                        flag = true;
                                                                        break;
                                                                    }
                                                                }
                                                            }
                                                            if (!flag)
                                                            {
                                                                entity2.RelatedEntities[key2].Entities.Add(item.Value.Entities[j]);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        if (!string.IsNullOrEmpty(PrimaryAttributeName) && !fieldValueEntity.Attributes.ContainsKey(PrimaryAttributeName))
                                        {
                                            string name = GenerationName.GetName(fieldValueEntity, entity2);
                                            if (!string.IsNullOrEmpty(name))
                                            {
                                                fieldValueEntity[PrimaryAttributeName] = name;
                                            }
                                        }
                                        foreach (KeyValuePair<string, object> attribute3 in fieldValueEntity.Attributes)
                                        {
                                            entity2[attribute3.Key] = fieldValueEntity.Attributes[attribute3.Key];
                                        }
                                        if (IsCurrencyField)
                                        {
                                            entity2["transactioncurrencyid"] = UploaderSettings.CurrencyGuid;
                                        }
                                        if (IsCreatorField)
                                        {
                                            entity2["ddsm_creatorrecordtype"] = new OptionSetValue(962080001);
                                        }
                                        if (!string.IsNullOrEmpty(_msRsName))
                                        {
                                            Relationship key3 = new Relationship(_msRsName);
                                            if (entity2.RelatedEntities.ContainsKey(key3))
                                            {
                                                EntityCollection entityCollection = new EntityCollection();
                                                entityCollection.Entities.AddRange(entity2.RelatedEntities[key3].Entities);
                                                entity2.RelatedEntities.Remove(key3);
                                                entity2.RelatedEntities.Add(key3, UpdatedMilestones(entityCollection, entity4));
                                            }
                                        }
                                        if (!string.IsNullOrEmpty(_fnlcRsName))
                                        {
                                            Relationship key4 = new Relationship(_fnlcRsName);
                                            if (entity2.RelatedEntities.ContainsKey(key4))
                                            {
                                                foreach (Entity entity5 in entity2.RelatedEntities[key4].Entities)
                                                {
                                                    if (AutoNumberingChild != null && !entity5.Attributes.ContainsKey("ddsm_autonumbering"))
                                                    {
                                                        string number2 = AutoNumberingChild.GetNumber();
                                                        if (!string.IsNullOrEmpty(number2))
                                                        {
                                                            entity5["ddsm_autonumbering"] = number2;
                                                        }
                                                        string name2 = GenerationNameChild.GetName(entity5, entity5);
                                                        if (!string.IsNullOrEmpty(name2))
                                                        {
                                                            entity5["ddsm_name"] = name2;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        CreateRequest createRequest = new CreateRequest();
                                        createRequest.Target = entity2;
                                        executeMultipleRequest.Requests.Add(createRequest);
                                        list.Add(entityUniqueId);
                                        num++;
                                        recEntity.EntityGuid = default(Guid);
                                        recEntity.SourceId = (Guid)entity4["activityid"];
                                        recEntity.Status = false;
                                        recEntity.StatusDb = false;
                                        currentEntities.AddOrUpdate(entityUniqueId, recEntity, (string oldkey, CreatedEntity oldvalue) => recEntity);
                                    }
                                    goto IL_0eb0;
                                }
                                LogService.AddMessage(SheetName + ":  Excel Unique GUID: " + entityUniqueId + "; Not unique in sheet " + SheetName + " list", Enums.MessageType.ERROR, Enums.LogType.Log, onlySocket: false);
                                SetStateResponse setStateResponse = (SetStateResponse)orgService.Execute(new SetStateRequest
                                {
                                    EntityMoniker = new EntityReference("ddsm_exceldata", (Guid)entity4["activityid"]),
                                    State = new OptionSetValue(2),
                                    Status = new OptionSetValue(3)
                                });
                            }
                        }
                        catch (Exception ex2)
                        {
                            num++;
                            LogService.AddMessage("Error > " + SheetName + ":  Excel Unique GUID: " + entityUniqueId + "; Message: " + ex2.Message, Enums.MessageType.FATAL);
                            goto IL_0eb0;
                        }
                        continue;
                    }
                    goto IL_0eb0;
                    IL_0eb0:
                    if (num == num2 * _limitItemColletcion)
                    {
                        if (executeMultipleRequest.Requests.Count > 0)
                        {
                            _requestEntities = new ConcurrentDictionary<string, CreatedEntity>();
                            _createUpdate.CreateUpdateEntityRecords(executeMultipleRequest, list, currentEntities, _requestEntities, statisticRecords, LogService, _totalRecordCount);
                            if (_requestEntities.Count > 0)
                            {
                                _createUpdate.UpdateSourceRecords(_requestEntities, LogService);
                            }
                        }
                        if (SkippedEntities.Count > 0)
                        {
                            _createUpdate.UpdateSourceRecords(SkippedEntities, LogService);
                            SkippedEntities = new ConcurrentDictionary<string, CreatedEntity>();
                        }
                        num2++;
                        list = new List<string>();
                        executeMultipleRequest = new ExecuteMultipleRequest
                        {
                            Requests = new OrganizationRequestCollection(),
                            Settings = new ExecuteMultipleSettings
                            {
                                ContinueOnError = true,
                                ReturnResponses = true
                            }
                        };
                    }
                }
                if (list.Count > 0 && executeMultipleRequest.Requests.Count > 0)
                {
                    _requestEntities = new ConcurrentDictionary<string, CreatedEntity>();
                    _createUpdate.CreateUpdateEntityRecords(executeMultipleRequest, list, currentEntities, _requestEntities, statisticRecords, LogService, _totalRecordCount);
                    if (_requestEntities.Count > 0)
                    {
                        _createUpdate.UpdateSourceRecords(_requestEntities, LogService);
                    }
                }
                if (SkippedEntities.Count > 0)
                {
                    _createUpdate.UpdateSourceRecords(SkippedEntities, LogService);
                    SkippedEntities = new ConcurrentDictionary<string, CreatedEntity>();
                }
            }
        }

        private string GetPrimaryAttributeName(IOrganizationService orgService, string entityLogicalName)
        {
            string result = string.Empty;
            MetadataFilterExpression metadataFilterExpression = new MetadataFilterExpression(LogicalOperator.And);
            metadataFilterExpression.Conditions.Add(new MetadataConditionExpression("LogicalName", MetadataConditionOperator.Equals, entityLogicalName));
            MetadataPropertiesExpression metadataPropertiesExpression = new MetadataPropertiesExpression
            {
                AllProperties = false
            };
            metadataPropertiesExpression.PropertyNames.Add("PrimaryNameAttribute");
            EntityQueryExpression query = new EntityQueryExpression
            {
                Criteria = metadataFilterExpression,
                Properties = metadataPropertiesExpression
            };
            RetrieveMetadataChangesRequest request = new RetrieveMetadataChangesRequest
            {
                Query = query
            };
            RetrieveMetadataChangesResponse retrieveMetadataChangesResponse = (RetrieveMetadataChangesResponse)orgService.Execute(request);
            if (retrieveMetadataChangesResponse != null && retrieveMetadataChangesResponse.EntityMetadata != null && retrieveMetadataChangesResponse.EntityMetadata.Count == 1)
            {
                result = retrieveMetadataChangesResponse.EntityMetadata[0].PrimaryNameAttribute;
            }
            return result;
        }

        private OneToManyRelationshipMetadata GetRelationship(IOrganizationService orgService, string targetEntity, string referencedEntity)
        {
            try
            {
                RetrieveEntityRequest request = new RetrieveEntityRequest
                {
                    EntityFilters = EntityFilters.Relationships,
                    LogicalName = targetEntity
                };
                RetrieveEntityResponse retrieveEntityResponse = (RetrieveEntityResponse)orgService.Execute(request);
                OneToManyRelationshipMetadata[] manyToOneRelationships = retrieveEntityResponse.EntityMetadata.ManyToOneRelationships;
                List<OneToManyRelationshipMetadata> list = (from r in manyToOneRelationships
                                                            where r.ReferencedEntity == referencedEntity
                                                            select r).ToList();
                return (list.Count == 0) ? null : list[0];
            }
            catch (Exception)
            {
                return null;
            }
        }

        private int GetCountAllRecords(IOrganizationService orgService, Guid id, string logicalName)
        {
            int result = 0;
            try
            {
                string query = "\r\n                <fetch  distinct='false' mapping='logical' aggregate='true'>\r\n                <entity name = 'ddsm_exceldata'>\r\n                    <attribute name = 'activityid' alias = 'records' aggregate = 'count' />\r\n                    <filter type = 'and'>\r\n                        <condition attribute = 'ddsm_datauploader' operator= 'eq' value = '" + id.ToString() + "' />\r\n                        <condition attribute = 'ddsm_logicalname' operator= 'eq' value = '" + logicalName + "' />\r\n                    </filter >\r\n               </entity >\r\n               </fetch >";
                EntityCollection entityCollection = orgService.RetrieveMultiple(new FetchExpression(query));
                result = (int)((AliasedValue)entityCollection.Entities[0]["records"]).Value;
            }
            catch (Exception)
            {
            }
            return result;
        }

        private int ConvertColumnToMsIndex(string columnName)
        {
            int result = -1;
            try
            {
                string value = columnName.Remove(0, 9).Trim();
                result = Convert.ToInt32(value);
            }
            catch (Exception)
            {
            }
            return result;
        }

        private EntityCollection UpdatedMilestones(EntityCollection msc, Entity dr)
        {
            EntityCollection entityCollection = new EntityCollection(msc.Entities.ToList());
            string text = string.Empty;
            object obj = DataUploader.ConverAttributToDateTimeUtc("today", UploaderSettings.TimeZoneInfo);
            int num = -1;
            int currentIdx = 0;
            if (_msColumnList.Count > 0)
            {
                foreach (Entity entity in entityCollection.Entities)
                {
                    currentIdx++;
                    if (!entity.Attributes.ContainsKey("ddsm_duration") || entity["ddsm_duration"] == null)
                    {
                        entity["ddsm_duration"] = 0;
                    }
                    string key = _msColumnList.FirstOrDefault((KeyValuePair<string, int> x) => x.Value == currentIdx).Key;
                    if (!string.IsNullOrEmpty(key) && dr.Attributes.ContainsKey("ddsm_" + key.ToLower()))
                    {
                        string attributeValue = dr.GetAttributeValue<string>("ddsm_" + key.ToLower());
                        //entity["ddsm_actualend"] = (object)DataUploader.ConverAttributToDateTimeUtc(attributeValue, UploaderSettings.TimeZoneInfo);
                        entity["ddsm_actualend"] = (object)DataUploader.ConverAttributToDateTimeUtc("12-12-2018", UploaderSettings.TimeZoneInfo);
                        entity["ddsm_status"] = new OptionSetValue(962080002);
                        if (!string.IsNullOrEmpty(text))
                        {
                            obj = (entity["ddsm_actualstart"] = (object)DataUploader.ConverAttributToDateTimeUtc(text, UploaderSettings.TimeZoneInfo));
                        }
                        text = attributeValue;
                    }
                    else if (!string.IsNullOrEmpty(text))
                    {
                        obj = (entity["ddsm_actualstart"] = (object)DataUploader.ConverAttributToDateTimeUtc(text, UploaderSettings.TimeZoneInfo));
                        entity["ddsm_status"] = new OptionSetValue(962080001);
                        text = string.Empty;
                        num = currentIdx;
                    }
                }
            }
            using (IEnumerator<Entity> enumerator2 = entityCollection.Entities.GetEnumerator())
            {
                if (enumerator2.MoveNext())
                {
                    Entity current2 = enumerator2.Current;
                    if (num == -1 && current2.GetAttributeValue<OptionSetValue>("ddsm_status").Value == 962080000)
                    {
                        current2["ddsm_status"] = new OptionSetValue(962080001);
                    }
                }
            }
            return entityCollection;
        }


    }
}
