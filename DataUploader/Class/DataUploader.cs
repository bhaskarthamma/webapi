﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace DataUploader
{

    public class ParsedRecords
    {
        public Int64 RequestCount { get; set; } = 0;
        public Int64 ResponceCount { get; set; } = 0;
        public Int64 ErrorCount { get; set; } = 0;
        public Int64 ExcludedDataCount { get; set; } = 0;
    }

    public class DataUploader
    {
        public static string ParseAttributToDateTime(object inputDate)
        {
            try
            {
                if (inputDate.GetType().Name == "DateTime")
                {
                    return Convert.ToDateTime(inputDate).ToShortDateString();
                }
                if (inputDate.GetType().Name == "Double")
                {
                    return DateTime.FromOADate(Convert.ToDouble(inputDate)).ToShortDateString();
                }
                if (inputDate.GetType().Name == "String")
                {
                    if (inputDate.ToString() == "")
                    {
                        return null;
                    }
                    if (inputDate.ToString() == "today")
                    {
                        return DateTime.Now.ToShortDateString();
                    }
                    return Convert.ToDateTime(inputDate.ToString()).ToShortDateString();
                }
                return null;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static dynamic ConverAttributToDateTimeUtc(object xlsxDate)
        {
            try
            {
                if (xlsxDate.GetType().Name == "DateTime")
                {
                    return xlsxDate;
                }
                if (xlsxDate.GetType().Name == "Double")
                {
                    return DateTime.FromOADate(Convert.ToDouble(xlsxDate)).ToUniversalTime();
                }
                if (xlsxDate.GetType().Name == "String")
                {
                    if (xlsxDate.ToString() == "")
                    {
                        return null;
                    }
                    if (xlsxDate.ToString() == "today")
                    {
                        return DateTime.UtcNow;
                    }
                    return Convert.ToDateTime(xlsxDate.ToString()).ToUniversalTime();
                }
                return null;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static dynamic ConverAttributToDateTimeUtc(object inputDate, TimeZoneInfo timeZoneInfo)
        {
            try
            {
                if (inputDate.GetType().Name == "DateTime")
                {
                    return TimeZoneInfo.ConvertTime(DateTime.Parse(Convert.ToDateTime(inputDate).ToShortDateString()), timeZoneInfo);
                }
                if (inputDate.GetType().Name == "Double")
                {
                    return TimeZoneInfo.ConvertTime(DateTime.Parse(DateTime.FromOADate(Convert.ToDouble(inputDate)).ToShortDateString()), timeZoneInfo);
                }
                if (inputDate.GetType().Name == "String")
                {
                    if (inputDate.ToString() == "")
                    {
                        return null;
                    }
                    if (inputDate.ToString() == "today")
                    {
                        return TimeZoneInfo.ConvertTime(DateTime.Parse(DateTime.Now.ToShortDateString()), timeZoneInfo);
                    }
                    return TimeZoneInfo.ConvertTime(DateTime.Parse(Convert.ToDateTime(inputDate.ToString()).ToShortDateString()), timeZoneInfo);
                }
                return null;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static bool IsNumeric(object attrValue)
        {
            try
            {
                if (attrValue == null || attrValue is DateTime)
                {
                    throw new Exception("Value '" + attrValue.ToString() + "' is not a number");
                }
                if (attrValue is short || attrValue is int || attrValue is long || attrValue is decimal || attrValue is float || attrValue is double || attrValue is bool)
                {
                    return true;
                }
                if (!double.TryParse(Convert.ToString(attrValue, CultureInfo.InvariantCulture), NumberStyles.Any, NumberFormatInfo.InvariantInfo, out double _))
                {
                    throw new Exception("Value '" + attrValue.ToString() + "' is not a number");
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static void SaveLog(IOrganizationService orgService, Guid recordUploaded, List<string> logger, string fieldName, bool isAddLog = true)
        {
            string text = string.Empty;
            if (isAddLog)
            {
                QueryExpression queryExpression = new QueryExpression();
                queryExpression.EntityName = "ddsm_datauploader";
                queryExpression.ColumnSet = new ColumnSet(fieldName);
                QueryExpression queryExpression2 = queryExpression;
                queryExpression2.Criteria.AddCondition("ddsm_datauploaderid", ConditionOperator.Equal, recordUploaded);
                EntityCollection entityCollection = orgService.RetrieveMultiple(queryExpression2);
                if (entityCollection[0].Attributes.ContainsKey(fieldName))
                {
                    text = entityCollection[0].GetAttributeValue<string>(fieldName);
                }
            }
            Entity entity = new Entity("ddsm_datauploader", recordUploaded);
            text = ((!(text != string.Empty)) ? string.Join("\n", logger) : (text + "\n" + string.Join("\n", logger)));
            text = (string)(entity[fieldName] = text.Substring((text.Length > 1000000) ? (text.Length - 1000000) : 0, (text.Length > 1000000) ? 1000000 : text.Length));
            orgService.Update(entity);
        }

        public static void SetUploadStatus(IOrganizationService orgService, int status, Guid recordId)
        {
            try
            {
                Entity entity = new Entity("ddsm_datauploader", recordId);
                entity.Attributes.Add("ddsm_status", new OptionSetValue(status));
                orgService.Update(entity);
            }
            catch (Exception ex)
            {
            }
        }

        public static OptionMetadata[] GetUploadStatus(IOrganizationService orgService)
        {
            RetrieveAttributeRequest request = new RetrieveAttributeRequest
            {
                EntityLogicalName = "ddsm_datauploader",
                LogicalName = "ddsm_status",
                RetrieveAsIfPublished = true
            };
            RetrieveAttributeResponse retrieveAttributeResponse = (RetrieveAttributeResponse)orgService.Execute(request);
            PicklistAttributeMetadata picklistAttributeMetadata = (PicklistAttributeMetadata)retrieveAttributeResponse.AttributeMetadata;
            return picklistAttributeMetadata.OptionSet.Options.ToArray();
        }

        public static string GetTextUploadStatus(int optionValue, OptionMetadata[] uploadStatus)
        {
            string result = string.Empty;
            foreach (OptionMetadata optionMetadata in uploadStatus)
            {
                if (optionValue == optionMetadata.Value.Value)
                {
                    result = optionMetadata.Label.LocalizedLabels[0].Label.ToString();
                    break;
                }
            }
            return result;
        }

        public static bool DoesFieldExist(IOrganizationService orgService, string entityName, string fieldName)
        {
            RetrieveEntityRequest request = new RetrieveEntityRequest
            {
                EntityFilters = EntityFilters.Attributes,
                LogicalName = entityName
            };
            RetrieveEntityResponse retrieveEntityResponse = (RetrieveEntityResponse)orgService.Execute(request);
            return retrieveEntityResponse.EntityMetadata.Attributes.FirstOrDefault((AttributeMetadata element) => element.LogicalName == fieldName) != null;
        }

        private static Guid GetSystemUserId(string name, IOrganizationService orgService)
        {
            QueryByAttribute queryByAttribute = new QueryByAttribute();
            queryByAttribute.EntityName = "systemuser";
            queryByAttribute.ColumnSet = new ColumnSet("systemuserid");
            QueryByAttribute queryByAttribute2 = queryByAttribute;
            queryByAttribute2.AddAttributeValue("fullname", name);
            EntityCollection entityCollection = orgService.RetrieveMultiple(queryByAttribute2);
            return entityCollection.Entities[0].Id;
        }

        public static IOrganizationService GetSystemOrgService(IOrganizationServiceFactory serviceFactory, IOrganizationService orgService)
        {
            return serviceFactory.CreateOrganizationService(GetSystemUserId("SYSTEM", orgService));
        }

    }
}
