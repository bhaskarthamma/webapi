﻿using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using DocumentFormat.OpenXml.Spreadsheet;
using Excel;
using DocumentFormat.OpenXml.Packaging;
using static DataUploader.Helper.Errors;
using DataUploader.Helper;
using JsonConvert = CoreUtils.Wrap.JsonConvert;
using EntityJson = CoreUtils.DataUploader.Dto.EntityJson;
using DataUploader.Extentions;

namespace DataUploader.Parser
{
    public class ParseExcel: IDisposable
    {
        private readonly IOrganizationService _orgService;

        private readonly DataUploaderSettings _thisSettings;

        private readonly MemoryStream _msFile;

        private readonly ConcurrentDictionary<string, EntityJson> _configObject;

        private long _amountParsingErrors;

        private readonly LogService _logService;

        private List<Task> _tasks;

        private bool _disposed = false;

        public ParseExcel(IOrganizationService orgService, MemoryStream msFile, DataUploaderSettings thisSettings)
        {
            _orgService = orgService;
            _msFile = msFile;
            _thisSettings = thisSettings;
            _configObject = JsonConvert.DeserializeObject<ConcurrentDictionary<string, EntityJson>>(_thisSettings.JsonObjects);
            _amountParsingErrors = 0L;
            _logService = new LogService(_thisSettings);
        }

        public bool ParseXls()
        {
            List<OutputReadAsDt> listOutputReadAsDt = new List<OutputReadAsDt>();
            IExcelDataReader excelDataReader = ExcelReaderFactory.CreateBinaryReader(_msFile);
            DataSet dataSet = excelDataReader.AsDataSet();
            excelDataReader.Close();
            _logService.AddMessage(Errors.GetTextError(380008));
            _tasks = new List<Task>();
            foreach (DataTable table in dataSet.Tables)
            {
                DataRow[] source = (from row in table.AsEnumerable()
                                    where row["A"] != null && string.Compare(Convert.ToString(row["A"].ToString().Trim()), string.Empty) !=0
                                    select row).ToArray();
                DataTable dataTable2 = source.CopyToDataTable();
                dataTable2.TableName = table.TableName;
                if (dataTable2.Rows.Count != 0 && _configObject[dataTable2.TableName] != null && !string.IsNullOrEmpty(_configObject[dataTable2.TableName].Name))
                {
                    _logService.AddMessage(Errors.GetTextError(380004) + dataTable2.TableName);
                    ReadAsDataTable readAsDataTable = new ReadAsDataTable(_orgService, _thisSettings, _configObject, dataTable2, dataTable2.TableName);
                    Task item = Task.Factory.StartNew(delegate
                    {
                        OutputReadAsDt outputReadAsDt = readAsDataTable.Xls();
                        _amountParsingErrors += outputReadAsDt.TableParsingErrors;
                        listOutputReadAsDt.Add(outputReadAsDt);
                    });
                    _tasks.Add(item);
                }
            }
            Task.WaitAll(_tasks.ToArray(), -1);
            for (int i = 0; i < listOutputReadAsDt.Count; i++)
            {
                listOutputReadAsDt[i].LogService.SaveLog(_orgService);
                listOutputReadAsDt[i].LogService.Dispose();
            }
            _logService.AddMessage(Errors.GetTextError(380009));
            _logService.SaveLog(_orgService);
            dataSet.Tables.Clear();
            dataSet.Dispose();
            if (_amountParsingErrors == 0)
            {
                return false;
            }
            return true;
        }

        public bool ParseXlsx()
        {
            List<OutputReadAsDt> listOutputReadAsDt = new List<OutputReadAsDt>();
            using (SpreadsheetDocument spreadsheetDocument = SpreadsheetDocument.Open(_msFile, isEditable: true))
            {
                WorkbookPart workbookPart = spreadsheetDocument.WorkbookPart;
                IEnumerable<Sheet> source = spreadsheetDocument.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>();
                int num = 0;
                try
                {
                    num = source.Count();
                }
                catch
                {
                    num = 0;
                }
                if (num == 0)
                {
                    return false;
                }
                _logService.AddMessage(Errors.GetTextError(380008));
                foreach (EntityJson value in _configObject.Values)
                {
                    if (!string.IsNullOrEmpty(value.Name))
                    {
                    }
                    string sheetName = _configObject.FirstOrDefault((KeyValuePair<string, EntityJson> x) => x.Value.Name.ToLower() == value.Name.ToLower()).Key;
                    Sheet sheet = (from s in source
                                   where (string)s.Name == sheetName
                                   select s).FirstOrDefault();
                    WorksheetPart worksheetPart = (WorksheetPart)spreadsheetDocument.WorkbookPart.GetPartById(sheet.Id.Value);
                    _logService.AddMessage(Errors.GetTextError(380004) + sheetName);
                    ReadAsDataTable readAsDataTable = new ReadAsDataTable(_orgService, _thisSettings, _configObject, spreadsheetDocument, worksheetPart, sheetName);
                    Task item = Task.Factory.StartNew(delegate
                    {
                        OutputReadAsDt outputReadAsDt = readAsDataTable.Xlsx();
                        _amountParsingErrors += outputReadAsDt.TableParsingErrors;
                        listOutputReadAsDt.Add(outputReadAsDt);
                    });
                    _tasks.Add(item);
                }
            }
            Task.WaitAll(_tasks.ToArray(), -1);
            for (int i = 0; i < listOutputReadAsDt.Count; i++)
            {
                listOutputReadAsDt[i].LogService.SaveLog(_orgService);
                listOutputReadAsDt[i].LogService.Dispose();
            }
            _logService.AddMessage(Errors.GetTextError(380009));
            _logService.SaveLog(_orgService);
            if (_amountParsingErrors == 0)
            {
                return false;
            }
            return true;
        }

        public void Dispose()
        {
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    if (_configObject != null)
                    {
                        _configObject?.Clear();
                    }
                    _logService.Dispose();
                }
                _disposed = true;
            }
        }
    }
}
