﻿namespace DataUploader.Helper
{
    public class Enums
    {
        public enum MessageStatus
        {
            START,
            FINISH
        }

        public enum MessageType
        {
            SUCCESS,
            INFORMATION,
            ERROR,
            FATAL,
            DEBUG
        }

        public enum LogType
        {
            Log,
            LogDataIssues
        }

        public enum MilestoneStatus
        {
            NotStarted = 962080000,
            Active,
            Completed,
            Skipped
        }

        public enum StatusXLSRecord
        {
            NotProcessed,
            Processed
        }

        public enum UploadStatus
        {
            FileUploadStarted = 962080000,
            FileUploadCompleted,
            FileUploadFailed,
            ParsingFile,
            ParsingFileCompleted,
            ParsingFileFailed,
            RecordsProcessing,
            RecordsUploadCompleted,
            RecordsUploadFailed,
            MeasagureCalculationInESP,
            RecordsRecalculation,
            ImportFailed,
            UploadCompletedSuccessfully
        }

        public enum CreatorRecordType
        {
            UnassignedValue = int.MinValue,
            Front = 962080000,
            DataUploader = 962080001,
            Portal = 962080002,
            PrepTool = 962080003
        }
    }
}
