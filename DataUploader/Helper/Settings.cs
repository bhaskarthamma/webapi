﻿using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;

namespace DataUploader.Helper
{
    public class DataUploaderSettings
    {
        public string FileName
        {
            get;
            set;
        } = string.Empty;


        public int StartRowSheet
        {
            get;
            set;
        } = 1;


        public bool CallExecuteMultiple
        {
            get;
            set;
        } = false;


        public int GlobalRequestsCount
        {
            get;
            set;
        } = 100;


        public int GlobalPageRecordsCount
        {
            get;
            set;
        } = 1000;


        public int DedupRules
        {
            get;
            set;
        } = 962080000;


        public bool DuplicateDetect
        {
            get;
            set;
        } = true;


        public int EspRecalcData
        {
            get;
            set;
        } = 962080000;


        public int TypeConfig
        {
            get;
            set;
        } = -2147483648;


        public int CreatorRecordType
        {
            get;
            set;
        } = -2147483648;


        public int UploadStatus
        {
            get;
            set;
        } = -2147483648;


        public string JsonObjects
        {
            get;
            set;
        } = string.Empty;


        public TimeZoneInfo TimeZoneInfo
        {
            get;
            set;
        }

        public EntityReference CurrencyGuid
        {
            get;
            set;
        } = new EntityReference();


        public Guid UserGuid
        {
            get;
            set;
        } = Guid.Empty;


        public EntityReference TargetEntity
        {
            get;
            set;
        } = null;


        public string ParserRemoteApiUrl
        {
            get;
            set;
        } = string.Empty;


        public string CreatorRemoteApiUrl
        {
            get;
            set;
        } = string.Empty;


        public string CalculateRemoteApiUrl
        {
            get;
            set;
        } = string.Empty;


        public List<string> OrderEntitiesList
        {
            get;
            set;
        } = new List<string>();


        public List<string> DeduplicationEntitiesList
        {
            get;
            set;
        } = new List<string>();


        public Dictionary<string, string> MappingImpl
        {
            get;
            set;
        } = new Dictionary<string, string>();


        public Dictionary<string, List<string>> DmnRules
        {
            get;
            set;
        } = new Dictionary<string, List<string>>();


        public int GlobalErrorCount
        {
            get;
            set;
        } = 5;
    }
    }
