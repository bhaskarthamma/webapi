﻿using DataUploader.Extentions;
using DataUploader.Helper;
using Microsoft.Xrm.Sdk.Messages;
using System.Collections.Concurrent;
using System.Collections.Generic;

namespace DataUploader.Interfaces
{
    public interface IBaseCreateUpdate
    {
        void CreateUpdateEntityRecords(ExecuteMultipleRequest emRequest, List<string> setUniqueId, ConcurrentDictionary<string, CreatedEntity> dictionaryEntities, ConcurrentDictionary<string, CreatedEntity> requestEntities, StatisticRecords statisticRecords, LogService logService, int totalEntitiesCount = 0);

        void UpdateSourceRecords(ConcurrentDictionary<string, CreatedEntity> dictionaryEntities, LogService logService);

    }
}
