﻿using DataUploader.Extentions;
using DataUploader.Helper;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections.Concurrent;
using EntityJson = CoreUtils.DataUploader.Dto.EntityJson;

namespace DataUploader.Interfaces
{
    public interface IBaseGetData
    {
        Entity GetDataRelationMapping(string targetEntity, string sourceEntity, Guid sourceGuid);

        Entity GetEntityAttrsValue(Guid recordGuid, string entityName, ColumnSet columns);

        EntityCollection GetDataByEntityName(string entityName, ConcurrentDictionary<string, EntityJson> objects, Guid recordUploaded, bool statusRecord);

        PaginationRetriveMultiple GetDataByEntityName(string entityName, ConcurrentDictionary<string, EntityJson> objects, Guid recordUploaded, bool statusRecord, int pageNumber, dynamic pagingCookie, bool moreRecords, int recordCount);

        Guid VerifyDuplicateRecord(Entity entityObj, string entityName, string sheetName, string uniqueId);

        bool ComparingAttributeValues(object oAttribute, object oAttribute2);


    }
}
