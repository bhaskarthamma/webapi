﻿using Microsoft.Xrm.Sdk;
using DataUploader.Helper;

namespace DataUploader.Interfaces
{
    public interface IBaseLogService
    {
        void AddMessage(string message = "", Enums.MessageType messageType = Enums.MessageType.INFORMATION, Enums.LogType logType = Enums.LogType.Log, bool onlySocket = true);

        void SaveLog(IOrganizationService orgService, bool isAddLog = true);
    }
}
