﻿using DataUploader.Helper;
using DataUploader.Interfaces;
using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Concurrent;
using System.Runtime.CompilerServices;
using System.Security.Cryptography;
using System.Text;

namespace DataUploader.Extentions
{
    public class CacheService : IBaseCacheService, IDisposable
    {
        private readonly ConcurrentDictionary<string, object> _casheRelationDataAttributes = new ConcurrentDictionary<string, object>();

        private readonly ConcurrentDictionary<string, object> _collectionAttrValue = new ConcurrentDictionary<string, object>();

        private readonly ConcurrentDictionary<string, Entity> _collectionEntityValue = new ConcurrentDictionary<string, Entity>();

        private readonly ConcurrentDictionary<string, object> _collectionNullValue = new ConcurrentDictionary<string, object>();

        private bool _disposed = false;

        public CacheService()
        {
        }

        public void AddOrUpdate(string keyStrCache, object oAttribute)
        {
            if (!string.IsNullOrEmpty(keyStrCache))
            {
                MD5 mD5CryptoServiceProvider = new MD5CryptoServiceProvider();
                byte[] numArray = mD5CryptoServiceProvider.ComputeHash(Encoding.UTF8.GetBytes(keyStrCache));
                string str = BitConverter.ToString(numArray).Replace("-", string.Empty);
                if (oAttribute == null)
                {
                    this._collectionNullValue.AddOrUpdate(str, new IsNull(), (string oldkey, object oldvalue) => new IsNull());
                }
                else
                {
                    this._collectionAttrValue.AddOrUpdate(str, oAttribute, (string oldkey, object oldvalue) => oAttribute);
                }
            }
        }

        public void AddOrUpdate(string keyStrCache, RelationDataAttributes oRelationData)
        {
            if (!string.IsNullOrEmpty(keyStrCache))
            {
                MD5 mD5CryptoServiceProvider = new MD5CryptoServiceProvider();
                byte[] numArray = mD5CryptoServiceProvider.ComputeHash(Encoding.UTF8.GetBytes(keyStrCache));
                string str = BitConverter.ToString(numArray).Replace("-", string.Empty);
                if (oRelationData == null)
                {
                    this._collectionNullValue.AddOrUpdate(str, new IsNull(), (string oldkey, object oldvalue) => new IsNull());
                }
                else
                {
                    this._casheRelationDataAttributes.AddOrUpdate(str, oRelationData, (string oldkey, object oldvalue) => oRelationData);
                }
            }
        }

        public void AddOrUpdate(string keyStrCache, Entity oEntity, bool isEnity)
        {
            if (!string.IsNullOrEmpty(keyStrCache))
            {
                MD5 mD5CryptoServiceProvider = new MD5CryptoServiceProvider();
                byte[] numArray = mD5CryptoServiceProvider.ComputeHash(Encoding.UTF8.GetBytes(keyStrCache));
                string str = BitConverter.ToString(numArray).Replace("-", string.Empty);
                if (oEntity == null)
                {
                    this._collectionNullValue.AddOrUpdate(str, new IsNull(), (string oldkey, object oldvalue) => new IsNull());
                }
                else
                {
                    this._collectionEntityValue.AddOrUpdate(str, oEntity, (string oldkey, Entity oldvalue) => oEntity);
                }
            }
        }

        public bool ContainsKey(string keyStrCache)
        {
            bool flag;
            MD5 mD5CryptoServiceProvider = new MD5CryptoServiceProvider();
            byte[] numArray = mD5CryptoServiceProvider.ComputeHash(Encoding.UTF8.GetBytes(keyStrCache));
            string str = BitConverter.ToString(numArray).Replace("-", string.Empty);
            if (this._collectionNullValue.ContainsKey(str))
            {
                flag = true;
            }
            else if (this._collectionAttrValue.ContainsKey(str))
            {
                flag = true;
            }
            else if (!this._casheRelationDataAttributes.ContainsKey(str))
            {
                flag = (!this._collectionEntityValue.ContainsKey(str) ? false : true);
            }
            else
            {
                flag = true;
            }
            return flag;
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this._disposed)
            {
                if (disposing)
                {
                    if (this._collectionNullValue != null)
                    {
                        ConcurrentDictionary<string, object> strs = this._collectionNullValue;
                        if (strs != null)
                        {
                            strs.Clear();
                        }
                        else
                        {
                        }
                    }
                    if (this._collectionAttrValue != null)
                    {
                        ConcurrentDictionary<string, object> strs1 = this._collectionAttrValue;
                        if (strs1 != null)
                        {
                            strs1.Clear();
                        }
                        else
                        {
                        }
                    }
                    if (this._casheRelationDataAttributes != null)
                    {
                        ConcurrentDictionary<string, object> strs2 = this._casheRelationDataAttributes;
                        if (strs2 != null)
                        {
                            strs2.Clear();
                        }
                        else
                        {
                        }
                    }
                    if (this._collectionEntityValue != null)
                    {
                        ConcurrentDictionary<string, Entity> strs3 = this._collectionEntityValue;
                        if (strs3 != null)
                        {
                            strs3.Clear();
                        }
                        else
                        {
                        }
                    }
                }
                this._disposed = true;
            }
        }

        public dynamic Get(string keyStrCache)
        {
            object item;
            if (!string.IsNullOrEmpty(keyStrCache))
            {
                MD5 mD5CryptoServiceProvider = new MD5CryptoServiceProvider();
                byte[] numArray = mD5CryptoServiceProvider.ComputeHash(Encoding.UTF8.GetBytes(keyStrCache));
                string str = BitConverter.ToString(numArray).Replace("-", string.Empty);
                if (this._collectionNullValue.ContainsKey(str))
                {
                    if (this._collectionNullValue[str].GetType().FullName == "DataUploader.Helper.IsNull")
                    {
                        item = null;
                        return item;
                    }
                }
                else if (!this._collectionAttrValue.ContainsKey(str))
                {
                    if (!this._casheRelationDataAttributes.ContainsKey(str))
                    {
                        goto Label1;
                    }
                    item = (RelationDataAttributes)this._casheRelationDataAttributes[str];
                    return item;
                }
                else
                {
                    item = this._collectionAttrValue[str];
                    return item;
                }
                Label1:;

            }
            item = null;
            return item;
        }

        public dynamic Get(string keyStrCache, bool isEnity)
        {
            object item;
            if (!string.IsNullOrEmpty(keyStrCache))
            {
                MD5 mD5CryptoServiceProvider = new MD5CryptoServiceProvider();
                byte[] numArray = mD5CryptoServiceProvider.ComputeHash(Encoding.UTF8.GetBytes(keyStrCache));
                string str = BitConverter.ToString(numArray).Replace("-", string.Empty);
                if (!this._collectionNullValue.ContainsKey(str))
                {
                    if (!this._collectionEntityValue.ContainsKey(str))
                    {
                        goto Label1;
                    }
                    item = this._collectionEntityValue[str];
                    return item;
                }
                else if (this._collectionNullValue[str].GetType().FullName == "DataUploader.Helper.IsNull")
                {
                    item = null;
                    return item;
                }
                Label1:;

            }
            item = null;
            return item;
        }
    }
}