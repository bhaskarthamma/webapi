// DataUploader.Extentions.ChildrenRelationships
using DataUploader.Extentions;
using DataUploader.Helper;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataUploader.Extentions
{
    public class ChildrenRelationships
    {
        private List<Task> _tasks;

        private readonly IOrganizationService _orgService;

        private readonly ConcurrentDictionary<string, DataCollection<Entity>> _msEntities;

        private readonly ConcurrentDictionary<string, CreatedEntity> _requestEntities;

        private readonly LogService _logService;

        private readonly string _lookupName;

        private readonly string _parentEntityName;

        public ChildrenRelationships(IOrganizationService orgService, ConcurrentDictionary<string, CreatedEntity> requestEntities, ConcurrentDictionary<string, DataCollection<Entity>> msEntities, string lookupName, string parentEntityName, LogService logService)
        {
            _orgService = orgService;
            _msEntities = msEntities;
            _requestEntities = requestEntities;
            _logService = logService;
            _lookupName = lookupName;
            _parentEntityName = parentEntityName;
        }

        public void Create()
        {
            ExecuteMultipleRequest executeMultipleRequest = new ExecuteMultipleRequest
            {
                Requests = new OrganizationRequestCollection(),
                Settings = new ExecuteMultipleSettings
                {
                    ContinueOnError = true,
                    ReturnResponses = false
                }
            };
            _tasks = new List<Task>();
            try
            {
                foreach (KeyValuePair<string, CreatedEntity> requestEntity in _requestEntities)
                {
                    if (_msEntities.ContainsKey(requestEntity.Key))
                    {
                        foreach (Entity item2 in _msEntities[requestEntity.Key])
                        {
                            Entity entity = item2;
                            entity[_lookupName] = new EntityReference(_parentEntityName, requestEntity.Value.EntityGuid);
                            CreateRequest createRequest = new CreateRequest();
                            createRequest.Target = entity;
                            executeMultipleRequest.Requests.Add(createRequest);
                        }
                        if (executeMultipleRequest.Requests.Count > 0)
                        {
                            TaskExecuteMultiple taskEMultiple = new TaskExecuteMultiple(_orgService, executeMultipleRequest);
                            Task item = Task.Factory.StartNew(delegate
                            {
                                taskEMultiple.Run();
                            });
                            _tasks.Add(item);
                            executeMultipleRequest = new ExecuteMultipleRequest
                            {
                                Requests = new OrganizationRequestCollection(),
                                Settings = new ExecuteMultipleSettings
                                {
                                    ContinueOnError = true,
                                    ReturnResponses = false
                                }
                            };
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logService.AddMessage("Error > Create Milestones: " + ex.Message, Enums.MessageType.FATAL, Enums.LogType.Log, onlySocket: false);
            }
            Task.WaitAll(_tasks.ToArray(), -1);
        }
    }
}