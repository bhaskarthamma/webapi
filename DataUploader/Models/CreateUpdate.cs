﻿// DataUploader.Extentions.CreateUpdate
using CoreUtils.Service.Implementation.DataBase;
using CoreUtils.Service.Interfaces.DataBase;
using CoreUtils.Wrap;
using DataUploader.Extentions;
using DataUploader.Helper;
using DataUploader.Interfaces;
using DmnEngineApp;
using DmnEngineApp.Dto;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataUploader.Extentions
{
    public class CreateUpdate : IBaseCreateUpdate
    {
        private readonly IOrganizationService _orgService;

        private readonly string _sheetName;

        private readonly string _logicalName;

        private readonly bool _callExecuteMultiple;

        private List<string> _dmnRules;

        private readonly IDataBaseService _dbService;

        public List<Task> dmnTasks;

        private readonly Guid _userId;

        public CreateUpdate(IOrganizationService orgService, Guid userId, string sheetName, string logicalName, List<string> dmnRules, bool callExecuteMultiple = false)
        {
            _orgService = orgService;
            _sheetName = sheetName;
            _logicalName = logicalName;
            _callExecuteMultiple = callExecuteMultiple;
            _dmnRules = dmnRules;
            _dbService = new DataBaseService(_orgService);
            dmnTasks = new List<Task>();
            _userId = userId;
        }

        public void CreateUpdateEntityRecords(ExecuteMultipleRequest emRequest, List<string> setUniqueId, ConcurrentDictionary<string, CreatedEntity> dictionaryEntities, ConcurrentDictionary<string, CreatedEntity> requestEntities, StatisticRecords statisticRecords, LogService logService, int totalEntitiesCount = 0)
        {
            ParallelOptions opts = new ParallelOptions { MaxDegreeOfParallelism = 25 };
            int num = -1;
            if (!_callExecuteMultiple)
            {
                List<Entity> list = new List<Entity>();
                List<Entity> list2 = new List<Entity>();
                List<string> list3 = new List<string>();
                List<string> list4 = new List<string>();
                try
                {
                    foreach (OrganizationRequest request in emRequest.Requests)
                    {
                        num++;
                        if (request.RequestName == "Update")
                        {
                            list2.Add(((UpdateRequest)request).Target);
                            list4.Add(setUniqueId[num]);
                        }
                        else if (request.RequestName == "Create")
                        {
                            list.Add(((CreateRequest)request).Target);
                            list3.Add(setUniqueId[num]);
                        }
                    }
                    if (list.Count > 0)
                    {
                          ConcurrentDictionary<int, Guid> concurrentDictionary = _dbService.CreateEntities(list, totalEntitiesCount);

                             List<string> recGuids = new List<string>();
                             foreach (KeyValuePair<int, Guid> item3 in concurrentDictionary)
                             {
                                 statisticRecords.Created++;
                                 dictionaryEntities[list3[item3.Key]].EntityGuid = item3.Value;
                                 recGuids.Add(item3.Value.ToString());
                                 dictionaryEntities[list3[item3.Key]].Status = true;
                                 CreatedEntity recordCreate = dictionaryEntities[list3[item3.Key]];
                                 try
                                 {
                                     requestEntities.AddOrUpdate(list3[item3.Key], recordCreate, (string oldkey, CreatedEntity oldvalue) => recordCreate);
                                 }
                                 catch (Exception x)
                                 {

                                     Console.WriteLine("Message: " + x.Message);
                                     logService.AddMessage("Message: " + x.Message + " - " + x.InnerException, Enums.MessageType.FATAL);

                                 }
                             }


                             if (recGuids.Count > 0 && _dmnRules.Count > 0)
                             {
                                 Task item = Task.Factory.StartNew(delegate
                                {
                                 DmnDto dmnDto2 = new DmnDto();
                                 DmnEntryPoint dmnEntryPoint2 = new DmnEntryPoint(new InputArgsDto
                                 {
                                     BusinessRuleIds = _dmnRules,
                                     RecordIds = recGuids,
                                     TargetEntityName = _logicalName,
                                     UserId = _userId,
                                     IsExecuteRule = false
                                 });
                                  dmnEntryPoint2.LaunchDmn(_orgService);
                                  dmnDto2 = dmnEntryPoint2.DmnDto;
                            });
                                 dmnTasks.Add(item);
                             }

                             if (list2.Count > 0)
                             {
                                 _dbService.UpdateEntities(list2);
                                 for (int i = 0; i < list2.Count; i++)
                                 {
                                     statisticRecords.Updated++;
                                     dictionaryEntities[list4[i]].Status = true;
                                     CreatedEntity recordCreate2 = dictionaryEntities[list4[i]];
                                     requestEntities.AddOrUpdate(list4[i], recordCreate2, (string oldkey, CreatedEntity oldvalue) => recordCreate2);
                                 }
                             }
                       
                    }
                }
                catch (Exception x)
                {
                    Console.WriteLine("Message: " + x.Message);
                    logService.AddMessage("Message: " + x.Message + " - " + x.InnerException, Enums.MessageType.FATAL);

                }
            }
            else
            {
                ExecuteMultipleResponse executeMultipleResponse = (ExecuteMultipleResponse)_orgService.Execute(emRequest);
                List<string> recGuids2 = new List<string>();
                foreach (ExecuteMultipleResponseItem response in executeMultipleResponse.Responses)
                {
                    if (response.Response != null)
                    {
                        dictionaryEntities[setUniqueId[response.RequestIndex]].Status = true;
                        if (response.Response.Results.ContainsKey("id"))
                        {
                            statisticRecords.Created++;
                            dictionaryEntities[setUniqueId[response.RequestIndex]].EntityGuid = new Guid(response.Response.Results["id"].ToString());
                            recGuids2.Add(response.Response.Results["id"].ToString());
                        }
                        else
                        {
                            statisticRecords.Updated++;
                        }
                        CreatedEntity recordCreate3 = dictionaryEntities[setUniqueId[response.RequestIndex]];
                        requestEntities.AddOrUpdate(setUniqueId[response.RequestIndex], recordCreate3, (string oldkey, CreatedEntity oldvalue) => recordCreate3);
                    }
                    else if (response.Fault != null)
                    {
                        logService.AddMessage("Error > " + _sheetName + ":  Excel Unique GUID: " + setUniqueId[response.RequestIndex] + "; Request: " + emRequest.Requests[response.RequestIndex].RequestName + "; Index: " + (response.RequestIndex + 1) + "; Message: " + response.Fault.Message + "; TraceText: " + response.Fault.TraceText, Enums.MessageType.FATAL);
                    }
                }
                if (recGuids2.Count > 0 && _dmnRules.Count > 0)
                {
                    Task item2 = Task.Factory.StartNew(delegate
                    {
                        DmnDto dmnDto = new DmnDto();
                        DmnEntryPoint dmnEntryPoint = new DmnEntryPoint(new InputArgsDto
                        {
                            BusinessRuleIds = _dmnRules,
                            RecordIds = recGuids2,
                            TargetEntityName = _logicalName,
                            UserId = _userId,
                            IsExecuteRule = false
                        });
                        dmnEntryPoint.LaunchDmn(_orgService);
                        dmnDto = dmnEntryPoint.DmnDto;
                    });
                    dmnTasks.Add(item2);
                }
            }
        }

        public void UpdateSourceRecords(ConcurrentDictionary<string, CreatedEntity> dictionaryEntities, LogService logService)
        {
            List<Entity> list = new List<Entity>();
            Dictionary<string, CreatedEntity> dictionary = (from x in dictionaryEntities
                                                            where x.Value.Status && !x.Value.StatusDb
                                                            select x).ToDictionary((KeyValuePair<string, CreatedEntity> x) => x.Key, (KeyValuePair<string, CreatedEntity> x) => x.Value);
            ExecuteMultipleRequest executeMultipleRequest = new ExecuteMultipleRequest
            {
                Requests = new OrganizationRequestCollection(),
                Settings = new ExecuteMultipleSettings
                {
                    ContinueOnError = true,
                    ReturnResponses = false
                }
            };
            foreach (string key in dictionary.Keys)
            {
                Guid entityGuid = dictionary[key].EntityGuid;
                if (dictionary[key].EntityGuid != default(Guid))
                {
                    Guid sourceId = dictionary[key].SourceId;
                    if (dictionary[key].SourceId != default(Guid))
                    {
                        Entity entity = new Entity("ddsm_exceldata", dictionary[key].SourceId);
                        entity["ddsm_processed"] = true;
                        Dictionary<string, CreatedEntity> dictionary2 = new Dictionary<string, CreatedEntity>();
                        dictionary2.Add(key, dictionary[key]);
                        string text2 = (string)(entity["ddsm_entityobjectdictionary"] = JsonConvert.SerializeObject(dictionary2));
                        entity["regardingobjectid"] = new EntityReference(_logicalName, dictionary[key].EntityGuid);
                        entity["statecode"] = new OptionSetValue(1);
                        entity["statuscode"] = new OptionSetValue(2);
                        list.Add(entity);
                        UpdateRequest updateRequest = new UpdateRequest();
                        updateRequest.Target = entity;
                        executeMultipleRequest.Requests.Add(updateRequest);
                    }
                }
            }
            if (executeMultipleRequest.Requests.Count > 0)
            {
                if (!_callExecuteMultiple)
                {
                    try
                    {
                        _dbService.UpdateEntities(list);
                    }
                    catch (Exception)
                    {
                    }
                }
                else
                {
                    ExecuteMultipleResponse executeMultipleResponse = (ExecuteMultipleResponse)_orgService.Execute(executeMultipleRequest);
                    foreach (ExecuteMultipleResponseItem response in executeMultipleResponse.Responses)
                    {
                        if (response.Response != null)
                        {
                            if (!response.Response.Results.ContainsKey("id"))
                            {
                            }
                        }
                        else if (response.Fault == null)
                        {
                        }
                    }
                }
            }
        }
    }
}