﻿// DataUploader.Extentions.GetData
using CoreUtils.DataUploader.Dto;
using DataUploader.Extentions;
using DataUploader.Helper;
using DataUploader.Interfaces;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;

namespace DataUploader.Extentions
{
    public class GetData : IBaseGetData
    {
        private readonly IOrganizationService _orgService;

        private readonly LogService _logService;

        public GetData(IOrganizationService orgService, LogService logService)
        {
            _orgService = orgService;
            _logService = logService;
        }

        public Entity GetDataRelationMapping(string targetEntity, string sourceEntity, Guid sourceGuid)
        {
            InitializeFromRequest initializeFromRequest = new InitializeFromRequest();
            initializeFromRequest.TargetEntityName = targetEntity;
            initializeFromRequest.EntityMoniker = new EntityReference(sourceEntity, sourceGuid);
            initializeFromRequest.TargetFieldType = TargetFieldType.All;
            InitializeFromResponse initializeFromResponse = (InitializeFromResponse)_orgService.Execute(initializeFromRequest);
            return initializeFromResponse.Entity;
        }

        public Entity GetEntityAttrsValue(Guid recordGuid, string entityName, ColumnSet columns)
        {
            return _orgService.Retrieve(entityName, recordGuid, columns);
        }

        public EntityCollection GetDataByEntityName(string entityName, ConcurrentDictionary<string, EntityJson> objects, Guid recordUploaded, bool statusRecord)
        {
            string key = objects.FirstOrDefault((KeyValuePair<string, EntityJson> x) => x.Value.Name.ToLower() == entityName).Key;
            if (key != null)
            {
                EntityCollection entityCollection = new EntityCollection();
                try
                {
                    QueryExpression queryExpression = new QueryExpression();
                    queryExpression.EntityName = "ddsm_exceldata";
                    queryExpression.ColumnSet = new ColumnSet(allColumns: true);
                    queryExpression.Criteria = new FilterExpression(LogicalOperator.And);
                    queryExpression.Criteria.AddCondition(new ConditionExpression("subject", ConditionOperator.Equal, key));
                    queryExpression.Criteria.AddCondition(new ConditionExpression("ddsm_processed", ConditionOperator.Equal, statusRecord));
                    queryExpression.Criteria.AddCondition(new ConditionExpression("ddsm_datauploader", ConditionOperator.Equal, recordUploaded));
                    if (!statusRecord)
                    {
                        queryExpression.Criteria.AddCondition(new ConditionExpression("statecode", ConditionOperator.Equal, 0));
                    }
                    int num = 1;
                    RetrieveMultipleResponse retrieveMultipleResponse = new RetrieveMultipleResponse();
                    do
                    {
                        queryExpression.PageInfo.Count = 5000;
                        queryExpression.PageInfo.PagingCookie = ((num == 1) ? null : retrieveMultipleResponse.EntityCollection.PagingCookie);
                        queryExpression.PageInfo.PageNumber = num++;
                        RetrieveMultipleRequest retrieveMultipleRequest = new RetrieveMultipleRequest();
                        retrieveMultipleRequest.Query = queryExpression;
                        retrieveMultipleResponse = (RetrieveMultipleResponse)_orgService.Execute(retrieveMultipleRequest);
                        entityCollection.Entities.AddRange(retrieveMultipleResponse.EntityCollection.Entities);
                    }
                    while (retrieveMultipleResponse.EntityCollection.MoreRecords);
                    return (entityCollection.Entities.Count > 0) ? entityCollection : null;
                }
                catch (Exception ex)
                {
                    _logService.AddMessage("Error > Method getDataByEntityName; Sheet Name: " + key + "; Message: " + ex.Message, Enums.MessageType.FATAL);
                }
            }
            return null;
        }

        public PaginationRetriveMultiple GetDataByEntityName(string entityName, ConcurrentDictionary<string, EntityJson> objects, Guid recordUploaded, bool statusRecord, int pageNumber, dynamic pagingCookie, bool moreRecords, int recordCount)
        {
            string key = objects.FirstOrDefault((KeyValuePair<string, EntityJson> x) => x.Value.Name.ToLower() == entityName).Key;
            PaginationRetriveMultiple paginationRetriveMultiple = new PaginationRetriveMultiple();
            paginationRetriveMultiple.RetrieveCollection = new EntityCollection();
            paginationRetriveMultiple.PageNumber = pageNumber;
            paginationRetriveMultiple.PagingCookie = (object)pagingCookie;
            paginationRetriveMultiple.MoreRecords = moreRecords;
            if (key != null)
            {
                try
                {
                    QueryExpression queryExpression = new QueryExpression();
                    queryExpression.EntityName = "ddsm_exceldata";
                    queryExpression.ColumnSet = new ColumnSet(allColumns: true);
                    queryExpression.Criteria = new FilterExpression(LogicalOperator.And);
                    queryExpression.Criteria.AddCondition(new ConditionExpression("subject", ConditionOperator.Equal, key));
                    queryExpression.Criteria.AddCondition(new ConditionExpression("ddsm_processed", ConditionOperator.Equal, statusRecord));
                    queryExpression.Criteria.AddCondition(new ConditionExpression("ddsm_datauploader", ConditionOperator.Equal, recordUploaded));
                    if (!statusRecord)
                    {
                        queryExpression.Criteria.AddCondition(new ConditionExpression("statecode", ConditionOperator.Equal, 0));
                    }
                    RetrieveMultipleResponse retrieveMultipleResponse = new RetrieveMultipleResponse();
                    queryExpression.PageInfo.Count = recordCount;
                    queryExpression.PageInfo.PagingCookie = (string)((pageNumber == 1) ? null : pagingCookie);
                    queryExpression.PageInfo.PageNumber = pageNumber++;
                    RetrieveMultipleRequest retrieveMultipleRequest = new RetrieveMultipleRequest();
                    retrieveMultipleRequest.Query = queryExpression;
                    retrieveMultipleResponse = (RetrieveMultipleResponse)_orgService.Execute(retrieveMultipleRequest);
                    paginationRetriveMultiple.PagingCookie = retrieveMultipleResponse.EntityCollection.PagingCookie;
                    paginationRetriveMultiple.PageNumber = pageNumber;
                    paginationRetriveMultiple.MoreRecords = retrieveMultipleResponse.EntityCollection.MoreRecords;
                    paginationRetriveMultiple.RetrieveCollection.Entities.AddRange(retrieveMultipleResponse.EntityCollection.Entities);
                    return paginationRetriveMultiple;
                }
                catch (Exception ex)
                {
                    _logService.AddMessage("Error > Method getDataByEntityName; Sheet Name: " + key + "; Message: " + ex.Message, Enums.MessageType.FATAL);
                }
            }
            return null;
        }

        public Guid VerifyDuplicateRecord(Entity entityObj, string entityName, string sheetName, string uniqueId)
        {
            Guid result = Guid.Empty;
            RetrieveDuplicatesRequest retrieveDuplicatesRequest = new RetrieveDuplicatesRequest();
            retrieveDuplicatesRequest.BusinessEntity = entityObj;
            retrieveDuplicatesRequest.MatchingEntityName = entityName;
            retrieveDuplicatesRequest.PagingInfo = new PagingInfo
            {
                PageNumber = 1,
                Count = 50
            };
            try
            {
                RetrieveDuplicatesResponse retrieveDuplicatesResponse = (RetrieveDuplicatesResponse)_orgService.Execute(retrieveDuplicatesRequest);
                using (IEnumerator<Entity> enumerator = retrieveDuplicatesResponse.DuplicateCollection.Entities.GetEnumerator())
                {
                    if (enumerator.MoveNext())
                    {
                        Entity current = enumerator.Current;
                        result = current.Id;
                    }
                }
            }
            catch (Exception ex)
            {
                _logService.AddMessage("Error > Method VerifyDuplicateRecord; Sheet Name: " + sheetName + ";  Excel Unique GUID: " + uniqueId + "; Message: " + ex.Message, Enums.MessageType.FATAL);
            }
            return result;
        }

        public bool ComparingAttributeValues(object oAttribute, object oAttribute2)
        {
            bool result = false;
            try
            {
                if (oAttribute.GetType().Equals(typeof(OptionSetValue)) && oAttribute2.GetType().Equals(typeof(OptionSetValue)) && !string.IsNullOrEmpty(Convert.ToString((oAttribute as OptionSetValue).Value)) && !string.IsNullOrEmpty(Convert.ToString((oAttribute2 as OptionSetValue).Value)) && int.Parse(Convert.ToString((oAttribute2 as OptionSetValue).Value)) != int.Parse(Convert.ToString((oAttribute2 as OptionSetValue).Value)))
                {
                    result = true;
                }
                else if (oAttribute.GetType().Equals(typeof(Money)) && oAttribute2.GetType().Equals(typeof(Money)) && !string.IsNullOrEmpty(Convert.ToString((oAttribute as Money).Value)) && !string.IsNullOrEmpty(Convert.ToString((oAttribute2 as Money).Value)) && decimal.Parse(Convert.ToString((oAttribute2 as Money).Value)) != decimal.Parse(Convert.ToString((oAttribute2 as Money).Value)))
                {
                    result = true;
                }
                else if (oAttribute.GetType().Equals(typeof(EntityReference)) && oAttribute2.GetType().Equals(typeof(EntityReference)) && !string.IsNullOrEmpty(Convert.ToString((oAttribute as EntityReference).Id)) && !string.IsNullOrEmpty(Convert.ToString((oAttribute2 as EntityReference).Id)) && Convert.ToString((oAttribute as EntityReference).Id) != Convert.ToString((oAttribute2 as EntityReference).Id))
                {
                    result = true;
                }
                else if (oAttribute.GetType().Equals(typeof(int)) && oAttribute2.GetType().Equals(typeof(int)))
                {
                    if (Convert.ToInt32(oAttribute) != Convert.ToInt32(oAttribute2))
                    {
                        result = true;
                    }
                }
                else if (oAttribute.GetType().Equals(typeof(decimal)) && oAttribute2.GetType().Equals(typeof(decimal)))
                {
                    if (Convert.ToDecimal(oAttribute) != Convert.ToDecimal(oAttribute2))
                    {
                        result = true;
                    }
                }
                else if (!string.IsNullOrEmpty(Convert.ToString(oAttribute)) && !string.IsNullOrEmpty(Convert.ToString(oAttribute2)) && Convert.ToString(oAttribute) != Convert.ToString(oAttribute2))
                {
                    result = true;
                }
            }
            catch (Exception)
            {
                result = false;
            }
            return result;
        }
    }
}