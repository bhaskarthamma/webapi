﻿// DataUploader.Extentions.LogService
using CoreUtils.Wrap;
using DataUploader.Helper;
using DataUploader.Interfaces;
using DataUploader.Service;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;

namespace DataUploader.Extentions { 
public class LogService : IBaseLogService, IDisposable
{
    private readonly ConcurrentDictionary<string, List<string>> _logList;

    private readonly List<string> _fieldName = new List<string>(new string[2]
    {
        "ddsm_log",
        "ddsm_logdataissues"
    });

    private readonly Guid _uploaderId;

    private readonly string _separator = "--------------------------------------------------";

    private bool _disposed = false;

    private WsMessageUploaderService WsMessageService
    {
        get;
    }

    public LogService(DataUploaderSettings thisSettings)
    {
        _uploaderId = thisSettings.TargetEntity.Id;
        _logList = new ConcurrentDictionary<string, List<string>>();
        for (int i = 0; i < _fieldName.Count; i++)
        {
            _logList.AddOrUpdate(_fieldName[i], new List<string>(), (string oldkey, List<string> oldvalue) => new List<string>());
        }
        WsMessageService = new WsMessageUploaderService(thisSettings);
    }

    public void AddMessage(string message = "", Enums.MessageType messageType = Enums.MessageType.INFORMATION, Enums.LogType logType = Enums.LogType.Log, bool onlySocket = true)
    {
        Enums.MessageStatus status = Enums.MessageStatus.START;
        if (!string.IsNullOrEmpty(message))
        {
            if (!onlySocket)
            {
                switch (messageType)
                {
                    case Enums.MessageType.ERROR:
                    case Enums.MessageType.FATAL:
                        _logList[_fieldName[(int)logType]].Add("ERROR: " + message);
                        Log.Error("DU >>> " + message);
                        break;
                    case Enums.MessageType.SUCCESS:
                    case Enums.MessageType.INFORMATION:
                        _logList[_fieldName[(int)logType]].Add(message);
                        Log.Debug("DU >>> " + message);
                        break;
                    case Enums.MessageType.DEBUG:
                        _logList[_fieldName[(int)logType]].Add("DEBUG: " + message);
                        Log.Debug("DU >>> " + message);
                        break;
                }
            }
            else
            {
                switch (messageType)
                {
                    case Enums.MessageType.SUCCESS:
                    case Enums.MessageType.INFORMATION:
                        WsMessageService.SendMessage(messageType, message, status);
                        break;
                    case Enums.MessageType.ERROR:
                    case Enums.MessageType.FATAL:
                        WsMessageService.SendMessage(Enums.MessageType.ERROR, message, status);
                        Log.Error("DU >>> " + message);
                        break;
                    case Enums.MessageType.DEBUG:
                        Log.Debug("DU >>> " + message);
                        break;
                }
            }
        }
    }

    public void AddSeparator(string separator = "", Enums.LogType logType = Enums.LogType.Log)
    {
        if (string.IsNullOrEmpty(separator))
        {
            separator = _separator;
        }
        _logList[_fieldName[(int)logType]].Add(separator);
    }

    public void SaveLog(IOrganizationService orgService, bool isAddLog = true)
    {
        try
        {
            bool flag = true;
            for (int i = 0; i < _fieldName.Count; i++)
            {
                if (_logList[_fieldName[i]].Count > 0)
                {
                    flag = false;
                    break;
                }
            }
            if (!flag)
            {
                EntityCollection entityCollection = null;
                string[] array = new string[_fieldName.Count];
                if (isAddLog)
                {
                    ColumnSet columnSet = new ColumnSet();
                    for (int j = 0; j < _fieldName.Count; j++)
                    {
                        columnSet.AddColumn(_fieldName[j]);
                    }
                    QueryExpression queryExpression = new QueryExpression
                    {
                        EntityName = "ddsm_datauploader",
                        ColumnSet = columnSet
                    };
                    queryExpression.Criteria.AddCondition("ddsm_datauploaderid", ConditionOperator.Equal, _uploaderId);
                    entityCollection = orgService.RetrieveMultiple(queryExpression);
                }
                Entity entity = new Entity("ddsm_datauploader", _uploaderId);
                for (int k = 0; k < _fieldName.Count; k++)
                {
                    array[k] = string.Empty;
                    if (isAddLog && (entityCollection?[0].Attributes.ContainsKey(_fieldName[k]) ?? false))
                    {
                        array[k] = entityCollection[0].GetAttributeValue<string>(_fieldName[k]);
                    }
                    if (_logList[_fieldName[k]].Count > 0)
                    {
                        if (!string.IsNullOrEmpty(array[k]))
                        {
                            array[k] = array[k] + "\n" + string.Join("\n", _logList[_fieldName[k]]);
                        }
                        else
                        {
                            array[k] = string.Join("\n", _logList[_fieldName[k]]);
                        }
                        array[k] = array[k].Substring((array[k].Length > 1000000) ? (array[k].Length - 1000000) : 0, (array[k].Length > 1000000) ? 1000000 : array[k].Length);
                        entity[_fieldName[k]] = array[k];
                    }
                }
                if (entity.Attributes.Count > 0)
                {
                    orgService.Update(entity);
                }
                for (int l = 0; l < _fieldName.Count; l++)
                {
                    _logList.AddOrUpdate(_fieldName[l], new List<string>(), (string oldkey, List<string> oldvalue) => new List<string>());
                }
            }
        }
        catch (Exception ex)
        {
            Log.Error("DU >>> Error > " + ex.Message);
            for (int m = 0; m < _fieldName.Count; m++)
            {
                _logList.AddOrUpdate(_fieldName[m], new List<string>(), (string oldkey, List<string> oldvalue) => new List<string>());
            }
        }
    }

    public void Dispose()
    {
        Dispose(disposing: true);
        GC.SuppressFinalize(this);
    }

    protected virtual void Dispose(bool disposing)
    {
        if (!_disposed)
        {
            if (disposing)
            {
                for (int i = 0; i < _fieldName.Count; i++)
                {
                    if (_logList[_fieldName[i]] != null)
                    {
                        _logList[_fieldName[i]]?.Clear();
                    }
                }
                if (_logList != null)
                {
                    _logList?.Clear();
                }
            }
            _disposed = true;
        }
    }
}
}