﻿
namespace DataUploader.Extentions
{
    public class OutputReadAsDt
    {
        public LogService LogService
        {
            get;
            set;
        }

        public long TableParsingErrors { get; set; } = (long)0;

        public OutputReadAsDt()
        {
        }
    }
}