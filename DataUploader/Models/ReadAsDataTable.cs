﻿using CoreUtils.DataUploader.Dto;
using CoreUtils.Wrap;
using DataUploader;
using DataUploader.Extentions;
using DataUploader.Helper;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;

namespace DataUploader.Extentions
{

    public class ReadAsDataTable : IDisposable
    {
        private readonly IOrganizationService _orgService;

        private readonly DataUploaderSettings _thisSettings;

        private readonly ConcurrentDictionary<string, EntityJson> _configObject;

        private readonly DataTable _dt;

        private readonly string _dtName;

        private readonly SpreadsheetDocument _spreadSheetDocument;

        private readonly WorksheetPart _worksheetPart;

        private readonly LogService _logService;

        private readonly OutputReadAsDt _outputReadAsDt;

        private bool _disposed = false;

        private readonly int _limitItemColletcion;

        public ReadAsDataTable(IOrganizationService orgService, DataUploaderSettings thisSettings, ConcurrentDictionary<string, EntityJson> configObject, DataTable dt, string dtName)
        {
            _orgService = orgService;
            _dt = dt;
            _thisSettings = thisSettings;
            _configObject = configObject;
            _dtName = dtName;
            _logService = new LogService(_thisSettings);
            _outputReadAsDt = new OutputReadAsDt();
            _limitItemColletcion = _thisSettings.GlobalRequestsCount;
        }

        public ReadAsDataTable(IOrganizationService orgService, DataUploaderSettings thisSettings, ConcurrentDictionary<string, EntityJson> configObject, SpreadsheetDocument spreadSheetDocument, WorksheetPart worksheetPart, string dtName)
        {
            _orgService = orgService;
            _thisSettings = thisSettings;
            _configObject = configObject;
            _dtName = dtName;
            _spreadSheetDocument = spreadSheetDocument;
            _worksheetPart = worksheetPart;
            _logService = new LogService(_thisSettings);
            _outputReadAsDt = new OutputReadAsDt();
            _limitItemColletcion = _thisSettings.GlobalRequestsCount * 5;
        }

        public OutputReadAsDt Xls()
        {
            _outputReadAsDt.TableParsingErrors = 0L;
            ParsedRecords parsedRecords = new ParsedRecords();
            List<string> list = new List<string>();
            string text = _configObject[_dtName].Name.ToLower();
            ConcurrentDictionary<string, EntityJson> concurrentDictionary = new ConcurrentDictionary<string, EntityJson>();
            concurrentDictionary.AddOrUpdate(_dtName, _configObject[_dtName], (string oldkey, EntityJson oldvalue) => _configObject[_dtName]);
            Guid id = _thisSettings.TargetEntity.Id;
            List<string> list2 = new List<string>();
            List<string> list3 = new List<string>();
            ExecuteMultipleRequest executeMultipleRequest = new ExecuteMultipleRequest
            {
                Requests = new OrganizationRequestCollection(),
                Settings = new ExecuteMultipleSettings
                {
                    ContinueOnError = true,
                    ReturnResponses = true
                }
            };
            long num = 0L;
            long num2 = 0L;
            long num3 = 1L;
            string key = _configObject[_dtName].Attributes.FirstOrDefault((KeyValuePair<string, AttrJson> x) => x.Value.ColumnName.ToLower() == (_dtName + " GUID").ToLower()).Key;
            foreach (DataRow row in _dt.Rows)
            {
                num++;
                if (num > _thisSettings.StartRowSheet)
                {
                    Entity entity = new Entity("ddsm_exceldata");
                    entity["subject"] = _dtName;
                    entity["ddsm_processed"] = false;
                    entity["ddsm_datauploader"] = new EntityReference("ddsm_datauploader", id);
                    entity["ddsm_logicalname"] = text.ToLower();
                    entity["ddsm_jsondata"] = JsonConvert.SerializeObject(concurrentDictionary);
                    if (!string.IsNullOrEmpty(key) && !string.IsNullOrEmpty(Convert.ToString(row[key])))
                    {
                        if (list2.Contains(Convert.ToString(row[key])))
                        {
                            if (!list3.Contains(Convert.ToString(row[key])))
                            {
                                list3.Add(Convert.ToString(row[key]));
                            }
                        }
                        else
                        {
                            list2.Add(Convert.ToString(row[key]));
                        }
                    }
                    foreach (KeyValuePair<string, AttrJson> attribute in _configObject[_dtName].Attributes)
                    {
                        if (row[attribute.Key].ToString() != "")
                        {
                            entity["ddsm_" + attribute.Key.ToLower()] = Convert.ToString(row[attribute.Key]).Trim();
                            if (attribute.Value.AttrType == "DateTime")
                            {
                                entity["ddsm_" + attribute.Key.ToLower()] = DataUploader.ParseAttributToDateTime(row[attribute.Key]);
                            }
                        }
                    }
                    CreateRequest createRequest = new CreateRequest();
                    createRequest.Target = entity;
                    executeMultipleRequest.Requests.Add(createRequest);
                    num2++;
                    if (num2 == num3 * _limitItemColletcion)
                    {
                        GenerationEntity(executeMultipleRequest, parsedRecords, list, _thisSettings.CallExecuteMultiple);
                        num3++;
                        executeMultipleRequest = new ExecuteMultipleRequest
                        {
                            Requests = new OrganizationRequestCollection(),
                            Settings = new ExecuteMultipleSettings
                            {
                                ContinueOnError = true,
                                ReturnResponses = true
                            }
                        };
                    }
                }
            }
            if (executeMultipleRequest.Requests.Count > 0)
            {
                GenerationEntity(executeMultipleRequest, parsedRecords, list, _thisSettings.CallExecuteMultiple);
                num3++;
                executeMultipleRequest = new ExecuteMultipleRequest
                {
                    Requests = new OrganizationRequestCollection(),
                    Settings = new ExecuteMultipleSettings
                    {
                        ContinueOnError = true,
                        ReturnResponses = true
                    }
                };
            }
            parsedRecords.RequestCount += num2;
            if (list3.Count > 0)
            {
                _logService.AddMessage(Errors.GetTextError(380010) + string.Join(", ", list3) + " not unique values in sheet " + _dtName + " list", Enums.MessageType.ERROR, Enums.LogType.Log, onlySocket: false);
                _outputReadAsDt.TableParsingErrors += list3.Count;
            }
            if (list.Count > 0)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    _logService.AddMessage(list[i], Enums.MessageType.ERROR, Enums.LogType.Log, onlySocket: false);
                }
            }
            _logService.AddMessage(Errors.GetTextError(380006) + "'" + _dtName + "';  " + Errors.GetTextError(380055) + parsedRecords.RequestCount + "; " + Errors.GetTextError(380056) + parsedRecords.ResponceCount + "; " + Errors.GetTextError(380057) + parsedRecords.ExcludedDataCount + ";", Enums.MessageType.INFORMATION, Enums.LogType.Log, onlySocket: false);
            _outputReadAsDt.LogService = _logService;
            return _outputReadAsDt;
        }

        public OutputReadAsDt Xlsx()
        {
            _outputReadAsDt.TableParsingErrors = 0L;
            ParsedRecords parsedRecords = new ParsedRecords();
            List<string> list = new List<string>();
            ConcurrentDictionary<string, EntityJson> concurrentDictionary = new ConcurrentDictionary<string, EntityJson>();
            concurrentDictionary.AddOrUpdate(_dtName, _configObject[_dtName], (string oldkey, EntityJson oldvalue) => _configObject[_dtName]);
            string text = _configObject[_dtName].Name.ToLower();
            Guid id = _thisSettings.TargetEntity.Id;
            List<string> list2 = new List<string>();
            List<string> list3 = new List<string>();
            OpenXmlReader openXmlReader = OpenXmlReader.Create(_worksheetPart);
            long num = 0L;
            long num2 = 0L;
            long num3 = 1L;
            string key = _configObject[_dtName].Attributes.FirstOrDefault((KeyValuePair<string, AttrJson> x) => x.Value.ColumnName.ToLower() == (_dtName + " GUID").ToLower()).Key;
            ExecuteMultipleRequest executeMultipleRequest = new ExecuteMultipleRequest
            {
                Requests = new OrganizationRequestCollection(),
                Settings = new ExecuteMultipleSettings
                {
                    ContinueOnError = true,
                    ReturnResponses = true
                }
            };
            while (openXmlReader.Read())
            {
                if (openXmlReader.ElementType == typeof(Row))
                {
                    if (openXmlReader.HasAttributes)
                    {
                        num = Convert.ToInt32(openXmlReader.Attributes.First((OpenXmlAttribute a) => a.LocalName == "r").Value);
                    }
                    if (num > _thisSettings.StartRowSheet)
                    {
                        openXmlReader.ReadFirstChild();
                        Entity entity = new Entity("ddsm_exceldata");
                        entity["subject"] = _dtName;
                        entity["ddsm_logicalname"] = text.ToLower();
                        entity["ddsm_processed"] = false;
                        entity["ddsm_datauploader"] = new EntityReference("ddsm_datauploader", id);
                        entity["ddsm_jsondata"] = JsonConvert.SerializeObject(concurrentDictionary);
                        do
                        {
                            if (openXmlReader.ElementType == typeof(Cell))
                            {
                                Cell cell = (Cell)openXmlReader.LoadCurrentElement();
                                string column = GetColumnName(cell.CellReference);
                                string attrType = _configObject[_dtName].Attributes.FirstOrDefault((KeyValuePair<string, AttrJson> x) => x.Key.ToLower() == column.ToLower()).Value.AttrType;
                                if (!string.IsNullOrEmpty(key) && key == column)
                                {
                                    string attributeValue = entity.GetAttributeValue<string>("ddsm_" + column.ToLower());
                                    if (!string.IsNullOrEmpty(attributeValue))
                                    {
                                        if (list2.Contains(attributeValue))
                                        {
                                            if (!list3.Contains(attributeValue))
                                            {
                                                list3.Add(attributeValue);
                                            }
                                        }
                                        else
                                        {
                                            list2.Add(attributeValue);
                                        }
                                    }
                                }
                                entity["ddsm_" + column.ToLower()] = Convert.ToString(GetCellValue(_spreadSheetDocument, cell, isDate: false)).Trim();
                                if (attrType.ToString() == "DateTime")
                                {
                                    entity["ddsm_" + column.ToLower()] = DataUploader.ParseAttributToDateTime(Convert.ToString(GetCellValue(_spreadSheetDocument, cell, isDate: true)).Trim());
                                }
                            }
                        }
                        while (openXmlReader.ReadNextSibling());
                        CreateRequest createRequest = new CreateRequest();
                        createRequest.Target = entity;
                        executeMultipleRequest.Requests.Add(createRequest);
                        num2++;
                        if (num2 == num3 * _limitItemColletcion)
                        {
                            GenerationEntity(executeMultipleRequest, parsedRecords, list, _thisSettings.CallExecuteMultiple);
                            num3++;
                            executeMultipleRequest = new ExecuteMultipleRequest
                            {
                                Requests = new OrganizationRequestCollection(),
                                Settings = new ExecuteMultipleSettings
                                {
                                    ContinueOnError = true,
                                    ReturnResponses = true
                                }
                            };
                        }
                    }
                }
            }
            if (executeMultipleRequest.Requests.Count > 0)
            {
                GenerationEntity(executeMultipleRequest, parsedRecords, list, _thisSettings.CallExecuteMultiple);
                num3++;
                executeMultipleRequest = new ExecuteMultipleRequest
                {
                    Requests = new OrganizationRequestCollection(),
                    Settings = new ExecuteMultipleSettings
                    {
                        ContinueOnError = true,
                        ReturnResponses = true
                    }
                };
            }
            parsedRecords.RequestCount += num2;
            if (list3.Count > 0)
            {
                _logService.AddMessage(Errors.GetTextError(380010) + string.Join(", ", list3) + " not unique values in sheet " + _dtName + " list", Enums.MessageType.ERROR, Enums.LogType.Log, onlySocket: false);
                _outputReadAsDt.TableParsingErrors += list3.Count;
            }
            if (list.Count > 0)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    _logService.AddMessage(list[i], Enums.MessageType.ERROR, Enums.LogType.Log, onlySocket: false);
                }
            }
            _logService.AddMessage(Errors.GetTextError(380006) + "'" + _dtName + "';  " + Errors.GetTextError(380055) + parsedRecords.RequestCount + "; " + Errors.GetTextError(380056) + parsedRecords.ResponceCount + "; " + Errors.GetTextError(380057) + parsedRecords.ExcludedDataCount + ";", Enums.MessageType.INFORMATION, Enums.LogType.Log, onlySocket: false);
            _outputReadAsDt.LogService = _logService;
            return _outputReadAsDt;
        }

        private string GetCellValue(SpreadsheetDocument spreadSheetDocument, Cell cell, bool isDate)
        {
            SharedStringTablePart sharedStringTablePart = spreadSheetDocument.WorkbookPart.SharedStringTablePart;
            string text = cell.CellValue.InnerXml;
            try
            {
                if (cell.DataType == null)
                {
                    text = GetCellValueWithoutConsideringDataType(spreadSheetDocument, cell, isDate);
                }
                if (cell.DataType != null && cell.DataType.HasValue)
                {
                    switch (cell.DataType.Value)
                    {
                        case CellValues.SharedString:
                            text = (isDate ? ConverToDateValue(sharedStringTablePart.SharedStringTable.ChildElements[int.Parse(text)].InnerText) : sharedStringTablePart.SharedStringTable.ChildElements[int.Parse(text)].InnerText);
                            break;
                        case CellValues.Boolean:
                            {
                                string a = text;
                                text = ((!(a == "0")) ? "TRUE" : "FALSE");
                                break;
                            }
                    }
                }
            }
            catch (ArgumentOutOfRangeException)
            {
                text = null;
            }
            return text;
        }

        private string GetColumnName(string cellName)
        {
            Regex regex = new Regex("[A-Za-z]+");
            Match match = regex.Match(cellName);
            return match.Value;
        }

        private string GetFormatedValue(SpreadsheetDocument spreadSheetDocument, Cell cell, CellFormat cellformat, bool isDate)
        {
            string text = cell.InnerText;
            if (isDate)
            {
                text = ConverToDateValue(text);
            }
            return text;
        }

        private string GetCellValueWithoutConsideringDataType(SpreadsheetDocument spreadSheetDocument, Cell cell, bool isDate)
        {
            CellFormat cellFormat = GetCellFormat(spreadSheetDocument, cell);
            if (cellFormat != null)
            {
                return GetFormatedValue(spreadSheetDocument, cell, cellFormat, isDate);
            }
            if (isDate)
            {
                return ConverToDateValue(cell.InnerText);
            }
            return cell.InnerText;
        }

        private CellFormat GetCellFormat(SpreadsheetDocument spreadSheetDocument, Cell cell)
        {
            WorkbookPart workbookPart = spreadSheetDocument.WorkbookPart;
            if (cell.StyleIndex != null && cell.StyleIndex.HasValue)
            {
                int value = (int)cell.StyleIndex.Value;
                return (CellFormat)workbookPart.WorkbookStylesPart.Stylesheet.CellFormats.ElementAt(value);
            }
            return null;
        }

        private Dictionary<uint, string> BuildFormatMappingsFromXlsx(SpreadsheetDocument spreadSheetDocument)
        {
            Dictionary<uint, string> dictionary = new Dictionary<uint, string>();
            WorkbookStylesPart workbookStylesPart = spreadSheetDocument.WorkbookPart.WorkbookStylesPart;
            IEnumerable<NumberingFormats> enumerable = workbookStylesPart.Stylesheet.ChildElements.OfType<NumberingFormats>();
            foreach (NumberingFormats item in enumerable)
            {
                IEnumerable<NumberingFormat> enumerable2 = item.ChildElements.OfType<NumberingFormat>();
                foreach (NumberingFormat item2 in enumerable2)
                {
                    dictionary.Add(item2.NumberFormatId.Value, item2.FormatCode);
                }
            }
            return dictionary;
        }

        private string ConverToDateValue(string value)
        {
            try
            {
                if (DataUploader.IsNumeric(value))
                {
                    return Convert.ToString(DateTime.FromOADate(Convert.ToDouble(value)));
                }
                return Convert.ToString(DataUploader.ParseAttributToDateTime(value));
            }
            catch (Exception)
            {
                return null;
            }
        }

        private void GenerationEntity(ExecuteMultipleRequest emRequest, ParsedRecords pRecords, List<string> loggerErrors, bool callExecuteMultiple)
        {
            callExecuteMultiple = true;
            ExecuteMultipleRequest executeMultipleRequest = new ExecuteMultipleRequest
            {
                Requests = new OrganizationRequestCollection(),
                Settings = new ExecuteMultipleSettings
                {
                    ContinueOnError = true,
                    ReturnResponses = true
                }
            };
            int num = -1;
            if (!callExecuteMultiple)
            {
                foreach (OrganizationRequest request in emRequest.Requests)
                {
                    num++;
                    try
                    {
                        if (request.RequestName == "Create")
                        {
                            Guid guid = _orgService.Create(((CreateRequest)request).Target);
                            pRecords.ResponceCount++;
                        }
                    }
                    catch (Exception ex)
                    {
                        List<string> list = new List<string>();
                        Entity errRecord = (Entity)emRequest.Requests[num].Parameters["Target"];
                        string key = _configObject.FirstOrDefault((KeyValuePair<string, EntityJson> x) => x.Value.Name.ToLower() == errRecord.GetAttributeValue<string>("ddsm_name").ToLower()).Key;
                        string pattern = "\\'ddsm_\\w\\'";
                        MatchCollection matchCollection = Regex.Matches(ex.Message, pattern);
                        foreach (Match item in matchCollection)
                        {
                            string text = item.Value.Replace("'", "");
                            string fieldKey = text.Replace("ddsm_", "").ToUpper();
                            string columnName = _configObject[key].Attributes.FirstOrDefault((KeyValuePair<string, AttrJson> x) => x.Key == fieldKey).Value.ColumnName;
                            list.Add(columnName);
                            errRecord.Attributes.Remove(text);
                        }
                        loggerErrors.Add(Errors.GetTextError(380050) + key + Errors.GetTextError(380051) + string.Join(",", list) + Errors.GetTextError(380052) + (num + 1) + Errors.GetTextError(380053) + ex.Message);
                        CreateRequest createRequest = new CreateRequest();
                        createRequest.Target = errRecord;
                        executeMultipleRequest.Requests.Add(createRequest);
                    }
                }
                if (executeMultipleRequest.Requests.Count > 0)
                {
                    loggerErrors.Add(Errors.GetTextError(380007));
                    num = -1;
                    foreach (OrganizationRequest request2 in executeMultipleRequest.Requests)
                    {
                        try
                        {
                            if (request2.RequestName == "Create")
                            {
                                Guid guid2 = _orgService.Create(((CreateRequest)request2).Target);
                                pRecords.ExcludedDataCount++;
                            }
                        }
                        catch (Exception ex2)
                        {
                            pRecords.ErrorCount++;
                            loggerErrors.Add(Errors.GetTextError(380054) + ex2.Message);
                        }
                    }
                }
            }
            else
            {
                ExecuteMultipleResponse executeMultipleResponse = (ExecuteMultipleResponse)_orgService.Execute(emRequest);
                foreach (ExecuteMultipleResponseItem response in executeMultipleResponse.Responses)
                {
                    if (response.Response != null)
                    {
                        pRecords.ResponceCount++;
                        if (!response.Response.Results.ContainsKey("id"))
                        {
                        }
                    }
                    else if (response.Fault != null)
                    {
                        List<string> list2 = new List<string>();
                        Entity errRecord2 = (Entity)emRequest.Requests[response.RequestIndex].Parameters["Target"];
                        string key2 = _configObject.FirstOrDefault((KeyValuePair<string, EntityJson> x) => x.Value.Name.ToLower() == errRecord2.GetAttributeValue<string>("ddsm_name").ToLower()).Key;
                        string pattern2 = "\\'ddsm_\\w\\'";
                        MatchCollection matchCollection2 = Regex.Matches(response.Fault.Message, pattern2);
                        foreach (Match item2 in matchCollection2)
                        {
                            string text2 = item2.Value.Replace("'", "");
                            string fieldKey2 = text2.Replace("ddsm_", "").ToUpper();
                            string columnName2 = _configObject[key2].Attributes.FirstOrDefault((KeyValuePair<string, AttrJson> x) => x.Key == fieldKey2).Value.ColumnName;
                            list2.Add(columnName2);
                            errRecord2.Attributes.Remove(text2);
                        }
                        loggerErrors.Add(Errors.GetTextError(380050) + key2 + Errors.GetTextError(380051) + string.Join(",", list2) + Errors.GetTextError(380052) + (response.RequestIndex + 1) + Errors.GetTextError(380053) + response.Fault.Message);
                        CreateRequest createRequest2 = new CreateRequest();
                        createRequest2.Target = errRecord2;
                        executeMultipleRequest.Requests.Add(createRequest2);
                    }
                }
                if (executeMultipleRequest.Requests.Count > 0)
                {
                    loggerErrors.Add(Errors.GetTextError(380007));
                    executeMultipleResponse = (ExecuteMultipleResponse)_orgService.Execute(executeMultipleRequest);
                    foreach (ExecuteMultipleResponseItem response2 in executeMultipleResponse.Responses)
                    {
                        if (response2.Response != null)
                        {
                            pRecords.ExcludedDataCount++;
                            if (!response2.Response.Results.ContainsKey("id"))
                            {
                            }
                        }
                        else if (response2.Fault != null)
                        {
                            pRecords.ErrorCount++;
                            loggerErrors.Add(Errors.GetTextError(380054) + response2.Fault.Message);
                        }
                    }
                }
            }
        }

        public void Dispose()
        {
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    if (_configObject != null)
                    {
                        _configObject?.Clear();
                    }
                    _dt?.Clear();
                    _logService.Dispose();
                }
                _disposed = true;
            }
        }
    }
}