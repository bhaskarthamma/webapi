﻿// DataUploader.Extentions.TaskExecuteMultiple
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;

public class TaskExecuteMultiple
{
    private readonly IOrganizationService _orgService;

    private readonly ExecuteMultipleRequest _emRequest;

    public TaskExecuteMultiple(IOrganizationService orgService, ExecuteMultipleRequest emRequest)
    {
        _orgService = orgService;
        _emRequest = emRequest;
    }

    public void Run()
    {
        ExecuteMultipleResponse executeMultipleResponse = (ExecuteMultipleResponse)_orgService.Execute(_emRequest);
    }
}
