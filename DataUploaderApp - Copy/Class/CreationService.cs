﻿using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections.Concurrent;
using DataUploader.Helper;
using DataUploader.Creator;
using static DataUploader.Helper.Enums;
using static DataUploader.Helper.Errors;
using System.Diagnostics;
using JsonConvert = CoreUtils.Wrap.JsonConvert;
using EntityJson = CoreUtils.DataUploader.Dto.EntityJson;
using DataUploader.Extentions;

namespace DataUploaderApp.Service
{
    internal class CreationService
    {
        private readonly DataUploaderSettings _thisSettings;
        private readonly IOrganizationService _orgService;
        private bool _disposed = false;
        private LogService _logService;

        private ConcurrentDictionary<string, ConcurrentDictionary<string, CreatedEntity>> RecordEntities { get; set; }
        private ConcurrentDictionary<string, EntityJson> ConfigObjects { get; set; }

        public CreationService(UploaderService uploaderService)
        {
            _thisSettings = uploaderService.GetSettings();
            _orgService = uploaderService.GetOrgService();

            RecordEntities = new ConcurrentDictionary<string, ConcurrentDictionary<string, CreatedEntity>>();
            ConfigObjects = JsonConvert.DeserializeObject<ConcurrentDictionary<string, EntityJson>>(_thisSettings.JsonObjects);
            foreach (KeyValuePair<string, EntityJson> obj in ConfigObjects)
            {
                RecordEntities.AddOrUpdate(obj.Value.Name, new ConcurrentDictionary<string, CreatedEntity>(), (oldkey, oldvalue) => new ConcurrentDictionary<string, CreatedEntity>());
            }
        }

        public CreationService(DataUploaderSettings thisSettings, IOrganizationService orgService)
        {
            _thisSettings = thisSettings;
            _orgService = orgService;

            RecordEntities = new ConcurrentDictionary<string, ConcurrentDictionary<string, CreatedEntity>>();
            ConfigObjects = JsonConvert.DeserializeObject<ConcurrentDictionary<string, EntityJson>>(_thisSettings.JsonObjects);
            foreach (KeyValuePair<string, EntityJson> obj in ConfigObjects)
            {
                RecordEntities.AddOrUpdate(obj.Value.Name, new ConcurrentDictionary<string, CreatedEntity>(), (oldkey, oldvalue) => new ConcurrentDictionary<string, CreatedEntity>());
            }
        }

        public void Run()
        {
            StartingService();
        }

        private void StartingService()
        {
            var sw = Stopwatch.StartNew();
            long startSeconds = 0, endSeconds = 0;

            _logService = new LogService(_thisSettings);

            _logService.AddSeparator(" ");
            _logService.AddMessage(GetTextError(380011), MessageType.INFORMATION, LogType.Log, false);
            _logService.AddSeparator();
            _logService.SaveLog(_orgService);
            _logService.AddSeparator(" ");
            _logService.AddSeparator("***");

            string _entityName = string.Empty;
            for (var i = 0; i < _thisSettings.OrderEntitiesList.Count; i++)
            {
                _entityName = (_thisSettings.OrderEntitiesList[i]).Trim();
                if (!string.IsNullOrEmpty(ConfigObjects.FirstOrDefault(x => x.Value.Name.ToLower() == _entityName).Key))
                {
                    var createRecords = new CreateRecords(RecordEntities, _thisSettings);
                    startSeconds = sw.ElapsedMilliseconds;
                    createRecords.CreateRecord(_orgService, _entityName);
                    endSeconds = sw.ElapsedMilliseconds;
                    _logService.AddMessage("Entity LogicalName: " + _entityName + "; Creating records took " + (endSeconds - startSeconds) + " ms", MessageType.DEBUG, LogType.Log, false);
                    createRecords.Dispose();
                }
            }

            foreach (KeyValuePair<string, EntityJson> obj in ConfigObjects)
            {
                _entityName = obj.Value.Name;
                if (!_thisSettings.OrderEntitiesList.Contains(_entityName))
                {
                    var createRecords = new CreateRecords(RecordEntities, _thisSettings);
                    startSeconds = sw.ElapsedMilliseconds;
                    createRecords.CreateRecord(_orgService, _entityName);
                    endSeconds = sw.ElapsedMilliseconds;
                    _logService.AddMessage("Entity LogicalName: " + _entityName + "; Creating records took " + (endSeconds - startSeconds) + " ms", MessageType.DEBUG, LogType.Log, false);
                    createRecords.Dispose();
                }
            }

            _logService.AddSeparator();
            _logService.AddMessage(GetTextError(380012) + ";" + " *** " + sw.ElapsedMilliseconds / 1000 + " sec", MessageType.INFORMATION, LogType.Log, false);
            _logService.SaveLog(_orgService);
            _logService.Dispose();

            //Finish
            DataUploader.DataUploader.SetUploadStatus(_orgService, (int)UploadStatus.RecordsUploadCompleted, _thisSettings.TargetEntity.Id);


            if (_thisSettings.EspRecalcData != 962080002)
            {
            }
            else {
            }

            sw.Stop();
        }

        //Insert Measures Id to TaskQueue Entity
        private void MeasTaskQueue(IOrganizationService orgService, ConcurrentDictionary<string, CreatedEntity> measureEntities, List<string> logger, DataUploaderSettings thisSettings)
        {

            logger.Add(" ");
            logger.Add(GetTextError(380013));
            //Sorting Meas Ids by Parent Proj
            IComparer<CreatedEntity> myComparerGuidString = new CompareGuidString() as IComparer<CreatedEntity>;
            var sortMeasureEntities = measureEntities.OrderBy(x => (CreatedEntity)x.Value, myComparerGuidString);

            var measIds = new UserInputObj2();
            measIds.DataFields = null;
            measIds.SmartMeasures = new List<Guid>();
            int j = 1, i = 0;

            foreach (var key in sortMeasureEntities)
            {
                var smkey = key.Key;
                measIds.SmartMeasures.Add(measureEntities[smkey].EntityGuid);

                if (i == 9999 * j || i == (measureEntities.Count - 1))
                {
                    Entity taskQueue = new Entity("ddsm_taskqueue");
                    taskQueue["ddsm_name"] = "Measures Task Queue-" + Convert.ToString(j) + " - " + Convert.ToString(DataUploader.DataUploader.ConverAttributToDateTimeUtc("today", thisSettings.TimeZoneInfo) + " (UTC)");
                    taskQueue["ddsm_taskentity"] = new OptionSetValue(962080000);//Measure
                    taskQueue["ddsm_entityrecordtype"] = new OptionSetValue(962080000);//Measure
                    taskQueue["ddsm_datauploader"] = new EntityReference("ddsm_datauploader", thisSettings.TargetEntity.Id);
                    taskQueue["ddsm_processeditems0"] = JsonConvert.SerializeObject(measIds);

                    orgService.Create(taskQueue);
                    logger.Add(GetTextError(380014) + "'" + taskQueue["ddsm_name"].ToString() + "'");
                    j++;
                    measIds = new UserInputObj2();
                    measIds.DataFields = null;
                    measIds.SmartMeasures = new List<Guid>();
                }
                i++;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _logService.Dispose();
                }
                _disposed = true;
            }
        }

    }
}
