﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using static DataUploader.Helper.Errors;
using DataUploader.Helper;
using JsonConvert = CoreUtils.Wrap.JsonConvert;
using UploaderSettingsJson = CoreUtils.DataUploader.Dto.UploaderSettingsJson;
using UserSettingsJson = CoreUtils.DataUploader.Dto.UserSettingsJson;

namespace DataUploaderApp.Service
{
    internal class UploaderService
    {
        private readonly DataUploaderSettings _thisSettings;
        private readonly IOrganizationService _orgService;
        private readonly string _usersettingsLogicalName = "usersettings";

        public UploaderService(DataUploaderSettings thisSettings, IOrganizationService orgService, bool isFullSettings = false) {
            _orgService = orgService;
            if (!isFullSettings)
            {
                _thisSettings = UpdateSettings(thisSettings);
                
                if (_thisSettings.CurrencyGuid == Guid.Empty)
                {
                    _thisSettings.CurrencyGuid = GetGuidofCurrency(_orgService);
                }
            }
            else {
                _thisSettings = thisSettings;
            }
        }

        /*
        public void Run()
        {

        }
        */

        public DataUploaderSettings GetSettings()
        {
            return _thisSettings;
        }

        public IOrganizationService GetOrgService()
        {
            return _orgService;
        }

        private DataUploaderSettings UpdateSettings(DataUploaderSettings thisSettings)
        {
            List<string> logger = new List<string>();
            string jsonStr = string.Empty;

            QueryExpression recordUploadedQuery = new QueryExpression
            {
                EntityName = "ddsm_datauploader",
                ColumnSet = new ColumnSet(
                    "ddsm_name"
      ,"ddsm_jsondata"
    , "ddsm_settings"
    , "ddsm_recalculationtype"
    , "ddsm_shortname"
    , "ddsm_settings"
    , "ddsm_usersettings"
                )
            };
            recordUploadedQuery.Criteria = new FilterExpression(LogicalOperator.And);
            recordUploadedQuery.Criteria.AddCondition(new ConditionExpression("statecode", ConditionOperator.Equal, "Active"));
            recordUploadedQuery.Criteria.AddCondition("ddsm_datauploaderid", ConditionOperator.Equal, thisSettings.TargetEntity.Id);
            EntityCollection recordUploadedRetrieve = _orgService.RetrieveMultiple(recordUploadedQuery);

            if (!(recordUploadedRetrieve != null && recordUploadedRetrieve.Entities.Count > 0))
            {
                
                logger.Add(GetTextError(380001));

                Entity dataUploader = new Entity("ddsm_datauploader", thisSettings.TargetEntity.Id);
                dataUploader.Attributes.Add("ddsm_log", string.Join("\n", logger));
                _orgService.Update(dataUploader);

                throw new Exception(GetTextError(380001));

            }

            if (recordUploadedRetrieve[0].Attributes.ContainsKey("ddsm_name"))
                thisSettings.FileName = recordUploadedRetrieve[0].GetAttributeValue<string>("ddsm_name");

            if (recordUploadedRetrieve[0].Attributes.ContainsKey("ddsm_shortname"))
                thisSettings.TypeConfig = (int)recordUploadedRetrieve[0].GetAttributeValue<OptionSetValue>("ddsm_shortname").Value;

            if (recordUploadedRetrieve[0].Attributes.ContainsKey("ddsm_recalculationtype"))
                thisSettings.EspRecalcData = (int)recordUploadedRetrieve[0].GetAttributeValue<OptionSetValue>("ddsm_recalculationtype").Value;

            if (recordUploadedRetrieve[0].Attributes.ContainsKey("ddsm_jsondata"))
                thisSettings.JsonObjects = (string)recordUploadedRetrieve[0].GetAttributeValue<string>("ddsm_jsondata");

            if (recordUploadedRetrieve[0].Attributes.ContainsKey("ddsm_settings"))
            {
              //  string jsonUploaderSettings = (string)recordUploadedRetrieve[0].GetAttributeValue<string>("ddsm_settings");
              //  UploaderSettingsJson uploaderSettings = JsonConvert.DeserializeObject<UploaderSettingsJson>(jsonUploaderSettings);

                string attributeValue = recordUploadedRetrieve[0].GetAttributeValue<string>("ddsm_settings");
                UploaderSettingsJson uploaderSettings = JsonConvert.DeserializeObject<UploaderSettingsJson>(attributeValue);

                thisSettings.StartRowSheet = (uploaderSettings.ExcelIsHeader) ? 1 : 0;
                thisSettings.AllowSimultaneous = uploaderSettings.AllowSimultaneous;
                thisSettings.CallExecuteMultiple = uploaderSettings.CallExecuteMultiple;
                thisSettings.DedupRules = uploaderSettings.DedupRules;
                thisSettings.DuplicateDetect = uploaderSettings.DuplicateDetect;
                thisSettings.GlobalPageRecordsCount = uploaderSettings.GlobalPageRecordsCount;
                thisSettings.GlobalRequestsCount = uploaderSettings.GlobalRequestsCount;
                thisSettings.OrderEntitiesList = (uploaderSettings.OrderEntitiesList.Length > 0) ? new List<string>(uploaderSettings.OrderEntitiesList) : (new List<string>());

                thisSettings.ParserRemoteCalculationApiUrl = uploaderSettings.ParserRemoteCalculationApiUrl;
                thisSettings.CreatorRemoteCalculationApiUrl = uploaderSettings.CreatorRemoteCalculationApiUrl;

                //Temporary
                var mappImpl = (uploaderSettings.MappingImplementation.Length > 0) ? new List<string>(uploaderSettings.MappingImplementation) : (new List<string>());
                for (var i = 0; i < mappImpl.Count; i++)
                {
                    var entityGuid = mappImpl[i].Split('|').ToList();
                    if (!thisSettings.MappingImpl.ContainsKey(entityGuid[0]))
                    {
                        thisSettings.MappingImpl.Add(entityGuid[0], entityGuid[1]);
                    }
                }
            }
            if (recordUploadedRetrieve[0].Attributes.ContainsKey("ddsm_usersettings"))
            {
                string jsonUserSettings = (string)recordUploadedRetrieve[0].GetAttributeValue<string>("ddsm_usersettings");
                UserSettingsJson userSettings = JsonConvert.DeserializeObject<UserSettingsJson>(jsonUserSettings);

                if (!string.IsNullOrEmpty(userSettings.TransactionCurrencyId)) {
                    thisSettings.CurrencyGuid = new Guid(userSettings.TransactionCurrencyId);
                }

                if (!string.IsNullOrEmpty(userSettings.UserId))
                {
                    thisSettings.UserGuid = new Guid(userSettings.UserId);
                }

                if (userSettings.TimezoneCode != -2147483648)
                {
                    try
                    {
                        var query = new QueryExpression("timezonedefinition");
                        query.ColumnSet = new ColumnSet("timezonecode", "standardname");

                        query.Criteria.AddCondition("timezonecode", ConditionOperator.Equal, userSettings.TimezoneCode);
                        Entity timeZoneDefinitions = _orgService.RetrieveMultiple(query).Entities.FirstOrDefault();

                        thisSettings.TimeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(timeZoneDefinitions.Attributes["standardname"].ToString());

                    }
                    catch (Exception e) {
                        string windowsTimeZoneName = GetTimeZoneByCrmConnection(_orgService, thisSettings.UserGuid);
                        thisSettings.TimeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(windowsTimeZoneName);
                    }
                } else {
                    string windowsTimeZoneName = GetTimeZoneByCrmConnection(_orgService, thisSettings.UserGuid);
                    thisSettings.TimeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(windowsTimeZoneName);
                }

            }


            //var _curr = TransactionCurrency.GetCurrency(orgService, thisSettings.UserGuid, "ddsm_project");

            return thisSettings;
        }

        //Get User TimeZone info
        private string GetTimeZoneByCrmConnection(IOrganizationService orgService, Guid userId)
        {
            string timeZoneCode = "92"; //UTC
            var query = new QueryExpression(_usersettingsLogicalName);
            query.ColumnSet = new ColumnSet("systemuserid", "timezonecode");
            query.Criteria.AddCondition("systemuserid", ConditionOperator.Equal, userId);

            Entity userSettings = orgService.RetrieveMultiple(query).Entities.FirstOrDefault();
            if (userSettings != null && userSettings.Attributes.ContainsKey("timezonecode"))
            {
                timeZoneCode = userSettings.Attributes["timezonecode"].ToString();
            }
            else
            {
                return TimeZone.CurrentTimeZone.StandardName; //return local
            }

            query = new QueryExpression("timezonedefinition");
            query.ColumnSet = new ColumnSet("timezonecode", "standardname");

            query.Criteria.AddCondition("timezonecode", ConditionOperator.Equal, timeZoneCode);
            Entity timeZoneDefinitions = orgService.RetrieveMultiple(query).Entities.FirstOrDefault();

            return timeZoneDefinitions.Attributes["standardname"].ToString();
        }

        //Get User Currency Info
        private static Guid GetGuidofCurrency(IOrganizationService orgService)
        {
            Guid currencyGuid = new Guid();

            QueryExpression queryCurrency = new QueryExpression("currency")
            {
                EntityName = "transactioncurrency",
                ColumnSet = new ColumnSet(true),
                Criteria = new FilterExpression()
            };
            queryCurrency.Criteria.AddCondition("isocurrencycode", ConditionOperator.Equal, "CAD");
            try
            {
                DataCollection<Entity> entityData = orgService.RetrieveMultiple(queryCurrency).Entities;
                int count = entityData.Count;

                if (count > 0)
                {
                    currencyGuid = (Guid)entityData[0].Id;

                }
                else
                {
                    queryCurrency = new QueryExpression("currency")
                    {
                        EntityName = "transactioncurrency",
                        ColumnSet = new ColumnSet(true),
                        Criteria = new FilterExpression()
                    };
                    entityData = orgService.RetrieveMultiple(queryCurrency).Entities;
                    count = entityData.Count;
                    if (count > 0)
                        currencyGuid = (Guid)entityData[0].Id;
                }
            }
            catch (Exception e)
            {
                return Guid.Empty;
            }

            return currencyGuid;
        }

    }
}
