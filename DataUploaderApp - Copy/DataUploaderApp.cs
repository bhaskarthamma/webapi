﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Tooling.Connector;
using System;
using System.Configuration;
using System.Diagnostics;
using System.ServiceModel;
using DataUploader.Helper;
using DataUploaderApp.Helper;
using DataUploaderApp.Service;
using System.Net;

namespace DataUploaderApp
{
    public class DataUploaderApp
    {

        static void Main(string[] args)
        {
            try
            {
                var sw = Stopwatch.StartNew();
                Console.WriteLine($"***Start Program:{sw.ElapsedMilliseconds/1000}");
                /*
                if (args.Length == 0)
                    throw new ArgumentException("Input params is empty");
                    */
                var program = new DataUploaderApp();
                var thisSettings = new DataUploaderSettings();
                /*
                var input = args[0];
                var jsonImput = JsonConvert.DeserializeObject<AppArg>(input);
                */
                var jsonInput = new AppArg();
                jsonInput.TargetGuid = "9F28C091-97D1-E811-8168-480FCFF475A1";

                if (string.IsNullOrEmpty(jsonInput.TargetGuid))
                    throw new ArgumentException("Input params is empty");

                thisSettings.TargetEntity = new EntityReference("ddsm_datauploader", string.IsNullOrEmpty(jsonInput.TargetGuid) ? Guid.Empty : new Guid(jsonInput.TargetGuid));

                var crmConnect = new CrmConnect();

                program.InitCrmClient(crmConnect);


                //program.RunDataUploaderParser(thisSettings, crmConnect.OrgService);
                program.RunDataUploaderCreator(thisSettings, crmConnect.OrgService);
                Console.WriteLine($"***End Program:{sw.ElapsedMilliseconds/1000}");

            }
            catch (FaultException<OrganizationServiceFault> ex)
            {
                var message = ex.Message;
                Console.WriteLine($"{message}");
                throw new FaultException(message);
            }
        }

        //Parser
        public static void StartParserApp(DataUploaderSettings thisSettings, IOrganizationService orgService)
        {
            try
            {

                var program = new DataUploaderApp();

                program.RunDataUploaderParser(thisSettings, orgService, true);

            }
            catch (FaultException<OrganizationServiceFault> ex)
            {
                var message = ex.Message;
                throw new FaultException(message);
            }
        }
        public static void StartParserApp(AppArg jsonInput, IOrganizationService orgService)
        {
            try
            {
                var program = new DataUploaderApp();
                var thisSettings = new DataUploaderSettings();

                if (string.IsNullOrEmpty(jsonInput.TargetGuid))
                    throw new ArgumentException("*** DataUploader GUID is NULL");

                thisSettings.TargetEntity = new EntityReference("ddsm_datauploader", string.IsNullOrEmpty(jsonInput.TargetGuid) ? Guid.Empty : new Guid(jsonInput.TargetGuid));

                program.RunDataUploaderParser(thisSettings, orgService);

            }
            catch (FaultException<OrganizationServiceFault> ex)
            {
                var message = ex.Message;
                throw new FaultException(message);
            }
        }
        private void RunDataUploaderParser(DataUploaderSettings thisSettings, IOrganizationService orgService, bool isFullSettings = false)
        {
            if (thisSettings.TargetEntity.Id == Guid.Empty)
                return;
            var uploaderService = new UploaderService(thisSettings, orgService, isFullSettings);
            var parserService = new ParserService(uploaderService);
            parserService.Run();
            parserService.Dispose();
        }

        //Creator
        public static void StartCreatorApp(DataUploaderSettings thisSettings, IOrganizationService orgService)
        {
            try
            {
                var program = new DataUploaderApp();

                program.RunDataUploaderCreator(thisSettings, orgService, true);

            }
            catch (FaultException<OrganizationServiceFault> ex)
            {
                var message = ex.Message;
                throw new FaultException(message);
            }
        }
        public static void StartCreatorApp(AppArg jsonInput, IOrganizationService orgService)
        {
            try
            {
                var program = new DataUploaderApp();
                var thisSettings = new DataUploaderSettings();

                if (string.IsNullOrEmpty(jsonInput.TargetGuid))
                    throw new ArgumentException("*** DataUploader GUID is NULL");
                thisSettings.TargetEntity = new EntityReference("ddsm_datauploader", string.IsNullOrEmpty(jsonInput.TargetGuid) ? Guid.Empty : new Guid(jsonInput.TargetGuid));

                program.RunDataUploaderCreator(thisSettings, orgService);

            }
            catch (FaultException<OrganizationServiceFault> ex)
            {
                var message = ex.Message;
                throw new FaultException(message);
            }
        }
        private void RunDataUploaderCreator(DataUploaderSettings thisSettings, IOrganizationService orgService, bool isFullSettings = false)
        {
            if (thisSettings.TargetEntity.Id == Guid.Empty)
                return;
            var uploaderService = new UploaderService(thisSettings, orgService, isFullSettings);
            var creationService = new CreationService(uploaderService);
            creationService.Run();
            creationService.Dispose();
        }



        private void InitCrmClient(CrmConnect crmConnect)
        {

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            // Get the CRM connection string and connect to the CRM Organization
            var crmConnectionString = ConfigurationManager.ConnectionStrings["DataUploaderConnection"].ConnectionString;
            var client = new CrmServiceClient(crmConnectionString);
            var orgServiceProxy = client.OrganizationServiceProxy;

            crmConnect.ConnectionStrings = crmConnectionString;
            crmConnect.CrmServiceClient = client;
            crmConnect.OrgService = orgServiceProxy;
        }
    }
}
