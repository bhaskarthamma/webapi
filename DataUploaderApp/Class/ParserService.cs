﻿using System;
using System.Text;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System.IO;
using DataUploader.Helper;
using static DataUploader.Helper.Enums;
using static DataUploader.Helper.Errors;
using DataUploader.Parser;
using DataUploader.Extentions;
using System.Diagnostics;
using System.Net.Http;
using JsonConvert = CoreUtils.Wrap.JsonConvert;

namespace DataUploaderApp.Service
{
    internal class ParserService
    {
        private readonly DataUploaderSettings _thisSettings;
        private readonly IOrganizationService _orgService;
        private bool _disposed = false;
        private readonly LogService _logService;

        public ParserService(UploaderService uploaderService)
        {
            _thisSettings = uploaderService.GetSettings();
            _orgService = uploaderService.GetOrgService();
            _logService = new LogService(_thisSettings);
        }

        public void Run()
        {
            GetFile();
        }

        private void GetFile()
        {
            bool parseError = false;

            var sw = Stopwatch.StartNew();

            try
            {

                _logService.AddMessage(GetTextError(380008), MessageType.INFORMATION, LogType.Log, false);
                _logService.AddSeparator();
                _logService.SaveLog(_orgService);


                QueryExpression duNotes = new QueryExpression { EntityName = "annotation", ColumnSet = new ColumnSet("filename", "subject", "annotationid", "documentbody") };
                duNotes.Criteria.AddCondition("objectid", ConditionOperator.Equal, _thisSettings.TargetEntity.Id);
                EntityCollection notesRetrieve = _orgService.RetrieveMultiple(duNotes);

                if (notesRetrieve != null && notesRetrieve.Entities.Count == 1)
                {
                    string fileName = notesRetrieve.Entities[0].Attributes["filename"].ToString();

                    if (_thisSettings.UploadStatus != (int)UploadStatus.ParsingFile)
                        DataUploader.DataUploader.SetUploadStatus(_orgService, (int)UploadStatus.ParsingFile, _thisSettings.TargetEntity.Id);


                    byte[] fileContent = Convert.FromBase64String(notesRetrieve.Entities[0].Attributes["documentbody"].ToString());

                    using (var msFile = new MemoryStream())
                    {

                        msFile.Write(fileContent, 0, fileContent.Length);

                        var parseExcel = new ParseExcel(_orgService, msFile, _thisSettings);

                        if (Path.GetExtension(fileName).ToString().ToLower() == ".xlsx")
                        {

                            parseError = parseExcel.ParseXlsx();

                            if (!parseError)
                            {
                                DataUploader.DataUploader.SetUploadStatus(_orgService, (int)UploadStatus.ParsingFileCompleted, _thisSettings.TargetEntity.Id);

                                //Set File Parsed True
                                /*
                                Entity setParsed = new Entity("ddsm_datauploader", UploaderSettings.TargetEntity.Id);
                                setParsed.Attributes.Add("ddsm_fileparsed", true);
                                orgService.Update(setParsed);
                                */
                                //StartNewService(orgService, UploaderSettings, false);

                            }

                        }
                        else if (Path.GetExtension(fileName).ToString().ToLower() == ".xls")
                        {
                            parseError = parseExcel.ParseXls();

                            if (!parseError)
                            {
                                DataUploader.DataUploader.SetUploadStatus(_orgService, (int)UploadStatus.ParsingFileCompleted, _thisSettings.TargetEntity.Id);

                                //Set File Parsed True
                                /*
                                Entity setParsed = new Entity("ddsm_datauploader", UploaderSettings.TargetEntity.Id);
                                setParsed.Attributes.Add("ddsm_fileparsed", true);
                                orgService.Update(setParsed);
                                */

                                //StartNewService(orgService, UploaderSettings, false);

                            }

                        }
                        else
                        {
                            _logService.AddMessage(GetTextError(380003), MessageType.ERROR, LogType.Log, false);
                            parseError = true;
                        }
                        parseExcel.Dispose();
                    }

                }
                if (!parseError)
                {
                    _logService.AddSeparator();
                    _logService.AddMessage(GetTextError(380009) + ";" + " *** " + sw.ElapsedMilliseconds / 1000 + " sec", MessageType.INFORMATION, LogType.Log, false);
                    _logService.SaveLog(_orgService);

                    //Start Creator Service
                    StartNewService(_orgService, _thisSettings, false);

                }
                else
                {
                    _logService.AddSeparator();
                    _logService.AddMessage(GetTextError(380023) + ";" + " *** " + sw.ElapsedMilliseconds / 1000 + " sec", MessageType.ERROR, LogType.Log, false);
                    _logService.SaveLog(_orgService);

                    DataUploader.DataUploader.SetUploadStatus(_orgService, (int)UploadStatus.ParsingFileFailed, _thisSettings.TargetEntity.Id);
                }

            }
            catch (Exception e)
            {
                _logService.AddMessage("Error > " + e.Message, MessageType.FATAL, LogType.Log, false);
                _logService.SaveLog(_orgService);

                DataUploader.DataUploader.SetUploadStatus(_orgService, (int)UploadStatus.ParsingFileFailed, _thisSettings.TargetEntity.Id);

            }

            sw.Stop();
        }

        private void StartNewService(IOrganizationService orgService, DataUploaderSettings thisSettings, bool isParser = true)
        {
            try
            {
                var json = JsonConvert.SerializeObject(thisSettings);

                var client = new HttpClient();
                var content = new StringContent(json, Encoding.UTF8, "application/json");

                //Creator Api
                string remoteApiUrl = ((!string.IsNullOrEmpty(thisSettings.CreatorRemoteCalculationApiUrl)) ? thisSettings.CreatorRemoteCalculationApiUrl : thisSettings.ParserRemoteCalculationApiUrl) + "RunDataUploaderCreatorWithSettings";

                var postResult = client.PostAsync(remoteApiUrl, content);
                postResult.Wait();
                //var _postResult = postResult.Result.IsSuccessStatusCode;
            }
            catch (Exception e)
            {
                //
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _logService.Dispose();
                }
                _disposed = true;
            }
        }


    }
}
