using DataUploader.Helper;
using DataUploaderApp.Helper;
using DataUploaderApp.Service;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Xrm.Tooling.Connector;
using System;
using System.Configuration;
using System.Diagnostics;
using System.Net;
using System.ServiceModel;

namespace DataUploaderApp
{
	public class DataUploaderApp
	{
		public DataUploaderApp()
		{
		}

		private void InitCrmClient(CrmConnect crmConnect)
		{
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
			string connectionString = ConfigurationManager.ConnectionStrings["DataUploaderConnection"].ConnectionString;
			CrmServiceClient crmServiceClient = new CrmServiceClient(connectionString);

          
           OrganizationServiceProxy organizationServiceProxy = crmServiceClient.OrganizationServiceProxy;
			crmConnect.ConnectionStrings = connectionString;
			crmConnect.CrmServiceClient = crmServiceClient;
			crmConnect.OrgService = organizationServiceProxy;
		}

		private static void Main(string[] args)
		{
			try
			{
				Stopwatch stopwatch = Stopwatch.StartNew();
				Console.WriteLine(string.Format("***Start Program:{0}", stopwatch.ElapsedMilliseconds / (long)1000));
				DataUploaderApp dataUploaderApp = new DataUploaderApp();
				DataUploaderSettings dataUploaderSetting = new DataUploaderSettings();
				AppArg appArg = new AppArg()
				{
					TargetGuid = "001C6DE8-74D6-E811-812E-82350759871D"//"BD52A3AC-17D3-E811-812E-82350759871D"// "065B575F-E4D2-E811-812E-82350759871D"
                };
				if (string.IsNullOrEmpty(appArg.TargetGuid))
				{
					throw new ArgumentException("Input params is empty");
				}
				dataUploaderSetting.TargetEntity = new EntityReference("ddsm_datauploader", (string.IsNullOrEmpty(appArg.TargetGuid) ? Guid.Empty : new Guid(appArg.TargetGuid)));
				CrmConnect crmConnect = new CrmConnect();
				dataUploaderApp.InitCrmClient(crmConnect);
               
                //dataUploaderApp.RunDataUploaderParser(dataUploaderSetting, crmConnect.OrgService, false);
                //Console.WriteLine(string.Format("***End Program:{0}", stopwatch.ElapsedMilliseconds / (long)1000));
                dataUploaderApp.RunDataUploaderCreator(dataUploaderSetting, crmConnect.OrgService, false);
            }
			catch (FaultException<OrganizationServiceFault> faultException)
			{
				string message = faultException.Message;
				Console.WriteLine(string.Format("{0}", message));
				throw new FaultException(message);
			}
		}

		private void RunDataUploaderCalculate(DataUploaderSettings thisSettings, IOrganizationService orgService, bool isFullSettings = false)
		{
			if (thisSettings.TargetEntity.Id != Guid.Empty)
			{
				CalculationService calculationService = new CalculationService(new UploaderService(thisSettings, orgService, isFullSettings));
				calculationService.Run();
				calculationService.Dispose();
			}
		}

		private void RunDataUploaderCreator(DataUploaderSettings thisSettings, IOrganizationService orgService, bool isFullSettings = false)
		{
			if (thisSettings.TargetEntity.Id != Guid.Empty)
			{
				CreationService creationService = new CreationService(new UploaderService(thisSettings, orgService, isFullSettings));
				creationService.Run();
				creationService.Dispose();
			}
		}

		private void RunDataUploaderParser(DataUploaderSettings thisSettings, IOrganizationService orgService, bool isFullSettings = false)
		{
			if (thisSettings.TargetEntity.Id != Guid.Empty)
			{
				ParserService parserService = new ParserService(new UploaderService(thisSettings, orgService, isFullSettings));
				parserService.Run();
				parserService.Dispose();
			}
		}

		public static void StartCalculateApp(DataUploaderSettings thisSettings, IOrganizationService orgService)
		{
			try
			{
				(new DataUploaderApp()).RunDataUploaderCalculate(thisSettings, orgService, true);
			}
			catch (FaultException<OrganizationServiceFault> faultException)
			{
				throw new FaultException(faultException.Message);
			}
		}

		public static void StartCalculateApp(AppArg jsonInput, IOrganizationService orgService)
		{
			try
			{
				DataUploaderApp dataUploaderApp = new DataUploaderApp();
				DataUploaderSettings dataUploaderSetting = new DataUploaderSettings();
				if (string.IsNullOrEmpty(jsonInput.TargetGuid))
				{
					throw new ArgumentException("*** DataUploader GUID is NULL");
				}
				dataUploaderSetting.TargetEntity = new EntityReference("ddsm_datauploader", (string.IsNullOrEmpty(jsonInput.TargetGuid) ? Guid.Empty : new Guid(jsonInput.TargetGuid)));
				dataUploaderApp.RunDataUploaderCalculate(dataUploaderSetting, orgService, false);
			}
			catch (FaultException<OrganizationServiceFault> faultException)
			{
				throw new FaultException(faultException.Message);
			}
		}

		public static void StartCreatorApp(DataUploaderSettings thisSettings, IOrganizationService orgService)
		{
			try
			{
				(new DataUploaderApp()).RunDataUploaderCreator(thisSettings, orgService, true);
			}
			catch (FaultException<OrganizationServiceFault> faultException)
			{
				throw new FaultException(faultException.Message);
			}
		}

		public static void StartCreatorApp(AppArg jsonInput, IOrganizationService orgService)
		{
			try
			{
				DataUploaderApp dataUploaderApp = new DataUploaderApp();
				DataUploaderSettings dataUploaderSetting = new DataUploaderSettings();
				if (string.IsNullOrEmpty(jsonInput.TargetGuid))
				{
					throw new ArgumentException("*** DataUploader GUID is NULL");
				}
				dataUploaderSetting.TargetEntity = new EntityReference("ddsm_datauploader", (string.IsNullOrEmpty(jsonInput.TargetGuid) ? Guid.Empty : new Guid(jsonInput.TargetGuid)));
				dataUploaderApp.RunDataUploaderCreator(dataUploaderSetting, orgService, false);
			}
			catch (FaultException<OrganizationServiceFault> faultException)
			{
				throw new FaultException(faultException.Message);
			}
		}

		public static void StartParserApp(DataUploaderSettings thisSettings, IOrganizationService orgService)
		{
			try
			{
				(new DataUploaderApp()).RunDataUploaderParser(thisSettings, orgService, true);
			}
			catch (FaultException<OrganizationServiceFault> faultException)
			{
				throw new FaultException(faultException.Message);
			}
		}

		public static void StartParserApp(AppArg jsonInput, IOrganizationService orgService)
		{
			try
			{
				DataUploaderApp dataUploaderApp = new DataUploaderApp();
				DataUploaderSettings dataUploaderSetting = new DataUploaderSettings();
				if (string.IsNullOrEmpty(jsonInput.TargetGuid))
				{
					throw new ArgumentException("*** DataUploader GUID is NULL");
				}
				dataUploaderSetting.TargetEntity = new EntityReference("ddsm_datauploader", (string.IsNullOrEmpty(jsonInput.TargetGuid) ? Guid.Empty : new Guid(jsonInput.TargetGuid)));
				dataUploaderApp.RunDataUploaderParser(dataUploaderSetting, orgService, false);
			}
			catch (FaultException<OrganizationServiceFault> faultException)
			{
				throw new FaultException(faultException.Message);
			}
		}
	}
}