using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Tooling.Connector;
using System;
using System.Runtime.CompilerServices;

namespace DataUploaderApp.Helper
{
	internal class CrmConnect
	{
		public string ConnectionStrings { get; set; } = null;

		public Microsoft.Xrm.Tooling.Connector.CrmServiceClient CrmServiceClient { get; set; } = null;

		public IOrganizationService OrgService { get; set; } = null;

		public CrmConnect()
		{
		}
	}
}