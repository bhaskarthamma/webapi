using CoreUtils.Utils;
using DataUploader;
using DataUploader.Extentions;
using DataUploader.Helper;
using EspCalculation.App;
using EspCalculation.Options;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.CompilerServices;

namespace DataUploaderApp.Service
{
	internal class CalculationService
	{

        private readonly DataUploaderSettings _thisSettings;

        private readonly IOrganizationService _orgService;

        private bool _disposed = false;

        private readonly LogService _logService;

        private PaginationRetriveMultiple _entityCollectionPagination;

        public CalculationService(UploaderService uploaderService)
        {
            _thisSettings = uploaderService.GetSettings();
            _orgService = uploaderService.GetOrgService();
            _logService = new LogService(_thisSettings);
            _entityCollectionPagination = new PaginationRetriveMultiple();
        }

        public void Run()
        {
            List<Guid> list = new List<Guid>();
            _logService.AddMessage(Errors.GetTextError(380013));
            if (_thisSettings.EspRecalcData == 962080002)
            {
                do
                {
                    _entityCollectionPagination = (PaginationRetriveMultiple)this.GetDataByEntityName("ddsm_measure", true, _logService, _entityCollectionPagination.PageNumber, _entityCollectionPagination.PagingCookie, _entityCollectionPagination.MoreRecords);
                    if (_entityCollectionPagination != null && _entityCollectionPagination.RetrieveCollection.Entities.Count > 0)
                    {
                        if (_thisSettings.UploadStatus != 962080009)
                        {
                            DataUploader.DataUploader.SetUploadStatus(_orgService, 962080009, _thisSettings.TargetEntity.Id);
                            _thisSettings.UploadStatus = 962080009;
                        }
                        list = (from x in _entityCollectionPagination.RetrieveCollection.Entities
                                select x.Id).ToList();
                        /*
                        AppGroupCalculateMeasure appGroupCalculateMeasure = new AppGroupCalculateMeasure(_orgService, new CalculationOptions
                        {
                            Config = SettingsProvider.GetConfig<ESPConfig>(_orgService),
                            Target = _thisSettings.TargetEntity,
                            UserId = _thisSettings.UserGuid
                        }, null, list);
                        appGroupCalculateMeasure.Run();
                        */
                    }
                }
                while (_entityCollectionPagination != null && _entityCollectionPagination.MoreRecords);
                _entityCollectionPagination = new PaginationRetriveMultiple();
            }
            DataUploader.DataUploader.SetUploadStatus(_orgService, 962080010, _thisSettings.TargetEntity.Id);
            DataUploader.DataUploader.SetUploadStatus(_orgService, 962080012, _thisSettings.TargetEntity.Id);
            _logService.AddMessage("Upload Completed Successfully", Enums.MessageType.SUCCESS);
        }

        private PaginationRetriveMultiple GetDataByEntityName(string entityName, bool statusRecord, LogService logService, int pageNumber, dynamic pagingCookie, bool moreRecords)
        {
            PaginationRetriveMultiple paginationRetriveMultiple = new PaginationRetriveMultiple();
            paginationRetriveMultiple.RetrieveCollection = new EntityCollection();
            paginationRetriveMultiple.PageNumber = pageNumber;
            paginationRetriveMultiple.PagingCookie = (object)pagingCookie;
            paginationRetriveMultiple.MoreRecords = moreRecords;
            if (entityName != null)
            {
                try
                {
                    QueryExpression queryExpression = new QueryExpression();
                    queryExpression.EntityName = "ddsm_exceldata";
                    queryExpression.ColumnSet = new ColumnSet("regardingobjectid");
                    queryExpression.Criteria = new FilterExpression(LogicalOperator.And);
                    queryExpression.Criteria.AddCondition(new ConditionExpression("ddsm_logicalname", ConditionOperator.Equal, entityName));
                    queryExpression.Criteria.AddCondition(new ConditionExpression("ddsm_processed", ConditionOperator.Equal, statusRecord));
                    queryExpression.Criteria.AddCondition(new ConditionExpression("ddsm_datauploader", ConditionOperator.Equal, _thisSettings.TargetEntity.Id));
                    if (!statusRecord)
                    {
                        queryExpression.Criteria.AddCondition(new ConditionExpression("statecode", ConditionOperator.Equal, 0));
                    }
                    RetrieveMultipleResponse retrieveMultipleResponse = new RetrieveMultipleResponse();
                    queryExpression.PageInfo.Count = _thisSettings.GlobalPageRecordsCount;
                    queryExpression.PageInfo.PagingCookie = (string)((pageNumber == 1) ? null : pagingCookie);
                    queryExpression.PageInfo.PageNumber = pageNumber++;
                    RetrieveMultipleRequest retrieveMultipleRequest = new RetrieveMultipleRequest();
                    retrieveMultipleRequest.Query = queryExpression;
                    retrieveMultipleResponse = (RetrieveMultipleResponse)_orgService.Execute(retrieveMultipleRequest);
                    paginationRetriveMultiple.PagingCookie = retrieveMultipleResponse.EntityCollection.PagingCookie;
                    paginationRetriveMultiple.PageNumber = pageNumber;
                    paginationRetriveMultiple.MoreRecords = retrieveMultipleResponse.EntityCollection.MoreRecords;
                    paginationRetriveMultiple.RetrieveCollection.Entities.AddRange(retrieveMultipleResponse.EntityCollection.Entities);
                    return paginationRetriveMultiple;
                }
                catch (Exception ex)
                {
                    logService.AddMessage("Error > Method getDataByEntityName; EntityLogicalName: " + entityName + "; Message: " + ex.Message, Enums.MessageType.FATAL);
                }
            }
            return null;
        }

        public void Dispose()
        {
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _logService.Dispose();
                }
                _disposed = true;
            }
        }
    }
}