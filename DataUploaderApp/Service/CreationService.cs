using CoreUtils.DataUploader.Dto;
using CoreUtils.Wrap;
using DataUploader;
using DataUploader.Creator;
using DataUploader.Extentions;
using DataUploader.Helper;
using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace DataUploaderApp.Service
{
	internal class CreationService
	{
        private readonly DataUploaderSettings _thisSettings;

        private readonly IOrganizationService _orgService;

        private bool _disposed = false;

        private LogService _logService;

        private ConcurrentDictionary<string, ConcurrentDictionary<string, CreatedEntity>> RecordEntities
        {
            get;
            set;
        }

        private ConcurrentDictionary<string, EntityJson> ConfigObjects
        {
            get;
            set;
        }

        public CreationService(UploaderService uploaderService)
        {
            _thisSettings = uploaderService.GetSettings();
            _orgService = uploaderService.GetOrgService();
            RecordEntities = new ConcurrentDictionary<string, ConcurrentDictionary<string, CreatedEntity>>();
            ConfigObjects = JsonConvert.DeserializeObject<ConcurrentDictionary<string, EntityJson>>(_thisSettings.JsonObjects);
            foreach (KeyValuePair<string, EntityJson> configObject in ConfigObjects)
            {
                RecordEntities.AddOrUpdate(configObject.Value.Name, new ConcurrentDictionary<string, CreatedEntity>(), (string oldkey, ConcurrentDictionary<string, CreatedEntity> oldvalue) => new ConcurrentDictionary<string, CreatedEntity>());
            }
        }

        public CreationService(DataUploaderSettings thisSettings, IOrganizationService orgService)
        {
            _thisSettings = thisSettings;
            _orgService = orgService;
            RecordEntities = new ConcurrentDictionary<string, ConcurrentDictionary<string, CreatedEntity>>();
            ConfigObjects = JsonConvert.DeserializeObject<ConcurrentDictionary<string, EntityJson>>(_thisSettings.JsonObjects);
            foreach (KeyValuePair<string, EntityJson> configObject in ConfigObjects)
            {
                RecordEntities.AddOrUpdate(configObject.Value.Name, new ConcurrentDictionary<string, CreatedEntity>(), (string oldkey, ConcurrentDictionary<string, CreatedEntity> oldvalue) => new ConcurrentDictionary<string, CreatedEntity>());
            }
        }

        public void Run()
        {
            StartingService();
        }

        private void StartingService()
        {
            Stopwatch stopwatch = Stopwatch.StartNew();
            long num = 0L;
            long num2 = 0L;
            _logService = new LogService(_thisSettings);
            _logService.AddSeparator(" ");
            _logService.AddMessage(Errors.GetTextError(380011), Enums.MessageType.INFORMATION, Enums.LogType.Log, onlySocket: false);
            _logService.AddSeparator();
            _logService.SaveLog(_orgService);
            _logService.AddSeparator(" ");
            _logService.AddSeparator("***");
            string _entityName = string.Empty;
            bool flag = true;
            if (_thisSettings.UploadStatus != 962080006)
            {
                DataUploader.DataUploader.SetUploadStatus(_orgService, 962080006, _thisSettings.TargetEntity.Id);
                _thisSettings.UploadStatus = 962080006;
            }
            for (int i = 0; i < _thisSettings.OrderEntitiesList.Count; i++)
            {
                if (!flag)
                {
                    break;
                }
                _entityName = _thisSettings.OrderEntitiesList[i].Trim();
                if (!string.IsNullOrEmpty(ConfigObjects.FirstOrDefault((KeyValuePair<string, EntityJson> x) => x.Value.Name.ToLower() == _entityName).Key))
                {
                    CreateRecords createRecords = new CreateRecords(RecordEntities, _thisSettings);
                    num = stopwatch.ElapsedMilliseconds;
                    flag = createRecords.CreateRecord(_orgService, _entityName);
                    num2 = stopwatch.ElapsedMilliseconds;
                    _logService.AddMessage("Entity LogicalName: " + _entityName + "; Creating records took " + (num2 - num) + " ms", Enums.MessageType.DEBUG, Enums.LogType.Log, onlySocket: false);
                    createRecords.Dispose();
                }
            }
            foreach (KeyValuePair<string, EntityJson> configObject in ConfigObjects)
            {
                if (!flag)
                {
                    break;
                }
                _entityName = configObject.Value.Name;
                if (!_thisSettings.OrderEntitiesList.Contains(_entityName))
                {
                    CreateRecords createRecords2 = new CreateRecords(RecordEntities, _thisSettings);
                    num = stopwatch.ElapsedMilliseconds;
                    flag = createRecords2.CreateRecord(_orgService, _entityName);
                    num2 = stopwatch.ElapsedMilliseconds;
                    _logService.AddMessage("Entity LogicalName: " + _entityName + "; Creating records took " + (num2 - num) + " ms", Enums.MessageType.DEBUG, Enums.LogType.Log, onlySocket: false);
                    createRecords2.Dispose();
                }
            }
            _logService.AddSeparator();
            _logService.AddMessage(Errors.GetTextError(380012) + "; *** " + stopwatch.ElapsedMilliseconds / 1000 + " sec", Enums.MessageType.INFORMATION, Enums.LogType.Log, onlySocket: false);
            _logService.SaveLog(_orgService);
            if (flag)
            {
                DataUploader.DataUploader.SetUploadStatus(_orgService, 962080007, _thisSettings.TargetEntity.Id);
                switch (_thisSettings.EspRecalcData)
                {
                    case 962080002:
                        StarCalculationService(_orgService, _thisSettings);
                        break;
                    case 962080003:
                        DataUploader.DataUploader.SetUploadStatus(_orgService, 962080012, _thisSettings.TargetEntity.Id);
                        _logService.AddMessage("Upload Completed Successfully", Enums.MessageType.SUCCESS);
                        break;
                }
            }
            else
            {
                DataUploader.DataUploader.SetUploadStatus(_orgService, 962080008, _thisSettings.TargetEntity.Id);
                _logService.AddMessage(Errors.GetTextError(380026), Enums.MessageType.ERROR, Enums.LogType.Log, onlySocket: false);
            }
            _logService.Dispose();
            stopwatch.Stop();
        }

        private void StarCalculationService(IOrganizationService orgService, DataUploaderSettings thisSettings)
        {
            try
            {
                string content = JsonConvert.SerializeObject(thisSettings);
                HttpClient httpClient = new HttpClient();
                StringContent content2 = new StringContent(content, Encoding.UTF8, "application/json");
                string requestUri = ((!string.IsNullOrEmpty(thisSettings.CalculateRemoteApiUrl)) ? thisSettings.CalculateRemoteApiUrl : thisSettings.ParserRemoteApiUrl) + "RunDataUploaderCalculateWithSettings";
                Task<HttpResponseMessage> task = httpClient.PostAsync(requestUri, content2);
                task.Wait();
            }
            catch (Exception)
            {
            }
        }

        public void Dispose()
        {
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _logService.Dispose();
                }
                _disposed = true;
            }
        }
    }
}