using CoreUtils.Wrap;
using DataUploader;
using DataUploader.Extentions;
using DataUploader.Helper;
using DataUploader.Parser;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace DataUploaderApp.Service
{
	internal class ParserService
	{
        private readonly DataUploaderSettings _thisSettings;

        private readonly IOrganizationService _orgService;

        private bool _disposed = false;

        private readonly LogService _logService;

        public ParserService(UploaderService uploaderService)
        {
            _thisSettings = uploaderService.GetSettings();
            _orgService = uploaderService.GetOrgService();
            _logService = new LogService(_thisSettings);
        }

        public void Run()
        {
            GetFile();
        }

        private void GetFile()
        {
            bool flag = false;
            Stopwatch stopwatch = Stopwatch.StartNew();
            try
            {
                _logService.AddMessage(Errors.GetTextError(380008), Enums.MessageType.INFORMATION, Enums.LogType.Log, onlySocket: false);
                _logService.AddSeparator();
                _logService.SaveLog(_orgService);
                QueryExpression queryExpression = new QueryExpression();
                queryExpression.EntityName = "annotation";
                queryExpression.ColumnSet = new ColumnSet("filename", "subject", "annotationid", "documentbody");
                QueryExpression queryExpression2 = queryExpression;
                queryExpression2.Criteria.AddCondition("objectid", ConditionOperator.Equal, _thisSettings.TargetEntity.Id);
                EntityCollection entityCollection = _orgService.RetrieveMultiple(queryExpression2);
                if (entityCollection != null && entityCollection.Entities.Count == 1)
                {
                    string path = entityCollection.Entities[0].Attributes["filename"].ToString();
                    DataUploader.DataUploader.SetUploadStatus(_orgService, 962080003, _thisSettings.TargetEntity.Id);
                    _thisSettings.UploadStatus = 962080003;
                    byte[] array = Convert.FromBase64String(entityCollection.Entities[0].Attributes["documentbody"].ToString());
                    using (MemoryStream memoryStream = new MemoryStream())
                    {
                        memoryStream.Write(array, 0, array.Length);
                        ParseExcel parseExcel = new ParseExcel(_orgService, memoryStream, _thisSettings);
                        if (Path.GetExtension(path).ToString().ToLower() == ".xlsx")
                        {
                            flag = parseExcel.ParseXlsx();
                        }
                        else if (Path.GetExtension(path).ToString().ToLower() == ".xls")
                        {
                            flag = parseExcel.ParseXls();
                        }
                        else
                        {
                            _logService.AddMessage(Errors.GetTextError(380003), Enums.MessageType.ERROR, Enums.LogType.Log, onlySocket: false);
                            flag = true;
                        }
                        parseExcel.Dispose();
                    }
                }
                if (!flag)
                {
                    _logService.AddSeparator();
                    _logService.AddMessage(Errors.GetTextError(380009) + "; *** " + stopwatch.ElapsedMilliseconds / 1000 + " sec", Enums.MessageType.INFORMATION, Enums.LogType.Log, onlySocket: false);
                    _logService.SaveLog(_orgService);
                    DataUploader.DataUploader.SetUploadStatus(_orgService, 962080004, _thisSettings.TargetEntity.Id);
                    RunCreationService(_orgService, _thisSettings, isParser: false);
                }
                else
                {
                    _logService.AddSeparator();
                    _logService.AddMessage(Errors.GetTextError(380023) + "; *** " + stopwatch.ElapsedMilliseconds / 1000 + " sec", Enums.MessageType.ERROR, Enums.LogType.Log, onlySocket: false);
                    _logService.SaveLog(_orgService);
                    DataUploader.DataUploader.SetUploadStatus(_orgService, 962080005, _thisSettings.TargetEntity.Id);
                }
            }
            catch (Exception ex)
            {
                _logService.AddMessage("Error > " + ex.Message, Enums.MessageType.FATAL, Enums.LogType.Log, onlySocket: false);
                _logService.SaveLog(_orgService);
                DataUploader.DataUploader.SetUploadStatus(_orgService, 962080005, _thisSettings.TargetEntity.Id);
            }
            stopwatch.Stop();
        }

        private void RunCreationService(IOrganizationService orgService, DataUploaderSettings thisSettings, bool isParser = true)
        {
            try
            {
                string content = JsonConvert.SerializeObject(thisSettings);
                HttpClient httpClient = new HttpClient();
                StringContent content2 = new StringContent(content, Encoding.UTF8, "application/json");
                string requestUri = ((!string.IsNullOrEmpty(thisSettings.CreatorRemoteApiUrl)) ? thisSettings.CreatorRemoteApiUrl : thisSettings.ParserRemoteApiUrl) + "RunDataUploaderCreatorWithSettings";
                Task<HttpResponseMessage> task = httpClient.PostAsync(requestUri, content2);
                task.Wait();
            }
            catch (Exception)
            {
            }
        }

        public void Dispose()
        {
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _logService.Dispose();
                }
                _disposed = true;
            }
        }

    }
}