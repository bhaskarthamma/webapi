using CoreUtils.DataUploader.Dto;
using CoreUtils.Service.Implementation;
using CoreUtils.Wrap;
using DataUploader.Helper;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace DataUploaderApp.Service
{
	internal class UploaderService
	{
        private readonly DataUploaderSettings _thisSettings;

        private readonly IOrganizationService _orgService;

        private readonly string _usersettingsLogicalName = "usersettings";

        public UploaderService(DataUploaderSettings thisSettings, IOrganizationService orgService, bool isFullSettings = false)
        {
            _orgService = orgService;
            if (!isFullSettings)
            {
                _thisSettings = UpdateSettings(thisSettings);
            }
            else
            {
                _thisSettings = thisSettings;
            }
        }

        public DataUploaderSettings GetSettings()
        {
            return _thisSettings;
        }

        public IOrganizationService GetOrgService()
        {
            return _orgService;
        }

        private DataUploaderSettings UpdateSettings(DataUploaderSettings thisSettings)
        {
            List<string> list = new List<string>();
            string empty = string.Empty;
            QueryExpression queryExpression = new QueryExpression();
            queryExpression.EntityName = "ddsm_datauploader";
            queryExpression.ColumnSet = new ColumnSet("ddsm_name", "ddsm_jsondata", "ddsm_settings", "ddsm_recalculationtype", "ddsm_shortname", "ddsm_settings", "ddsm_usersettings");
            QueryExpression queryExpression2 = queryExpression;
            queryExpression2.Criteria = new FilterExpression(LogicalOperator.And);
            queryExpression2.Criteria.AddCondition(new ConditionExpression("statecode", ConditionOperator.Equal, "Active"));
            queryExpression2.Criteria.AddCondition("ddsm_datauploaderid", ConditionOperator.Equal, thisSettings.TargetEntity.Id);
            EntityCollection entityCollection = _orgService.RetrieveMultiple(queryExpression2);
            if (entityCollection == null || entityCollection.Entities.Count <= 0)
            {
                list.Add(Errors.GetTextError(380001));
                Entity entity = new Entity("ddsm_datauploader", thisSettings.TargetEntity.Id);
                entity.Attributes.Add("ddsm_log", string.Join("\n", list));
                _orgService.Update(entity);
                throw new Exception(Errors.GetTextError(380001));
            }
            if (entityCollection[0].Attributes.ContainsKey("ddsm_name"))
            {
                thisSettings.FileName = entityCollection[0].GetAttributeValue<string>("ddsm_name");
            }
            if (entityCollection[0].Attributes.ContainsKey("ddsm_shortname"))
            {
                thisSettings.TypeConfig = entityCollection[0].GetAttributeValue<OptionSetValue>("ddsm_shortname").Value;
            }
            if (entityCollection[0].Attributes.ContainsKey("ddsm_recalculationtype"))
            {
                thisSettings.EspRecalcData = entityCollection[0].GetAttributeValue<OptionSetValue>("ddsm_recalculationtype").Value;
            }
            if (entityCollection[0].Attributes.ContainsKey("ddsm_jsondata"))
            {
                thisSettings.JsonObjects = entityCollection[0].GetAttributeValue<string>("ddsm_jsondata");
            }
            if (entityCollection[0].Attributes.ContainsKey("ddsm_settings"))
            {
                string attributeValue = entityCollection[0].GetAttributeValue<string>("ddsm_settings");
                UploaderSettingsJson uploaderSettingsJson = JsonConvert.DeserializeObject<UploaderSettingsJson>(attributeValue);
                thisSettings.StartRowSheet = (uploaderSettingsJson.ExcelIsHeader ? 1 : 0);
                thisSettings.CallExecuteMultiple = uploaderSettingsJson.CallExecuteMultiple;
                thisSettings.DedupRules = uploaderSettingsJson.DedupRules;
                thisSettings.DuplicateDetect = uploaderSettingsJson.DuplicateDetect;
                thisSettings.GlobalPageRecordsCount = uploaderSettingsJson.GlobalPageRecordsCount;
                thisSettings.GlobalRequestsCount = uploaderSettingsJson.GlobalRequestsCount;
                thisSettings.OrderEntitiesList = ((uploaderSettingsJson.OrderEntitiesList.Length != 0) ? new List<string>(uploaderSettingsJson.OrderEntitiesList) : new List<string>());
                thisSettings.DeduplicationEntitiesList = ((uploaderSettingsJson.DeduplicationEntitiesList.Length != 0) ? new List<string>(uploaderSettingsJson.DeduplicationEntitiesList) : new List<string>());
                thisSettings.ParserRemoteApiUrl = uploaderSettingsJson.ParserRemoteCalculationApiUrl;
                thisSettings.CreatorRemoteApiUrl = uploaderSettingsJson.CreatorRemoteCalculationApiUrl;
                for (int i = 0; i < uploaderSettingsJson.MappingImplementation.Length; i++)
                {
                    thisSettings.MappingImpl.Add(uploaderSettingsJson.MappingImplementation[i].Name, uploaderSettingsJson.MappingImplementation[i].MappingId);
                }
                for (int j = 0; j < uploaderSettingsJson.DmnRules.Length; j++)
                {
                    thisSettings.DmnRules.Add(uploaderSettingsJson.DmnRules[j].Name, new List<string>(uploaderSettingsJson.DmnRules[j].RulesId));
                }
            }
            if (entityCollection[0].Attributes.ContainsKey("ddsm_usersettings"))
            {
                string attributeValue2 = entityCollection[0].GetAttributeValue<string>("ddsm_usersettings");
                UserSettingsJson userSettingsJson = JsonConvert.DeserializeObject<UserSettingsJson>(attributeValue2);
                if (!string.IsNullOrEmpty(userSettingsJson.UserId))
                {
                    thisSettings.UserGuid = new Guid(userSettingsJson.UserId);
                }
                thisSettings.CurrencyGuid = TransactionCurrency.GetCurrency(_orgService, thisSettings.UserGuid);
                if (userSettingsJson.TimezoneCode != -2147483648)
                {
                    try
                    {
                        QueryExpression queryExpression3 = new QueryExpression("timezonedefinition");
                        queryExpression3.ColumnSet = new ColumnSet("timezonecode", "standardname");
                        queryExpression3.Criteria.AddCondition("timezonecode", ConditionOperator.Equal, userSettingsJson.TimezoneCode);
                        Entity entity2 = _orgService.RetrieveMultiple(queryExpression3).Entities.FirstOrDefault();
                        thisSettings.TimeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(entity2.Attributes["standardname"].ToString());
                    }
                    catch (Exception)
                    {
                        string timeZoneByCrmConnection = GetTimeZoneByCrmConnection(_orgService, thisSettings.UserGuid);
                        thisSettings.TimeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(timeZoneByCrmConnection);
                    }
                }
                else
                {
                    string timeZoneByCrmConnection2 = GetTimeZoneByCrmConnection(_orgService, thisSettings.UserGuid);
                    thisSettings.TimeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(timeZoneByCrmConnection2);
                }
            }
            return thisSettings;
        }

        private string GetTimeZoneByCrmConnection(IOrganizationService orgService, Guid userId)
        {
            string text = "92";
            QueryExpression queryExpression = new QueryExpression(_usersettingsLogicalName);
            queryExpression.ColumnSet = new ColumnSet("systemuserid", "timezonecode");
            queryExpression.Criteria.AddCondition("systemuserid", ConditionOperator.Equal, userId);
            Entity entity = orgService.RetrieveMultiple(queryExpression).Entities.FirstOrDefault();
            if (!(entity?.Attributes.ContainsKey("timezonecode") ?? false))
            {
                return TimeZone.CurrentTimeZone.StandardName;
            }
            text = entity.Attributes["timezonecode"].ToString();
            queryExpression = new QueryExpression("timezonedefinition");
            queryExpression.ColumnSet = new ColumnSet("timezonecode", "standardname");
            queryExpression.Criteria.AddCondition("timezonecode", ConditionOperator.Equal, text);
            Entity entity2 = orgService.RetrieveMultiple(queryExpression).Entities.FirstOrDefault();
            return entity2.Attributes["standardname"].ToString();
        }
    }
}