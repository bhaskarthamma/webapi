﻿namespace DmnEngineApp.Core.DmnExpression
{
    internal interface IExpressionManager
    {
        bool GetResult();
    }
}