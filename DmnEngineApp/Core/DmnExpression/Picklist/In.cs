﻿using System.Linq;

namespace DmnEngineApp.Core.DmnExpression.Picklist
{
    internal class In : PicklistExpression
    {
        public In(string op, string sourceValue, string[] exprValue)
            : base(op, sourceValue, exprValue)
        {
        }

        protected override bool Evaluate()
        {
            var result = _exprValue.Contains(_sourceValue);
            return result;
        }
    }
}