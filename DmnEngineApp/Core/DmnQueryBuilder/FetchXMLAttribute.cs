﻿using System.Xml.Serialization;

namespace DmnEngineApp.Core.DmnQueryBuilder
{
    public class FetchXMLAttribute
    {
        [XmlAttribute("name")]
        public string Name { get; set; }

        [XmlAttribute("alias")]
        public string Alias { get; set; }

        [XmlAttribute("aggregate")]
        public string Aggregate { get; set; }
    }
}