﻿namespace DmnEngineApp.Core.Dto
{
    public class OutputClauseDto
    {
        public string Id { get; set; }

        public string Label { get; set; }

        public string Name { get; set; }

        public string TypeRef { get; set; }

        public string Value { get; set; }
    }
}