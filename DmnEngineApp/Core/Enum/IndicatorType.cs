﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DmnEngineApp.Core.Enum
{
    public enum IndicatorType
    {
        Decimal = 962080000, Money = 962080001, Integer = 962080002, DateTime = 962080003, Picklist = 962080004, String = 962080005, Lookup = 962080006, Memo = 962080007
    }
}
