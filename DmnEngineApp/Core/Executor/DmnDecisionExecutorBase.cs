﻿using System.Collections.Generic;
using System.Linq;
using DmnEngineApp.Core.Executor.Function;
using DmnEngineApp.Core.Interfaces;
using DmnEngineApp.Dto;
using DmnEngineApp.Service;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Metadata;

namespace DmnEngineApp.Core.Executor
{
    internal class DmnDecisionExecutorBase : IDmnDecisionExecutorBase
    {
        private static string _defaultExprOperator;

        private readonly DataService _dataService;

        private IExecutor _executor;

        /// <summary>
        ///     ctor
        /// </summary>
        /// <param name="targetFullFieldName"></param>
        public DmnDecisionExecutorBase(DmnDto dmnDto, string dataType, string outputFullPath)
        {
            _orgService = dmnDto.CrmDto.OrgService;
            _targetEntity = dmnDto.TargetEntity;
            _dataService = dmnDto.DataService;

            InitOutput(outputFullPath);
            switch (dataType.ToLower())
            {
                case "integer":
                    _executor = new Function.Integer.Executor();
                    break;
                case "decimal":
                    _executor = new Function.Decimal.Executor();
                    break;
                case "string":
                    _executor = new Function.String.Executor();
                    break;
                case "money":
                    _executor = new Function.Money.Executor();
                    break;
                case "datetime":
                    _executor = new Function.DateTime.Executor();
                    break;
                case "picklist":
                    _executor = new Function.Picklist.Executor();
                    break;
                case "outputvalue":
                    _executor = new Function.OutputValue.Executor();
                    break;
            }
        }

        /// <summary>
        /// </summary>
        public Entity _outputEntity { get; set; }

        /// <summary>
        /// </summary>
        public string _outputField { get; set; }

        /// <summary>
        /// </summary>
        //private  Entity _targetEntity { get; set; }
        private Entity _targetEntity { get; }


        private IOrganizationService _orgService { get; }

        public string Run(string expression, DmnDto dmnDto)
        {
            var expFunction = GetExprFunction(expression);
            var expArgs = GetExprArgs(expression, dmnDto.CrmDto.OrgService);
            if (expFunction == string.Empty) return expArgs;
            var arg = ConvertArgsToArray(expArgs);

            return Run(expFunction, arg, dmnDto);
        }

        /// <summary>
        /// </summary>
        /// <param name="function"></param>
        /// <param name="args"></param>
        public string Run(string function, string[] args, DmnDto dmnDto)
        {
            //if we are working with relationships
            CheckRelationFunction(function);
            var dmnFunction = new DmnFunction(dmnDto);
            var rs = _executor.PerformOperation(function, args, dmnFunction);
            return rs;
        }

        private void CheckRelationFunction(string function)
        {
            var relFunctions = new List<string>
            {
                "count",
                "rel_max",
                "rel_min",
                "rel_avg",
                "sum"
            };
            if (!relFunctions.Contains(function.ToLower()))
                return;
            _executor = new Function.Relationship.Executor();
        }

        private void InitOutput(string outputFullPath)
        {
            //the path is empty
            if (string.Empty == outputFullPath)
            {
                _outputField = string.Empty;
                _outputEntity = null;
                return;
            }
            //only field
            if (outputFullPath.IndexOf('.') == -1)
            {
                _outputField = outputFullPath;
            }
            else
            {
                var pathArr = outputFullPath.Split('.').ToArray();
                _outputField = pathArr.LastOrDefault();
                GetRelatedEntity(_targetEntity, pathArr.Skip(1).ToArray(), _orgService);
            }
        }

        private void GetRelatedEntity(Entity targetEntity, string[] entityPath, IOrganizationService orgService,
            int entityIndex = 0
        )
        {
            if (entityPath.Length <= entityIndex + 1)
            {
                _outputEntity = targetEntity;
                return;
            }
            var link = entityPath[entityIndex];
            entityIndex++;
            object lookUp = null;
            if (!targetEntity.Attributes.TryGetValue(link, out lookUp))
                GetRelatedEntity(targetEntity, entityPath, orgService, entityIndex);
            var relatedEntity = (EntityReference) lookUp;
            var output = _dataService.RetreiveEntity(relatedEntity.LogicalName, relatedEntity.Id);

            GetRelatedEntity(output, entityPath, orgService, entityIndex);
        }


        /// <summary>
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        private string[] ConvertArgsToArray(string args)
        {
            return args.IndexOf(',') != -1 ? args.Split(',') : new[] {args};
        }

        /// <summary>
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        private string GetExprFunction(string expression)
        {
            return expression.IndexOf('^') != -1 ? expression.Split('^')[0] : string.Empty;
        }

        /// <summary>
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        private string GetExprArgs(string expression, IOrganizationService Service)
        {
            return expression.IndexOf('^') != -1 ? expression.Split('^')[1] : GetSourceValue(expression, Service);
        }


        private string GetSourceValue(string fullFieldName, IOrganizationService Service)
        {
            var fieldParts = fullFieldName.Split('.');

            if (fieldParts.First() != _targetEntity.LogicalName)
                return null;

            //remove base entity name from array
            fieldParts = fieldParts.Skip(1).ToArray();

            var attName = fieldParts.FirstOrDefault();

            var baseEntity = _dataService.RetreiveEntity(_targetEntity.LogicalName, _targetEntity.Id);

            //get first field
            object firstFieldValue = null;

            var attMetadata = _dataService.GetAttributeMetadata(baseEntity.LogicalName, attName).FirstOrDefault();

            baseEntity.Attributes.TryGetValue(attName, out firstFieldValue);
            if (firstFieldValue == null)
                return GetEmptyResultByType(attMetadata);

            if (firstFieldValue is OptionSetValue)
                return GetOptionSetValue(attName, firstFieldValue);


            if (firstFieldValue.GetType() == typeof(Money))
                return (firstFieldValue as Money).Value.ToString();


            //if field is not lookup
            if (firstFieldValue.GetType() != typeof(EntityReference))
                return firstFieldValue.ToString();

            //if field is lookup
            object lookupValue = null;
            baseEntity.Attributes.TryGetValue(attName, out lookupValue);
            var resultValue = GetLookupInnerValue(lookupValue as EntityReference, fieldParts.Skip(1).ToArray(), Service);
            return GetRS(resultValue);
        }

        private string GetRS(object value)
        {
            if (value == null)
                return "0";
            if (value.GetType() == typeof(Money))
                return (value as Money).Value.ToString();
            return value.ToString();
        }

        private string GetEmptyResultByType(AttributeMetadata attrType)
        {
            var defaultNumericRs = "0";
            switch (attrType.AttributeType)
            {
                case AttributeTypeCode.Integer:
                    return defaultNumericRs;
                case AttributeTypeCode.Decimal:
                    return defaultNumericRs;
                case AttributeTypeCode.Double:
                    return defaultNumericRs;
                case AttributeTypeCode.Money:
                    return defaultNumericRs;
                case AttributeTypeCode.String:
                    return string.Empty;
                case AttributeTypeCode.Boolean:
                    return "false";
                case AttributeTypeCode.DateTime:
                    return string.Empty;
                case AttributeTypeCode.Picklist:
                    return string.Empty;
                case AttributeTypeCode.Memo:
                    return string.Empty;
            }
            return string.Empty;
        }

        private string GetOptionSetValue(string attName, object fieldValue)
        {
            var attMetadata = _dataService.GetAttributeMetadata(_targetEntity.LogicalName, attName).FirstOrDefault();
            var optionSetKey = (fieldValue as OptionSetValue).Value;
            return GetOptionSetValueLabel(optionSetKey, attMetadata);
        }


        private static string GetOptionSetValueLabel(int optionSetKey, AttributeMetadata fieldMetadata)
        {
            var attMetadata = (EnumAttributeMetadata) fieldMetadata;

            if (attMetadata == null)
                return optionSetKey.ToString();


            return
                attMetadata.OptionSet.Options.Where(x => x.Value == optionSetKey)
                    .FirstOrDefault()
                    .Label.UserLocalizedLabel.Label;
        }

        private object GetLookupInnerValue(EntityReference lookup, string[] fields, IOrganizationService Service)
        {
            if (lookup == null)
                return null;

            object value = null;

            var entity = _dataService.RetreiveEntity(lookup.LogicalName, lookup.Id);
            if (!entity.Attributes.TryGetValue(fields.First(), out value))
                return null;

            if (value is EntityReference)
                value = GetLookupInnerValue(value as EntityReference, fields.Skip(1).ToArray(), Service);

            return value;
        }
    }
}