﻿using DmnEngineApp.Dto;

namespace DmnEngineApp.Core.Executor.Function.DateTime
{
    public class Now : DmnFunction
    {
        public Now(DmnDto dmnDto) : base(dmnDto)
        {
        }

        public string Evaluate(string[] args)
        {
            var now = System.DateTime.Now;
            return now.ToString("MM/dd/yyyy");
        }
    }
}