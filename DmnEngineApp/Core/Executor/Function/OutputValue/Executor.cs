﻿using System;
using DmnEngineApp.Core.Executor.Function.DateTime;
using DmnEngineApp.Core.Executor.Function.Decimal;
using DmnEngineApp.Core.Executor.Function.String;

namespace DmnEngineApp.Core.Executor.Function.OutputValue
{
    internal class Executor : IExecutor
    {
        public string PerformOperation(string op, string[] args, DmnFunction dmnFunction)
        {
            return Execute(op, args, dmnFunction);
        }


        private string Execute(string op, string[] args, DmnFunction dmnFunction)
        {
            try
            {
                switch (op)
                {
                    case "addition":
                        var sum = new Sum(dmnFunction._dmnDto);
                        return sum.Evaluate(args);
                    case "subtraction":
                        var subtraction = new Subtraction(dmnFunction._dmnDto);
                        return subtraction.Evaluate(args);
                    case "multiply":
                        var multiply = new Multiply(dmnFunction._dmnDto);
                        return multiply.Evaluate(args);
                    case "divide":
                        var divide = new Divide(dmnFunction._dmnDto);
                        return divide.Evaluate(args);
                    case "subtractionDates":
                        var subtractionDates = new SubtractionDates(dmnFunction._dmnDto);
                        return subtractionDates.Evaluate(args);
                    case "concat":
                        var concat = new Concat(dmnFunction._dmnDto);
                        return concat.Evaluate(args);
                    case "now":
                        var now = new Now(dmnFunction._dmnDto);
                        return now.Evaluate(args);
                    case "addDays":
                        var addDays = new AddDays(dmnFunction._dmnDto);
                        return addDays.Evaluate(args);
                    case "addMonths":
                        var addMonths = new AddMonths(dmnFunction._dmnDto);
                        return addMonths.Evaluate(args);
                    case "addYears":
                        var addYears = new AddYears(dmnFunction._dmnDto);
                        return addYears.Evaluate(args);
                }
            }

            catch (Exception e)
            {
            }
            return string.Empty;
        }
    }
}