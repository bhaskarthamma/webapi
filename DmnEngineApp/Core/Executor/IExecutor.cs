﻿using DmnEngineApp.Core.Executor.Function;
using DmnEngineApp.Dto;

namespace DmnEngineApp.Core.Executor
{
    public interface IExecutor
    {
        string PerformOperation(string op, string[] args, DmnFunction dmnFunction);
    }
}