﻿using DmnEngineApp.Dto;

namespace DmnEngineApp.Core.Interfaces
{
    public interface IDmnDecisionExecutorBase
    {
        string Run(string expression, DmnDto dmnDto);
    }
}