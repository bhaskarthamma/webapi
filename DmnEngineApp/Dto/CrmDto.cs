﻿using Microsoft.Xrm.Sdk;

namespace DmnEngineApp.Dto
{
    public class CrmDto
    {
        //Provides programmatic access to the metadata and data for an organization.
        public IOrganizationService OrgService { get; set; }
    }
}