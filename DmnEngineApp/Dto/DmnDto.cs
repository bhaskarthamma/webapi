﻿using System;
using System.Collections.Concurrent;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Metadata;
using ProcessFlowBuilder.Service;
using DataService = DmnEngineApp.Service.DataService;

namespace DmnEngineApp.Dto
{
    public class DmnDto
    {
        public EntityCollection BusinessRules;


        public ConcurrentDictionary<Guid, Entity> EntityCache = new ConcurrentDictionary<Guid, Entity>();

        public ConcurrentDictionary<string, string> EntityDisplayName =
            new ConcurrentDictionary<string, string>();

        public ConcurrentDictionary<string, EntityMetadata> EntityMetadata =
            new ConcurrentDictionary<string, EntityMetadata>();

        public ConcurrentDictionary<Guid, Entity> OutputEntities = new ConcurrentDictionary<Guid, Entity>();

        public PfBuilder PfBuilder;

        public DmnDto()
        {
            TargetEntity = null;

            BusinessRules = new EntityCollection();
            EntityMetadata = new ConcurrentDictionary<string, EntityMetadata>();
            EntityCache = new ConcurrentDictionary<Guid, Entity>();
            EntityDisplayName = new ConcurrentDictionary<string, string>();


            DataService = null;
            ResultDto = new ResultDto();
            OutputEntities = new ConcurrentDictionary<Guid, Entity>();
        }

        public InputArgsDto InputArgs { get; set; }
        public CrmDto CrmDto { get; set; }
        public Entity TargetEntity { get; set; }


        public DataService DataService { get; set; }
        public ResultDto ResultDto { get; set; }
    }
}