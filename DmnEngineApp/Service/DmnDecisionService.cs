﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using DmnEngineApp.Core;
using DmnEngineApp.Core.CalculationHelpers;
using DmnEngineApp.Core.CustomException;
using DmnEngineApp.Core.Dto.Rule;
using DmnEngineApp.Core.Executor;
using DmnEngineApp.Dto;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Metadata;
using ProcessFlowBuilder.Service;
using Serilog;
using SocketClients.Dto;

namespace DmnEngineApp.Service
{
    internal class DmnDecisionService
    {
        private readonly DataService _dataService;
        private readonly DmnDto _dmnDto;
        private readonly DmnService _dmnService;

        public DmnDecisionService(DmnDto dmnDto)
        {
            _dmnDto = dmnDto;
            _dmnService = new DmnService(dmnDto);
            _dataService = new DataService(dmnDto);
            dmnDto.DataService = _dataService;
        }

        /// <summary>
        ///     Prepare business rules for each records
        /// </summary>
        public void Run()
        {
            try
            {
                //Create web socket message
                var wsMessageService =
                    new WSMessageService(new WsCreateMsgDto
                    {
                        UserId = _dmnDto.InputArgs.UserId,
                        EntityLogicalName = _dmnDto.InputArgs.TargetEntityName
                    });
                var messageStart =
                    $"Start calculate";
                if (!_dmnDto.InputArgs.IsExecuteRule)
                    wsMessageService.SendMessage(WSMessageService.MessageType.INFORMATION, messageStart,
                        WSMessageService.CalcMessageStatus.START);

                foreach (var recordId in _dmnDto.InputArgs.RecordIds)
                {
                    //Init target entity
                    InitTargetEntityReference(recordId);

                    if (_dmnDto.TargetEntity == null)
                        continue;

                    PrepareBusinessRules();
                }
                //if business rule is not "EXECUTOR" need update entity with calculated value
                if (!_dmnDto.InputArgs.IsExecuteRule)
                {
                    var messageEnd =
                        $"Finish calculate.";
                    wsMessageService.SendMessage(WSMessageService.MessageType.SUCCESS, messageEnd,
                        WSMessageService.CalcMessageStatus.FINISH);
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }


        private void UpdateCrmDb()
        {
            var outputEntities = _dmnDto.OutputEntities;
            var start = 0;
            var offset = 100;
            var counter = 1;
            var tasks = new List<Task>();
            if (outputEntities.Count > offset)
            {
                counter = outputEntities.Count / offset;
                var remainder = outputEntities.Count % offset;
                if (remainder > 0)
                    counter++;
            }
            for (var i = 0; i < counter; i++)
            {
                var btachEntities = outputEntities.Skip(start).Take(offset).ToList();
                var bufTask = Task.Factory.StartNew(() => _dataService.UpdateRequest(btachEntities));
                tasks.Add(bufTask);
                start += offset;
            }
            Task.WaitAll(tasks.ToArray(), -1);
            _dmnDto.OutputEntities.Clear();
        }


        /// <summary>
        ///     Init targer entity
        /// </summary>
        private void InitTargetEntityReference(string recordId)
        {
            try
            {
                var entityLogicalName = _dmnDto.InputArgs.TargetEntityName;
                var targetEntity = _dataService.RetreiveEntity(entityLogicalName, recordId);
                if (targetEntity == null)
                    return;
                _dmnDto.TargetEntity = targetEntity;
            }
            catch (Exception e)
            {
                Log.Error(e, "InitTargetEntityReference");
            }
        }

        /// <summary>
        ///     Get lisf of business rules and start execution decisions
        /// </summary>
        private void PrepareBusinessRules()
        {
            //ger business rules ids
            var listBusinessRulesId = _dmnDto.InputArgs.BusinessRuleIds;
            //ger business rules guids
            var listBusinessRulesGuid = _dataService.ConvertListIdToListGuid(listBusinessRulesId);
            //get business rules object
            var businessRules = _dmnService.GetDmnSchema(listBusinessRulesGuid);

            //process flow service
            _dmnDto.PfBuilder = new PfBuilder(_dmnDto.InputArgs.UserId, _dmnDto.CrmDto.OrgService,
                _dmnDto.TargetEntity, listBusinessRulesGuid, _dmnDto.InputArgs.MilestoneId,
                _dmnDto.InputArgs.IsMilestoneProcessFlow);
            _dmnDto.PfBuilder.CreateHistory();

            if (businessRules.Entities.Count == 0)
            {
                Log.Information("Business rules is Empty!");
                _dmnDto.PfBuilder.UpdateStepStatus(_dmnDto.InputArgs.MilestoneId,
                    ProcessFlowBuilder.Service.DataService.RecordStatus.Active);
                return;
            }

            _dmnDto.BusinessRules = businessRules;

            //execute business rules
            ExecuteBusinessRules();
        }

        /// <summary>
        ///     Execute each business rules
        /// </summary>
        private void ExecuteBusinessRules()
        {
            var tmpbusinessRule = new Guid();
            try
            {
                var businessRules = _dmnDto.BusinessRules;


                _dmnDto.PfBuilder.UpdateStepStatus(_dmnDto.InputArgs.MilestoneId,
                    ProcessFlowBuilder.Service.DataService.RecordStatus.InProgress);

                //loop in business rules
                foreach (var businessRule in businessRules.Entities)
                {
                    tmpbusinessRule = businessRule.Id;
                    _dmnDto.PfBuilder.UpdateBusinessRuleStatus(businessRule.Id,
                        ProcessFlowBuilder.Service.DataService.RecordStatus.Active);
                    //create ws message
                    var wsMessageService =
                        new WSMessageService(new WsCreateMsgDto
                        {
                            UserId = _dmnDto.InputArgs.UserId,
                            EntityLogicalName = _dmnDto.InputArgs.TargetEntityName,
                            RecordId = _dmnDto.TargetEntity.Id
                        });
                    var message = string.Empty;

                    if (!_dmnDto.InputArgs.IsExecuteRule)
                    {
                        object businessRuleDescription = null;
                        if (businessRule.Attributes.TryGetValue("ddsm_shortdescription", out businessRuleDescription))
                            message = businessRuleDescription.ToString();

                        wsMessageService.SendMessage(WSMessageService.MessageType.INFORMATION, message,
                            WSMessageService.CalcMessageStatus.START);
                    }
                    //get decisions from the decision table
                    var decisions = _dmnService.GetDecisions(businessRule);

                    if (decisions == null)
                    {
                        _dmnDto.PfBuilder.UpdateBusinessRuleStatus(businessRule.Id,
                            ProcessFlowBuilder.Service.DataService.RecordStatus.Done);
                        continue;
                    }

                    //execute decisions
                    ExecuteDecision(decisions);

                    if (!_dmnDto.InputArgs.IsExecuteRule)
                    {
                        UpdateCrmDb();
                        wsMessageService.SendMessage(WSMessageService.MessageType.SUCCESS, message,
                            WSMessageService.CalcMessageStatus.FINISH);


                        _dmnDto.PfBuilder.UpdateBusinessRuleStatus(businessRule.Id,
                            ProcessFlowBuilder.Service.DataService.RecordStatus.Done);
                    }
                }
                _dmnDto.PfBuilder.UpdateStepStatus(_dmnDto.InputArgs.MilestoneId,
                    ProcessFlowBuilder.Service.DataService.RecordStatus.Active);

                //run call back function
                ExecuteCallBackWorkflow();
            }
            catch
                (Exception ex)
            {
                var wsMessageService =
                    new WSMessageService(new WsCreateMsgDto
                    {
                        UserId = _dmnDto.InputArgs.UserId,
                        EntityLogicalName = _dmnDto.InputArgs.TargetEntityName,
                        RecordId = _dmnDto.TargetEntity.Id
                    });

                _dmnDto.PfBuilder.UpdateBusinessRuleStatus(tmpbusinessRule,
                    ProcessFlowBuilder.Service.DataService.RecordStatus.Error);

                wsMessageService.SendMessage(WSMessageService.MessageType.ERROR, ex.Message,
                    WSMessageService.CalcMessageStatus.START);

                Log.Error(ex, "ExecuteBusinessRules");
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        ///     Run decision
        /// </summary>
        /// <param name="outputEntities"></param>
        /// <param name="decision"></param>
        private void RunDecision(ConcurrentDictionary<Guid, Entity> outputEntities, OutputEntryDto decision)
        {
            try
            {
                //get base executor
                var dmnDecisionExecutor = new DmnDecisionExecutorBase(_dmnDto, decision.DataType,
                    decision.OutputFullPath);

                //get output entity
                var outputEntity = dmnDecisionExecutor._outputEntity;

                var result = string.Empty;

                //check output function
                if (DmnAction.SetValue.ToString().ToLower() ==
                    GetExprFunction(decision.Value.TrimStart('{').TrimEnd('}')))
                {
                    result = ExecuteDecision(decision.Value.TrimStart('{').TrimEnd('}'), _dmnDto);
                }
                else if (DmnAction.ExecuteDMN.ToString().ToLower() ==
                         GetExprFunction(decision.Value.TrimStart('{').TrimEnd('}')))
                {
                    var linkName = GetExprArgs(decision.Value.TrimStart('{').TrimEnd('}'), _dmnDto);
                    CreateDmnJobs(linkName);
                    return;
                }
                else if (DmnAction.OutputValue.ToString().ToLower() ==
                         GetExprFunction(decision.Value.TrimStart('{').TrimEnd('}')))
                {
                    var expression = decision.Value;
                    var formula = string.Empty;
                    if (expression.IndexOf('^') != -1)
                    {
                        var args = expression.Split('^');
                        for (var i = 1; i < args.Length; i++)
                            formula += args[i] + "^";
                        formula = formula.TrimEnd('^');
                        formula = formula.TrimStart('{').TrimEnd('}');
                        if (_dataService.IsBool(formula))
                        {
                            _dmnDto.ResultDto.Result = formula;
                            return;
                        }

                        formula = "{{" + formula + "}}";
                    }
                    var formulaCalculator = new FormulaCalculator(dmnDecisionExecutor);
                    result = formulaCalculator.Calculate(formula, decision.DataType, _dmnDto);
                    _dmnDto.ResultDto.Result = result;
                    return;
                }
                else if (DmnAction.StopBusinessRule.ToString().ToLower() ==
                         GetExprFunction(decision.Value.TrimStart('{').TrimEnd('}')))
                {
                    var message = string.Empty;
                    var expression = decision.Value.TrimStart('{').TrimEnd('}');
                    if (expression.IndexOf('^') != -1)
                    {
                        var args = expression.Split('^');
                        message = args[1];
                    }
                    throw new Exception(message);
                }
                else
                {
                    var formulaCalculator = new FormulaCalculator(dmnDecisionExecutor);
                    result = formulaCalculator.Calculate(decision.Value, decision.DataType, _dmnDto);
                }

                var entity = new Entity(outputEntity.LogicalName, outputEntity.Id);
                if (!outputEntities.ContainsKey(entity.Id))
                    outputEntities.TryAdd(outputEntity.Id, entity);

                var tmpOut = new Entity();

                if (!outputEntities.TryGetValue(outputEntity.Id, out tmpOut))
                    return;
                //update entity
                UpdateEntity(tmpOut, dmnDecisionExecutor._outputField, result);
                //update cahe
                UpdateCacheData(entity, dmnDecisionExecutor._outputField, result);
            }
            catch (Exception e)
            {
                Log.Error(e, "RunDecision");
                throw new Exception(e.Message);
            }
        }

        /// <summary>
        ///     Update Cached Data
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="attr"></param>
        /// <param name="value"></param>
        public void UpdateCacheData(Entity entity, string attr, string value)
        {
            if (!_dmnDto.EntityCache.ContainsKey(entity.Id))
                _dmnDto.EntityCache.TryAdd(entity.Id, entity);

            var tmpOut = new Entity();

            if (!_dmnDto.EntityCache.TryGetValue(entity.Id, out tmpOut))
                return;

            UpdateEntity(tmpOut, attr, value);
        }

        /// <summary>
        ///     Execute decisions
        /// </summary>
        /// <param name="decisions"></param>
        /// <param name="businessRule"></param>
        private void ExecuteDecision(List<OutputEntryDto> decisions)
        {
            try
            {
                //cache  output entities
                var outputEntities = _dmnDto.OutputEntities;


                //execute decisions
                foreach (var decision in decisions)
                {
                    if (decision == null) continue;
                    RunDecision(outputEntities, decision);
                }
            }
            catch (Exception e)
            {
                Log.Error(e, "ExecuteDecision");
                throw new Exception(e.Message);
            }
        }

        /// <summary>
        ///     Update entity
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="attr"></param>
        /// <param name="value"></param>
        private void UpdateEntity(Entity entity, string attr, string value)
        {
            try
            {
                if (entity == null || string.IsNullOrEmpty(attr))
                    return;
                var attMetadata = _dataService.GetAttributeMetadata(entity.LogicalName, attr).FirstOrDefault();
                if (attMetadata == null)
                    return;
                var attrType = attMetadata.AttributeType;

                if (attrType == null)
                    return;

                switch (attrType)
                {
                    case AttributeTypeCode.Integer:
                        entity.Attributes[attr] = (int) Math.Round(double.Parse(value));
                        break;
                    case AttributeTypeCode.Decimal:
                        decimal amount;
                        decimal.TryParse(value, NumberStyles.Any, CultureInfo.InvariantCulture, out amount);
                        entity.Attributes[attr] = amount;
                        break;
                    case AttributeTypeCode.Double:
                        entity.Attributes[attr] = double.Parse(value);
                        break;
                    case AttributeTypeCode.Money:
                        entity.Attributes[attr] = new Money(decimal.Parse(value));
                        break;
                    case AttributeTypeCode.String:
                        entity.Attributes[attr] = value.GetType() == typeof(string) ? value : value;
                        break;
                    case AttributeTypeCode.Boolean:
                        entity.Attributes[attr] = bool.Parse(value);
                        break;
                    case AttributeTypeCode.DateTime:
                        DateTime? date = null;
                        if (value == "''" || string.IsNullOrEmpty(value))
                        {
                            entity.Attributes[attr] = date;
                            break;
                        }
                        date = DateTime.Parse(value);
                        entity.Attributes[attr] = date;
                        break;
                    case AttributeTypeCode.Picklist:
                        if (value == "''")
                        {
                            entity.Attributes[attr] = null;
                            break;
                        }
                        var key = GetPickListKeyByValue(value, attMetadata);
                        var option = new OptionSetValue(key);
                        entity.Attributes[attr] = option;
                        break;
                    case AttributeTypeCode.Memo:
                        entity.Attributes[attr] = value.GetType() == typeof(string) ? value : value;
                        break;
                    case AttributeTypeCode.Lookup:
                        var lookUpMetadata = (LookupAttributeMetadata) attMetadata;
                        var targets = lookUpMetadata.Targets;
                        if (targets.Length != 0)
                            entity.Attributes[attr] = new EntityReference(targets[0], new Guid(value));
                        break;
                }
            }
            catch (FormatException e)
            {
                var wsMessageService =
                    new WSMessageService(new WsCreateMsgDto
                    {
                        UserId = _dmnDto.InputArgs.UserId,
                        EntityLogicalName = _dmnDto.InputArgs.TargetEntityName,
                        RecordId = _dmnDto.TargetEntity.Id
                    });

                wsMessageService.SendMessage(WSMessageService.MessageType.ERROR, e.Message,
                    WSMessageService.CalcMessageStatus.START);
                var message = $"{e.Message}. {entity.LogicalName},{entity.Id},{attr}={value}";
                Log.Error(e, $"{message}");
                throw new FormatException(e.Message);
            }
        }


        private int GetPickListKeyByValue(string value, AttributeMetadata fieldMetadata)
        {
            try
            {
                var attMetadata = (EnumAttributeMetadata) fieldMetadata;
                var optionMetadata = attMetadata.OptionSet.Options.FirstOrDefault();
                var key = optionMetadata.Value;

                var firstOrDefault = attMetadata.OptionSet.Options
                    .FirstOrDefault(x => x.Label.UserLocalizedLabel.Label == value);

                if (firstOrDefault != null)
                    key = firstOrDefault.Value;

                return (int) key;
            }
            catch (Exception)
            {
                throw new ExecutorException();
            }
        }

        /// <summary>
        ///     Execute decision
        /// </summary>
        /// <param name="decision"></param>
        /// <param name="dmnDecisionExecutor"></param>
        private string ExecuteDecision(string decision, DmnDto dmnDto)
        {
            return GetExprArgs(decision, dmnDto);
        }

        /// <summary>
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        private static string[] ConvertArgsToArray(string args)
        {
            return args.IndexOf(',') != -1 ? args.Split(',') : new[] {args};
        }

        /// <summary>
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        private static string GetExprFunction(string expression)
        {
            return expression.IndexOf('^') != -1 ? expression.Split('^')[0].ToLower() : expression.ToLower();
        }

        /// <summary>
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        private string GetExprArgs(string expression, DmnDto dmnDto)
        {
            return expression.IndexOf('^') != -1
                ? GetSourceValue(expression.Split('^')[1], dmnDto)
                : expression.Split('^')[1];
            //return expression.IndexOf('^') != -1 ? expression.Split('^')[1] : expression;
        }


        public string GetSourceValue(string fullFieldName, DmnDto dmnDto)
        {
            var targetEntity = dmnDto.TargetEntity;
            var fieldParts = fullFieldName.Split('.');

            if (fieldParts.Length == 1)
                return fieldParts.FirstOrDefault();

            if (fieldParts.First() != targetEntity.LogicalName)
                return null;

            //remove base entity name from array
            fieldParts = fieldParts.Skip(1).ToArray();

            var baseEntity = _dataService.RetreiveEntity(targetEntity.LogicalName, targetEntity.Id);
            var attMetadata =
                _dataService.GetAttributeMetadata(baseEntity.LogicalName, fieldParts.FirstOrDefault()).FirstOrDefault();
            //get first field
            object firstFieldValue = null;
            baseEntity.Attributes.TryGetValue(fieldParts.FirstOrDefault(), out firstFieldValue);
            if (firstFieldValue == null)
                return GetEmptyResultByType(attMetadata);

            //if field is not lookup
            if (firstFieldValue.GetType() != typeof(EntityReference))
                return GetRS(firstFieldValue);
            //if field is lookup
            object lookupValue = null;
            baseEntity.Attributes.TryGetValue(fieldParts.First(), out lookupValue);
            var resultValue = GetLookupInnerValue(lookupValue as EntityReference, fieldParts.Skip(1).ToArray());
            return GetRS(resultValue);
        }

        private string GetRS(object value)
        {
            if (value == null)
                return "0";
            if (value.GetType() == typeof(Money))
                return (value as Money).Value.ToString();
            return value.ToString();
        }

        private object GetLookupInnerValue(EntityReference lookup, string[] fields)
        {
            if (lookup == null)
                return null;

            object value = null;

            var entity = _dataService.RetreiveEntity(lookup.LogicalName, lookup.Id);
            entity.Attributes.TryGetValue(fields.First(), out value);

            if (value is EntityReference)
                value = GetLookupInnerValue(value as EntityReference, fields.Skip(1).ToArray());

            return value;
        }

        private string GetEmptyResultByType(AttributeMetadata attrType)
        {
            var defaultNumericRs = "0";
            switch (attrType.AttributeType)
            {
                case AttributeTypeCode.Integer:
                    return defaultNumericRs;
                case AttributeTypeCode.Decimal:
                    return defaultNumericRs;
                case AttributeTypeCode.Double:
                    return defaultNumericRs;
                case AttributeTypeCode.Money:
                    return defaultNumericRs;
                case AttributeTypeCode.String:
                    return string.Empty;
                case AttributeTypeCode.Boolean:
                    return "false";
                case AttributeTypeCode.DateTime:
                    return string.Empty;
                case AttributeTypeCode.Picklist:
                    return string.Empty;
                case AttributeTypeCode.Memo:
                    return string.Empty;
            }
            return string.Empty;
        }

        /// <summary>
        ///     Create DMN jobs
        /// </summary>
        /// <param name="linkName"></param>
        private void CreateDmnJobs(string linkName)
        {
            try
            {
                var args = ConvertArgsToArray(linkName);
                var relService = new RelationshipService(_dmnDto, args[1]);
                var o2M = relService.GetOneToManyRelationshipsMetadata();
                var callBackRecordId = Guid.Empty;
                var m2O = relService.GetManyToOneRelationshipsMetadata();
                var businessRuleId = args[0];


                var o2MId = new List<Guid>();
                var entityLogicalName = string.Empty;
                var entityIdFieldName = string.Empty;
                if (o2M != null)
                    if (o2M.Count() != 0)
                    {
                        foreach (var entity in o2M)
                        {
                            o2MId.Add(entity.Id);
                            entityLogicalName = entity.LogicalName;
                            entityIdFieldName = $"{entityLogicalName}id";
                            callBackRecordId = entity.Id;
                        }


                        _dataService.RetreiveBatchEntities(o2MId, entityLogicalName, entityIdFieldName);

                        foreach (var entity in o2M)
                        {
                            var inputArg = CreateInputArgDto(entity, businessRuleId);
                            var dmnDto = new DmnDto
                            {
                                EntityCache = _dmnDto.EntityCache,
                                EntityMetadata = _dmnDto.EntityMetadata,
                                EntityDisplayName = _dmnDto.EntityDisplayName,
                                OutputEntities = _dmnDto.OutputEntities,
                                CrmDto = _dmnDto.CrmDto,
                                InputArgs = inputArg
                            };

                            var dmnDecisionService = new DmnDecisionService(dmnDto);
                            dmnDecisionService.Run();
                        }
                    }
                if (m2O != null)
                {
                    var inputArg = CreateInputArgDto(m2O, businessRuleId);

                    var dmnDto = new DmnDto
                    {
                        EntityCache = _dmnDto.EntityCache,
                        EntityMetadata = _dmnDto.EntityMetadata,
                        EntityDisplayName = _dmnDto.EntityDisplayName,
                        OutputEntities = _dmnDto.OutputEntities,
                        CrmDto = _dmnDto.CrmDto,
                        InputArgs = inputArg
                    };

                    var dmnDecisionService = new DmnDecisionService(dmnDto);
                    dmnDecisionService.Run();

                    callBackRecordId = m2O.Id;
                }

                _dmnDto.InputArgs.CallBackRecordId = callBackRecordId;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        /// <summary>
        /// </summary>
        private void ExecuteCallBackWorkflow()
        {
            var callBackRecordId = _dmnDto.InputArgs.CallBackRecordId;
            if (callBackRecordId == Guid.Empty)
                callBackRecordId = _dmnDto.TargetEntity.Id;
            if (string.IsNullOrEmpty(_dmnDto.InputArgs.CallBackWorkflowId))
                return;

            var request = new ExecuteWorkflowRequest
            {
                WorkflowId = new Guid(_dmnDto.InputArgs.CallBackWorkflowId),
                EntityId = callBackRecordId
            };
            // Execute the workflow.
            _dmnDto.CrmDto.OrgService.Execute(request);
        }

        private InputArgsDto CreateInputArgDto(Entity entity, string businessRuleId)
        {
            var inputParams = new InputArgsDto {TargetEntityName = entity.LogicalName};
            inputParams.RecordIds.Add(entity.Id.ToString());
            inputParams.BusinessRuleIds.Add(businessRuleId);
            inputParams.UserId = _dmnDto.InputArgs.UserId;
            inputParams.IsExecuteRule = true;
            return inputParams;
        }

        /// <summary>
        ///     Main  DMN Actions
        /// </summary>
        private enum DmnAction
        {
            SetValue,
            ExecuteDMN,
            OutputValue,
            StopBusinessRule
        }
    }
}