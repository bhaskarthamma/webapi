﻿using System;
using System.Collections.Generic;
using System.Linq;
using CoreUtils.Utils;
using CoreUtils.Wrap;
using DmnEngineApp.Service;
using EspCalculation.Options;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;

namespace EspCalculation.App
{
    public class AppCalculateMeasure : BaseApp
    {
        #region internal fields
        private readonly string _mainEntity = "ddsm_measure";

        #endregion
        internal override string StartOperationMsg { get { return "ESP Calculation is started."; } }

        public AppCalculateMeasure(IOrganizationService service, CalculationOptions options, Action callbackAction = null) : base(service, options, callbackAction)
        {
        }

        public override void Run()
        {
            var mtList = CrmHelper.GetMTRefsByMeasure(new List<Guid>() { CalcOptions.Target.Id });

            var userInput = JsonConvert.DeserializeObject<Dictionary<string, string>>(CalcOptions.UserInput);
            //Update mads
            if (mtList?.Entities?.Count > 0)
            {
                UpdateMeasureMads(userInput, mtList?.Entities[0], CalcOptions.TargetDate);
            }
            Log.Debug("After UpdateMeasureMads");
            var calcRequest = CrmHelper.BuildCalculationRequest(mtList, EspConfig.LastToken, CalcOptions.TargetDate);
            var calcResult = EspService.Calculate(calcRequest);

            CrmHelper.UpdateMeasures(calcResult);

            Completed = true;
            Log.Debug("Manual ESP measure calculation finished successfully!");
            Result = JsonConvert.SerializeObject(new { MeasureName = "", MeasureID = CalcOptions.Target?.Id });
            WsMessageService?.SendMessage(WSMessageService.MessageType.SUCCESS, "ESP Calculation is finished.", WSMessageService.CalcMessageStatus.FINISH);
        }

        private void UpdateMeasureMads(Dictionary<string, string> userInputObj, Entity measure, DateTime targetDate)
        {
            Log.Debug("in UpdateMeasureMads");
            Log.Debug("form Data Count: " + userInputObj?.Count);

            try
            {
                var measureForMadUpdate = new Entity(measure.LogicalName, measure.Id);

                var mIndicatorsValues = CrmHelper.GetMIndicatorsValue(measure);
                CrmHelper.ProcessedMeasuresIndicatorsValues.Add(measure.Id, mIndicatorsValues); //store data without actual values, only names and id's for update

                var actualVersion = CrmHelper.GetSMLatestVersion(measure, targetDate);
                var currentMadMapping = CrmHelper.GetMapping(actualVersion?.GetAttributeValue<EntityReference>("ddsm_madmappingid"));

                var request = new ExecuteMultipleRequest()
                {
                    Settings = new ExecuteMultipleSettings()
                    {
                        ReturnResponses = true,
                        ContinueOnError = true,

                    },
                    Requests = new OrganizationRequestCollection()

                };
                //process mad
                if (currentMadMapping != null)
                {
                    foreach (var mad in userInputObj) //?.DataFields
                    {
                        var crmName = currentMadMapping.FirstOrDefault(x => x.Key.ToLower().Equals(mad.Key.ToLower())).Value;
                        if (crmName != null)
                        {
                            //fill measure entity field
                            if (!string.IsNullOrEmpty(crmName.AttrLogicalName))
                            {
                                if (crmName.Entity.Equals(_mainEntity))
                                {
                                    CrmHelper.SetMADAttributeValue(crmName, mad.Value, measureForMadUpdate);
                                }
                            }// fill measure indicator
                            else
                            {
                                var indName = crmName.AttrDisplayName.Trim().ToLower();
                                var indicator = mIndicatorsValues.Entities.FirstOrDefault(x => x
                                    .GetAttributeValue<string>("ddsm_name")
                                    .Trim()
                                    .ToLower()
                                    .Equals(indName));
                                decimal value;
                                decimal.TryParse(mad.Value?.ToString(), out value);
                                if (indicator != null)
                                {
                                    var indicatorRecord = new Entity("ddsm_indicatorvalue", indicator.Id);

                                    switch (crmName.AttrType)
                                    {
                                        case "Money":
                                            indicatorRecord.Attributes["ddsm_amount"] = new Money(value);
                                            break;
                                        case "Integer":
                                        case "Decimal":
                                            indicatorRecord.Attributes["ddsm_value"] = value;
                                            break;
                                    }


                                    request.Requests.Add(new UpdateRequest()
                                    {
                                        Target = indicatorRecord
                                    });
                                }
                                else
                                {
                                    Log.Debug($"Measure Indicator Value with name: {crmName.AttrDisplayName.ToLower()} wasn't found for measure: {measure.Id}. MAD can't be updated.");
                                }
                            }
                        }
                    }
                    UpdateRequest upd = new UpdateRequest { Target = measureForMadUpdate };

                    request.Requests.Add(upd);
                    var resp = (ExecuteMultipleResponse)OrganizationService.Execute(request);
                    resp.ResultToLog();
                }
            }
            catch (Exception e)
            {
                Log.Error(e, "ERROR");
            }
        }

    }
}
