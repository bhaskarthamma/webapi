﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CoreUtils.Utils;
using CoreUtils.Wrap;
using DmnEngineApp.Service;
using EspCalculation.Api;
using EspCalculation.Model;
using EspCalculation.Options;
using EspCalculation.Utils;
using Microsoft.Xrm.Sdk;
using SocketClients.Dto;


namespace EspCalculation.App
{
    public abstract class BaseApp : IBaseApp
    {
        #region Base props
        public bool Completed { get; set; }
        public string Result { get; set; }
        public string Json { get; set; }
        public IOrganizationService OrganizationService { get; set; }

        internal Action Callback;
        internal Tracer Logger;

        internal List<string> ProcessingLog;
        internal CrmHelper CrmHelper;
        internal WSMessageService WsMessageService;
        internal ESPConfig EspConfig;
        internal CalculationOptions CalcOptions;
        internal EspService EspService;

        internal abstract string StartOperationMsg { get; }
        #endregion

        protected BaseApp(IOrganizationService orgService, CalculationOptions options, Action callbackAction, EntityReference target = null)
        {
            CalcOptions = options;
            if (CalcOptions == null)
            {
                Completed = false;
                var msg = "Calculate Options is null.";
                Result = msg;
                Log.Fatal(msg);
                WsMessageService?.SendMessage(WSMessageService.MessageType.ERROR, msg, WSMessageService.CalcMessageStatus.FINISH);
                return;
            }

            if (CalcOptions.TargetDate == DateTime.MinValue)
            {
                CalcOptions.TargetDate = DateTime.UtcNow;
            }

            if (CalcOptions != null)
            {
                WsMessageService = new WSMessageService(new WsCreateMsgDto { UserId = CalcOptions.UserId, EntityLogicalName = CalcOptions?.Target?.LogicalName });
            }
            OrganizationService = orgService;

            Init(callbackAction);
        }

        private void Init(Action callbackAction)
        {
            EspConfig = SettingsProvider.GetConfig<ESPConfig>(OrganizationService);
            EspService = new EspService(EspConfig, OrganizationService);

            Callback = callbackAction;
            Logger = new Tracer();
            CrmHelper = new CrmHelper(OrganizationService);
        }

        public void RunApp()
        {
            WsMessageService?.SendMessage(WSMessageService.MessageType.INFORMATION, string.Format(StartOperationMsg, CalcOptions?.TargetDate), WSMessageService.CalcMessageStatus.START);
            Run();
        }

        public abstract void Run();

        public Task RunAsync()
        {
            return Task.Run(() => RunApp());
        }
    }
}
