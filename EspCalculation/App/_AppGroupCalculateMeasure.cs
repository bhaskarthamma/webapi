﻿using System;
using DDSM.CommonProvider;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using DDSM.SmartMeasureCalculationPlugin.Model;
using DDSM.SmartMeasureCalculationPlugin.Options;
using DDSM.SmartMeasureCalculationPlugin.Utils;
using Microsoft.Xrm.Sdk;
using Newtonsoft.Json;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;
using Serilog;

namespace DDSM.SmartMeasureCalculationPlugin.App
{
    public class _AppGroupCalculateMeasure : BaseApp
    {
        #region Internal Fields
        internal string Url = "";
        internal Dictionary<string, MappingItem> MappingData;
        internal Dictionary<string, MappingItem> QddMappingData;
        internal readonly string MainEntity = "ddsm_measure";
        internal GroupCalculateOptions _options;

        IList<Guid> processedProject = new List<Guid>();
        IList<Guid> processedProgOff = new List<Guid>();
        IList<Guid> processedMasures = new List<Guid>();
        Dictionary<Guid, Dictionary<string, MappingItem>> personalMads = new Dictionary<Guid, Dictionary<string, MappingItem>>();
        Dictionary<Guid, Dictionary<string, MappingItem>> personalQdds = new Dictionary<Guid, Dictionary<string, MappingItem>>();
#if WITH_TASKQ
        UserInputObj2 measureIDs;
#endif


        internal TaskQueue.StatusFileDataUploading CurrentOperationStatus
        {
            get { return TaskQueue.StatusFileDataUploading.ESPRecalculation; }
            set { throw new NotImplementedException(); }
        }
        bool _fromMeasuregrid;
        #endregion

        public _AppGroupCalculateMeasure(string connectionSting, string Json, GroupCalculateOptions options) : base(connectionSting, Json)
        {
            _options = options;
        }

        public _AppGroupCalculateMeasure(Common _objCommon, GroupCalculateOptions options) : base(_objCommon)
        {
            _options = options;
        }

        public override void Run()
        {
            Log.Debug("GroupCalculateMeasure: in ExecuteActivity() ");
            Stopwatch tStopwatch = new Stopwatch();
            tStopwatch.Start();


            DataUploader = GetDataUploaderRef();

            //try
            //{
            //    StaticSocketClient.SendAsync("test", "Start AppGroupCalculateMeasure", SocketChannelEnum.Esp);
            //}
            //catch (Exception e)
            //{
            //    Log.Error(e.Message + e.StackTrace);
            //}
            var measureIDsJson = _options.UserInput;
            Log.Debug("Options JSON: " + JsonConvert.SerializeObject(_options));

            if (!string.IsNullOrEmpty(measureIDsJson))
            {
                measureIDs = JsonConvert.DeserializeObject<UserInputObj2>(measureIDsJson);
                Log.Debug("measureIDsJson != null: " + measureIDsJson);
            }
            EntityReference target = _options.Project;
            ESPConfig espConfig;
            if (_options.Config != null)
            {
                Log.Debug("_options.Config is exist.");
                //_helper.Config = _options.Config;
                espConfig = _options.Config;
            }
            else
            {
                Log.Warning("_options.Config NOT exist. try get config");
                _helper.GetConfig();
                espConfig = new ESPConfig(_helper.Config);
            }

            //if UserInput is empty and target entity is ddsm_project try get measure list by target id 
            if (target?.LogicalName == "ddsm_project" && measureIDs == null)
            {
                measureIDs = _crmHelper.GetMeasureByTarget(target);
            }
            else
            {
                Log.Debug(" measures list is empty ");
            }
            _fromMeasuregrid = _options.FromMeasureGrid;

            try
            {
                //_helper.GetConfig();
                var successMeasCount = 0;
                if (_helper.IsConfigValID(espConfig))
                {
                    #region Get currentAuthenticationResponse

                    if (_helper.IsTokenNeedUpdate())
                    {
                        AuthenticationResponse currentAuthenticationResponse;

                        Url = _helper.GetAuthUrl();

                        using (var serviceRequest = new WebClient())
                        {
                            var responseBytes = serviceRequest.DownloadString(Url);
                            currentAuthenticationResponse =
                                JsonConvert.DeserializeObject<AuthenticationResponse>(responseBytes);
                            if (currentAuthenticationResponse.ResponseHeader.StatusOk)
                            {
                                _helper.Token = currentAuthenticationResponse.ResponseHeader.AuthenticationTokenBytes;
                                _helper.LastRequestReceivedData = currentAuthenticationResponse.ResponseHeader.RequestReceivedDateTime;

                                _helper.UpdateConfig(espConfig);
                            }
                            else
                            {
                                throw new AccessViolationException("Can't get authorization token using esp login details");
                            }
                        }
                    }
                    else
                    {
                        _helper.LastRequestReceivedData = (DateTime)_helper.Config.Attributes["ddsm_requestreceiveddate"];
                        _helper.Token = Convert.FromBase64String(_helper.Config.Attributes["ddsm_esplasttoken"].ToString());
                    }

                    #endregion

                    if (measureIDs?.SmartMeasures != null)
                    {

                        Log.Debug("Meaure count " + measureIDs.SmartMeasures.Count);

                        var allProcessedCount = 0;
                        var GroupCount = espConfig.DataPortion;
                        while (allProcessedCount < measureIDs.SmartMeasures.Count)
                        {
                            var packGroup = new UserInputObj2();
                            packGroup.DataFields = measureIDs.DataFields;
                            packGroup.SmartMeasures = measureIDs.SmartMeasures.Skip(allProcessedCount).Take((int)GroupCount).ToList();
                            allProcessedCount += packGroup.SmartMeasures.Count;

                            var smartMeasureData = GetMeasuresInfo(packGroup.SmartMeasures);
                            var processedMeasures = new Dictionary<Guid, Guid>();
                            if (smartMeasureData?.Entities.Count > 0)
                            {
                                var calcRequest = BuildCalculationRequest(smartMeasureData, processedMeasures, processedProject, processedProgOff, processedMasures, personalMads, personalQdds);
                                //Call calc request
                                CalculationResponse resultCalculationResponse = null;
                                Url = _helper.GetCalculatioUrl();
                                var request = JsonConvert.SerializeObject(calcRequest);

                                var client = new WebClient
                                {
                                    Headers = { [HttpRequestHeader.ContentType] = "application/json" }
                                };
                                var response = client.UploadString(Url, "POST", request);

                                resultCalculationResponse = JsonConvert.DeserializeObject<CalculationResponse>(response);
                                UpdateMeasures(resultCalculationResponse, packGroup, processedMeasures, personalMads, personalQdds, OrganizationService);
                                Log.Debug("After update UpdateMeasures");
                                successMeasCount++;
                            }
                            else
                            {
                                Log.Debug("The project does not have any smart measure for calculation. Collect data for Parent calculation task!");
                            }
                        }
                        // now we getting all measure (not only smart) 
                        GetProjectsByMeasure(measureIDs.SmartMeasures, processedProject, processedProgOff, processedMasures);
                    }
                    else
                    {
                        Log.Error("You have no project for measure calculation");
                    }
                }
                else
                {
                    throw new AccessViolationException("Can't get authorization token using esp login details. Please contact to your administrator.");
                }
                Result = JsonConvert.SerializeObject(new
                {
                    MeasureName = "",
                    MeasureID = "",
                    completecount = successMeasCount
                });

                Completed = true;
                Log.Debug("After Set Complete");
            }
            catch (Exception ex)
            {
                //SetErrorStatusForDA();
                Log.Error("FATAL ERROR: " + ex.Message + ex.StackTrace);
                Result =
                   JsonConvert.SerializeObject(
                        new
                        {
                            MeasureName = "",
                            MeasureID = "",
                            completecount = 0
                        });
                Completed = false;
            }
            finally
            {
#if WITH_TASKQ
                processCollectedData(processedProject);
                Result = JsonConvert.SerializeObject(new UserInputObj2 { SmartMeasures = processedProject.ToList() });
                tStopwatch.Stop();
                Log.Debug("Stop AppGroupCalculateMeasure! Elapsed: " + tStopwatch.ElapsedMilliseconds + " ms");
                //StaticSocketClient.SendAsync(_options.UserId.ToString(), "Stop AppGroupCalculateMeasure! Elapsed: " +tStopwatch.ElapsedMilliseconds + " ms", SocketChannelEnum.Esp);
#endif
                UpdateDACurrentOperationStatus();
            }
        }

        #region Internal methods



        private void GetProjectsByMeasure(List<Guid> IDs, IList<Guid> processedProject, IList<Guid> processedProgOff, IList<Guid> processedMasures)
        {
            try
            {
                Log.Debug("in GetProjectsByMeasure");
                // clear 
                var measureIds = IDs?.Where(x => !x.Equals(Guid.Empty)).ToList();
                if (IDs != null)
                {
                    Log.Debug("IDs count: " + IDs.Count);
                    Log.Debug("measureIds count: " + measureIds.Count);
                }

                var expr = new QueryExpression
                {
                    EntityName = MainEntity,
                    ColumnSet = new ColumnSet("ddsm_projecttomeasureid", "ddsm_programofferingsid"),
                };
                Log.Debug("after init expr");

                // Guid calcTypeId = _crmHelper.GetMeasureCalcType();

                expr.Criteria = new FilterExpression
                {
                    FilterOperator = LogicalOperator.And,
                    Conditions =
                    {
                        new ConditionExpression("ddsm_measureid", ConditionOperator.In, measureIds),
                    }
                };


                var measures = OrganizationService.RetrieveMultiple(expr);
                Log.Debug("after RetrieveMultiple(expr)");

                foreach (var meas in measures.Entities)
                {
                    collectProcessedData(meas, processedProject, processedProgOff, processedMasures);
                }
            }
            catch (Exception e)
            {
                Log.Debug("Erorr in GetProjectsByMeasure()" + e.Message + e.StackTrace);
            }
        }

#if WITH_TASKQ
        private void processCollectedData(IList<Guid> processedProject)
        {
            var taskQueue = new TaskQueue(OrganizationService);

            if (processedProject.Count > 0)
            {
                taskQueue.Create(new DDSM_Task(TaskQueue.TaskEntity.Project)
                {
                    ProcessedItems0 = processedProject.Select(x => x).Distinct().ToList()
                }, TaskQueue.TaskEntity.Project, dataUploader: DataUploader
                );
            }
        }
#endif

        private CalculationRequest BuildCalculationRequest(EntityCollection measureData, IDictionary<Guid, Guid> processedMeasures, IList<Guid> processedProject, IList<Guid> processedProgOff,
            IList<Guid> processedMasures, IDictionary<Guid, Dictionary<string, MappingItem>> personalMads, IDictionary<Guid, Dictionary<string, MappingItem>> personalQdds)
        {
            var aCalculationRequest = new CalculationRequest { RequestHeader = { AuthenticationTokenBytes = _helper.Token } };
            var processedCount = 0;
            foreach (var measureItem in measureData.Entities)
            {
                var calculatorCalculationRequest = new CalculatorCalculationRequest
                {
                    CalculatorID = new Guid((measureItem.Attributes["ddsm_measuretemplate1.ddsm_espsmartmeasureid"] as AliasedValue).Value.ToString()),
                    TargetDateTime = DateTime.UtcNow
                };
                var curentMad = JsonConvert.DeserializeObject<List<ESPWSVariableDefinitionResponse>>(measureItem.Attributes["ddsm_madfields"].ToString());
                MappingData = _crmHelper.GetMadMapping(measureItem, groupRecalc: true);
                QddMappingData = _crmHelper.GetQddMapping(measureItem, groupRecalc: true);

                personalMads.Add(measureItem.Id, MappingData); //store personal mads to list
                personalQdds.Add(measureItem.Id, QddMappingData);
                // get data
                var data = GetMeasureData(curentMad, MappingData, measureItem.Attributes["ddsm_measureid"].ToString());


                // fill request from DB values
                foreach (var mad in curentMad)
                {
                    Log.Debug("--" + mad.Name.ToLower());
                    var crmFiled = MappingData.FirstOrDefault(x => x.Key.ToLower().Equals(mad.Name.ToLower()));
                    var variableValueRequest = _crmHelper.GenerateESPWSVariableValueRequest(crmFiled, mad, data, MainEntity);

                    if (calculatorCalculationRequest.InputVariables.FirstOrDefault(x => x.Name.Equals(mad.Name)) == null)
                    {
                        calculatorCalculationRequest.InputVariables.Add(variableValueRequest);
                    }
                }
                processedCount++;
#if WITH_TASKQ
                collectProcessedData(data, processedProject, processedProgOff, processedMasures);
#endif
                if (curentMad.Count == calculatorCalculationRequest.InputVariables.Count)
                {
                    aCalculationRequest.CalculatorCalculationRequestList.Add(calculatorCalculationRequest);
                    processedMeasures.Add(new Guid(measureItem.Attributes["ddsm_measureid"].ToString()), calculatorCalculationRequest.CalculatorID);
                }
                else
                {
                    Log.Debug("WARN: " + $"Mapping MAD are not successfully. Measure: {measureItem.Attributes["ddsm_name"]}. Current MAD count: {curentMad.Count}, mapped only: {calculatorCalculationRequest.InputVariables.Count}. Please check MAD mapping for current Smart Measure Template Or contact your administrator.");
                }
            }

            if (aCalculationRequest.CalculatorCalculationRequestList.Count == 0)
            {
                throw new Exception("Can't build Calculation request. No one mapped Smart Measure");
            }
            return aCalculationRequest;
        }

#if WITH_TASKQ
        private void collectProcessedData(Entity data, IList<Guid> processedProject, IList<Guid> processedProgOff, IList<Guid> processedMasures)
        {
            Log.Debug("in collectProcessedData");

            var projRef = data?.GetAttributeValue<EntityReference>("ddsm_projecttomeasureid");
            processedProject?.Add(projRef.Id);

            var progOffRef = data?.GetAttributeValue<EntityReference>("ddsm_programofferingsid");
            processedProgOff?.Add(progOffRef.Id);

            //collect measure ids  
            processedMasures?.Add(data.Id);
        }
#endif

        private Entity GetMeasureData(List<ESPWSVariableDefinitionResponse> curentMad, Dictionary<string, MappingItem> _mappingData, string ID)
        {
            var sortedMadList = (from x in curentMad
                                 join y in _mappingData on x.Name.ToLower() equals y.Key.ToLower()
                                 select new KeyValuePair<string, string>(y.Value.AttrName.ToLower(), y.Value.Entity));

            var sortedMad = new Dictionary<string, string>();
            foreach (var item in sortedMadList)
            {
                if (!sortedMad.ContainsKey(item.Key))
                {
                    sortedMad.Add(item.Key, item.Value);
                }
            }

            var queryMain = new QueryExpression
            {
                EntityName = MainEntity
            };

            var relatedEntity = new RelationshipQueryCollection();

            foreach (var key in sortedMad.Values.GroupBy(x => x))
            {
                //fill measure columns
                if (key.Key.Equals(MainEntity))
                {
                    foreach (var item in sortedMad)
                    {
                        if (!string.IsNullOrEmpty(item.Key) && item.Value == key.Key)
                            queryMain.ColumnSet.AddColumn(item.Key);
                    }
                }//add relates
                else
                {
                    if (key.Key.Equals("ddsm_project"))
                    {
                        var relationship = new Relationship { SchemaName = "ddsm_ddsm_project_ddsm_measure" };

                        var query = new QueryExpression
                        {
                            EntityName = key.Key
                        };

                        foreach (var item in sortedMad)
                        {
                            if (!string.IsNullOrEmpty(item.Key) && item.Value == key.Key)
                                query.ColumnSet.AddColumn(item.Key);
                        }

                        relatedEntity.Add(relationship, query);
                    }
                    if (key.Key.Equals("account"))
                    {
                        var relationship = new Relationship { SchemaName = "ddsm_account_ddsm_measure" };

                        var query = new QueryExpression
                        {
                            EntityName = key.Key
                        };

                        foreach (var item in sortedMad)
                        {
                            if (!string.IsNullOrEmpty(item.Key) && item.Value == key.Key)
                                query.ColumnSet.AddColumn(item.Key);
                        }
                        relatedEntity.Add(relationship, query);

                    }
                    if (key.Key.Equals("ddsm_site"))
                    {
                        var relationship = new Relationship { SchemaName = "ddsm_ddsm_site_ddsm_measure" };

                        var query = new QueryExpression
                        {
                            EntityName = key.Key
                        };

                        foreach (var item in sortedMad)
                        {
                            if (!string.IsNullOrEmpty(item.Key) && item.Value == key.Key)
                                query.ColumnSet.AddColumn(item.Key);
                        }

                        relatedEntity.Add(relationship, query);
                    }
                }
#if WITH_TASKQ
                //get ProjId,POId,
                if (!queryMain.ColumnSet.Columns.Contains("ddsm_projecttomeasureid"))
                    queryMain.ColumnSet.AddColumn("ddsm_projecttomeasureid");

                if (!queryMain.ColumnSet.Columns.Contains("ddsm_programofferingsid"))
                    queryMain.ColumnSet.AddColumn("ddsm_programofferingsid");
#endif

            }

            var request = new RetrieveRequest
            {
                ColumnSet = queryMain.ColumnSet,
                Target = new EntityReference { Id = new Guid(ID), LogicalName = MainEntity },
                RelatedEntitiesQuery = relatedEntity
            };

            var response = (RetrieveResponse)OrganizationService.Execute(request);

            var result = new Dictionary<string, object>();

            foreach (var relEnt in response.Entity.RelatedEntities)
            {
                result.Add(relEnt.Value.EntityName, relEnt.Value.Entities[0].Attributes);
            }
            return response.Entity;
        }

        private EntityCollection GetMeasuresInfo(List<Guid> IDs, bool isSmartMeasure = true)
        {
            // clear 
            var measureIds = IDs.Where(x => !x.Equals(Guid.Empty)).ToList();
            var expr = new QueryExpression
            {
                EntityName = MainEntity,
                ColumnSet = new ColumnSet(allColumns: true)
            };
            expr.LinkEntities.Add(new LinkEntity(MainEntity, "ddsm_measuretemplate", "ddsm_measureselector", "ddsm_measuretemplateid", JoinOperator.Inner)
            {
                LinkEntities = {
                    new LinkEntity("ddsm_measuretemplate","ddsm_mapping","ddsm_mappingid","ddsm_mappingid",JoinOperator.Inner)  { Columns = new ColumnSet("ddsm_jsondata")  },
                    new LinkEntity("ddsm_measuretemplate","ddsm_mapping","ddsm_qddmappingid","ddsm_mappingid",JoinOperator.Inner)  { Columns = new ColumnSet("ddsm_jsondata")  },
                }
            });
            expr.LinkEntities[0].Columns.AddColumns("ddsm_madfields", "ddsm_espsmartmeasureid", "ddsm_mappingid");

            Guid calcTypeId = _crmHelper.GetMeasureCalcType();

            if (_fromMeasuregrid)
            {
                if (isSmartMeasure)
                {
                    expr.Criteria = new FilterExpression
                    {
                        FilterOperator = LogicalOperator.And,
                        Conditions = { new ConditionExpression("ddsm_measureid", ConditionOperator.In, measureIds),
                            new ConditionExpression("ddsm_calculationtype", ConditionOperator.Equal, calcTypeId),
                            new ConditionExpression("ddsm_disablerecalculation", ConditionOperator.NotEqual, true),
                            //   new ConditionExpression("ddsm_recalculatemeasure", ConditionOperator.Equal, true),
                        }
                    };
                }
                else
                {
                    Log.Debug("get data for non smart measure");
                    expr.Criteria = new FilterExpression
                    {
                        FilterOperator = LogicalOperator.And,
                        Conditions = { new ConditionExpression("ddsm_measureid", ConditionOperator.In, measureIds),
                            new ConditionExpression("ddsm_calculationtype", ConditionOperator.NotEqual, calcTypeId),
                            new ConditionExpression("ddsm_disablerecalculation", ConditionOperator.NotEqual, true),
                            new ConditionExpression("ddsm_recalculatemeasure", ConditionOperator.Equal, true),
                        }
                    };
                }
            }
            else
            {
                expr.Criteria = new FilterExpression
                {
                    FilterOperator = LogicalOperator.And,
                    Conditions = { new ConditionExpression("ddsm_projecttomeasureid", ConditionOperator.In, measureIds),
                        new ConditionExpression("ddsm_calculationtype", ConditionOperator.Equal, calcTypeId),
                        new ConditionExpression("ddsm_disablerecalculation", ConditionOperator.NotEqual, true)
                    }
                };
            }

            return OrganizationService.RetrieveMultiple(expr);
        }

        private void UpdateMeasures(CalculationResponse calcResult, UserInputObj2 userInput, IDictionary<Guid, Guid> processedMeasures, Dictionary<Guid, Dictionary<string, MappingItem>> personalMads, Dictionary<Guid, Dictionary<string, MappingItem>> personalQdds, IOrganizationService service)
        {
            Log.Debug("in UpdateMeasures");
            Dictionary<string, MappingItem> QDDmappingData;
            Dictionary<string, MappingItem> MADmappingData;

            var requestWithResults = new ExecuteMultipleRequest()
            {
                // Assign settings that define execution behavior: continue on error, return responses. 
                Settings = new ExecuteMultipleSettings()
                {
                    ContinueOnError = false,
                    ReturnResponses = true
                },
                // Create an empty organization request collection.
                Requests = new OrganizationRequestCollection()
            };

            foreach (var measureCalculation in calcResult.ESPWSCalculatorCalculationList)
            {

                if (!measureCalculation.ResponseHeader.StatusOk)
                {
                    Log.Debug($"Upgrading measure with error. Error code: {calcResult.ResponseHeader.ErrorCode}. \nError: {calcResult.GetFullESPError()}.");
                    continue;
                }
                Entity measureForUpdateOrig;
                Entity measureForUpdate;
                if (userInput.DataFields != null && userInput.DataFields.ContainsKey("ddsm_recalculationgroup") && !string.IsNullOrEmpty(userInput.DataFields["ddsm_recalculationgroup"]))
                {
                    var rrr = processedMeasures.FirstOrDefault(x => x.Value.Equals(measureCalculation.ID));
                    MADmappingData = personalMads.FirstOrDefault(x => x.Key.Equals(rrr.Key)).Value;
                    QDDmappingData = personalQdds.FirstOrDefault(x => x.Key.Equals(rrr.Key)).Value;

                    measureForUpdateOrig = OrganizationService.Retrieve(MainEntity, rrr.Key, new ColumnSet(allColumns: true));
                    processedMeasures.Remove(rrr.Key);
                    measureForUpdate = new Entity(MainEntity);

                    foreach (var attr in measureForUpdateOrig.Attributes)
                    {
                        if (attr.Key != "ddsm_measureid")
                        {
                            measureForUpdate.Attributes.Add(attr);
                        }
                    }
                }
                else
                {
                    var rrr = processedMeasures.FirstOrDefault(x => x.Value.Equals(measureCalculation.ID));
                    MADmappingData = personalMads.FirstOrDefault(x => x.Key.Equals(rrr.Key)).Value;
                    QDDmappingData = personalQdds.FirstOrDefault(x => x.Key.Equals(rrr.Key)).Value;
                    measureForUpdate = new Entity(MainEntity, rrr.Key);
                    processedMeasures.Remove(rrr.Key);
                }

                // set mads
                Log.Debug("fill mads");
                foreach (var result in measureCalculation.ESPWSVariableCalculationList)
                {
                    var mapItem = MADmappingData.FirstOrDefault(x => x.Key.ToLower().Equals(result.Name.ToLower()) && !string.IsNullOrEmpty(x.Value.AttrName));

                    if (mapItem.Value != null && mapItem.Key != null && mapItem.Value.Entity.Equals(MainEntity))
                    {
                        _crmHelper.SetAttributeValue(mapItem.Value, result, measureForUpdate);
                    }
                }
                //set qdds
                foreach (var result in measureCalculation.ESPWSVariableCalculationList)
                {
                    try
                    {
                        var mapItem = QDDmappingData.FirstOrDefault(x => x.Key.ToLower().Equals(result.Name.ToLower()) && !string.IsNullOrEmpty(x.Value.AttrName));

                        if (mapItem.Value != null && mapItem.Key != null && mapItem.Value.Entity.Equals(MainEntity))
                        {
                            _crmHelper.SetAttributeValue(mapItem.Value, result, measureForUpdate);
                        }
                    }
                    catch (Exception ex)
                    {
                        Log.Error(" error in fill qdd: " + ex.Message + ex.StackTrace);
                        throw;
                    }
                }

                var valueOfrecalcGroup = "";

                if (userInput.DataFields != null && userInput.DataFields.TryGetValue("ddsm_recalculationgroup", out valueOfrecalcGroup))
                    measureForUpdate["ddsm_recalculationgroup"] = valueOfrecalcGroup;

                measureForUpdate["ddsm_recalculationdate"] = measureForUpdate["ddsm_esptargetdate"] = DateTime.UtcNow;
                measureForUpdate["ddsm_espcalculationsuccessful"] = true;
                measureForUpdate["ddsm_calculationrecordstatus"] = new OptionSetValue((int)CalculateMeasure.CalculationRecordStatus.NotReady); // ready for calculation of parent

                if (userInput.DataFields != null && !string.IsNullOrEmpty(valueOfrecalcGroup))
                {
                    var createRequest = new CreateRequest { Target = measureForUpdate };
                    requestWithResults.Requests.Add(createRequest);
                }
                else
                {
                    var updateRequest = new UpdateRequest { Target = measureForUpdate };
                    requestWithResults.Requests.Add(updateRequest);
                }
            }


            // Execute all the requests in the request collection using a single web method call.
            ExecuteMultipleResponse responseWithResults = (ExecuteMultipleResponse)service.Execute(requestWithResults);
            if (responseWithResults.IsFaulted)
            {
                foreach (var resp in responseWithResults.Responses)
                {
                    if (resp.Fault != null)
                    {
                        Log.Debug("in ExecuteMultipleResponse: " + resp.Fault.Message);
                    }
                }
            }
        }
        private void UpdateDACurrentOperationStatus()
        {
            Log.Debug("in UpdateDACurrentOperationStatus ");
            //  if (CurrentOperationStatus != null)
            {
                Log.Debug("Current Operation Status: " + CurrentOperationStatus);

                if (DataUploader?.Id != Guid.Empty)
                {
                    OrganizationService.Update(new Entity(DataUploader.LogicalName, DataUploader.Id)
                    {
                        Attributes = new AttributeCollection() { new KeyValuePair<string, object>("ddsm_statusfiledatauploading", new OptionSetValue((int)CurrentOperationStatus)) }
                    });
                }
            }
        }


        private EntityReference GetDataUploaderRef()
        {
            var result = new EntityReference();
            try
            {
                var target = _options.Target;
                if (target.LogicalName != "ddsm_taskqueue")
                    return new EntityReference();

                var taskQueue = OrganizationService.Retrieve(target.LogicalName, target.Id, new ColumnSet("ddsm_datauploader"));
                result = taskQueue?.GetAttributeValue<EntityReference>("ddsm_datauploader");
            }
            catch (Exception ex)
            {
                Log.Debug("Error on GetDataUploaderRef() " + ex.Message);
                return result;
            }
            return result;
        }


        #endregion
    }
}
