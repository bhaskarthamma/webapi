namespace EspCalculation.Model.Enum
{
    public enum CreatorRecordType
    {
        Front = 962080000,
        DataUploader = 962080001,
        Portal = 962080002
    };
}