﻿namespace EspCalculation.Model.Enum
{
    public enum ESPDataType
    {
        ComboboxDoubleDollars0Decimal = 0,
        ComboboxDoubleDollars2Decimal = 1,
        ComboboxDouble0Decimal = 2,
        ComboboxDouble1Decimal = 3,
        ComboboxDouble2Decimal = 4,
        ComboboxDouble3Decimal = 5,
        ComboboxDouble4Decimal = 6,
        ComboboxDouble5Decimal = 7,
        ComboboxDouble6Decimal = 8,
        ComboboxDoubleInteger = 9,//
        ComboboxStringValue = 10,//
        TextboxDoubleDollars0Decimal = 11,//
        TextboxDoubleDollars2Decimal = 12,//
        TextboxDouble0Decimal = 13,//
        TextboxDouble1Decimal = 14,//
        TextboxDouble2Decimal = 15,//
        TextboxDouble3Decimal = 16,//
        TextboxDouble4Decimal = 17,//
        TextboxDouble5Decimal = 18,//
        TextboxDouble6Decimal = 19,//
        TextboxDoubleInteger = 20,//
        TextboxStringValue = 21,//
        TextboxStringDateMMDDYYYY = 22 //
    }
}
