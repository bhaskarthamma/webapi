﻿using System.Runtime.Serialization;

namespace EspCalculation.Model
{
    [DataContract]
    public class RequestHeader
    {
        [DataMember]
        public byte[] AuthenticationTokenBytes { get; set; }
    }
}