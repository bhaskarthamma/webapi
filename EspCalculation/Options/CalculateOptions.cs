using System;

namespace EspCalculation.Options
{
    public class CalculateOptions: BaseCalculationConfig
    {
        public Guid MeasureId { get; set; }
    }
}