﻿using Microsoft.Xrm.Sdk;

namespace DDSM.SmartMeasureCalculationPlugin.Options
{
    public class RecalculateOptions
    {
        public EntityReference Measure { get; set; }
        public string MeasureType { get; set; }
        public bool DisableRecalculation { get; set; }

    }
}
