﻿using System;
using System.Collections.Generic;
using System.Linq;
using DDSM.SmartMeasureCalculationPlugin.API;
using DDSM.SmartMeasureCalculationPlugin.Model;
//using Newtonsoft.Json;

namespace DDSM.SmartMeasureCalculationPlugin.Utils
{
    public class ApiHelper
    {
        List<EspwsVariableDefinitionResponse> _variableDefinitions;
        List<EspwsResponseVariableValue> _variableDefinitionsOutput;
        static byte[] _authenticationTokenBytes = null;
        private bool _cbIsCacheUsed;
        private Dictionary<string, object> _credential;
        private CalculatorWebServiceConsumer _calsService;

        public ApiHelper( Dictionary<string, object> credential)
        {
            
            _credential = credential;
            _calsService = new CalculatorWebServiceConsumer();
        }
      


        /// <summary>
        /// Return JSON Dictionary, when key = Measure Name, Value = list of MAD fields
        /// </summary>
        /// <returns></returns>
        public string GetAllMadDefs()
        {
            //   throw new Exception("test ex");
            if (_authenticationTokenBytes == null)
            {
                var response = _calsService.Authenticate(_credential["URI"].ToString(),
                      _credential["UserName"].ToString(), _credential["Password"].ToString());
                _authenticationTokenBytes = response.ResponseHeader.AuthenticationTokenBytes;
            }
            var measureLibrarysList = GetAvailableCalculators(); //measureLibrarys
            var result = new Dictionary<string, List<EspwsVariableDefinitionResponse>>();
            try
            {
                var request = new CalculatorDataDefinitionsRequest
                {
                    RequestHeader = { AuthenticationTokenBytes = _authenticationTokenBytes }
                };

                foreach (var measureLib in measureLibrarysList)
                {
                    foreach (var measure in measureLib.CalculatorList)
                    {
                        request.CalculatorDefinitionRequestList.Add(new CalculatorDefinitionRequest { CalculatorId = measure.Id, TargetDateTime = DateTime.UtcNow });
                    }
                }
                CalculatorDataDefinitionsResponse madDefinitionResponse = CalculatorWebServiceConsumer.GetCalculatorDataDefinitions(_credential["URI"].ToString(), request);

                if (madDefinitionResponse != null && madDefinitionResponse.ResponseHeader.StatusOk)
                {
                    foreach (var measureMad in madDefinitionResponse.EspwsCalculatorDataDefinitionResponseList)
                    {
                        result.Add(measureMad.Name, measureMad.EspVariableDefinitionList);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }



            return ""; // JsonConvert.SerializeObject(result, Formatting.Indented);
        }

        public List<EspwsCalculatorLibraryResponse> GetAvailableCalculators()
        {
            var result = new List<EspwsCalculatorLibraryResponse>();
            try
            {
             //   throw new Exception("test ex");
                if (_authenticationTokenBytes == null)
                {
                    var response = _calsService.Authenticate(_credential["URI"].ToString(),
                          _credential["UserName"].ToString(), _credential["Password"].ToString());
                    _authenticationTokenBytes = response.ResponseHeader.AuthenticationTokenBytes;
                }


                var availableCalculatorRequest = new AvailableCalculatorsRequest
                {
                    RequestHeader = { AuthenticationTokenBytes = _authenticationTokenBytes }
                };


                var availableCalculatorsResponse =
                    _calsService.GetAvailableCalculators(_credential["URI"].ToString(),
                        availableCalculatorRequest);

                if (availableCalculatorsResponse != null && availableCalculatorsResponse.ResponseHeader.StatusOk)
                {
                    // Save the returned authentication token
                    _authenticationTokenBytes = availableCalculatorsResponse.ResponseHeader.AuthenticationTokenBytes;
                    _cbIsCacheUsed = availableCalculatorsResponse.ResponseHeader.IsDataFromCache;

                    // Sort the libraries
                    availableCalculatorsResponse.CalculatorLibraryResponseList.Sort(
                        (x, y) =>
                            (x != null && y != null)
                                ? string.Compare(x.Name, y.Name, StringComparison.Ordinal)
                                : 0);

                    // Populate Library combo box.
                    //   cbCalculatorLibraries.Items.Clear();

                    foreach (var espCalculatorLibrary in availableCalculatorsResponse.CalculatorLibraryResponseList)
                    {
                        // Sort the smart measures
                        espCalculatorLibrary.CalculatorList.Sort(
                            (x, y) =>
                                (x != null && y != null)
                                    ? string.Compare(x.Name, y.Name, StringComparison.Ordinal)
                                    : 0);
                        result.Add(espCalculatorLibrary);

                    }

                    // _authenticationTokenBytes = availableCalculatorsResponse.ResponseHeader.AuthenticationTokenBytes;
                }
                return result;
            }
            catch (Exception ex)
            {
                //  ShowError("Error getting available smart measures", ex);
                throw;
            }
            return result;

        }


        /// <summary>
        /// return list of mad field that are unique for all measure
        /// </summary>
        /// <returns></returns>
        public string GetUniqueMad()
        {

            var measureLibrarysList = GetAvailableCalculators(); //measureLibrarys
            var result = new List<EspwsVariableDefinitionResponse>();
            if (_authenticationTokenBytes == null)
            {
                var response = _calsService.Authenticate(_credential["URI"].ToString(),
                      _credential["UserName"].ToString(), _credential["Password"].ToString());
                _authenticationTokenBytes = response.ResponseHeader.AuthenticationTokenBytes;
            }

            try
            {
                // var espwsVariableDefinitionResponses = new List<ESPWSVariableDefinitionResponse>();
                var request = new CalculatorDataDefinitionsRequest
                {
                    RequestHeader = { AuthenticationTokenBytes = _authenticationTokenBytes }
                };

                foreach (var measureLib in measureLibrarysList)
                {
                    foreach (var measure in measureLib.CalculatorList)
                    {
                        request.CalculatorDefinitionRequestList.Add(new CalculatorDefinitionRequest { CalculatorId = measure.Id, TargetDateTime = DateTime.UtcNow });

                    }
                }
                CalculatorDataDefinitionsResponse madDefinitionResponse = CalculatorWebServiceConsumer.GetCalculatorDataDefinitions(_credential["URI"].ToString(), request);

                if (madDefinitionResponse != null && madDefinitionResponse.ResponseHeader.StatusOk)
                {
                    foreach (var measureMad in madDefinitionResponse.EspwsCalculatorDataDefinitionResponseList)
                    {
                        if (result.Count > 0)
                        {
                            // result.Distinct((IEqualityComparer<DDSM.ESPSmartMeasurePlugin.Model.ESPWSVariableDefinitionResponse>)measureMad.ESPVariableDefinitionList);
                            result = result.Except(measureMad.EspVariableDefinitionList, new Comparer()).ToList();
                            //  result.Union(measureMad.ESPVariableDefinitionList);
                        }
                        else
                        {
                            result.AddRange(measureMad.EspVariableDefinitionList);
                        }

                    }
                }
            }
            catch (Exception)
            {

                throw;
            }



            return ""; //JsonConvert.SerializeObject(result, Formatting.Indented);
        }
    }
}
