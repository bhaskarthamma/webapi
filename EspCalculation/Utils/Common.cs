﻿using System;
using System.Activities;
using System.Collections.Generic;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using Microsoft.Xrm.Sdk.Metadata.Query;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Workflow;

namespace DDSM.SmartMeasureCalculationPlugin.Utils
{
    public class CustomITracingService : ITracingService
    {
        public List<string> Storage;
        public CustomITracingService()
        {
            Storage = new List<string>();
        }

        public void Trace(string format, params object[] args)
        {
            Storage.Add(format);
        }
    }

    public class Common
    {
        public CustomITracingService TracingService;
        public IWorkflowContext Context;
        public IOrganizationServiceFactory ServiceFactory;
        public IOrganizationService Service;
        private IOrganizationService SystemService;

        public Common(CodeActivityContext executionContext)
        {
            TracingService = new CustomITracingService();// executionContext.GetExtension<ITracingService>();
            Context = executionContext.GetExtension<IWorkflowContext>();
            ServiceFactory = executionContext.GetExtension<IOrganizationServiceFactory>();
            Service = ServiceFactory.CreateOrganizationService(Context.UserId);
        }

        /// <summary>
        /// Query the Metadata to get the Entity Schema Name from the Object Type Code
        /// </summary>
        /// <param name="objectTypeCode"></param>
        /// <param name="service"></param>
        /// <returns>Entity Schema Name</returns>
        public string SGetEntityNameFromCode(string objectTypeCode, IOrganizationService service)
        {
            var entityFilter = new MetadataFilterExpression(LogicalOperator.And);
            entityFilter.Conditions.Add(new MetadataConditionExpression("ObjectTypeCode", MetadataConditionOperator.Equals, Convert.ToInt32(objectTypeCode)));
            var entityQueryExpression = new EntityQueryExpression
            {
                Criteria = entityFilter
            };
            var retrieveMetadataChangesRequest = new RetrieveMetadataChangesRequest
            {
                Query = entityQueryExpression,
                ClientVersionStamp = null
            };
            var response = (RetrieveMetadataChangesResponse)service.Execute(retrieveMetadataChangesRequest);

            var entityMetadata = response.EntityMetadata[0];
            return entityMetadata.SchemaName.ToLower();
        }

        public List<string> GetEntityAttributesToClone(string entityName, IOrganizationService service)
        {
            var atts = new List<string>();
            var req = new RetrieveEntityRequest
            {
                EntityFilters = EntityFilters.Attributes,
                LogicalName = entityName
            };

            var res = (RetrieveEntityResponse)service.Execute(req);
            foreach (var attMetadata in res.EntityMetadata.Attributes)
            {
                if (attMetadata.IsValidForCreate.Value && !attMetadata.IsPrimaryId.Value)
                {
                    atts.Add(attMetadata.LogicalName);
                }
            }

            return (atts);
        }

        public Guid GetSystemUserId(string name, IOrganizationService service)
        {
            QueryByAttribute queryUsers = new QueryByAttribute
            {
                EntityName = "systemuser",
                ColumnSet = new ColumnSet("systemuserid")
            };

            queryUsers.AddAttributeValue("fullname", name);
            EntityCollection retrievedUsers = service.RetrieveMultiple(queryUsers);
            Guid systemUserId = ((Entity)retrievedUsers.Entities[0]).Id;

            return systemUserId;
        }

        /// <summary>
        /// Return OrgService from System User
        /// </summary>
        /// <returns></returns>
        public IOrganizationService GetSystemOrgService()
        {
            return SystemService ??
                   (SystemService = ServiceFactory.CreateOrganizationService(GetSystemUserId("SYSTEM", Service)));
        }
    }
}
