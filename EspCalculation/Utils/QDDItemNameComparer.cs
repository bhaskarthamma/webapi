﻿using System.Collections.Generic;
using EspCalculation.Model;

namespace EspCalculation.Utils
{
    public class QDDItemNameComparer : IEqualityComparer<ESPWSVariableDefinitionResponse>
    {
        public int GetHashCode(ESPWSVariableDefinitionResponse co)
        {
            if (co == null)
            {
                return 0;
            }
            return co.Name.GetHashCode();
        }

        public bool Equals(ESPWSVariableDefinitionResponse x1, ESPWSVariableDefinitionResponse x2)
        {

            return x1.Name == x2.Name;
        }
    }
}