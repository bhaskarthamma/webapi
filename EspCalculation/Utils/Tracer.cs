using System.Collections.Generic;
using Microsoft.Xrm.Sdk;

namespace EspCalculation.Utils
{
    public class Tracer : ITracingService
    {
        public List<string> Logs { get; set; }

        public Tracer()
        {
            Logs = new List<string>();
        }

        public void Trace(string logMmsg)
        {
            Logs?.Add(logMmsg);
        }

        public void Trace(string format, params object[] args)
        {
            Logs?.Add(string.Format(format, args));
        }
    }
}