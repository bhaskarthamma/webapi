﻿using System;
using System.Configuration;
using EspCalculation.App;
using EspCalculation.Model;
using EspCalculation.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Tooling.Connector;

namespace EspCalculationTests
{
    [TestClass]
    public class AppCalculateMeasureTests
    {
        private BaseApp _app;
        private readonly IOrganizationService _service;

        public AppCalculateMeasureTests()
        {
            _service = GetOrgService();
        }

        [TestMethod]
        public void CalcOptionsIsNull()
        {
           // var calcOptions = GetCalculationOptions();
            _app = new AppCalculateMeasure(_service, null);
            _app.Run();

            Assert.IsFalse(_app.Completed);
            Assert.AreEqual(_app.Result, "Calculate Options is null.");
        }

        [TestMethod]
        public void MeasureIdIsEmpty()
        {
            var options = GetCalculationOptions();
            _app = new AppCalculateMeasure(_service, options);
            _app.Run();

            Assert.IsFalse(_app.Completed);
           // Assert.AreEqual(_app.Result, $"Measure ID:{options?.MeasureId}. Measure wasn't created. ESP Calculating aborted."); //TODO: fix msg
        }



        private IOrganizationService GetOrgService()
        {
            var crmConnectionString = ConfigurationManager.ConnectionStrings["CRMConnectionString"].ConnectionString;


            // Get the CRM connection string and connect to the CRM Organization

            var client = new CrmServiceClient(crmConnectionString);
            return client.OrganizationServiceProxy;
        }

        private CalculationOptions GetCalculationOptions()
        {
            return new CalculationOptions
            {
                UserInput = "",
                Config = new ESPConfig(),
                TargetDate = DateTime.UtcNow,
                UserId = Guid.Empty,
                Target = new EntityReference()
            };
        }

    }
}
