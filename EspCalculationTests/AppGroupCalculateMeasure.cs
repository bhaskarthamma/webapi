﻿using System;
using System.Configuration;
using EspCalculation.App;
using EspCalculation.Model;
using EspCalculation.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Tooling.Connector;

namespace EspCalculationTests
{
    [TestClass]
    public class AppGroupCalculateMeasureTests
    {
        private BaseApp _app;
        private readonly IOrganizationService _service;

        public AppGroupCalculateMeasureTests()
        {
            _service = GetOrgService();
        }

        [TestMethod]
        public void GroupCalculateOptionsIsNull()
        {
            _app = new AppGroupCalculateMeasure(_service, null, null);
            _app.Run();

            Assert.IsFalse(_app.Completed);
            Assert.AreEqual(_app.Result, "Calculate Options is null.");
        }

        //[TestMethod]
        //public void MeasureIdIsEmpty()
        //{
        //    var options = GetGroupCalculateOptions();
        //    _app = new AppGroupCalculateMeasure(_service, options,null);
        //    _app.Run();

        //    Assert.IsFalse(_app.Completed);
        //  //  Assert.AreEqual(_app.Result, $"Measure ID:{options?.}. Measure wasn't created. ESP Calculating aborted.");
        //}

        private IOrganizationService GetOrgService()
        {
            var crmConnectionString = ConfigurationManager.ConnectionStrings["CRMConnectionString"].ConnectionString;
            // Get the CRM connection string and connect to the CRM Organization

            var client = new CrmServiceClient(crmConnectionString);
            return client.OrganizationServiceProxy;
        }

        private CalculationOptions GetGroupCalculateOptions()
        {
            return new CalculationOptions
            {
                Config = new ESPConfig(),
                Target = new EntityReference(),
                UserInput = "",
                TargetDate = DateTime.UtcNow,
                UserId = Guid.Empty,
            };
        }

    }
}
