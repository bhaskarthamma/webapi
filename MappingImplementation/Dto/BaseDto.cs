﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using CoreUtils.Dto;
using MappingImplementation.Service.Interfaces;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Metadata;

namespace MappingImplementation.Dto
{
    public class BaseDto
    {
        public ConcurrentDictionary<Guid, Entity> EntityCache;

        public ConcurrentDictionary<string, string> EntityDisplayName;

        public ConcurrentDictionary<string, EntityMetadata> EntityMetadata;
        public ConcurrentDictionary<string, List<OneToManyRelationshipMetadata>> ManyToOneRelationshipMetadata;
        public ConcurrentDictionary<Guid, EntityCollection> MappingEntities;
        public ConcurrentDictionary<Guid, EntityCollection> MappingImplementationEntities;

        public ConcurrentDictionary<string, List<OneToManyRelationshipMetadata>> OneToManyRelationshipMetadata;
        public ConcurrentDictionary<int, EntityCollection> ReferencingEntities;


        public BaseDto()
        {
            EntityMetadata = new ConcurrentDictionary<string, EntityMetadata>();
            EntityCache = new ConcurrentDictionary<Guid, Entity>();
            EntityDisplayName = new ConcurrentDictionary<string, string>();
            OneToManyRelationshipMetadata = new ConcurrentDictionary<string, List<OneToManyRelationshipMetadata>>();
            ManyToOneRelationshipMetadata = new ConcurrentDictionary<string, List<OneToManyRelationshipMetadata>>();
            ReferencingEntities = new ConcurrentDictionary<int, EntityCollection>();
            MappingImplementationEntities = new ConcurrentDictionary<Guid, EntityCollection>();
            MappingEntities = new ConcurrentDictionary<Guid, EntityCollection>();

            //target entity
            Target = new Entity();
            MappingImplementation = Guid.Empty;
            CreatorRecordType = MappingImplementationDto.RecordCreator.Front;
            CurrentUserId = Guid.Empty;
        }

        public Guid CurrentUserId { get; set; }

        //Oganization Service
        public IOrganizationService OrgService { get; set; }

        //Target Entity
        public Entity Target { get; set; }

        //Tracing Service

        public Guid MappingImplementation { get; set; }

        public IDataService DataService { get; set; }
        public IRelationshipService RelationshipService { get; set; }
        public MappingImplementationDto.RecordCreator CreatorRecordType { get; set; }
    }
}