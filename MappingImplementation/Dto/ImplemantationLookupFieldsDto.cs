﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MappingImplementation.Dto
{
    public class ImplementationLookupFieldsDto
    {
        public string EntityLogicalName { get; set; }

        public List<ImplementationField> ImplementFields { get; set; }

        public ImplementationLookupFieldsDto()
        {
            ImplementFields = new List<ImplementationField>();
        }

        public class ImplementationField
        {
            public string EntityLogicalName { get; set; }

            public string LookupLogicalName { get; set; }
        }
    }
}
