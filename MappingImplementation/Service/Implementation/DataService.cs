﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using CoreUtils.Service.Implementation;
using CoreUtils.Wrap;
using MappingImplementation.Dto;
using MappingImplementation.Service.Interfaces;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using Microsoft.Xrm.Sdk.Query;

namespace MappingImplementation.Service.Implementation
{
    public class DataService : IDataService
    {
        public enum MappingDirection
        {
            M2O = 962080001,
            O2M = 962080000
        }

        private readonly BaseDto _baseDto;

        private readonly IOrganizationService _orgService;

        public DataService(BaseDto baseDto)
        {
            _baseDto = baseDto;
            _orgService = baseDto.OrgService;
        }

        /// <summary>
        ///     Set value to the target entity use mapping
        /// </summary>
        /// <param name="targetEntity"></param>
        /// <param name="sourceEntity"></param>
        /// <param name="mapping"></param>
        public void InitTargetEntityFromSource(Entity targetEntity, Entity sourceEntity, List<MappingDto> mapping)
        {
            try
            {
                SetCurrency(targetEntity);

                foreach (var item in mapping)
                {
                    if (string.IsNullOrEmpty(item.SourceFieldLogicalName) ||
                        string.IsNullOrEmpty(item.TargetFieldLogicalName) ||
                        item.SourceFieldType.ToLower() != item.TargetFieldType.ToLower())
                        continue;
                    object value = null;
                    var sourceFieldLogicalName = item.SourceFieldLogicalName.ToLower();
                    var targetFieldLogicalName = item.TargetFieldLogicalName.ToLower();
                    if (sourceEntity.Attributes.TryGetValue(targetFieldLogicalName, out value))
                        targetEntity[sourceFieldLogicalName] = sourceEntity[targetFieldLogicalName];
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        /// <summary>
        /// </summary>
        /// <returns></returns>
        public string GetSourceEntityLogicalNameFromMapping(Guid mappingId)
        {
            var mapping = RetreiveEntity("ddsm_mapping", mappingId);
            var sourceentity = string.Empty;

            object ddsmSourceentityTmp = null;
            if (mapping.Attributes.TryGetValue("ddsm_sourceentity", out ddsmSourceentityTmp))
                sourceentity = ddsmSourceentityTmp.ToString();
            return sourceentity;
        }

        /// <summary>
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, List<MappingDto>> GetMappingById(Guid mappingId)
        {
            var mappedDataResult = new Dictionary<string, List<MappingDto>>();
            var mapping = RetreiveEntity("ddsm_mapping", mappingId);

            object mappedMetaDataTmp = null;
            if (mapping.Attributes.TryGetValue("ddsm_jsondata", out mappedMetaDataTmp))
            {
                var mappedMetaDataJson = mappedMetaDataTmp.ToString();
                //mapped data in Dictionary
                mappedDataResult =
                    JsonConvert.DeserializeObject<Dictionary<string, List<MappingDto>>>(mappedMetaDataJson);
            }

            return mappedDataResult;
        }

        /// <summary>
        ///     Get mapping direction
        /// </summary>
        /// <param name="mappingId"></param>
        /// <returns>option set value</returns>
        public int GetMappingDirection(Guid mappingId)
        {
            var mapping = RetreiveEntity("ddsm_mapping", mappingId);

            object directionTmp = null;
            if (mapping.Attributes.TryGetValue("ddsm_direction", out directionTmp))
                return ((OptionSetValue) directionTmp).Value;
            return 0;
        }

        /// <summary>
        ///     Retreive entity
        /// </summary>
        /// <param name="logicalName"></param>
        /// <param name="recordId"></param>
        /// <returns></returns>
        public Entity RetreiveEntity(string logicalName, Guid recordId)
        {
            if (string.IsNullOrEmpty(logicalName) || recordId == Guid.Empty)
                return null;
            return ExecuteRetreive(logicalName, recordId);
        }

        /// <summary>
        /// </summary>
        /// <param name="implMappinId"></param>
        /// <returns></returns>
        public ImplMappingDto GetMappingImplementationById(Guid implMappinId)
        {
            var mappedDataResult = new ImplMappingDto();

            var rs = RetreiveEntity("ddsm_mappingimplementation", implMappinId);

            object implMappingDataObj = null;
            var implMappingData = string.Empty;
            if (rs.Attributes.TryGetValue("ddsm_json", out implMappingDataObj))
                implMappingData = implMappingDataObj.ToString();

            if (!string.IsNullOrEmpty(implMappingData))
                mappedDataResult = JsonConvert.DeserializeObject<ImplMappingDto>(implMappingData);
            return mappedDataResult;
        }

        /// <summary>
        /// </summary>
        /// <param name="mappingImplementation"></param>
        public void GetRelatedMapping(ImplMappingDto mappingImplementation)
        {
            var mappingImlementationListIds = GetMappingImlementationIds(mappingImplementation);
            if (mappingImlementationListIds.Count == 0) return;
            var mappingImlementationList = GetMappingImlementation(mappingImlementationListIds);
            var mappingGuids = new List<Guid>();
            foreach (var mappingImlementation in mappingImlementationList.Entities)
            {
                _baseDto.EntityCache.TryAdd(mappingImlementation.Id, mappingImlementation);

                object mappingTmp = null;
                if (mappingImlementation.Attributes.TryGetValue("ddsm_mapping", out mappingTmp))
                    mappingGuids.Add(((EntityReference) mappingTmp).Id);
            }
            var mappings = GetMapping(mappingGuids);
            foreach (var mapping in mappings.Entities)
                _baseDto.EntityCache.TryAdd(mapping.Id, mapping);
        }


        public bool IsAttributeExists(Entity entity, string attributeName)
        {
            var entityMetadata = GetAttributeMetadata(entity.LogicalName, attributeName).FirstOrDefault();
            if (entityMetadata == null)
                return false;
            return true;
        }

        private void SetCurrency(Entity targetEntity)
        {
            var transactioncurrency = TransactionCurrency.GetCurrency(_baseDto.OrgService, _baseDto.CurrentUserId,
                targetEntity.LogicalName);
            if (transactioncurrency != null && transactioncurrency.Id != Guid.Empty)
                targetEntity["transactioncurrencyid"] = transactioncurrency;
        }

        /// <summary>
        ///     Execute Query
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public EntityCollection RetrieveMultiple(QueryExpression query)
        {
            try
            {
                return _orgService.RetrieveMultiple(query);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        /// <summary>
        ///     Execute Query
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public OrganizationResponse ExecuteQuery(ExecuteMultipleRequest query)
        {
            try
            {
                return _orgService.Execute(query);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }


        /// <summary>
        ///     Retreive entity
        /// </summary>
        /// <param name="logicalName"></param>
        /// <param name="recordId"></param>
        /// <returns></returns>
        public Entity RetreiveEntity(string logicalName, string recordId)
        {
            if (string.IsNullOrEmpty(recordId) || string.IsNullOrEmpty(logicalName))
                return null;
            return ExecuteRetreive(logicalName, new Guid(recordId));
        }


        /// <summary>
        ///     Retreive Entity
        /// </summary>
        /// <param name="entityLogicalName"></param>
        /// <param name="recordId"></param>
        /// <returns></returns>
        private Entity ExecuteRetreive(string entityLogicalName, Guid recordId)
        {
            try
            {
                var entityCache = _baseDto.EntityCache;
                if (entityCache.ContainsKey(recordId))
                {
                    var tmp = new Entity();
                    if (entityCache.TryGetValue(recordId, out tmp))
                        return tmp;
                }
                var entity = _orgService.Retrieve(entityLogicalName, recordId, new ColumnSet(true));
                entityCache.TryAdd(recordId, entity);
                return entity;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }


        /// <summary>
        /// </summary>
        /// <param name="entityLogicalName"></param>
        /// <param name="attName"></param>
        /// <returns></returns>
        public IEnumerable<AttributeMetadata> GetAttributeMetadata(string entityLogicalName, string attName)
        {
            try
            {
                var entityMetadata = GetEntityMetadata(entityLogicalName);

                return entityMetadata?.Attributes.Where(att => att.LogicalName.ToLower() == attName.ToLower());
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public string GetEntityDisplayName(string entityLogicalName)
        {
            try
            {
                var entityMetadata = GetEntityMetadata(entityLogicalName);

                return entityMetadata.DisplayName.UserLocalizedLabel.Label;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public void RetreiveBatchEntities(List<Guid> entitiesGuids, string entityLogicalName, string fieldName)
        {
            try
            {
                var entityCache = _baseDto.EntityCache;
                var query = new QueryExpression
                {
                    EntityName = entityLogicalName,
                    ColumnSet = new ColumnSet(true),
                    Criteria = new FilterExpression
                    {
                        Conditions =
                        {
                            new ConditionExpression(fieldName, ConditionOperator.In, entitiesGuids)
                        }
                    }
                };
                var rs = _orgService.RetrieveMultiple(query).Entities;
                foreach (var item in rs)
                    entityCache.TryAdd(item.Id, item);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        /// <summary>
        /// </summary>
        /// <param name="entityLogicalName"></param>
        /// <returns></returns>
        private EntityMetadata GetEntityMetadata(string entityLogicalName)
        {
            var currentEntityMetadata = new EntityMetadata();

            try
            {
                var entityMetadata = _baseDto.EntityMetadata;
                if (entityMetadata.ContainsKey(entityLogicalName))
                {
                    var tmp = new EntityMetadata();
                    if (entityMetadata.TryGetValue(entityLogicalName, out tmp))
                        currentEntityMetadata = tmp;
                }
                else
                {
                    var request = new RetrieveEntityRequest
                    {
                        EntityFilters = EntityFilters.Attributes,
                        LogicalName = entityLogicalName,
                        RetrieveAsIfPublished = true
                    };

                    var response = (RetrieveEntityResponse) _orgService.Execute(request);
                    currentEntityMetadata = response.EntityMetadata;

                    entityMetadata.TryAdd(entityLogicalName, currentEntityMetadata);
                    _baseDto.EntityDisplayName.TryAdd(entityLogicalName,
                        response.EntityMetadata.DisplayName.UserLocalizedLabel.Label);
                }
                return currentEntityMetadata;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }


        /// <summary>
        ///     Update entity
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="attr"></param>
        /// <param name="value"></param>
        public void UpdateEntity(Entity entity, string attr, string value)
        {
            try
            {
                if (entity == null || string.IsNullOrEmpty(attr))
                    return;
                var attMetadata = GetAttributeMetadata(entity.LogicalName, attr).FirstOrDefault();
                var attrType = attMetadata?.AttributeType;
                if (attrType == null)
                    return;

                switch (attrType)
                {
                    case AttributeTypeCode.Integer:
                        entity.Attributes[attr] = (int) Math.Round(double.Parse(value));
                        break;
                    case AttributeTypeCode.Decimal:
                        decimal amount;
                        decimal.TryParse(value, NumberStyles.Any, CultureInfo.InvariantCulture, out amount);
                        entity.Attributes[attr] = amount;
                        break;
                    case AttributeTypeCode.Double:
                        entity.Attributes[attr] = double.Parse(value);
                        break;
                    case AttributeTypeCode.Money:
                        entity.Attributes[attr] = new Money(decimal.Parse(value));
                        break;
                    case AttributeTypeCode.String:
                        entity.Attributes[attr] = value;
                        break;
                    case AttributeTypeCode.Boolean:
                        entity.Attributes[attr] = bool.Parse(value);
                        break;
                    case AttributeTypeCode.DateTime:
                        DateTime? date = null;
                        if (value == "''" || string.IsNullOrEmpty(value))
                        {
                            entity.Attributes[attr] = date;
                            break;
                        }
                        date = DateTime.Parse(value);
                        entity.Attributes[attr] = date;
                        break;
                    case AttributeTypeCode.Picklist:
                        if (value == "''")
                        {
                            entity.Attributes[attr] = null;
                            break;
                        }
                        var key = GetPickListKeyByValue(value, attMetadata);
                        var option = new OptionSetValue(key);
                        entity.Attributes[attr] = option;
                        break;
                    case AttributeTypeCode.Memo:
                        entity.Attributes[attr] = value;
                        break;
                    case AttributeTypeCode.Lookup:
                        var lookUpMetadata = (LookupAttributeMetadata) attMetadata;
                        if (lookUpMetadata == null || lookUpMetadata.Targets.Length == 0)
                            break;
                        var entityLN = lookUpMetadata.Targets[0];
                        entity.Attributes[attr] = new EntityReference(entityLN, new Guid(value));
                        break;
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        private int GetPickListKeyByValue(string value, AttributeMetadata fieldMetadata)
        {
            try
            {
                var attMetadata = (EnumAttributeMetadata) fieldMetadata;
                var optionMetadata = attMetadata.OptionSet.Options.FirstOrDefault();
                var key = optionMetadata.Value;

                var firstOrDefault = attMetadata.OptionSet.Options
                    .FirstOrDefault(x => x.Label.UserLocalizedLabel.Label == value);

                if (firstOrDefault != null)
                    key = firstOrDefault.Value;

                return (int) key;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        /// <summary>
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public EntityCollection RetrieveMultipleQuery(FetchExpression query)
        {
            return _orgService.RetrieveMultiple(query);
        }


        /// <summary>
        ///     Convert list of ids  to list  of Guid
        /// </summary>
        /// <param name="listIds"></param>
        /// <returns></returns>
        public List<Guid> ConvertListIdToListGuid(List<string> listIds)
        {
            var rs = new List<Guid>();
            foreach (var id in listIds)
                rs.Add(new Guid(id));
            return rs;
        }

        /// <summary>
        ///     Execute multiple update request
        /// </summary>
        /// <param name="entities"></param>
        public void UpdateRequest(List<KeyValuePair<Guid, Entity>> entities)
        {
            try
            {
                var requestWithNoResults = new ExecuteMultipleRequest
                {
                    Settings = new ExecuteMultipleSettings
                    {
                        ContinueOnError = false,
                        ReturnResponses = false
                    },
                    Requests = new OrganizationRequestCollection()
                };

                foreach (var item in entities)
                {
                    var updateRequest = new UpdateRequest {Target = item.Value};
                    requestWithNoResults.Requests.Add(updateRequest);
                }
                _orgService.Execute(requestWithNoResults);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw;
            }
        }

        /// <summary>
        /// </summary>
        /// <param name="mappingImplementation"></param>
        /// <returns></returns>
        private List<Guid> GetMappingImlementationIds(ImplMappingDto mappingImplementation)
        {
            var mappingImplementationIds = new List<Guid>();
            foreach (var tab in mappingImplementation.Tabs)
            {
                if (tab.Lookup.LogicalName == null) continue;
                foreach (var relationship in tab.Relationships)
                {
                    if (relationship.MappingImplementation == null) continue;
                    if (relationship.MappingImplementation.Id != Guid.Empty)
                        mappingImplementationIds.Add(relationship.MappingImplementation.Id);
                }
            }
            return mappingImplementationIds;
        }

        /// <summary>
        /// </summary>
        /// <param name="mappingimplementation"></param>
        /// <returns></returns>
        private EntityCollection GetMappingImlementation(List<Guid> mappingimplementation)
        {
            var tmpListImplGuid = new List<Guid>();
            var result = new EntityCollection();
            foreach (var item in mappingimplementation)
                if (_baseDto.EntityCache.ContainsKey(item))
                    result.Entities.Add(_baseDto.EntityCache[item]);
                else
                    tmpListImplGuid.Add(item);

            if (tmpListImplGuid.Count == 0)
                return result;

            var query = new QueryExpression
            {
                ColumnSet = new ColumnSet("ddsm_mapping", "ddsm_json"),
                EntityName = "ddsm_mappingimplementation",
                Criteria = new FilterExpression
                {
                    Conditions =
                    {
                        new ConditionExpression("ddsm_mappingimplementationid", ConditionOperator.In,
                            tmpListImplGuid)
                    }
                }
            };

            var entities = RetrieveMultiple(query);
            foreach (var entity in entities.Entities)
                _baseDto.EntityCache.TryAdd(entity.Id, entity);

            result.Entities.AddRange(entities.Entities);

            return result;
        }

        private EntityCollection GetMapping(List<Guid> mappingIds)
        {
            var tmpListImplGuid = new List<Guid>();
            var result = new EntityCollection();
            foreach (var item in mappingIds)
                if (_baseDto.EntityCache.ContainsKey(item))
                    result.Entities.Add(_baseDto.EntityCache[item]);
                else
                    tmpListImplGuid.Add(item);

            if (tmpListImplGuid.Count == 0)
                return result;

            var query = new QueryExpression
            {
                ColumnSet = new ColumnSet(true),
                EntityName = "ddsm_mapping",
                Criteria = new FilterExpression
                {
                    Conditions =
                    {
                        new ConditionExpression("ddsm_mappingid", ConditionOperator.In, mappingIds)
                    }
                }
            };

            var entities = RetrieveMultiple(query);
            foreach (var entity in entities.Entities)
                _baseDto.EntityCache.TryAdd(entity.Id, entity);

            result.Entities.AddRange(entities.Entities);

            return result;
        }
    }
}