﻿using System;
using System.Collections.Generic;
using CoreUtils.Dto;
using MappingImplementation.Dto;
using MappingImplementation.Service.Interfaces;
using Microsoft.Xrm.Sdk;

namespace MappingImplementation.Service.Implementation
{
    public class MappingImplementationService : IMappingImplementationService
    {
        /// <summary>
        ///     ctor
        /// </summary>
        /// <param name="mappingImplementationDto"></param>
        public MappingImplementationService(MappingImplementationDto mappingImplementationDto)
        {
            BaseDto = new BaseDto
            {
                OrgService = mappingImplementationDto.OrgService,
                CreatorRecordType = mappingImplementationDto.CreatorRecordType,
                CurrentUserId = mappingImplementationDto.CurrentUserId
            };

            var dataService = new DataService(BaseDto);
            BaseDto.DataService = dataService;
            BaseDto.RelationshipService = new RelationshipService(BaseDto, dataService);
        }

        public BaseDto BaseDto { get; }

        /// <summary>
        ///     Execite mapping
        /// </summary>
        /// <returns>entity with relationships</returns>
        public List<Entity> ExecuteMapping(Entity target, Guid mappingImplementationId)
        {
            InitTargetEntity(target);
            InitMappingImplementation(mappingImplementationId);

            if (BaseDto.MappingImplementation == Guid.Empty)
                throw new Exception($"Mapping Implementation is empty");

            if (BaseDto.Target == null)
                throw new Exception($"Target Entity is null");

            IMappingImplementationManager mappingImplementationManager = new MappingImplementationManager(this);
            return mappingImplementationManager.UpdateEntityByMapping();
        }

        /// <summary>
        ///     Get lookup from mapping implementation
        /// </summary>
        /// <returns>Implementation LookupFields Dto object</returns>
        public ImplementationLookupFieldsDto GetImplementationLookup(Entity target, Guid mappingImplementationId)
        {
            InitTargetEntity(target);
            InitMappingImplementation(mappingImplementationId);

            var fields = new ImplementationLookupFieldsDto();
            //get mapping implementation
            var mappingImplementations = BaseDto.DataService.GetMappingImplementationById(BaseDto.MappingImplementation);
            foreach (var implemantation in mappingImplementations.Tabs)
            {
                fields.EntityLogicalName = BaseDto.Target.LogicalName;
                var implementationField = new ImplementationLookupFieldsDto.ImplementationField
                {
                    EntityLogicalName = implemantation.LogicalName,
                    LookupLogicalName = implemantation.Lookup.LogicalName
                };

                fields.ImplementFields.Add(implementationField);
            }
            return fields;
        }

        /// <summary>
        /// </summary>
        /// <param name="targetEntity"></param>
        /// <param name="sourceEntity"></param>
        /// <param name="mapping"></param>
        /// <returns></returns>
        public void DoMappingEntity(Entity targetEntity, Entity sourceEntity, List<MappingDto> mapping)
        {
            if (sourceEntity == null || targetEntity == null) return;

            //map target entity
            BaseDto.DataService.InitTargetEntityFromSource(targetEntity, sourceEntity, mapping);
        }

        public List<ImplMappingDto.Tab> GetImplementationTabs(ImplMappingDto mappingImplementation)
        {
            return mappingImplementation.Tabs;
        }

        private void InitTargetEntity(Entity target)
        {
            BaseDto.Target = target;
        }

        private void InitMappingImplementation(Guid mappingImplementationId)
        {
            BaseDto.MappingImplementation = mappingImplementationId;
        }
    }
}