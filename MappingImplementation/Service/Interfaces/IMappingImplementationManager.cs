﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xrm.Sdk;

namespace MappingImplementation.Service.Interfaces
{
    interface IMappingImplementationManager
    {
        /// <summary>
        /// Update/create entity by mapping implementation
        /// </summary>
        /// <returns></returns>
        List<Entity> UpdateEntityByMapping();

    }
}
