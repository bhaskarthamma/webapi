﻿using System.Web.Http;
using System.Web.Http.Cors;
using BLL.Services.Concrete;
using BLL.Services.Interfaces;
using EspCalculation.Options;


namespace OwinWebApi.Controllers
{
    [EnableCors("*", "*", "*")]
    public class EspController : ApiController
    {
        private readonly IEspService _espService;

        public EspController()
        {
            _espService = new EspService();
        }

        [HttpPost]
        public string CalculateMeasure([FromBody] CalculationOptions options)
        {
            _espService.CalculateMeasureAsync(options);
            return "ESP calculation is started";
        }

        [HttpPost]
        public string GetLatestMtData([FromBody] CalculationOptions options)
        {
            _espService.GetLatestMtData(options);
            return "Get Latest MT Data started";
        }

        [HttpPost]
        public string GroupCalculateMeasures([FromBody] CalculationOptions options)
        {
            _espService.GroupCalculateMeasuresAsync(options);
            return "ESP Group calculation is started";
        }

        [HttpPost]
        public string SyncMetadata([FromBody] CalculationOptions options)
        {
            _espService.SyncMetadata(options);
            return "ESP Sync Metadata is started";
        }
    }
}
