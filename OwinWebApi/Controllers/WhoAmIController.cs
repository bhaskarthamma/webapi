﻿using System;
using System.Web.Http;
using System.Web.Http.Cors;
using BLL.Services.Concrete;
using Newtonsoft.Json;
using OwinSelfHostWebApi.Models;
using Serilog;

namespace OwinWebApi.Controllers
{
    [EnableCors("*", "*", "*")]
    public class WhoAmIController : ApiController
    {
        private readonly WhoAmIService _whoAmIService;

        public WhoAmIController()
        {
            _whoAmIService = new WhoAmIService();
        }

        public string GetWhoAmI()
        {
            var userId = Guid.Empty;
            var orgUrl = string.Empty;
            var rs = new WhoAmI();
            try
            {
                userId = _whoAmIService.GetWhoAmI();
                orgUrl = _whoAmIService.GetOrganizationUrl();
                Log.Debug($"WhoAmI={userId}");
                rs.UserId = userId;
                rs.IsConnected = true;
                if (!string.IsNullOrEmpty(orgUrl))
                    rs.OrganizationUrl = orgUrl.Split(';')[0];

                return JsonConvert.SerializeObject(rs);
            }
            catch (Exception ex)
            {
                rs.UserId = userId;
                rs.OrganizationUrl = orgUrl;
                rs.IsConnected = false;
                rs.Message = ex.Message;
                return JsonConvert.SerializeObject(rs);
            }
        }
    }
}