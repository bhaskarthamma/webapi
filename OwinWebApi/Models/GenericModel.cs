﻿using System;

namespace OwinSelfHostWebApi.Models
{
    public class GenericModel<T>
    {
        public Guid UserId { get; set; }
        public T Model { get; set; }
    }
}