﻿using System;

namespace OwinWebApi.Models
{
    public class GuidModel
    {
        public Guid Id { get; set; }
    }
}