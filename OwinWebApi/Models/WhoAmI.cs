﻿using System;

namespace OwinSelfHostWebApi.Models
{
    public class WhoAmI
    {
        public WhoAmI()
        {
            UserId = Guid.Empty;
        }

        public string OrganizationUrl { get; set; }
        public Guid UserId { get; set; }
        public bool IsConnected { get; set; }
        public string Message { get; set; }
    }
}