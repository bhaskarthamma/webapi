﻿using System;
using System.Net.WebSockets;
using System.Threading.Tasks;
using Owin.WebSocket;

namespace OwinWebApi.Sockets
{
    [WebSocketRoute("/DynamicsCrm")]
    public class DynamicsCrmSocket : WebSocketConnection
    {
        public override Task OnMessageReceived(ArraySegment<byte> message, WebSocketMessageType type)
        {
            return SendText(message, true);
        }
    }
}