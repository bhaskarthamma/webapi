﻿using System;
using System.Collections.Generic;

namespace ProcessFlowBuilder.Dto
{
    public class MsPfMsgDto
    {

        public MsPfMsgDto()
        {
            Milestones = new List<Milestone>();
            Entities = new List<Entity>();
        }

        public List<Milestone> Milestones { get; set; }

        public List<Entity> Entities { get; set; }


        public class Milestone
        {
            public Guid Id;
            public int Status;

            public Milestone()
            {
                Id = Guid.Empty;
                BusinessRules = new List<BusinessRuleData>();
            }

            public List<BusinessRuleData> BusinessRules { get; set; }
        }

        public class Entity
        {
            public Guid Id;
            public string LogicalName;

            public Entity()
            {
                Id = Guid.Empty;
            }
        }


        public class BusinessRuleData
        {
            public Guid Id;
            public int Status;

            public BusinessRuleData()
            {
                Id = Guid.Empty;
            }
        }
    }
}