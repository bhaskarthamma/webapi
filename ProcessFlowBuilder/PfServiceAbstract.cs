﻿using System;
using ProcessFlowBuilder.Service;

namespace ProcessFlowBuilder
{
    public abstract class PfServiceAbstract
    {
        public Guid UserId { get; set; }

        public abstract void CreatePfHistory();

        public abstract void CreateStepHistory();

        public abstract void CreateBrHistory();

        public abstract void UpdateBusinessRuleStatus(Guid businessRule, DataService.RecordStatus status);

        public abstract void UpdateStepStatus(DataService.RecordStatus status);
        public abstract void UpdateStepStatus(Guid stepIdGuid, DataService.RecordStatus status);

        public abstract void CreateWsMessage();

        public abstract void SendWsMessage();
    }
}