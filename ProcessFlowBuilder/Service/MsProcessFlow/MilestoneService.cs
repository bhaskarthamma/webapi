﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using Microsoft.Xrm.Sdk.Query;
using ProcessFlowBuilder.Dto;

namespace ProcessFlowBuilder.Service.MsProcessFlow
{
    public class MilestoneService
    {
        private readonly MsDataService _dataService;
        private readonly Guid _msProcessFlowId;

        public Dictionary<Guid, List<Guid>> ListMilestoneToBusinessRule;
        public List<Guid> MilestoneGuidList;
        public Dictionary<Guid, Guid> OriginalMilestonToHistoryMileston;

        public Dictionary<Guid, List<Guid>> Pf2MilestonEntitiesList;

        public MilestoneService(MsDataService dataService)
        {
            _dataService = dataService;
            _msProcessFlowId = dataService.MsProcessFlowId;
            OriginalMilestonToHistoryMileston = new Dictionary<Guid, Guid>();
            Pf2MilestonEntitiesList = new Dictionary<Guid, List<Guid>>();
            MilestoneGuidList = new List<Guid>();
            ListMilestoneToBusinessRule = new Dictionary<Guid, List<Guid>>();
        }

        public List<Guid> ProcessFlowList { get; set; }


        /// <summary>
        /// </summary>
        /// <param name="referencingAttr"></param>
        /// <returns></returns>
        private EntityCollection GetMilestonesByTargetEntity(string referencingAttr)
        {
            //declare query statement
            var query = new QueryExpression("ddsm_milestone")
            {
                ColumnSet = new ColumnSet(),
                Criteria = new FilterExpression
                {
                    Conditions =
                    {
                        new ConditionExpression(referencingAttr, ConditionOperator.Equal, _dataService.TargetEntity.Id)
                    }
                }
            };
            query.AddOrder("ddsm_index", OrderType.Ascending);
            return _dataService.OrgService.RetrieveMultiple(query);
        }

        /// <summary>
        /// </summary>
        /// <param name="targetEntity"></param>
        /// <param name="referencingEntity"></param>
        /// <returns></returns>
        private string GetRefarencingAttr(string targetEntity, string referencingEntity)
        {
            var retrieveBankAccountEntityRequest = new RetrieveEntityRequest
            {
                EntityFilters = EntityFilters.Relationships,
                LogicalName = targetEntity
            };

            var retrieveBankEntityResponse =
                (RetrieveEntityResponse) _dataService.OrgService.Execute(retrieveBankAccountEntityRequest);

            var oneToManyRelationships = retrieveBankEntityResponse.EntityMetadata.OneToManyRelationships;
            var rs =
                oneToManyRelationships.Where(r => r.ReferencingEntity == referencingEntity).ToList();
            if (rs.Count == 0)
                return string.Empty;
            return rs[0].ReferencingAttribute;
        }

        private EntityCollection GetMilestone2Br(List<Guid> milestoneIdsList)
        {
            //declare query statement
            var query = new QueryExpression("ddsm_milestonebusinessrule")
            {
                ColumnSet = new ColumnSet("ddsm_businessruleid", "ddsm_milestoneid"),
                Criteria = new FilterExpression
                {
                    Conditions =
                    {
                        new ConditionExpression("ddsm_milestoneid", ConditionOperator.In, milestoneIdsList)
                    }
                }
            };
            var rs = _dataService.OrgService.RetrieveMultiple(query);
            foreach (var item in rs.Entities)
            {
                object milestone = null;
                var milestoneId = Guid.Empty;
                if (item.Attributes.TryGetValue("ddsm_milestoneid", out milestone))
                    milestoneId = ((EntityReference) milestone).Id;
                object businessrule = null;
                if (item.Attributes.TryGetValue("ddsm_businessruleid", out businessrule) && milestoneId != Guid.Empty)
                {
                    var businessruleid = ((EntityReference) businessrule).Id;
                    UpdateListMilestoneToBusinessRule(milestoneId, businessruleid);
                }
            }
            return rs;
        }

        /// <summary>
        ///     Update the  ListStepToBusinessRule
        /// </summary>
        /// <param name="milestoneId"></param>
        /// <param name="businessRuleId"></param>
        private void UpdateListMilestoneToBusinessRule(Guid milestoneId, Guid businessRuleId)
        {
            if (ListMilestoneToBusinessRule.ContainsKey(milestoneId))
            {
                var el = ListMilestoneToBusinessRule[milestoneId];
                if (!el.Contains(businessRuleId))
                    el.Add(businessRuleId);
                return;
            }
            ListMilestoneToBusinessRule.Add(milestoneId, new List<Guid> {businessRuleId});
        }

        /// <summary>
        ///     Update the  ListStepToBusinessRule
        /// </summary>
        /// <param name="pfId"></param>
        /// <param name="milestoneId"></param>
        private void UpdatePf2MilestonEntitiesList(Guid pfId, Guid milestoneId)
        {
            if (Pf2MilestonEntitiesList.ContainsKey(pfId))
            {
                var el = Pf2MilestonEntitiesList[pfId];
                if (!el.Contains(milestoneId))
                    el.Add(milestoneId);
                return;
            }
            Pf2MilestonEntitiesList.Add(pfId, new List<Guid> {milestoneId});
        }

        /// <summary>
        ///     Get list of milestone guids by list of business rules guids
        /// </summary>
        /// <param name="businesRuledList"></param>
        /// <returns></returns>
        public List<Guid> GetMilestoneByBrList(List<Guid> businesRuledList)
        {
            //declare query statement
            var query = new QueryExpression("ddsm_milestonebusinessrule")
            {
                ColumnSet = new ColumnSet("ddsm_milestoneid"),
                Distinct = true,
                Criteria = new FilterExpression
                {
                    Conditions =
                    {
                        new ConditionExpression("ddsm_businessruleid", ConditionOperator.In, businesRuledList)
                    }
                }
            };
            //execute query
            var milestonebusinessruleList = _dataService.OrgService.RetrieveMultiple(query);
            //create  list of milestone guids
            var milestoneGuidList = new List<Guid>();
            foreach (var milestonebusinessrule in milestonebusinessruleList.Entities)
            {
                object milestone = null;
                if (milestonebusinessrule.Attributes.TryGetValue("ddsm_milestoneid", out milestone))
                {
                    var milestoneRef = (EntityReference) milestone;
                    if (MilestoneGuidList.Contains(milestoneRef.Id))
                        milestoneGuidList.Add(milestoneRef.Id);
                }
            }


            return milestoneGuidList;
        }


        /// <summary>
        ///     Create a new ddsm_historystepstatus
        /// </summary>
        /// <returns></returns>
        public Guid AddMilestone(Guid milestoneId)
        {
            try
            {
                if (_msProcessFlowId == Guid.Empty)
                    return Guid.Empty;


                var historyMsStatusId = CreateHistoryMsStatus(milestoneId);

                UpdateOriginalStepToHistoryStep(milestoneId, historyMsStatusId);


                var requestWithResults2 = new ExecuteMultipleRequest
                {
                    Settings = new ExecuteMultipleSettings
                    {
                        ContinueOnError = false,
                        ReturnResponses = false
                    },
                    Requests = new OrganizationRequestCollection()
                };

                var request = new AssociateRequest();
                var historystepstatus = new EntityReference("ddsm_historymsstatus", historyMsStatusId);
                var pfHistory = new EntityReference("ddsm_historyms", _msProcessFlowId);
                request.Target = historystepstatus;
                request.RelatedEntities = new EntityReferenceCollection {pfHistory};
                request.Relationship = new Relationship("ddsm_ddsm_historyms_ddsm_historymsstatus");

                requestWithResults2.Requests.Add(request);

                _dataService.OrgService.Execute(requestWithResults2);
                return historyMsStatusId;
            }

            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        private void UpdateOriginalStepToHistoryStep(Guid originalStepId, Guid historyStep)
        {
            if (OriginalMilestonToHistoryMileston.ContainsKey(originalStepId))
                return;

            OriginalMilestonToHistoryMileston.Add(originalStepId, historyStep);
        }

        /// <summary>
        /// </summary>
        /// <param name="milestoneId"></param>
        /// <returns></returns>
        private Guid CreateHistoryMsStatus(Guid milestoneId)
        {
            try
            {
                var step = new Entity("ddsm_historymsstatus")
                {
                    ["ddsm_milestone"] = new EntityReference("ddsm_milestone", milestoneId),
                    ["ddsm_name"] = _dataService.GetFiledValue("ddsm_milestone", milestoneId, "ddsm_name"),
                    ["ddsm_status"] = new OptionSetValue((int) DataService.RecordStatus.NotStarted)
                };

                return _dataService.OrgService.Create(step);
            }
            catch (Exception e)
            {
                return Guid.Empty;
            }
        }


        public void UpdateStepStatus(DataService.RecordStatus status, Guid milestoneId,
            MsPfMsgDto milestoneList)
        {
            try
            {
                GetMilestoneStatus(milestoneId, milestoneList, status);
                var entity = new Entity("ddsm_historymsstatus", _dataService.MsStepId)
                {
                    ["ddsm_status"] = new OptionSetValue((int) status)
                };

                _dataService.OrgService.Update(entity);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        private void GetMilestoneStatus(Guid milestoneId, MsPfMsgDto milestoneDtoList, DataService.RecordStatus status)
        {
            var referencingAttr = GetRefarencingAttr(_dataService.TargetEntity.LogicalName, "ddsm_milestone");

            var milestones = GetMilestonesByTargetEntity(referencingAttr);

            var milList = new List<Guid>();

            foreach (var item in milestoneDtoList.Milestones)
                milList.Add(item.Id);

            var msStatus = DataService.RecordStatus.Done;
            foreach (var milestone in milestones.Entities)
            {
                if (milestone.Id == milestoneId)
                    msStatus = status;
                if (milList.Contains(milestone.Id))
                {
                    foreach (var item in milestoneDtoList.Milestones)
                        if (item.Id == milestone.Id)
                            item.Status = (int) msStatus;
                }
                else
                {
                    milestoneDtoList.Milestones.Add(
                        new MsPfMsgDto.Milestone {Id = milestone.Id, Status = (int) msStatus});
                }

                if (milestone.Id == milestoneId) break;
            }
        }

        private void UpdateOriginalMilestoneStatus(Guid milestoneId, DataService.RecordStatus status)
        {
            var entity = new Entity("ddsm_milestone", milestoneId)
            {
                ["ddsm_status"] = new OptionSetValue((int) status)
            };

            _dataService.OrgService.Update(entity);
        }


        /// <summary>
        ///     Create CreateExecuteMultipleRequest
        /// </summary>
        /// <param name="returnResponses"></param>
        /// <returns></returns>
        private ExecuteMultipleRequest CreateExecuteMultipleRequest(bool returnResponses = true)
        {
            //create a new history
            var executeMultipleRequest = new ExecuteMultipleRequest
            {
                Settings = new ExecuteMultipleSettings
                {
                    ContinueOnError = false,
                    ReturnResponses = returnResponses
                },
                Requests = new OrganizationRequestCollection()
            };
            return executeMultipleRequest;
        }

        private enum MsStatus
        {
            NotStarted = 962080000,
            Active = 962080001,
            Completed = 962080002,
            Skipped = 962080003
        }
    }
}