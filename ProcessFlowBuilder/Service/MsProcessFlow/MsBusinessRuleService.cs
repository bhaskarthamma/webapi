﻿using System;
using System.Collections.Generic;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;

namespace ProcessFlowBuilder.Service.MsProcessFlow
{
    public class MsBusinessRuleService
    {
        private readonly MsDataService _dataService;
        private readonly Guid _msStepId;

        public Dictionary<Guid, Guid> OriginalBrToHistoryBr;


        public MsBusinessRuleService(MsDataService dataService)
        {
            _dataService = dataService;
            OriginalBrToHistoryBr = new Dictionary<Guid, Guid>();
            _msStepId = dataService.MsStepId;
        }

        /// <summary>
        /// </summary>
        /// <param name="executeMultipleRequest"></param>
        /// <param name="businessRules"></param>
        /// <returns></returns>
        private ExecuteMultipleResponse CreateHistoryBrStatus(ExecuteMultipleRequest executeMultipleRequest,
            List<Guid> businessRules)
        {
            try
            {
                //explore all process flow
                foreach (var businessRule in businessRules)
                {
                    var br = new Entity("ddsm_historymsbrstatus")
                    {
                        ["ddsm_businessrule"] = new EntityReference("ddsm_businessrule", businessRule),
                        ["ddsm_name"] = _dataService.GetFiledValue("ddsm_businessrule", businessRule, "ddsm_name"),
                        ["ddsm_status"] = new OptionSetValue((int) DataService.RecordStatus.NotStarted)
                    };

                    var updateRequest = new CreateRequest
                    {
                        Target = br
                    };
                    executeMultipleRequest.Requests.Add(updateRequest);
                }

                return (ExecuteMultipleResponse) _dataService.OrgService.Execute(executeMultipleRequest);
            }
            catch (Exception e)
            {
                return null;
            }
        }

        /// <summary>
        /// </summary>
        /// <param name="businessRules"></param>
        public void AddProcessFlowBr(List<Guid> businessRules)
        {
            try
            {
                var executeMultipleRequest = CreateExecuteMultipleRequest();

                var responseWithResults = CreateHistoryBrStatus(executeMultipleRequest, businessRules);

                foreach (var item in responseWithResults.Responses)
                {
                    var request = (Entity) executeMultipleRequest.Requests[item.RequestIndex].Parameters["Target"];
                    var originalBr = (EntityReference) request.Attributes["ddsm_businessrule"];
                    var brHistoryId = new Guid(item.Response.Results["id"].ToString());
                    UpdateOriginalBrToHistoryBr(originalBr.Id, brHistoryId);
                }
                //Associate msstatus and msbrstatus
                var requestWithResults2 = new ExecuteMultipleRequest
                {
                    Settings = new ExecuteMultipleSettings
                    {
                        ContinueOnError = false,
                        ReturnResponses = true
                    },
                    Requests = new OrganizationRequestCollection()
                };

                foreach (var item in OriginalBrToHistoryBr)
                {
                    var request = new AssociateRequest();
                    var step = new EntityReference("ddsm_historymsstatus", _msStepId);
                    var busRule = new EntityReference("ddsm_historymsbrstatus", item.Value);
                    request.Target = step;
                    request.RelatedEntities = new EntityReferenceCollection {busRule};
                    request.Relationship = new Relationship("ddsm_ddsm_historymsstatus_ddsm_historymsbrstat");

                    requestWithResults2.Requests.Add(request);
                }

                _dataService.OrgService.Execute(requestWithResults2);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        private void UpdateOriginalBrToHistoryBr(Guid originalBrId, Guid brHistoryId)
        {
            if (OriginalBrToHistoryBr.ContainsKey(originalBrId))
                return;

            OriginalBrToHistoryBr.Add(originalBrId, brHistoryId);
        }


        public void UpdateBusinessRuleStatus(Guid businessRule, DataService.RecordStatus status)
        {
            try
            {
                var brHistoryId = Guid.Empty;
                if (OriginalBrToHistoryBr.ContainsKey(businessRule))
                    brHistoryId = OriginalBrToHistoryBr[businessRule];
                if (brHistoryId == Guid.Empty)
                    return;
                var entity = new Entity("ddsm_historymsbrstatus", brHistoryId)
                {
                    ["ddsm_status"] = new OptionSetValue((int) status)
                };
                _dataService.OrgService.Update(entity);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }


        /// <summary>
        ///     Create CreateExecuteMultipleRequest
        /// </summary>
        /// <param name="returnResponses"></param>
        /// <returns></returns>
        private ExecuteMultipleRequest CreateExecuteMultipleRequest(bool returnResponses = true)
        {
            //create a new history
            var executeMultipleRequest = new ExecuteMultipleRequest
            {
                Settings = new ExecuteMultipleSettings
                {
                    ContinueOnError = false,
                    ReturnResponses = returnResponses
                },
                Requests = new OrganizationRequestCollection()
            };
            return executeMultipleRequest;
        }
    }
}