﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using ProcessFlowBuilder.Service.ProcessFlow;

namespace ProcessFlowBuilder.Service.MsProcessFlow
{
    public class MsDataService
    {
        public enum RecordStatus
        {
            InProgress = 962080000,
            Error = 962080001,
            Done = 962080002
        }

        public List<EntityReference> CalculatedEntities;
        public ConcurrentDictionary<Guid, Entity> EntityCache = new ConcurrentDictionary<Guid, Entity>();

        public Dictionary<Guid, Guid> ListOriginalBrToHistoryBr;
        public Dictionary<Guid, List<Guid>> ListOriginalPfToHistoryPf;
        public Dictionary<Guid, Guid> ListOriginalStepToHistoryStep;
        public List<Guid> ListProcessFlowId;

        public Dictionary<Guid, List<Guid>> ListProcessFlowToStep;
        public Dictionary<Guid, List<Guid>> ListStepToBusinessRule;
        public IOrganizationService OrgService;

        public Entity TargetEntity;

        public MsDataService(IOrganizationService orgService, Entity targetEntity)
        {
            OrgService = orgService;
            TargetEntity = targetEntity;
            CalculatedEntities = new List<EntityReference>();

            ListOriginalBrToHistoryBr = new Dictionary<Guid, Guid>();
            ListOriginalPfToHistoryPf = new Dictionary<Guid, List<Guid>>();
            ListOriginalStepToHistoryStep = new Dictionary<Guid, Guid>();
            ListProcessFlowId = new List<Guid>();

            ListProcessFlowToStep = new Dictionary<Guid, List<Guid>>();
            ListStepToBusinessRule = new Dictionary<Guid, List<Guid>>();
        }

        public Guid MsProcessFlowId { get; set; }
        public Guid MsStepId { get; set; }


        public ProcessFlowService ProcessFlowService { get; set; }
        public MsProcessFlowService MsProcessFlowService { get; set; }
        public StepService StepService { get; set; }
        public MilestoneService MilestoneService { get; set; }
        public BusinessRuleService BusinessRuleService { get; set; }
        public MsBusinessRuleService MsBusinessRuleService { get; set; }

        public string GetFiledValue(string entityLn, Guid recordId, string filedName)
        {
            var rs = string.Empty;
            var entity = RetreiveEntity(entityLn, recordId);
            object fieldValue = null;
            if (entity.Attributes.TryGetValue(filedName, out fieldValue))
                rs = fieldValue.ToString();
            return rs;
        }

        private Entity RetreiveEntity(string entityLogicalName, Guid recordId)
        {
            try
            {
                var entityCache = EntityCache;
                if (entityCache.ContainsKey(recordId))
                {
                    var tmp = new Entity();
                    if (entityCache.TryGetValue(recordId, out tmp))
                        return tmp;
                }
                var entity = OrgService.Retrieve(entityLogicalName, recordId, new ColumnSet(true));
                entityCache.TryAdd(recordId, entity);
                return entity;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}