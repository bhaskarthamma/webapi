﻿using System;
using System.Collections.Generic;
using Microsoft.Xrm.Sdk;
using Newtonsoft.Json;
using ProcessFlowBuilder.Dto;
using SocketClients;

namespace ProcessFlowBuilder.Service.MsProcessFlow
{
    internal class MsPfService : PfServiceAbstract
    {
        private readonly List<Guid> _businessRuleIds;
        private readonly MsDataService _dataService;
        private readonly Guid _milestoneId;
        private readonly MsPfMsgDto _msPfMsgDto;

        public MsPfService(Guid userId, IOrganizationService orgService, Entity targetEntity,
            List<Guid> businessRuleIds, Guid milestoneId)
        {
            _businessRuleIds = businessRuleIds;
            UserId = userId;
            _dataService = new MsDataService(orgService, targetEntity);
            _milestoneId = milestoneId;
            _msPfMsgDto = new MsPfMsgDto();
        }

        public override void CreatePfHistory()
        {
            if (_milestoneId == Guid.Empty)
                return;
            var msProcessFlowService = new MsProcessFlowService(_dataService);

            _dataService.MsProcessFlowService = msProcessFlowService;


            //create a new record of the entity "ddsm_historypf"
            var pfId = _dataService.MsProcessFlowService.AddHistoryMsPfRecord();
            _dataService.MsProcessFlowId = pfId;
        }

        public override void CreateStepHistory()
        {
            if (_milestoneId == Guid.Empty)
                return;

            //create step service
            var milestoneService = new MilestoneService(_dataService);

            _dataService.MilestoneService = milestoneService;


            //create new records in the ddsm_historystepstatus
            var msStepId = milestoneService.AddMilestone(_milestoneId);
            _dataService.MsStepId = msStepId;

            _dataService.ListOriginalStepToHistoryStep = milestoneService.OriginalMilestonToHistoryMileston;
        }

        public override void CreateBrHistory()
        {
            if (_milestoneId == Guid.Empty)
                return;
            //if in the current process flow not exists any steps need exit from the method
            var msBusinessRuleService = new MsBusinessRuleService(_dataService);

            _dataService.MsBusinessRuleService = msBusinessRuleService;

            //create new in the ddsm_historybrstatus
            msBusinessRuleService.AddProcessFlowBr(_businessRuleIds);

            _dataService.ListOriginalBrToHistoryBr = msBusinessRuleService.OriginalBrToHistoryBr;
        }

        /// <summary>
        /// </summary>
        /// <param name="businessRule"></param>
        /// <param name="status"></param>
        public override void UpdateBusinessRuleStatus(Guid businessRule, DataService.RecordStatus status)
        {
            var isNeedUpdateStep = false;


            if (_dataService.MsBusinessRuleService == null) return;
            _dataService.MsBusinessRuleService.UpdateBusinessRuleStatus(businessRule, status);


            foreach (var step in _msPfMsgDto.Milestones)
            foreach (var bRule in step.BusinessRules)
                if (bRule.Id == businessRule)
                    bRule.Status = (int) status;
        }

        public override void UpdateStepStatus(DataService.RecordStatus status)
        {
            _dataService.MilestoneService.UpdateStepStatus(status, _milestoneId, _msPfMsgDto);
        }

        public override void CreateWsMessage()
        {
            var milestone = new MsPfMsgDto.Milestone
            {
                Id = _milestoneId,
                Status = (int) DataService.RecordStatus.Active
            };
            foreach (var br in _businessRuleIds)
                milestone.BusinessRules.Add(new MsPfMsgDto.BusinessRuleData
                {
                    Id = br,
                    Status = (int) DataService.RecordStatus.Active
                });
            _msPfMsgDto.Milestones.Add(milestone);
            var entity = new MsPfMsgDto.Entity
            {
                Id = _dataService.TargetEntity.Id,
                LogicalName = _dataService.TargetEntity.LogicalName
            };
            _msPfMsgDto.Entities.Add(entity);
        }

        public override void SendWsMessage()
        {
            var message = JsonConvert.SerializeObject(_msPfMsgDto);
            StaticSocketClient.Send(UserId, message, SocketChannelEnum.MsProcessFlow);
        }

        public override void UpdateStepStatus(Guid stepIdGuid, DataService.RecordStatus status)
        {
            _dataService.MilestoneService.UpdateStepStatus(status, stepIdGuid, _msPfMsgDto);
        }
    }
}