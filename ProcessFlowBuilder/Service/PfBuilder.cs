﻿using System;
using System.Collections.Generic;
using Microsoft.Xrm.Sdk;
using ProcessFlowBuilder.Dto;
using ProcessFlowBuilder.Service.MsProcessFlow;
using ProcessFlowBuilder.Service.ProcessFlow;

namespace ProcessFlowBuilder.Service
{
    public class PfBuilder
    {
        private readonly PfServiceAbstract _pfServiceAbstract;
        private ProcessFlowStepStatusMsgDto _processFlowStepStatusMsgDto;


        public PfBuilder(Guid userId, IOrganizationService orgService, Entity targetEntity,
            List<Guid> businessRuleIds, Guid milestoneId, bool isMilestoneProcessFlow = false)
        {
            if (isMilestoneProcessFlow)
                _pfServiceAbstract = new MsPfService(userId, orgService, targetEntity, businessRuleIds, milestoneId);
            else
                _pfServiceAbstract = new PfService(userId, orgService, targetEntity, businessRuleIds);
        }

        /// <summary>
        ///     Create history for business process flow
        /// </summary>
        public void CreateHistory()
        {
            _pfServiceAbstract.CreatePfHistory();
            _pfServiceAbstract.CreateStepHistory();
            _pfServiceAbstract.CreateBrHistory();


            _pfServiceAbstract.CreateWsMessage();


            //_pfServiceAbstract.SendWsMessage();
        }
        public void UpdateStepStatus(Guid stepIdGuid, DataService.RecordStatus status)
        {
            _pfServiceAbstract.UpdateStepStatus(stepIdGuid, status);
            _pfServiceAbstract.SendWsMessage();
        }

        public void UpdateBusinessRuleStatus(Guid businessRule, DataService.RecordStatus status)
        {
            _pfServiceAbstract.UpdateBusinessRuleStatus(businessRule, status);
            _pfServiceAbstract.SendWsMessage();
        }
    }
}