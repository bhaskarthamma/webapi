﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xrm.Sdk;
using Newtonsoft.Json;
using ProcessFlowBuilder.Dto;
using SocketClients;

namespace ProcessFlowBuilder.Service.ProcessFlow
{
    internal class PfService : PfServiceAbstract
    {
        private readonly List<Guid> _businessRuleIds;
        private readonly DataService _dataService;
        private readonly ProcessFlowStepStatusMsgDto _processFlowStepStatusMsgDto;

        public PfService(Guid userId, IOrganizationService orgService, Entity targetEntity,
            List<Guid> businessRuleIds)
        {
            UserId = userId;
            _businessRuleIds = businessRuleIds;
            _dataService = new DataService(orgService, targetEntity);
            _processFlowStepStatusMsgDto = new ProcessFlowStepStatusMsgDto();
        }

        /// <summary>
        ///     Create a new record of the entity "ddsm_historypf"
        /// </summary>
        public override void CreatePfHistory()
        {
            var processFlowService = new ProcessFlowService(_dataService);
            _dataService.ProcessFlowService = processFlowService;
            //get list of process flow  Ids from the ADT  by a list of business rule
            var processFlowIdList = processFlowService.GetPfIdListByBusinessRuleIdList(_businessRuleIds);
            _dataService.ListProcessFlowId = processFlowIdList;

            //if process flow does not exist  stopping of create history of process flow
            if (_dataService.ListProcessFlowId.Count == 0) return;

            //create a new record of the entity "ddsm_historypf"
            _dataService.ProcessFlowService.AddHistoryPfRecord();

            _dataService.ListOriginalPfToHistoryPf = processFlowService.ListOriginalPfToHistoryPf;
            _dataService.ListProcessFlowToStep = processFlowService.ListProcessFlowToStep;
            _dataService.ListStepToBusinessRule = processFlowService.ListStepToBusinessRule;
        }


        public override void CreateStepHistory()
        {
            //if in the current process flow not exists any steps need exit from the method
            if (_dataService.ListProcessFlowToStep == null || _dataService.ListProcessFlowToStep.Count == 0) return;

            //create step service
            var stepService = new StepService(_dataService);

            _dataService.StepService = stepService;

            //create new records in the ddsm_historystepstatus
            stepService.AddProcessFlowStep();

            _dataService.ListOriginalStepToHistoryStep = stepService.OriginalStepToHistoryStep;
        }

        public override void CreateBrHistory()
        {
            //if in the current process flow not exists any steps need exit from the method
            if (_dataService.ListProcessFlowToStep.Count == 0) return;
            var businessRuleService = new BusinessRuleService(_dataService);

            _dataService.BusinessRuleService = businessRuleService;

            //create new in the ddsm_historybrstatus
            businessRuleService.AddProcessFlowBr(_businessRuleIds);

            _dataService.ListOriginalBrToHistoryBr = businessRuleService.OriginalBrToHistoryBr;
        }

        public override void UpdateBusinessRuleStatus(Guid businessRule, DataService.RecordStatus status)
        {
            var isNeedUpdateStep = false;


            if (_dataService.BusinessRuleService == null) return;
            _dataService.BusinessRuleService.UpdateBusinessRuleStatus(businessRule);


            foreach (var step in _processFlowStepStatusMsgDto.Steps)
            foreach (var bRule in step.BusinessRules)
                if (bRule.Id == businessRule)
                    bRule.Status = (int) status;

            foreach (var step in _processFlowStepStatusMsgDto.Steps)
                isNeedUpdateStep = step.BusinessRules.All(x => x.Status == (int) status);


            if (isNeedUpdateStep)
                UpdateStepStatus(status);
            SendWsMessage();
        }

        public override void UpdateStepStatus(DataService.RecordStatus status)
        {
            _dataService.StepService.UpdateStepStatus(status);
            foreach (var step in _processFlowStepStatusMsgDto.Steps)
                step.Status = (int) status;
        }

        public override void CreateWsMessage()
        {
            _processFlowStepStatusMsgDto.ProcessFlowIdList = _dataService.ListProcessFlowId;
            foreach (var pf in _dataService.ListProcessFlowToStep)
            foreach (var stepId in pf.Value)
            {
                var step = new ProcessFlowStepStatusMsgDto.Step
                {
                    Id = stepId,
                    Status = (int) DataService.RecordStatus.Active
                };
                foreach (var bRuleId in _dataService.ListStepToBusinessRule[stepId])
                    step.BusinessRules.Add(new ProcessFlowStepStatusMsgDto.BusinessRuleData
                    {
                        Id = bRuleId,
                        Status = (int) DataService.RecordStatus.Active
                    });
                _processFlowStepStatusMsgDto.Steps.Add(step);
            }


            foreach (var item in _dataService.CalculatedEntities)
                _processFlowStepStatusMsgDto.Entities.Add(new ProcessFlowStepStatusMsgDto.Entity
                {
                    Id = item.Id,
                    LogicalName = item.LogicalName
                });
        }

        public override void SendWsMessage()
        {
            var message = JsonConvert.SerializeObject(_processFlowStepStatusMsgDto);
            StaticSocketClient.Send(UserId, message, SocketChannelEnum.ProcessFlow);
        }

        public override void UpdateStepStatus(Guid stepIdGuid, DataService.RecordStatus status)
        {
        }
    }
}