﻿using System;
using System.Configuration;
using StackExchange.Redis;

namespace RedisServer
{
    internal class RedisConnectorHelper
    {
        private static readonly Lazy<ConnectionMultiplexer> lazyConnection;

        static RedisConnectorHelper()
        {
            var redisConfigureIp = ConfigurationManager.AppSettings["RedisConfigureIp"];
            lazyConnection =
                new Lazy<ConnectionMultiplexer>(() => ConnectionMultiplexer.Connect(redisConfigureIp));
        }

        public static ConnectionMultiplexer Connection => lazyConnection.Value;
    }
}