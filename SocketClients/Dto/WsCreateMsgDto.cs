﻿using System;

namespace SocketClients.Dto
{
    public class WsCreateMsgDto
    {
        public string EntityLogicalName;
        public Guid MessageId;
        public Guid UserId;
        public Guid RecordId;


        public WsCreateMsgDto()
        {
            MessageId = Guid.Empty;
            UserId = Guid.Empty;
            RecordId = Guid.Empty;
        }
    }
}