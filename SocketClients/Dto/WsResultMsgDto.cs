﻿using System;
using System.Collections.Generic;

namespace SocketClients.Dto
{
    /// <summary>
    /// how to use:
    /// var wSMessageDto = new WsMessageDto();
    //wSMessageDto.PrimaryEntityId = _dmnDto.TargetEntity.Id;
    //wSMessageDto.MessageID = Guid.NewGuid();
    //wSMessageDto.MessageType = "INFORMATION";
    //wSMessageDto.PrimaryEntityName = _dmnDto.TargetEntity.LogicalName;
    //wSMessageDto.Status = "start";
    //wSMessageDto.UserId = _dmnDto.InputArgs.UserId;
    /// </summary>
    public class WsResultMsgDto
    {
        public WsResultMsgDto()
        {
            RelatedEnityName = new List<string>();
            RelatedEntity = new List<RelatedEntityData>();
        }

        public string Message { get; set; }
        public Guid MessageID { get; set; }
        public string MessageType { get; set; }
        public string PrimaryEntityName { get; set; }
        public Guid PrimaryEntityId { get; set; }

        public List<string> RelatedEnityName { get; set; }
        public List<RelatedEntityData> RelatedEntity { get; set; }
        public string Status { get; set; }
        public Guid UserId { get; set; }

        public class RelatedEntityData
        {
            public RelatedEntityData(Guid recordId, string entityLogicalName)
            {
                EntityLogicalName = entityLogicalName;
                RecordId = recordId;
            }

            public string EntityLogicalName { get; set; }
            public Guid RecordId { get; set; }
        }
    }
}