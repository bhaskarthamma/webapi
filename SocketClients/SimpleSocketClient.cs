﻿using System;
using System.Configuration;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using SocketClients.Extention;

namespace SocketClients
{
    public static class SimpleSocketClient
    {
        private static readonly string _connectionString = "SocketConnection";
        public static async Task SendMessageAsync(string message)
        {
            using (var client = new ClientWebSocket())
            {
                if (!ConfigurationManager.ConnectionStrings.HasConnectionString(_connectionString)) return;

                string uriStr = ConfigurationManager.ConnectionStrings[_connectionString].ToString();
                if (!string.IsNullOrEmpty(uriStr))
                {
                    var uri = new Uri(uriStr + "/" + SocketChannelEnum.DynamicsCrm);

                    await client.ConnectAsync(uri, CancellationToken.None);

                    byte[] bytes = Encoding.UTF8.GetBytes(message);
                    var arraySegment = new ArraySegment<byte>(bytes);

                    await client.SendAsync(arraySegment, WebSocketMessageType.Text, true, CancellationToken.None);
                }
            }
        }

        public static void SendMessage(string message, SocketChannelEnum socketChannelEnum)
        {
            using (var client = new ClientWebSocket())
            {
                if (!ConfigurationManager.ConnectionStrings.HasConnectionString(_connectionString)) return;

                string uriStr = ConfigurationManager.ConnectionStrings[_connectionString].ToString();
                   
                var uri = new Uri(uriStr + "/" + socketChannelEnum);

                client.ConnectAsync(uri, CancellationToken.None).Wait();

                byte[] bytes = Encoding.UTF8.GetBytes(message);
                var arraySegment = new ArraySegment<byte>(bytes);

                client.SendAsync(arraySegment, WebSocketMessageType.Text, true, CancellationToken.None).Wait();
            }
        }
    }
}
