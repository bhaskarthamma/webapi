﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using CoreUtils.Dto;
using MappingImplementation.Dto;
using MappingImplementation.Service.Implementation;
using MappingImplementation.Service.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Tooling.Connector;

namespace MappingImplementation.Tests
{
    /// <summary>
    ///     Summary description for MappingImplementationTest
    /// </summary>
    [TestClass]
    public class MappingImplementationTest
    {
        private readonly IMappingImplementationService _implementationService;

        public MappingImplementationTest()
        {
            OrgService = GetOrgService();
            _implementationService = new MappingImplementationService(new MappingImplementationDto
            {
                OrgService = OrgService,
                CreatorRecordType = MappingImplementationDto.RecordCreator.Front
            });
        }

        private BaseDto BaseDto { get; set; }
        private IOrganizationService OrgService { get; }

        [TestMethod]
        public void Execute_mapping_project()
        {
            var impl = new Guid("CB9AF6AA-E896-E711-80D8-663933383038");


            var result = new List<Entity>();
            var sw = Stopwatch.StartNew();
            //for (var i = 0; i < 5; i++)
            //{
            var target = new Entity("ddsm_project", new Guid())
            {
                ["ddsm_projecttemplateid"] = new EntityReference("ddsm_projecttemplate",
                    new Guid("141B2B2F-6B70-E711-80D1-663933383038"))
            };
            var swLocal = Stopwatch.StartNew();
            var entity = _implementationService.ExecuteMapping(target, impl);
            result.Add(target);
            Console.Write($"mapping time ={swLocal.Elapsed}");
            //}
            Console.Write($"total mapping time={sw.Elapsed}");
            //foreach (var item in result)
            OrgService.Create(entity[0]);

            Assert.AreEqual(5, result.Count);
        }

        [TestMethod]
        public void Execute_mapping_required_document()
        {
            var target = OrgService.Retrieve("ddsm_requireddocument", new Guid("7E618AE3-21A5-E711-80E4-626430656337"),
                new ColumnSet(true));
            var impl = new Guid("35E0E84E-819C-E711-80DD-663933383038");


            var rs = _implementationService.ExecuteMapping(target, impl);

            foreach (var item in rs)
                OrgService.Create(item);

            Assert.AreEqual(1, rs.Count);
        }

        [TestMethod]
        public void Can_Get_Lookup_Fields()
        {
            var target = OrgService.Retrieve("ddsm_requireddocument", new Guid("7E618AE3-21A5-E711-80E4-626430656337"),
                new ColumnSet(true));
            var impl = new Guid("35E0E84E-819C-E711-80DD-663933383038");
            var result = _implementationService.GetImplementationLookup(target, impl);

            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(object));
            //Assert.AreEqual(result.Contains("ddsm_project"), true);
        }

        private IOrganizationService GetOrgService()
        {
            var crmConnectionString = ConfigurationManager.ConnectionStrings["CRMConnectionString"].ConnectionString;


            // Get the CRM connection string and connect to the CRM Organization

            var client = new CrmServiceClient(crmConnectionString);
            return client.OrganizationServiceProxy;
        }

        #region Additional test attributes

        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //

        #endregion
    }
}